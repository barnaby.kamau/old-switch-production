/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package tamam.dtos;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;

/**
 *
 * @author Tony
 */
public class Receipt {
    @XmlAttribute(name = "LINES", required = true)
    protected int lines;
    @XmlElement(name = "SERIAL", required = true)
    protected String localDateTime;
    @XmlElement(name = "VALIDTO", required = true)
    protected String serverDateTime;
}
