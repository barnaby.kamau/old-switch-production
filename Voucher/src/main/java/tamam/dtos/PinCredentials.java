/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tamam.dtos;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 *
 * @author Tony
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PINCREDENTIALS")
public class PinCredentials {

    @XmlElement(name = "PIN", required = true)
    protected String pin;
    @XmlElement(name = "SERIAL", required = true)
    protected String serial;
    @XmlElement(name = "VALIDTO", required = true)
    protected String validTo;

    public String getPin() {
        return pin;
    }

    public void setPin(String pin) {
        this.pin = pin;
    }

    public String getSerial() {
        return serial;
    }

    public void setSerial(String serial) {
        this.serial = serial;
    }

    public String getValidTo() {
        return validTo;
    }

    public void setValidTo(String validTo) {
        this.validTo = validTo;
    }

}
