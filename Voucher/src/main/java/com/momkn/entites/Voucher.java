package com.momkn.entites;

public class Voucher {

	private String serial;
	private String pin;
	private double denomination;
	private long operatorId;
	
	public String getSerial() {
		return serial;
	}
	public void setSerial(String serial) {
		this.serial = serial;
	}
	public String getPin() {
		return pin;
	}
	public void setPin(String pin) {
		this.pin = pin;
	}
	public double getDenomination() {
		return denomination;
	}
	public void setDenomination(double denomination) {
		this.denomination = denomination;
	}
	public long getOperatorId() {
		return operatorId;
	}
	public void setOperatorId(long operatorId) {
		this.operatorId = operatorId;
	}

}
