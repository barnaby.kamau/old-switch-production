/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.momkn.entites;

import java.io.Serializable;
import java.util.List;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;
//import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 *
 * @author Antonios_knaguib
 */
public class Reciept implements Serializable {

    public String InvoiceId;
    public int ServiceId;
    public String PmtType;
    public String ServiceName;
    public int ProviderId;
    public String ProviderName;
    public int Categoryid;
    public String AgentCode;
    public String AgentName;
    public double Totalprice;
    public double Fees;
    public String AddedTime;
    public int ServiceCount;
    public int Status;
    public String Header;
    public String InvoiceDescription;
    public String Footer;
    public double TotalAmount;
    public String Date;
    public String Time;
    public int StatusCode;
    public String StatusDescription;
    public List<InqField> Fields;

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this,
                ToStringStyle.MULTI_LINE_STYLE);
    }

}
