package com.momkn.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;

import com.momkn.entites.Voucher;

public class VoucherDAO extends DAO {

	private static final String selectVoucherSQL = "SELECT VOUCHER_PIN,VOUCHER_SERIAL,"
			+ " AVAILABLE,UPDATED_DATE,TRANSACTION_ID"
			+ " FROM SERVICE_DENOMINATION SD "
                        + " INNER JOIN LOCAL_VOUCHER V ON SD.SERVICE_DENOMINATION_ID = V.SERVICE_DENOMINATION_ID "
			+ " WHERE SD.FRONT_END_SERVICE_ID = ? AND SD.SERVICE_DENOMINATION_VALUE = ? AND V.AVAILABLE = 1 AND ROWNUM  = 1 " ;

	public Voucher getVoucher(long serviceId, double denomination, String transactionId) throws Exception {
		Voucher voucher = null;
		try (Connection connection = getConnection();) {
			PreparedStatement selectVoucherPS = connection.prepareStatement(selectVoucherSQL, ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
			selectVoucherPS.setLong(1, serviceId);
			selectVoucherPS.setDouble(2, denomination);
			ResultSet resultSet = selectVoucherPS.executeQuery();
			if (resultSet.next()) {
				voucher = new Voucher();
				voucher.setPin(resultSet.getString("VOUCHER_PIN"));
				voucher.setSerial(resultSet.getString("VOUCHER_SERIAL"));
				resultSet.updateInt("AVAILABLE", 0);
				resultSet.updateTimestamp("UPDATED_DATE", new Timestamp(System.currentTimeMillis()));
				resultSet.updateString("TRANSACTION_ID", transactionId);
				resultSet.updateRow();
			}
		} catch (SQLException sqlException) {
                    sqlException.printStackTrace();
			throw new Exception("Error while Getting voucher service id [" + serviceId + "], denomination [" + denomination + "]."+sqlException.getMessage(), sqlException);
		}
		return voucher;
	}
}
