/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.momkn.dao;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 *
 * @author Tony
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "COMMAND")
@XmlRootElement(name = "COMMAND")
public class OrangeVoucherResponse {
    @XmlAttribute(name = "TYPE", required = true)
    protected String type;
    @XmlElement(name = "TXNSTATUS", required = true)
    protected String txnStatus;
    @XmlElement(name = "DATE", required = true)
    protected String date;
    @XmlElement(name = "EXTREFNUM", required = true)
    protected String requestId;
    @XmlElement(name = "TXNID", required = true)
    protected String trxId;
    @XmlElement(name = "MESSAGE", required = true)
    protected String message;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getTxnStatus() {
        return txnStatus;
    }

    public void setTxnStatus(String txnStatus) {
        this.txnStatus = txnStatus;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getRequestId() {
        return requestId;
    }

    public void setRequestId(String requestId) {
        this.requestId = requestId;
    }

    public String getTrxId() {
        return trxId;
    }

    public void setTrxId(String trxId) {
        this.trxId = trxId;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

}
