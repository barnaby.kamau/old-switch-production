package com.momkn.dao;

import java.io.InputStream;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Properties;
import javax.sql.DataSource;
import org.apache.log4j.Logger;
import com.momkn.PropertyAccessor;

import oracle.jdbc.pool.OracleDataSource;

public abstract class DAO {

	public static Logger logger = Logger.getLogger(DAO.class);

	private static final String POOL_PROPERTIES_FILE_NAME = "/resources/datasource.properties";

	private static Object dataSourceLock = new Object();
	private static DataSource dataSource = null;

	private static String dataSourceName = "momknDS";

	private static final long sleepTime = 10000;
	private static final long conectionRetryTimes = 5;

	protected void closeStatement(Statement statement) throws Exception {
		try {
			if (statement != null) {
				statement.close();
			}
		} catch (SQLException sqlException) {
			throw new Exception("Error while closing prepared statement.", sqlException);
		}
	}

	protected void closeResultSet(ResultSet resultSet) throws Exception {
		try {
			if (resultSet != null) {
				resultSet.close();
			}
		} catch (SQLException sqlException) {
			throw new Exception("Error while closing result set.", sqlException);
		}
	}

	protected void closeConnection(Connection connection) throws Exception {
		try {
			if (connection != null) {
				connection.close();
			}
		} catch (SQLException sqlException) {
			throw new Exception("Error while closing Connection", sqlException);
		}
	}

	protected Connection getConnection() throws Exception {

		long startTime = System.currentTimeMillis();
		for (int i = 0; i < conectionRetryTimes; i++) {
			try {
				Connection connection = null;
				if (dataSource == null) {
					synchronized (dataSourceLock) {
						if (dataSource == null)
							dataSource = initializeDataSource(POOL_PROPERTIES_FILE_NAME);
					}
				}
				connection = dataSource.getConnection();
				if (connection == null) {
					// Sleep while then loop again to get connection
					try {
						logger.error("No available connection, all connections in the pool are busy. Sleeping for [" + sleepTime + "] Millies.");
						Thread.sleep(sleepTime);
					} catch (InterruptedException interruptedException) {
						throw new Exception("Interrupt Error.", interruptedException);
					}
				} else {
					long endTime = System.currentTimeMillis();
					logger.debug("Execution Time for Geting connection from Pool  is [" + (endTime - startTime) + "] Millies.");
					return connection;
				}
			} catch (SQLException sqlException) {
				logger.error("Unable to establish connection.", sqlException);
			}
		}
		throw new Exception("Unable to establish connection.");
	}

	protected void commitConnection(Connection connection) throws Exception {
		try {
			connection.commit();
			logger.info("Commit operation achieved successfully.");
		} catch (SQLException e) {
			throw new Exception("Error while commiting the connection.", e);
		}
	}

	protected void rollbackConnection(Connection connection) throws Exception {
		try {
			connection.rollback();
			logger.info("Rollback operation achieved successfully.");
		} catch (SQLException e) {
			throw new Exception("Error while rollback the connection.", e);
		}
	}

	private synchronized DataSource initializeDataSource(String poolConfigFile) throws Exception {
		OracleDataSource dataSource = null;
		Properties properties = new Properties();
		try (InputStream resourceStream = DAO.class.getResourceAsStream(poolConfigFile)) {
			properties.load(resourceStream);
			dataSource = new OracleDataSource();
			dataSource.setURL(properties.getProperty(dataSourceName + ".jdbc.url"));
			dataSource.setUser(properties.getProperty(dataSourceName + ".jdbc.user"));
			dataSource.setPassword(properties.getProperty(dataSourceName + ".jdbc.password"));
			dataSource.setConnectionCachingEnabled(true);
			logger.info("Initialize Data Source with URL :" + dataSource.getURL());
			Properties cacheProperties = new Properties();
			cacheProperties.setProperty("ConnectionWaitTimeout", properties.getProperty(dataSourceName + ".jdbc.connectionWaitTimeout"));

			if (properties.getProperty(dataSourceName + ".jdbc.initialLimit") == null)
				cacheProperties.setProperty("InitialLimit", properties.getProperty(dataSourceName + ".jdbc.initialLimit"));
			else
				cacheProperties.setProperty("InitialLimit", properties.getProperty(dataSourceName + ".jdbc.initialLimit"));

			if (properties.getProperty(dataSourceName + ".jdbc.minLimit") == null)
				cacheProperties.setProperty("MinLimit", properties.getProperty(dataSourceName + ".jdbc.minLimit"));
			else
				cacheProperties.setProperty("MinLimit", properties.getProperty(dataSourceName + ".jdbc.minLimit"));

			if (properties.getProperty(dataSourceName + ".jdbc.maxLimit") == null)
				cacheProperties.setProperty("MaxLimit", properties.getProperty(dataSourceName + ".jdbc.maxLimit"));
			else
				cacheProperties.setProperty("MaxLimit", properties.getProperty(dataSourceName + ".jdbc.maxLimit"));

			cacheProperties.setProperty("ValidateConnection", "true");
			dataSource.setConnectionCacheProperties(cacheProperties);
			logger.info("Initialize Data Source " + dataSourceName + " using File [" + poolConfigFile + "] Success.");
			return dataSource;
		} catch (SQLException sqlException) {
			throw new Exception("Error while initializing Datasource.", sqlException);
		}
	}

}
