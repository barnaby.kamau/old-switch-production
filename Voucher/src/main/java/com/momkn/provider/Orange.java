/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.momkn.provider;

import com.momkn.dao.OrangeVoucherResponse;
import com.momkn.entites.RSRequest;
import com.momkn.entites.RSResponse;
import static com.momkn.manager.Manager.logger;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.security.cert.X509Certificate;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import org.apache.commons.lang.StringUtils;

/**
 *
 * @author Tony
 */
public class Orange implements VoucherProvider {

    private static final String HOST = "http://10.240.22.50:9898/pretups/C2SReceiver";
    private static final String TYPE = "EXPVEVDREQ";
    private static final String EXTNWCODE = "MO";
    private static final String PIN = "1970";
    private static final String PINEXTRA = "1357";
//    private static final String LOGINID = "TamamSt1";
//    private static final String PASSWORD = "Mkn@123";
    private static final String MSISDN = "01278809516";
    private static final String MSISDNEXTRA = "01278764051";
    private static final String REQUEST_GATEWAY_CODE = "EXTGW";
    private static final String REQUEST_GATEWAY_TYPE = "EXTGW";
    private static final String SERVICE_PORT = "190";
    private static final String SOURCE_TYPE = "EXTGG";

    @Override
    public RSResponse voucher(RSRequest request) {
        RSResponse response = new RSResponse();
        response.requestID = request.requestID;
        try {
            logger.info("[Orange][RequestID = " + request.requestID + " ] Provider Request Creation started");

            StringBuffer provider_Request = getProviderRequest(request);
            logger.info("[Orange][RequestID = " + request.requestID + " ] Provider Request : " + provider_Request.toString().replace(request.password, StringUtils.repeat("*", request.password.length())));
            logger.info("[Orange][RequestID = " + request.requestID + " ] Provider Request Creation ended");
            logger.info("[Orange][RequestID = " + request.requestID + " ] Provider Request Sending started");
            logger.info("[Orange][RequestID = " + request.requestID + " ] Host creation started");
            String fullURL = getFullURL(request).toString();
            logger.info("[Orange][RequestID = " + request.requestID + " ] Host : " + fullURL.replace(request.password, StringUtils.repeat("*", request.password.length())));
            logger.info("[Orange][RequestID = " + request.requestID + " ] Host Creation ended");
            String provider_Response = send(fullURL, provider_Request.toString());
            logger.info("[Orange][RequestID = " + request.requestID + " ] Provider Response : " + provider_Response);
            logger.info("[Orange][RequestID = " + request.requestID + " ] Provider Request Sending ended");
            logger.info("[Orange][RequestID = " + request.requestID + " ] Provider Response Unmarshal ended");
            GenericContentHandler handler = new GenericContentHandler();
//            <MESSAGE>Transaction number E210201.1529.110212 to buy Voucher card of 100 EGP is successful. Serial No. 1700001610695113, Voucher PIN 8848518264586452.</MESSAGE>
            OrangeVoucherResponse ProviderResponse = handler.unMarshal(provider_Response.replace("<!DOCTYPE COMMAND PUBLIC \"-//Ocam//DTD XML Command 1.0//EN\" \"xml/command.dtd\">", ""), OrangeVoucherResponse.class);
            logger.info("[Orange][RequestID = " + request.requestID + " ] Provider Response Unmarshal ended");
            if (ProviderResponse.getTxnStatus().equals("200")
                    || ProviderResponse.getTxnStatus().equals("200")
                    || ProviderResponse.getTxnStatus().equals("201")
                    || ProviderResponse.getTxnStatus().equals("202")
                    || ProviderResponse.getTxnStatus().equals("203")
                    || ProviderResponse.getTxnStatus().equals("204")
                    || ProviderResponse.getTxnStatus().equals("205")
                    || ProviderResponse.getTxnStatus().equals("207")
                    || ProviderResponse.getTxnStatus().equals("208")
                    || ProviderResponse.getTxnStatus().equals("209")
                    || ProviderResponse.getTxnStatus().equals("210")
                    || ProviderResponse.getTxnStatus().equals("211")
                    || ProviderResponse.getTxnStatus().equals("213")
                    || ProviderResponse.getTxnStatus().equals("214")
                    || ProviderResponse.getTxnStatus().equals("216")
                    || ProviderResponse.getTxnStatus().equals("222")
                    || ProviderResponse.getTxnStatus().equals("600")
                    || ProviderResponse.getTxnStatus().equals("601")
                    || ProviderResponse.getTxnStatus().equals("603")
                    || ProviderResponse.getTxnStatus().equals("3071")) {

                response.code = 200;
                response.message = ProviderResponse.getMessage();
                response.providerTransactionID = ProviderResponse.getMessage().substring(ProviderResponse.getMessage().indexOf("Transaction number") + "Transaction number".length() + 1, ProviderResponse.getMessage().indexOf("to buy Voucher card"));
                response.pin = ProviderResponse.getMessage().substring(ProviderResponse.getMessage().indexOf("Voucher PIN") + "Voucher PIN".length() + 1, ProviderResponse.getMessage().length() - 1);
                response.serialNumber = ProviderResponse.getMessage().substring(ProviderResponse.getMessage().indexOf("Serial No.") + "Serial No.".length() + 1, ProviderResponse.getMessage().indexOf(", Voucher PIN"));
            } else {
                logger.error("[Orange][RequestID = " + request.requestID + " ] Provider Error : " + ProviderResponse.getMessage());
                response.code = -2;
                response.message = "Provider Error : " + ProviderResponse.getMessage();
                response.providerTransactionID = "";
                response.pin = "";
                response.serialNumber = "";
            }
        } catch (java.net.SocketTimeoutException e) {
            logger.error(this.getClass().getSimpleName() + " - " + e);
            response.code = -300;
            response.message = "Timeout in Connection to provider : " + e;
            response.providerTransactionID = "";
            response.pin = "";
            response.serialNumber = "";
        } catch (Exception e) {
            logger.error(e);
            response.code = -3;
            response.message = "General Error : " + e;
            response.providerTransactionID = "";
            response.pin = "";
            response.serialNumber = "";
        }
        return response;
    }

    private String send(String fullURL, String xml) throws Exception {
        URL url = new URL(fullURL);
        URLConnection conn;
        if (HOST.contains("https")) {
            // Create a trust manager that does not validate certificate chains
            TrustManager[] trustAllCerts = new TrustManager[]{new X509TrustManager() {
                public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                    return null;
                }

                public void checkClientTrusted(X509Certificate[] certs, String authType) {
                }

                public void checkServerTrusted(X509Certificate[] certs, String authType) {
                }
            }};

            // Install the all-trusting trust manager
            final SSLContext sc = SSLContext.getInstance("SSL");
            sc.init(null, trustAllCerts, new java.security.SecureRandom());
            HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
            // Create all-trusting host name verifier
            HostnameVerifier allHostsValid = new HostnameVerifier() {
                public boolean verify(String hostname, SSLSession session) {
                    return true;
                }
            };
            // Install the all-trusting host verifier
            HttpsURLConnection.setDefaultHostnameVerifier(allHostsValid);

            conn = (HttpsURLConnection) url.openConnection();
        } else {
            conn = url.openConnection();
        }
        conn.setRequestProperty("Content-Type", "text/xml");
        conn.setRequestProperty("Accept-Language", "en-us");
        conn.setDoOutput(true);

        BufferedOutputStream bos = new BufferedOutputStream(conn.getOutputStream());
        bos.write(xml.getBytes("UTF-8"));
        bos.close();

        BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream()));
        String inputLine;
        StringBuffer response = new StringBuffer();

        while ((inputLine = in.readLine()) != null) {
            response.append(inputLine);
        }
        in.close();

        return response.toString();
    }

    private StringBuffer getProviderRequest(RSRequest request) {
        SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        String trxDate = DATE_FORMAT.format(new Date());
        StringBuffer provider_Request = new StringBuffer("<?xml version=\"1.0\"?>");
        provider_Request.append("<COMMAND>");
        provider_Request.append("<TYPE>").append(TYPE).append("</TYPE>");
        provider_Request.append("<DATE>").append(trxDate).append("</DATE>");
        provider_Request.append("<EXTNWCODE>").append(EXTNWCODE).append("</EXTNWCODE>");

        if (((request.serviceID == 25997) && (request.amount == 10.0))
                || ((request.serviceID == 25997) && (request.amount == 15.0))
                || ((request.serviceID == 25997) && (request.amount == 25.0))
                || ((request.serviceID == 25997) && (request.amount == 50.0))
                || ((request.serviceID == 25997) && (request.amount == 100.0))
                || ((request.serviceID == 4879) && (request.amount == 5.0))
                || ((request.serviceID == 161025) && (request.amount == 1.5))
                || ((request.serviceID == 161026) && (request.amount == 3.5))
                || ((request.serviceID == 25997) && (request.amount == 20.0))
                || ((request.serviceID == 52256) && (request.amount == 2.0))
                || ((request.serviceID == 52257) && (request.amount == 4.0))
                || ((request.serviceID == 25997) && (request.amount == 7.0))
                || ((request.serviceID == 25997) && (request.amount == 30.0))) {
            provider_Request.append("<MSISDN>").append(MSISDN).append("</MSISDN>");
            provider_Request.append("<PIN>").append(PIN).append("</PIN>");
        } else if (((request.serviceID == 25997) && (request.amount == 4.25))
                || ((request.serviceID == 25997) && (request.amount == 5.0))
                || ((request.serviceID == 25997) && (request.amount == 2.25))
                || ((request.serviceID == 130) && (request.amount == 7.0))
                || ((request.serviceID == 131) && (request.amount == 9.0))) {
            provider_Request.append("<MSISDN>").append(MSISDNEXTRA).append("</MSISDN>");
            provider_Request.append("<PIN>").append(PINEXTRA).append("</PIN>");
        }
        provider_Request.append("<LOGINID>").append("</LOGINID>");
        provider_Request.append("<PASSWORD>").append("</PASSWORD>");
        provider_Request.append("<EXTCODE></EXTCODE>");
        provider_Request.append("<EXTREFNUM>").append(request.requestID).append("</EXTREFNUM>");
        if (((request.serviceID == 25997) && (request.amount == 10.0))
                || ((request.serviceID == 25997) && (request.amount == 15.0))
                || ((request.serviceID == 25997) && (request.amount == 25.0))
                || ((request.serviceID == 25997) && (request.amount == 50.0))
                || ((request.serviceID == 25997) && (request.amount == 100.0))
                || ((request.serviceID == 4879) && (request.amount == 5.0))
                || ((request.serviceID == 161025) && (request.amount == 1.5))
                || ((request.serviceID == 161026) && (request.amount == 3.5))
                || ((request.serviceID == 25997) && (request.amount == 20.0))
                || ((request.serviceID == 52256) && (request.amount == 2.0))
                || ((request.serviceID == 52257) && (request.amount == 4.0))
                || ((request.serviceID == 25997) && (request.amount == 7.0))
                || ((request.serviceID == 25997) && (request.amount == 30.0))) {
            provider_Request.append("<MSISDN2>").append(MSISDN).append("</MSISDN2>");
        } else if (((request.serviceID == 25997) && (request.amount == 4.25))
                || ((request.serviceID == 25997) && (request.amount == 5.0))
                || ((request.serviceID == 25997) && (request.amount == 2.5))
                || ((request.serviceID == 130) && (request.amount == 7.0))
                || ((request.serviceID == 131) && (request.amount == 9.0))) {
            provider_Request.append("<MSISDN2>").append(MSISDNEXTRA).append("</MSISDN2>");
        }
        provider_Request.append("<AMOUNT>").append(request.amount).append("</AMOUNT>");
        provider_Request.append("<LANGUAGE1>0</LANGUAGE1>");
        provider_Request.append("<LANGUAGE2>0</LANGUAGE2>");
        provider_Request.append("<SELECTOR>1</SELECTOR>");
        provider_Request.append("</COMMAND>");
        return provider_Request;

    }

    private StringBuffer getFullURL(RSRequest request) {
        StringBuffer provider_Request = new StringBuffer(HOST);
        provider_Request.append("?");
        provider_Request.append("LOGIN=").append(request.userName).append("&");
        provider_Request.append("PASSWORD=").append(request.password).append("&");
        provider_Request.append("REQUEST_GATEWAY_CODE=").append(REQUEST_GATEWAY_CODE).append("&");
        provider_Request.append("REQUEST_GATEWAY_TYPE=").append(REQUEST_GATEWAY_TYPE).append("&");
        provider_Request.append("SERVICE_PORT=").append(SERVICE_PORT).append("&");
        provider_Request.append("SOURCE_TYPE=").append(SOURCE_TYPE);
        return provider_Request;

    }

    public static String getCurrentTimeStamp() {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date now = new Date();
        String strDate = sdf.format(now);
        return strDate;
    }

}
