package com.momkn.provider;

import org.apache.log4j.Logger;
import com.momkn.dao.VoucherDAO;
import com.momkn.entites.RSRequest;
import com.momkn.entites.RSResponse;
import com.momkn.entites.Voucher;

public class LocalVoucher implements VoucherProvider {

    private static Logger logger = Logger.getLogger(LocalVoucher.class);
    private VoucherDAO voucherDAO = null;

    @Override
    public RSResponse voucher(RSRequest request) {
        return loadVoucher(request.serviceID, request.amount, request.requestID);
    }

    public RSResponse loadVoucher(int serviceId, double amount, String requestId) {
        logger.info("Load voucher for request id " + requestId);
        RSResponse response = null;
        try {
            voucherDAO = new VoucherDAO();
            Voucher voucher = voucherDAO.getVoucher(serviceId, amount, requestId);
            if (voucher == null) {
                logger.info("No voucher for serviceID and amount");
                return null;
            }
            response = new RSResponse();
            response.pin = voucher.getPin();
            response.serialNumber = voucher.getSerial();
            response.code = 200;
            response.message = "Success";
            response.providerTransactionID = requestId;
            response.providerID = 4;
            response.requestID = requestId;
            logger.info("Load voucher for request id " + requestId + " loaded successfully with serial " + response.serialNumber);
            return response;
        } catch (Exception e) {
            logger.error(this.getClass().getSimpleName() + " - " + "Provider Error : can't extract voucher data, " + e.getMessage());
        }
        return response;
    }

    public static void main(String args[]) {
//		LocalVoucher localVoucher = new LocalVoucher();
//		localVoucher.loadVoucher(4764 , 7, "50");

        String msg = "Transaction number E210201.1529.110212 to buy Voucher card of 100 EGP is successful. Serial No. 1700001610695113, Voucher PIN 8848518264586452.";

        String ProviderTransactionID = msg.substring(msg.indexOf("Transaction number") + "Transaction number".length() + 1, msg.indexOf("to buy Voucher card"));
        String SerialNo = msg.substring(msg.indexOf("Serial No.") + "Serial No.".length() + 1, msg.indexOf(", Voucher PIN"));
        String pin = msg.substring(msg.indexOf("Voucher PIN") + "Voucher PIN".length() + 1, msg.length() - 1);

        String s = "123456789012";
        String s1 = pin.substring(0, 4);
        String s2 = pin.substring(4, 8);
        String s3 = pin.substring(8, 12);

        String dashedPin = s1 + "-" + s2 + "-" + s3;
        System.out.println(ProviderTransactionID);
        System.out.println(SerialNo);
        System.out.println(pin);
        System.out.println(pin);
    }
}
