/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.momkn.provider;

import com.momkn.entites.RSRequest;
import com.momkn.entites.RSResponse;

/**
 *
 * @author Tony
 */
public interface VoucherProvider {
    public RSResponse voucher(RSRequest request);
}
