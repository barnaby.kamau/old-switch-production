/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.momkn.provider;

import com.momkn.entites.RSRequest;
import com.momkn.entites.RSResponse;
import static com.momkn.manager.Manager.logger;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.security.cert.X509Certificate;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import org.apache.commons.lang.StringUtils;
import tamam.dtos.VoucherResponse;

/**
 *
 * @author Tony
 */
public class Tamam implements VoucherProvider {

    private static final String TERMINALID = "UA007819";
//    private static final String TERMINALID = "93889284";
//    private static final String EAN = "4260469316524";
    private static final String CURRENCY = "818";
    private static final String LANGUAGE = "ENG";
    private static final String CHARSPERLINE = "40";
    private static final String HOST = "https://precision.epayworldwide.com/up-interface";

    @Override
    public RSResponse voucher(RSRequest request) {
        RSResponse response = new RSResponse();
        response.requestID = request.requestID;
        //        Validations
        try {
            if (!isValidAmount(request)) {
                response.code = -1;
                response.message = "Invalid Voucher Amount.";
                return response;
            }
        } catch (Exception e) {
            response.code = -1;
            response.message = "Invalid Request.";
            return response;
        }
        try {
            logger.info(this.getClass().getSimpleName() + " - " + "Provider Request Creation started");
            StringBuffer provider_Request = getProviderRequest(request);
            logger.info(this.getClass().getSimpleName() + " - " + "Provider Request : " + provider_Request.toString().replace(request.password, StringUtils.repeat("*", request.password.length())));
            logger.info(this.getClass().getSimpleName() + " - " + "Provider Request Creation ended");
            logger.info(this.getClass().getSimpleName() + " - " + "Provider Request Sending started");
            String provider_Response = send(provider_Request.toString());
            logger.info(this.getClass().getSimpleName() + " - " + "Provider Response : " + provider_Response);
            logger.info(this.getClass().getSimpleName() + " - " + "Provider Request Sending ended");
            logger.info(this.getClass().getSimpleName() + " - " + "Provider Response Unmarshal ended");
            GenericContentHandler handler = new GenericContentHandler();
            VoucherResponse ProviderResponse = handler.unMarshal(provider_Response.toString(), VoucherResponse.class);
            logger.info(this.getClass().getSimpleName() + " - " + "Provider Response Unmarshal ended");
            if (ProviderResponse.getStatusCode() == 0) {

                response.code = 200;
                response.message = ProviderResponse.getStatusMessage();
                response.providerTransactionID = ProviderResponse.getProvidertrxId();
                response.pin = ProviderResponse.getPinCredentials().getPin();
                response.serialNumber = ProviderResponse.getPinCredentials().getSerial();

            } else {

                logger.error(this.getClass().getSimpleName() + " - " + ProviderResponse.getStatusMessage());
                response.code = -2;
                response.message = "Provider Error : " + ProviderResponse.getStatusMessage();
                response.providerTransactionID = "";
                response.pin = "";
                response.serialNumber = "";
            }
        } catch (java.net.SocketTimeoutException e) {
            logger.error(this.getClass().getSimpleName() + " - " + e);
            response.code = -300;
            response.message = "Timeout in Connection to provider : " + e;
            response.providerTransactionID = "";
            response.pin = "";
            response.serialNumber = "";
        } catch (Exception e) {
            logger.error(this.getClass().getSimpleName() + " - " + e);
            response.code = -3;
            response.message = "General Error : " + e;
            response.providerTransactionID = "";
            response.pin = "";
            response.serialNumber = "";
        }
        return response;
    }

    private String send(String xml) throws Exception {
        URL url = new URL(HOST);
        URLConnection conn;
        if (HOST.contains("https")) {
            // Create a trust manager that does not validate certificate chains
            TrustManager[] trustAllCerts = new TrustManager[]{new X509TrustManager() {
                public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                    return null;
                }

                public void checkClientTrusted(X509Certificate[] certs, String authType) {
                }

                public void checkServerTrusted(X509Certificate[] certs, String authType) {
                }
            }};

            // Install the all-trusting trust manager
            final SSLContext sc = SSLContext.getInstance("SSL");
            sc.init(null, trustAllCerts, new java.security.SecureRandom());
            HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
            // Create all-trusting host name verifier
            HostnameVerifier allHostsValid = new HostnameVerifier() {
                public boolean verify(String hostname, SSLSession session) {
                    return true;
                }
            };
            // Install the all-trusting host verifier
            HttpsURLConnection.setDefaultHostnameVerifier(allHostsValid);
            conn = (HttpsURLConnection) url.openConnection();
            conn.setConnectTimeout(40000); //set timeout to 5 seconds
        } else {
            conn = url.openConnection();
        }
        conn.setRequestProperty("Content-Type", "text/xml");
        conn.setDoOutput(true);

        BufferedOutputStream bos = new BufferedOutputStream(conn.getOutputStream());
        bos.write(xml.getBytes("UTF-8"));
        bos.close();

        BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream()));
        String inputLine;
        StringBuffer response = new StringBuffer();

        while ((inputLine = in.readLine()) != null) {
            response.append(inputLine);
        }
        in.close();

        return response.toString();
    }

    private StringBuffer getProviderRequest(RSRequest request) {
        StringBuffer provider_Request = new StringBuffer("<?xml version=\"1.0\" encoding=\"UTF - 8\" ?>");
        provider_Request.append("<REQUEST TYPE=\"PINPRINTING\">");
        provider_Request.append("<USERNAME>").append(request.userName).append("</USERNAME>");
        provider_Request.append("<PASSWORD>").append(request.password).append("</PASSWORD>");
        provider_Request.append("<TERMINALID>" + TERMINALID + "</TERMINALID>");
        provider_Request.append("<TXID>").append(request.requestID).append("</TXID>");
        provider_Request.append("<LOCALDATETIME>" + getCurrentTimeStamp() + "</LOCALDATETIME>");
        provider_Request.append("<CARD>");
        provider_Request.append("<EAN>").append(getEAN(request)).append("</EAN>");
        provider_Request.append("</CARD>");
        provider_Request.append("<AMOUNT>").append((int) Math.floor(request.amount * 100)).append("</AMOUNT>");
        provider_Request.append("<CURRENCY>").append(CURRENCY).append("</CURRENCY>");
        provider_Request.append("<RECEIPT>");
        provider_Request.append("<LANGUAGE>").append(LANGUAGE).append("</LANGUAGE>");
        provider_Request.append("<CHARSPERLINE>").append(CHARSPERLINE).append("</CHARSPERLINE>");
        provider_Request.append("<TYPE>").append("FULLTEXT").append("</TYPE>");
        provider_Request.append("</RECEIPT>");
        provider_Request.append("</REQUEST>");
        return provider_Request;

    }

    public static String getCurrentTimeStamp() {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date now = new Date();
        String strDate = sdf.format(now);
        return strDate;
    }

    private static String getEAN(RSRequest request) {
        String EAN = "";
        if (request.operatorID == 3) { //Etisalat
            if (request.amount == 0.5) {
                EAN = "8886666666055";
            } else if (request.amount == 1) {
                EAN = "8886666666056";
            } else if (request.amount == 1.5) {
                EAN = "8886666666057";
            } else if (request.amount == 2) {
                EAN = "4251755637126";
            } else if (request.amount == 3) {
                EAN = "8886666666058";
            } else if (request.amount == 3.5) {
                EAN = "8886666666159";
            } else if (request.amount == 4.25) {
                EAN = "4251755637133";
            } else if (request.amount == 5) {
                EAN = "8886666666059";
            } else if (request.amount == 7) {
                EAN = "8886666666161";
            } else if (request.amount == 9) {
                EAN = "4251755629060";
            } else if (request.amount == 10) {
                EAN = "8886666666060";
            } else if (request.amount == 15) {
                EAN = "8886666666061";
            } else if (request.amount == 25) {
                EAN = "8886666666062";
            } else if (request.amount == 30) {
                EAN = "8886666666970";
            } else if (request.amount == 50) {
                EAN = "8886666666063";
            } else if (request.amount == 100) {
                EAN = "8886666666064";

            }
        } else if (request.operatorID == 2) {//Mobinil
            if (request.amount == 0.5) {
                EAN = "8886666666000";
            } else if (request.amount == 20) {
                EAN = "8886666666550";
            } else if (request.amount == 1) {
                EAN = "8886666666001";
            } else if (request.amount == 1.5) {
                EAN = "8886666666556";
            } else if (request.amount == 3.5) {
                EAN = "8886666666557";
            } else if (request.amount == 4.25) {
                EAN = "4251755637829";
            } else if (request.amount == 10) {
                EAN = "8886666666003";
            } else if (request.amount == 100) {
                EAN = "8886666666007";
            } else if (request.amount == 15) {
                EAN = "8886666666004";
            } else if (request.amount == 2) {
                EAN = "8886666666561";
            } else if (request.amount == 4) {
                EAN = "8886666666562";
            } else if (request.amount == 25) {
                EAN = "8886666666005";
            } else if (request.serviceID == 4879 && request.amount == 5) {
                EAN = "8886666666555";
            } else if (request.amount == 50) {
                EAN = "8886666666006";
            } else if (request.serviceID == 25997 && request.amount == 5) { //faka
                EAN = "4251755655168";
            } else if (request.amount == 2.25) {
                EAN = "4251755655175";
            } else if (request.serviceID == 130 && request.amount == 7) {
                EAN = "4251755655298";
            } else if (request.serviceID == 131 && request.amount == 9) {
                EAN = "4251755655304";
            } else if (request.amount == 7) {
                EAN = "8886666666564";
            }
        } else if (request.operatorID == 1) { //Vodafone
            if (request.serviceID == 60140 && request.amount == 10) {
                EAN = "4260497360711";
            } else if (request.serviceID == 60138 && request.amount == 10) {
                EAN = "4260497360698";
            } else if (request.serviceID == 60139 && request.amount == 10) {
                EAN = "4260497360704";
            } else if (request.serviceID == 4740 && request.amount == 10) {
                EAN = "8886666666043";
            } else if (request.amount == 1.25) {
                EAN = "8886666666040";
            } else if (request.amount == 100) {
                EAN = "8886666666047";
            } else if (request.amount == 15) {
                EAN = "8886666666044";
            } else if (request.amount == 200) {
                EAN = "8886666666077";
            } else if (request.amount == 25) {
                EAN = "8886666666045";
            } else if (request.amount == 3) {
                EAN = "8886666666041";
            } else if (request.amount == 5) {
                EAN = "8886666666042";
            } else if (request.amount == 50) {
                EAN = "8886666666046";
            } else if (request.amount == 2.5) {
                EAN = "4260497360643";
            }
        } else {
        }
        return EAN;
    }

    private boolean isValidAmount(RSRequest request) {
        boolean isValid = false;
        if (request.operatorID == 3) { //Etisalat
            if (request.amount == 0.5
                    || request.amount == 1
                    || request.amount == 1.5
                    || request.amount == 2
                    || request.amount == 3
                    || request.amount == 4.25
                    || request.amount == 3.5
                    || request.amount == 5
                    || request.amount == 7
                    || request.amount == 9
                    || request.amount == 10
                    || request.amount == 15
                    || request.amount == 25
                    || request.amount == 30
                    || request.amount == 50
                    || request.amount == 100) {
                isValid = true;

            }
        } else if (request.operatorID == 2) { //Mobinil
            if (request.amount == 0.5
                    || request.amount == 1
                    || request.amount == 1.5
                    || request.amount == 2
                    || request.amount == 2.25
                    || request.amount == 3.5
                    || request.amount == 4
                    || request.amount == 4.25
                    || request.amount == 5
                    || request.amount == 7
                    || request.amount == 9
                    || request.amount == 10
                    || request.amount == 15
                    || request.amount == 20
                    || request.amount == 25
                    || request.amount == 50
                    || request.amount == 100) {
                isValid = true;

            }
        } else if (request.operatorID == 1) { //Vodafone
            if (request.amount == 10
                    || request.amount == 1.5
                    || request.amount == 100
                    || request.amount == 15
                    || request.amount == 200
                    || request.amount == 25
                    || request.amount == 3
                    || request.amount == 5
                    || request.amount == 50
                    || request.amount == 2.5) {

                isValid = true;
            }
        }

        return isValid;
    }
}
