/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.momkn.provider;

import static com.momkn.manager.Manager.logger;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import org.apache.commons.lang.StringUtils;

import com.momkn.entites.RSRequest;
import com.momkn.entites.RSResponse;

/**
 *
 * @author Tony
 */
public class Masary implements VoucherProvider {

    private final String USER_AGENT = "Mozilla/5.0";
    private final String IP = "e-masary.net";

    @Override
    public RSResponse voucher(RSRequest request) {
        RSResponse response = new RSResponse();
        response.requestID = request.requestID;
        try {
//https://IP/topup/service?action=voucheretisalat&id=***&password=***&msisdn=***&amount=***&timestamp=***&Sendertrx=***
            logger.info(this.getClass().getSimpleName() + " - " + "Provider Get Date Request Creation started");
            Map<String, String> map = new HashMap<>();
            map.put("id", request.userName);
            map.put("password", request.password);
            String getDateResponse = "";
            String getVoucherResponse = "";

            try {
//            send Get Date request
                getDateResponse = sendGet(buildGetDate(map));
//                System.out.println(buildGetDate(map)); //Test
//                getDateResponse = "2014-03-05T15:42:30.007"; //Test
                logger.info(this.getClass().getSimpleName() + " - " + "Provider Get Date Request Creation ended , GetDate Response : " + getDateResponse);

                logger.info(this.getClass().getSimpleName() + " - " + "Provider Request Creation started");
                try {
//            send Get Voucher request
                    map.put("msisdn", request.phone);
                    map.put("amount", String.valueOf(request.amount));
                    map.put("timestamp", getDateResponse);
                    map.put("Sendertrx", request.requestID);
                    String provider_Request = buildGetVoucherURL(map, request.operatorID);
                    logger.info(this.getClass().getSimpleName() + " - " + "Provider Request : " + provider_Request.replace(request.password, StringUtils.repeat("*", request.password.length())));
                    logger.info(this.getClass().getSimpleName() + " - " + "Provider Request Creation ended");
                    logger.info(this.getClass().getSimpleName() + " - " + "Provider Request Sending started");
                    getVoucherResponse = sendGet(provider_Request);
                    logger.info(this.getClass().getSimpleName() + " - " + "Provider GetVoucher Request Creation ended , GetVoucher Response : " + getVoucherResponse);
                    logger.info(this.getClass().getSimpleName() + " - " + "Provider Request Sending ended");

//                    System.out.println(buildGetVoucherURL(map, request.operatorID)); //Test
//                    getVoucherResponse = "1466133 59862681871972 7410564933"; //Test
                    if (getVoucherResponse != null) {
                        String[] values = getVoucherResponse.trim().split("\\s+");
                        if (values.length == 3) {
                            response.code = 200;
                            response.message = "Success";
                            response.providerTransactionID = values[0];
                            response.pin = values[1];
                            response.serialNumber = values[2];
                        } else {
                            logger.error(this.getClass().getSimpleName() + " - " + "Provider Error : can't extract voucher data " + getVoucherResponse);
                            response.code = -2;
                            response.message = "Provider Error : can't extract voucher data " + getVoucherResponse;
                            response.providerTransactionID = "";
                            response.pin = "";
                            response.serialNumber = "";
                        }
                    } else {
                        logger.error(this.getClass().getSimpleName() + " - " + "Provider Error : can't extract voucher data : null");
                        response.code = -2;
                        response.message = "Provider Error : can't extract voucher data : null";
                        response.providerTransactionID = "";
                        response.pin = "";
                        response.serialNumber = "";
                    }
                } catch (Exception e) {
                    logger.error(this.getClass().getSimpleName() + " - " + "Provider Error : Can't get Response from Get Voucher method with error: " + e);
                    response.code = -1;
                    response.message = "Provider Error : Can't get Response from Get Voucher method with error : " + e;
                    response.providerTransactionID = "";
                    response.pin = "";
                    response.serialNumber = "";

                }

            } catch (Exception e) {
                logger.error(this.getClass().getSimpleName() + " - " + "Provider Error : Can't get Response from Get Date method with error : " + e);
                response.code = -1;
                response.message = "Provider Error : Can't get Response from Get Date method with error : " + e;
                response.providerTransactionID = "";
                response.pin = "";
                response.serialNumber = "";

            }

        } catch (Exception e) {
            logger.error(e);
            response.code = -3;
            response.message = "General Error : " + e;
            response.providerTransactionID = "";
            response.pin = "";
            response.serialNumber = "";
        }

        return response;
    }

    private String buildGetDate(Map<String, String> map) {
        StringBuilder url = new StringBuilder();
        url.append("https://" + IP + "/topup/service?action=gettime");
        for (Map.Entry<String, String> entry : map.entrySet()) {
            url.append("&").append(entry.getKey()).append("=").append(entry.getValue());
        }
        return url.toString();
    }

    private String buildGetVoucherURL(Map<String, String> map, int operatorId) {
        StringBuilder url = new StringBuilder();
        url.append("https://" + IP + "/topup/service?action=");
        if (operatorId == 1) {
            url.append("voucheretisalat");
        } else if (operatorId == 2) {
            url.append("vouchermobinil");
        } else if (operatorId == 3) {
            url.append("vouchervodafone");
        }
        for (Map.Entry<String, String> entry : map.entrySet()) {
            url.append("&").append(entry.getKey()).append("=").append(entry.getValue());
        }
        return url.toString();
    }

    // HTTP GET request
    private String sendGet(String url) throws Exception {
//        System.out.println(url);
        URL obj = new URL(url);
        HttpURLConnection con = (HttpURLConnection) obj.openConnection();

        // optional default is GET
        con.setRequestMethod("GET");

        //add request header
        con.setRequestProperty("User-Agent", USER_AGENT);

        int responseCode = con.getResponseCode();

        BufferedReader in = new BufferedReader(
                new InputStreamReader(con.getInputStream()));
        String inputLine;
        StringBuffer response = new StringBuffer();

        while ((inputLine = in.readLine()) != null) {
            response.append(inputLine);
        }
        in.close();

        //print result
//        System.out.println(response.toString());
        return response.toString();

    }
}
