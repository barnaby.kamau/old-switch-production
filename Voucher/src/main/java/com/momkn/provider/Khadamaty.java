/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.momkn.provider;

import com.momkn.entites.Auth;
import com.momkn.entites.InqField;
import com.momkn.entites.KhadamatyResponse;
import com.momkn.entites.KhadamtyRequest;
import com.momkn.entites.RSRequest;
import com.momkn.entites.RSResponse;
import static com.momkn.manager.Manager.logger;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.config.ClientConfig;
import com.sun.jersey.api.client.config.DefaultClientConfig;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.UriBuilder;
import org.apache.commons.lang.StringUtils;

/**
 *
 * @author Tony
 */
public class Khadamaty implements VoucherProvider {

    int SERVICEID = 356;
    String AGENTCODE = "8364";
    int APPID = 27;
    String URL = "https://gateway.khadamaty.com.eg/V3/api/Payment";

    @Override
    public RSResponse voucher(RSRequest request) {
        RSResponse response = new RSResponse();
        response.requestID = request.requestID;
        try {
            logger.info("[Khadamaty][RequestID = " + request.requestID + " ] Provider Request Creation started");

            KhadamtyRequest providerRequest = getProviderRequest(request);
            logger.info("[Khadamaty][RequestID = " + request.requestID + " ] Provider Request : " + providerRequest.toString().replace(request.password, StringUtils.repeat("*", request.password.length())));
            logger.info("[Khadamaty][RequestID = " + request.requestID + " ] Provider Request Creation ended");
            logger.info("[Khadamaty][RequestID = " + request.requestID + " ] Provider Request Sending started");
            ClientConfig config = new DefaultClientConfig();
            Client client = Client.create(config);
            WebResource webResource = client.resource(UriBuilder.fromUri(URL).build());
            KhadamatyResponse providerResponse = webResource.type(MediaType.APPLICATION_JSON).post(KhadamatyResponse.class, providerRequest);
            logger.info("[Khadamaty] RequestID : " + request.requestID + " Provider Response : " + providerResponse);
            logger.info("[Khadamaty] RequestID : " + request.requestID + " Provider Request Sending ended");
            logger.info("[Khadamaty] RequestID : " + request.requestID + " Provider Response Unmarshal ended");
            if (providerResponse.StatusCode == 1) {
                response.code = 200;
                response.message = "Successful transaction";
                response.providerTransactionID = providerResponse.KHDTransId;
                for (InqField field : providerResponse.Fields) {
                    if (field.SFId == 1723) {
                        response.serialNumber = field.Value;
                    }
                    if (field.SFId == 1724) {
                        response.pin = field.Value;
                    }
                }
            } else {
                logger.error("[Khadamaty][RequestID = " + request.requestID + " ] Provider Error : " + providerResponse.PaymentStatus);
                response.code = -2;
                response.message = "[Khadamaty][RequestID = " + request.requestID + " ] Provider Error : " + providerResponse.PaymentStatus;
                response.providerTransactionID = "";
                response.pin = "";
                response.serialNumber = "";
            }
//        } catch (java.net.SocketTimeoutException e) {
//            logger.error(this.getClass().getSimpleName() + " - " + e);
//            response.code = -300;
//            response.message = "Timeout in Connection to provider : " + e;
//            response.providerTransactionID = "";
//            response.pin = "";
//            response.serialNumber = "";
        } catch (Exception e) {
            logger.error(e);
            response.code = -3;
            response.message = "General Error : " + e;
            response.providerTransactionID = "";
            response.pin = "";
            response.serialNumber = "";
        }
        return response;
    }

    public static String getCurrentTimeStamp() {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date now = new Date();
        String strDate = sdf.format(now);
        return strDate;
    }

    private KhadamtyRequest getProviderRequest(RSRequest request) {
        KhadamtyRequest providerRequest = new KhadamtyRequest();
        providerRequest.SenderTrans = request.requestID;
        providerRequest.ServiceId = SERVICEID;
        providerRequest.amount = request.amount;
        Auth auth = new Auth();
        auth.AgentCode = AGENTCODE;
        auth.AppId = APPID;
        auth.UserId = request.userName;
        auth.Password = request.password;
        providerRequest.Auth = auth;
        List<InqField> InqFields = new ArrayList<>();
        InqField field = new InqField();
        field.SFId = 971;
        field.Label = "رقم تليفون العميل";
        field.Value = request.phone;
        InqFields.add(field);
        providerRequest.InqFields = InqFields;
        return providerRequest;
    }

}
