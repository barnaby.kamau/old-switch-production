/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.momkn.manager;

import static javax.ws.rs.core.MediaType.APPLICATION_JSON;
import static javax.ws.rs.core.Response.ok;

import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;

import org.apache.log4j.Logger;
import org.json.JSONException;

import com.momkn.entites.RSRequest;
import com.momkn.entites.RSResponse;
import com.momkn.provider.Khadamaty;
import com.momkn.provider.Orange;
import com.momkn.provider.Tamam;
import com.momkn.provider.VoucherProvider;

@Path("/")
@Produces(APPLICATION_JSON)
public class Manager {

    public static Logger logger = Logger.getLogger("R");

    @POST
    public Response voucher(RSRequest request) throws JSONException {
        RSResponse response = null;
        logger.info("Voucher Request : " + request);
        VoucherProvider provider = null;
        provider = getProvider(request);
        response = new RSResponse();
        if (provider != null) {
            response = provider.voucher(request);
        } else {
            response.code = -4;
            response.message = "Provider Unavailable!";
            response.providerTransactionID = "";
            response.pin = "";
            response.serialNumber = "";
        }

        response.providerID = request.providerID;
        logger.info("Voucher Response : " + response);
        response.pin = getFormatedPIN(response.pin);
        return ok(response, APPLICATION_JSON).build();
    }

    @Path("checkTransaction")
    @POST
    public Response checkTransaction(RSRequest request) throws JSONException {
        RSResponse response = new RSResponse();
        logger.info("checkTransaction Request : " + request);
        logger.info("checkTransaction Response : " + response);
        return ok(response, APPLICATION_JSON).build();
    }

    private VoucherProvider getProvider(RSRequest request) {
        if (request.providerID == 17) {
            return new Orange();
        }
        if (request.providerID == 3) {
            return new Khadamaty();
        } 
        if (request.providerID == 1) {
            return new Tamam();
        } else {
            return null;
        }
    }
    
    private String getFormatedPIN(String pin) {
        String p = pin.replace("-", "");
        StringBuilder result = new StringBuilder();
        for (int i = 0; i < p.length(); i++) {
            if (i % 4 == 0 && i != 0) {
                result.append("-");
            }

            result.append(p.charAt(i));
        }
        return result.toString();

    }
}
