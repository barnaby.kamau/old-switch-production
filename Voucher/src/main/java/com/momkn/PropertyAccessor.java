package com.momkn;

import java.util.Properties;
import org.apache.log4j.Logger;

public class PropertyAccessor
{
	public static Logger logger = Logger.getLogger(PropertyAccessor.class);

	public static Properties loadProperties(String fileName)
	{
		Properties properties = new Properties();
		try
		{
			properties.load(Thread.currentThread().getContextClassLoader().getResourceAsStream((fileName)));
			logger.debug("File [" + fileName + "] has been read successfully.");
		}
		catch (Exception e)
		{
			logger.error("Error in loading properties file [" + fileName + "]: ", e);
		}
		return properties;
	}
}