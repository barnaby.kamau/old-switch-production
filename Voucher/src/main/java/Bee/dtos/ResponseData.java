/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Bee.dtos;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 *
 * @author Antonios_knaguib
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "data")
public class ResponseData {

    @XmlElement(name = "transactionId", required = true)
    protected String transactionId;
    @XmlElement(name = "transactionStatus", required = true)
    protected int transactionStatus;
    @XmlElement(name = "dateTime", required = true)
    protected String dateTime;
    @XmlElement(name = "info", required = true)
    protected String info;
    @XmlElement(name = "ccTransactionId", required = true)
    protected String ccTransactionId;
    @XmlElement(name = "serviceVersion", required = true)
    protected String serviceVersion;
    @XmlElement(name = "providerGroupList", required = true)
    protected ProviderGroupList providerGroupList;
    @XmlElement(name = "providerList", required = true)
    protected ProviderList providerList;
    @XmlElement(name = "serviceList", required = true)
    protected ServiceList serviceList;
    @XmlElement(name = "serviceInputParameterList", required = true)
    protected ServiceInputParameterList serviceInputParameterList;

    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    public int getTransactionStatus() {
        return transactionStatus;
    }

    public void setTransactionStatus(int transactionStatus) {
        this.transactionStatus = transactionStatus;
    }

    public String getDateTime() {
        return dateTime;
    }

    public void setDateTime(String dateTime) {
        this.dateTime = dateTime;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public String getCcTransactionId() {
        return ccTransactionId;
    }

    public void setCcTransactionId(String ccTransactionId) {
        this.ccTransactionId = ccTransactionId;
    }

    public String getServiceVersion() {
        return serviceVersion;
    }

    public void setServiceVersion(String serviceVersion) {
        this.serviceVersion = serviceVersion;
    }

    public ProviderGroupList getProviderGroupList() {
        return providerGroupList;
    }

    public void setProviderGroupList(ProviderGroupList providerGroupList) {
        this.providerGroupList = providerGroupList;
    }

    public ProviderList getProviderList() {
        return providerList;
    }

    public void setProviderList(ProviderList providerList) {
        this.providerList = providerList;
    }

    public ServiceList getServiceList() {
        return serviceList;
    }

    public void setServiceList(ServiceList serviceList) {
        this.serviceList = serviceList;
    }

    public ServiceInputParameterList getServiceInputParameterList() {
        return serviceInputParameterList;
    }

    public void setServiceInputParameterList(ServiceInputParameterList serviceInputParameterList) {
        this.serviceInputParameterList = serviceInputParameterList;
    }

}
