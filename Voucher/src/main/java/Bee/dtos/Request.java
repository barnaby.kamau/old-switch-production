/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Bee.dtos;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 *
 * @author Antonios_knaguib
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Request")
@XmlRootElement(name = "Request")
public class Request {

    @XmlAttribute(name = "action", required = true)
    protected ActionCode action;
    @XmlAttribute(name = "version", required = true)
    protected String version;
    @XmlElement(name = "login", required = true)
    protected String login;
    @XmlElement(name = "password", required = true)
    protected String password;
    @XmlElement(name = "locale", required = true)
    protected LocalID locale;
    @XmlElement(name = "data",required = true)
    protected RequestData data;

    public RequestData getData() {
        return data;
    }

    public void setData(RequestData data) {
        this.data = data;
    }

    public ActionCode getAction() {
        return action;
    }

    public void setAction(ActionCode action) {
        this.action = action;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public LocalID getLocale() {
        return locale;
    }

    public void setLocale(LocalID locale) {
        this.locale = locale;
    }

}
