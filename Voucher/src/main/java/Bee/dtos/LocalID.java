/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Bee.dtos;

import static Bee.dtos.ActionCode.valueOf;
import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;

/**
 *
 * @author Antonios_knaguib
 */
@XmlType(name = "locale")
@XmlEnum
public enum LocalID {

    en, ar;

    public String value() {
        return name();
    }

    public static LocalID fromValue(String v) {
        return valueOf(v);
    }

}
