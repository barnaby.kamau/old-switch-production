package com.mobinil.provider;

import com.mobinil.entites.RSRequest;
import com.mobinil.entites.RSResponse;

public interface Provider {
	
	public RSResponse inquiry(RSRequest request);
	public RSResponse payment(RSRequest request);

}
