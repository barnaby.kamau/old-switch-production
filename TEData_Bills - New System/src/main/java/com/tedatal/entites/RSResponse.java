/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tedatal.entites;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

/**
 *
 * @author Antonios_knaguib
 */
public class RSResponse {

    public int Code;
    public String Message;
    public String Customer_Name;
    public double Total_Amount;
    public String request_id;
    public String Old_Package_Expiration;
    public String End_Date;
    public int ReceiptNumber;

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this,
                ToStringStyle.MULTI_LINE_STYLE);
    }

}
