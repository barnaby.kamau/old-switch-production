/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.momken.tedata_bills;

import com.momkn.util.XMLGregorianCalendarConverter;
import java.math.BigDecimal;
import java.util.Date;
import javax.xml.datatype.XMLGregorianCalendar;
import net.tedata.webservices.v1.tedatacommontypes.RenewalFormType;
import net.tedata.webservices.v2.paymentmanagement.Method;
import net.tedata.webservices.v2.paymentmanagement.Mode;
import net.tedata.webservices.v2.paymentmanagement.PaymentManagementRequestType;
import net.tedata.webservices.v2.paymentmanagement.PaymentManagementResponseType;
import net.tedata.webservices.v2.paymentmanagement.PaymentRequestType;
//import net.tedata.webservices.v2.paymentmanagement.RenewalFormType;

/**
 *
 * @author Tony
 */
public class Test {

    public static void main(String[] args) {

        net.tedata.webservices.v2.paymentmanagement.PaymentManagementResponseType inquiryResponse = new PaymentManagementResponseType();
        net.tedata.webservices.v2.paymentmanagement.PaymentManagementResponseType paymentResponse = new PaymentManagementResponseType();

        inquiryResponse = inquiryBill();

        if (inquiryResponse.getMessageBody().getPaymentManagementResponse().getInquiryInfo().isErrorOccured()) {

            int errorCode = inquiryResponse.getMessageBody().getPaymentManagementResponse().getInquiryInfo().getErrorCode();
            String errorMessage = inquiryResponse.getMessageBody().getPaymentManagementResponse().getInquiryInfo().getErrorMessage();
            
        } else {
            String billDate = inquiryResponse.getMessageBody().getPaymentManagementResponse().getInquiryInfo().getNewExpiryDateAfterRenewal();
            String customerName = inquiryResponse.getMessageBody().getPaymentManagementResponse().getInquiryInfo().getCustomerName();
            int invoiceNumber = inquiryResponse.getMessageBody().getPaymentManagementResponse().getInquiryInfo().getCreatedInvoiceNumber();
            double dueAmount = inquiryResponse.getMessageBody().getPaymentManagementResponse().getInquiryInfo().getTotalDueForRenewal();
        }
        /*
         TODO Inquiry response
         
//            response.setBILL_DATE(inquiryResponseType.getNewExpiryDateAfterRenewal());
//            response.setCUSTOMER_NAME(inquiryResponseType.getCustomerName());
//            response.setREQUEST_ID(String.valueOf(inquiryResponseType.getCreatedInvoiceNumber()));
//            response.setAMOUNT(BigDecimal.valueOf(inquiryResponseType.getTotalDueForRenewal()));
//            response.setSTATUS_CODE("200");
//            response.setSTATUS_MESSAGE("Success");
//            TEData_TCPIP.logger.info("Status Code : " + response.getSTATUS_CODE());
//            System.out.println("Status Code : " + response.getSTATUS_CODE());
        */
        
        paymentResponse = paymentBill();

        if (paymentResponse.getMessageBody().getPaymentManagementResponse().getInquiryInfo().isErrorOccured()) {

            int errorCode = paymentResponse.getMessageBody().getPaymentManagementResponse().getInquiryInfo().getErrorCode();
            String errorMessage = paymentResponse.getMessageBody().getPaymentManagementResponse().getInquiryInfo().getErrorMessage();
            
        } else {
            int receiptNumber = paymentResponse.getMessageBody().getPaymentManagementResponse().getAddPaymentInfo().getRenewalResponse().getReceiptNumber();
//            String billDate = inquiryResponse.getMessageBody().getPaymentManagementResponse().getInquiryInfo().getNewExpiryDateAfterRenewal();
//            String customerName = inquiryResponse.getMessageBody().getPaymentManagementResponse().getInquiryInfo().getCustomerName();
//            int invoiceNumber = inquiryResponse.getMessageBody().getPaymentManagementResponse().getInquiryInfo().getCreatedInvoiceNumber();
//            double dueAmount = inquiryResponse.getMessageBody().getPaymentManagementResponse().getInquiryInfo().getTotalDueForRenewal();
        }
        
        /*
         TODO Payment Response
         */
//             response.setBILL_DATE(date.toString());
//                response.setCUSTOMER_NAME("");
//                response.setREQUEST_ID(String.valueOf(TEDataresponse.getRenewResponse().getReceiptNumber()));
//                response.setAMOUNT(BigDecimal.valueOf(0));
//                response.setSTATUS_CODE("200");
//                response.setSTATUS_MESSAGE("Success");
    }

    public static net.tedata.webservices.v2.paymentmanagement.PaymentManagementResponseType inquiryBill() {
        net.tedata.webservices.v2.paymentmanagement.PaymentManagementRequestType paymentManagementRequestType = new PaymentManagementRequestType();

        RenewalFormType renewalFormType = new RenewalFormType();
        renewalFormType.setAdslPhoneNumber("2343454");
        renewalFormType.setAreaCode(Integer.parseInt("086"));
        renewalFormType.setCustomerNumber("0");
        renewalFormType.setExternalPaymentTransactionNumber("MMKN-" + 12345567);
        renewalFormType.setIncludeCPERentalInRenewal(true);
        renewalFormType.setIncludeOptionPackInRenewal(true);
        renewalFormType.setNewDurationInMonths(0);
        renewalFormType.setNewLimitationType("");
        renewalFormType.setNewOptionPackPackageID(0);
        renewalFormType.setNewOptionPackPackagePrice(0);
        renewalFormType.setNewSpeed("");
        renewalFormType.setPaymentMethodID(Integer.parseInt("9"));
        renewalFormType.setRenewalAdminUserID(Integer.parseInt("23178"));
        renewalFormType.setRenewalLocationID(Integer.parseInt("439"));
        renewalFormType.setRenewalUserName("Momkn");
        renewalFormType.setPackageOfferTypeID(0);
        renewalFormType.setVoucherNumber("");
        renewalFormType.setUpgradeInTheMiddle(false);
        renewalFormType.setUseExistingDuration(true);
        renewalFormType.setUseExistingLimitationType(true);
        renewalFormType.setUseExistingPackageOfferTypeID(true);
        renewalFormType.setUseExistingSpeed(true);

        PaymentManagementRequestType.MessageBody messageBody = new PaymentManagementRequestType.MessageBody();
        net.tedata.webservices.v2.paymentmanagement.PaymentRequestType paymentRequestType = new net.tedata.webservices.v2.paymentmanagement.PaymentRequestType();
        paymentRequestType.setRenewalInfo(renewalFormType);
        messageBody.setPaymentManagementRequest(paymentRequestType);
        messageBody.setMethod(Method.BY_PHONE_NUMBER);
        messageBody.setMode(Mode.INQUIRY);
        paymentManagementRequestType.setMessageBody(messageBody);
        return paymentManagment(paymentManagementRequestType);

    }

    public static net.tedata.webservices.v2.paymentmanagement.PaymentManagementResponseType paymentBill() {

        net.tedata.webservices.v2.paymentmanagement.PaymentManagementRequestType paymentManagementRequestType = new PaymentManagementRequestType();

        RenewalFormType renewalFormType = new RenewalFormType();

        renewalFormType.setAdslPhoneNumber("2343454");
        renewalFormType.setAreaCode(Integer.parseInt("086"));
        renewalFormType.setCustomerNumber("0");
        renewalFormType.setExternalPaymentTransactionNumber("MMKN-" + 12345567);
        renewalFormType.setIncludeCPERentalInRenewal(true);
        renewalFormType.setIncludeOptionPackInRenewal(true);
        renewalFormType.setNewDurationInMonths(0);
        renewalFormType.setNewLimitationType("");
        renewalFormType.setNewOptionPackPackageID(0);
        renewalFormType.setNewOptionPackPackagePrice(0);
        renewalFormType.setNewSpeed("");
        renewalFormType.setPaymentMethodID(Integer.parseInt("9"));
        renewalFormType.setRenewalAdminUserID(Integer.parseInt("23178"));
        renewalFormType.setRenewalLocationID(Integer.parseInt("439"));
        renewalFormType.setRenewalUserName("Momkn");
        renewalFormType.setPackageOfferTypeID(0);
        renewalFormType.setVoucherNumber("");
        renewalFormType.setUpgradeInTheMiddle(false);
        renewalFormType.setUseExistingDuration(true);
        renewalFormType.setUseExistingLimitationType(true);
        renewalFormType.setUseExistingPackageOfferTypeID(true);
        renewalFormType.setUseExistingSpeed(true);

//        addPaymentRequest.setRenew(renewalFormType);
        XMLGregorianCalendarConverter xMLGregorianCalendarConverter = new XMLGregorianCalendarConverter();
        XMLGregorianCalendar xMLGregorianCalendar = xMLGregorianCalendarConverter.asXMLGregorianCalendar(new Date());

        PaymentManagementRequestType.MessageBody messageBody = new PaymentManagementRequestType.MessageBody();
        net.tedata.webservices.v2.paymentmanagement.PaymentRequestType paymentRequestType = new net.tedata.webservices.v2.paymentmanagement.PaymentRequestType();
        paymentRequestType.setRenewalInfo(renewalFormType);
        messageBody.setPaymentManagementRequest(paymentRequestType);
        messageBody.setMethod(Method.BY_PHONE_NUMBER);
        messageBody.setMode(Mode.ADD_PAYMENT);
        paymentManagementRequestType.setMessageBody(messageBody);
        return paymentManagment(paymentManagementRequestType);
    }

    public static net.tedata.webservices.v2.paymentmanagement.PaymentManagementResponseType paymentManagment(net.tedata.webservices.v2.paymentmanagement.PaymentManagementRequestType paymentManagementRequestType) {
        net.tedata.webservices.v2.paymentmanagement.PaymentManagementResponseType response = new PaymentManagementResponseType();
        try { // Call Web Service Operation
            net.tedata.webservices.v2.paymentmanagement.FEPaymentManagement service = new net.tedata.webservices.v2.paymentmanagement.FEPaymentManagement();
            net.tedata.webservices.v2.paymentmanagement.PaymentManagementPortType port = service.getFEPaymentManagementHttpPort();
            // TODO process result here
            response = port.paymentManagement(paymentManagementRequestType);
//            System.out.println("Result = " + result);
        } catch (Exception ex) {
            // TODO handle custom exceptions here
        }
        return response;
    }

}
