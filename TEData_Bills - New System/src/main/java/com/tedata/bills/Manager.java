/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tedata.bills;

/**
 *
 * @author Antonios_knaguib
 */

import com.tedata.provider.ProviderManager;
import com.tedatal.entites.GetTopUpPackRequest;
import com.tedatal.entites.GetTopUpPackResponse;
import com.tedatal.entites.RSRequest;
import com.tedatal.entites.RSResponse;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import static javax.ws.rs.core.MediaType.APPLICATION_JSON;
import javax.ws.rs.core.Response;
import static javax.ws.rs.core.Response.ok;
import org.apache.log4j.Logger;
import org.json.JSONException;

@Path("/")
@Produces(APPLICATION_JSON)
public class Manager {

    public static Logger logger = Logger.getLogger("R");

    @Path("bill")
    @GET
    public Response Inquiry(@QueryParam("request_id") String request_id, @QueryParam("phone") String phone, @QueryParam("telephone_code") String telephone_code, @QueryParam("user_name") String user_name, @QueryParam("password") String password, @Context HttpServletRequest httprequest) throws JSONException {
        RSResponse response = new RSResponse();
        RSRequest request = new RSRequest();
//        if (httprequest.getRemoteAddr().equals("197.44.51.2") || httprequest.getRemoteAddr().equals("41.39.135.74")) {
//            System.out.println("ip = " + httprequest.getRemoteAddr());
        request.request_id = request_id;
        request.Request_Type = 1;
        request.Phone = phone;
        request.telephone_code = telephone_code;
        request.user_name = user_name;
        request.password = password;
        logger.info("Inquiry Request : " + request);
        ProviderManager provider = new ProviderManager();
        response = provider.inquiry(request);
        logger.info("Inquiry Response : " + response);
        return ok(response, APPLICATION_JSON).build();
//        } else {
//            return Response.status(Response.Status.UNAUTHORIZED).entity("You are not authorized to use this service").build();
//        }
    }

    @Path("bill")
    @POST
    public Response payment(@QueryParam("request_id") String request_id, @QueryParam("phone") String phone, @QueryParam("telephone_code") String telephone_code, @QueryParam("amount") double amount, @QueryParam("user_name") String user_name, @QueryParam("password") String password, @Context HttpServletRequest httprequest) throws JSONException {
        RSResponse response = new RSResponse();
        RSRequest request = new RSRequest();
//        if (httprequest.getRemoteAddr().equals("197.44.51.2") || httprequest.getRemoteAddr().equals("41.39.135.74")) {
        request.request_id = request_id;
        request.Request_Type = 2;
        request.Phone = phone;
        request.Amount = amount;
        request.telephone_code = telephone_code;
        request.user_name = user_name;
        request.password = password;
        logger.info("Payment Request : " + request);
        ProviderManager provider = new ProviderManager();
        response = provider.payment(request);
        logger.info("Payment Response : " + response);
        return ok(response, APPLICATION_JSON).build();
//        } else {
//            return Response.status(Response.Status.UNAUTHORIZED).entity("You are not authorized to use this service").build();
//        }
    }

    @Path("topup")
    @GET
    public Response topUpInquiry(@QueryParam("request_id") String request_id, @QueryParam("phone") String phone, @QueryParam("telephone_code") String telephone_code, @QueryParam("amount") double amount, @QueryParam("user_name") String user_name, @QueryParam("password") String password, @Context HttpServletRequest httprequest) throws JSONException {
        RSResponse response = new RSResponse();
        RSRequest request = new RSRequest();
        request.request_id = request_id;
        request.Request_Type = 1;
        request.Phone = phone;
        request.telephone_code = telephone_code;
        request.Amount = amount;
        request.user_name = user_name;
        request.password = password;
        logger.info("topUpInquiry Request : " + request);
        ProviderManager provider = new ProviderManager();
        response = provider.topUpInquiry(request);
        logger.info("topUpInquiry Response : " + response);
        return ok(response, APPLICATION_JSON).build();
    }

    @Path("topup")
    @POST
    public Response topUppayment(@QueryParam("request_id") String request_id, @QueryParam("phone") String phone, @QueryParam("telephone_code") String telephone_code, @QueryParam("amount") double amount, @QueryParam("user_name") String user_name, @QueryParam("password") String password, @Context HttpServletRequest httprequest) throws JSONException {
        RSResponse response = new RSResponse();
        RSRequest request = new RSRequest();
        request.request_id = request_id;
        request.Request_Type = 2;
        request.Phone = phone;
        request.Amount = amount;
        request.telephone_code = telephone_code;
        request.user_name = user_name;
        request.password = password;
        logger.info("topUppayment Request : " + request);
        ProviderManager provider = new ProviderManager();
        response = provider.topUpPayment(request);
        logger.info("topUppayment Response : " + response);
        return ok(response, APPLICATION_JSON).build();
    }

    @Path("GetTopUpPackages")
    @GET
    public Response getTopUpPackages(@QueryParam("user_name") String user_name, @QueryParam("password") String password, @QueryParam("serviceName") String serviceName, @Context HttpServletRequest httprequest) throws JSONException {
        GetTopUpPackResponse response = new GetTopUpPackResponse();
        GetTopUpPackRequest request = new GetTopUpPackRequest();
        request.serviceName = serviceName;
        request.user_name = user_name;
        request.password = password;
        logger.info("getTopUpPackages Request : " + request);
        ProviderManager provider = new ProviderManager();
        response = provider.getTopUpPackages(request);
        logger.info("getTopUpPackages Response : " + response);
        return ok(response, APPLICATION_JSON).build();
//        } else {
//            return Response.status(Response.Status.UNAUTHORIZED).entity("You are not authorized to use this service").build();
//        }
    }

}
