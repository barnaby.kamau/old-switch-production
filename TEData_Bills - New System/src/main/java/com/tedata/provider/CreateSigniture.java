/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tedata.provider;

import com.google.common.hash.Hashing;
import java.io.FileInputStream;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.security.KeyStore;
import java.security.PrivateKey;

/**
 *
 * @author Tony
 */
public class CreateSigniture {

    public static void main(String[] args) {
        try {
            String keyAlies = "73d96d2d-c155-4317-bbda-b1cd3c561d3b";
            String KeyPassword = "password";

            String keyStorePath = "D:\\TEData New System\\Landline service\\Fixed Land Line Certificate_Test.pfx";
            KeyStore keystore = KeyStore.getInstance("PKCS12");
            InputStream input = new FileInputStream(keyStorePath);

            keystore.load(input, null);
            input.close();

            PrivateKey privateKey = (PrivateKey) keystore.getKey(keyAlies,
                    KeyPassword.toCharArray());
            System.out.println("++++++"+privateKey.toString());
            
            
            String sha256hex = Hashing.sha256()
                    .hashString("originalString", StandardCharsets.UTF_8)
                    .toString();
            System.out.println(sha256hex);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}
