/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tedata.provider;

import Manager.handler.HeaderHandlerResolver;
import com.momkn.util.XMLGregorianCalendarConverter;
import java.sql.Timestamp;
import java.util.Date;
import javax.xml.datatype.XMLGregorianCalendar;
import net.tedata.webservices.GetTopUpsPackagesRequest;
import net.tedata.webservices.RechargePackagesFaultsMsg;
import net.tedata.webservices.TopUpForPhoneNumberFault;
import net.tedata.webservices.TopUpInput;
import net.tedata.webservices.v1.tedatacommontypes.RenewalFormType;
import net.tedata.webservices.v2.paymentmanagement.Method;
import net.tedata.webservices.v2.paymentmanagement.Mode;
import net.tedata.webservices.v2.paymentmanagement.PaymentManagementRequestType;
import net.tedata.webservices.v2.paymentmanagement.PaymentManagementTEDServiceExceptionMessage;
import net.tedata.webservices.v2.tedatacommontypes.MessageHeaderType;
//import net.tedata.webservices.v2.paymentmanagement.RenewalFormType;

/**
 *
 * @author Tony
 */
public class TEData {

    public net.tedata.webservices.v2.paymentmanagement.PaymentManagementResponseType inquiry(String requestId, String areaCode, String phoneNumber) throws PaymentManagementTEDServiceExceptionMessage {
        net.tedata.webservices.v2.paymentmanagement.PaymentManagementRequestType paymentManagementRequestType = new PaymentManagementRequestType();

        RenewalFormType renewalFormType = new RenewalFormType();
        renewalFormType.setAdslPhoneNumber(phoneNumber);
        renewalFormType.setAreaCode(Integer.parseInt(areaCode));
        renewalFormType.setCustomerNumber("0");
        renewalFormType.setExternalPaymentTransactionNumber("Momkn-" + requestId);
        renewalFormType.setIncludeCPERentalInRenewal(true);
        renewalFormType.setIncludeOptionPackInRenewal(true);
        renewalFormType.setNewDurationInMonths(0);
        renewalFormType.setNewLimitationType("");
        renewalFormType.setNewOptionPackPackageID(0);
        renewalFormType.setNewOptionPackPackagePrice(0);
        renewalFormType.setNewSpeed("");
        renewalFormType.setPaymentMethodID(Integer.parseInt("9"));
        renewalFormType.setRenewalAdminUserID(Integer.parseInt("28366"));
        renewalFormType.setRenewalLocationID(Integer.parseInt("621"));
        renewalFormType.setRenewalUserName("Momkn");
        renewalFormType.setPackageOfferTypeID(0);
        renewalFormType.setVoucherNumber("");
        renewalFormType.setUpgradeInTheMiddle(false);
        renewalFormType.setUseExistingDuration(true);
        renewalFormType.setUseExistingLimitationType(true);
        renewalFormType.setUseExistingPackageOfferTypeID(true);
        renewalFormType.setUseExistingSpeed(true);
        
        MessageHeaderType header = new MessageHeaderType();
        header.setMessageID(requestId);
        header.setChannelID("Momkn");
        header.setTimestamp(new Timestamp(System.currentTimeMillis()).toInstant().toString());
        
        PaymentManagementRequestType.MessageBody messageBody = new PaymentManagementRequestType.MessageBody();
        net.tedata.webservices.v2.paymentmanagement.PaymentRequestType paymentRequestType = new net.tedata.webservices.v2.paymentmanagement.PaymentRequestType();
        paymentRequestType.setRenewalInfo(renewalFormType);
        messageBody.setPaymentManagementRequest(paymentRequestType);
        messageBody.setMethod(Method.BY_PHONE_NUMBER);
        messageBody.setMode(Mode.INQUIRY);
        paymentManagementRequestType.setMessageHeader(header);
        paymentManagementRequestType.setMessageBody(messageBody);
        return paymentManagment(paymentManagementRequestType);

    }

    public net.tedata.webservices.v2.paymentmanagement.PaymentManagementResponseType payment(String requestId, String areaCode, String phoneNumber) throws PaymentManagementTEDServiceExceptionMessage {

        net.tedata.webservices.v2.paymentmanagement.PaymentManagementRequestType paymentManagementRequestType = new PaymentManagementRequestType();

        RenewalFormType renewalFormType = new RenewalFormType();

        renewalFormType.setAdslPhoneNumber(phoneNumber);
        renewalFormType.setAreaCode(Integer.parseInt(areaCode));
        renewalFormType.setCustomerNumber("0");
        renewalFormType.setExternalPaymentTransactionNumber("Momkn-" + requestId);
        renewalFormType.setIncludeCPERentalInRenewal(true);
        renewalFormType.setIncludeOptionPackInRenewal(true);
        renewalFormType.setNewDurationInMonths(0);
        renewalFormType.setNewLimitationType("");
        renewalFormType.setNewOptionPackPackageID(0);
        renewalFormType.setNewOptionPackPackagePrice(0);
        renewalFormType.setNewSpeed("");
        renewalFormType.setPaymentMethodID(Integer.parseInt("9"));
        renewalFormType.setRenewalAdminUserID(Integer.parseInt("28366"));
        renewalFormType.setRenewalLocationID(Integer.parseInt("621"));
        renewalFormType.setRenewalUserName("Momkn");
        renewalFormType.setPackageOfferTypeID(0);
        renewalFormType.setVoucherNumber("");
        renewalFormType.setUpgradeInTheMiddle(false);
        renewalFormType.setUseExistingDuration(true);
        renewalFormType.setUseExistingLimitationType(true);
        renewalFormType.setUseExistingPackageOfferTypeID(true);
        renewalFormType.setUseExistingSpeed(true);

//        addPaymentRequest.setRenew(renewalFormType);
        XMLGregorianCalendarConverter xMLGregorianCalendarConverter = new XMLGregorianCalendarConverter();
        XMLGregorianCalendar xMLGregorianCalendar = XMLGregorianCalendarConverter.asXMLGregorianCalendar(new Date());

        MessageHeaderType header = new MessageHeaderType();
        header.setMessageID(requestId);
        header.setChannelID("Momkn");
        header.setTimestamp(new Timestamp(System.currentTimeMillis()).toInstant().toString());
        
        PaymentManagementRequestType.MessageBody messageBody = new PaymentManagementRequestType.MessageBody();
        net.tedata.webservices.v2.paymentmanagement.PaymentRequestType paymentRequestType = new net.tedata.webservices.v2.paymentmanagement.PaymentRequestType();
        paymentRequestType.setRenewalInfo(renewalFormType);
        messageBody.setPaymentManagementRequest(paymentRequestType);
        messageBody.setMethod(Method.BY_PHONE_NUMBER);
        messageBody.setMode(Mode.ADD_PAYMENT);

        paymentManagementRequestType.setMessageHeader(header);
        paymentManagementRequestType.setMessageBody(messageBody);
        return paymentManagment(paymentManagementRequestType);
    }

    public net.tedata.webservices.TopUpResult TopUpInquiry(String requestId, String areaCode, String phoneNumber, double amount) throws TopUpForPhoneNumberFault {
        net.tedata.webservices.TopUpInput request = new TopUpInput();
        request.setAddTopUpCharge(false);
        request.setAdminUserID(28367);
        request.setAdslPhoneNumber(phoneNumber);
        request.setAreaCode(Integer.parseInt(areaCode));
        request.setCustomerComment("");
        request.setCustomerNumber(0);
        request.setExternalPaymentTransactionNumber("MomknTU-" + requestId);
        request.setLocationID(622);
        request.setPackageOfferID(177);
        request.setPaymentMethodID(9);
        request.setServiceID(475);
        request.setTopUpAmount(amount);

        return topUpForPhoneNumber(request);

    }

    public net.tedata.webservices.TopUpResult TopUpPayment(String requestId, String areaCode, String phoneNumber, double amount) throws TopUpForPhoneNumberFault {
        net.tedata.webservices.TopUpInput request = new TopUpInput();
        request.setAddTopUpCharge(true);
        request.setAdminUserID(28367);
        request.setAdslPhoneNumber(phoneNumber);
        request.setAreaCode(Integer.parseInt(areaCode));
        request.setCustomerComment("");
        request.setCustomerNumber(0);
        request.setExternalPaymentTransactionNumber("MomknTU-" + requestId);
        request.setLocationID(622);
        request.setPackageOfferID(177);
        request.setPaymentMethodID(9);
        request.setServiceID(475);
        request.setTopUpAmount(amount);

        return topUpForPhoneNumber(request);
    }

    public net.tedata.webservices.GetTopUpsPackagesResponse getTopUpPackages(String serviceName) throws RechargePackagesFaultsMsg {
        net.tedata.webservices.GetTopUpsPackagesResponse response;
        net.tedata.webservices.GetTopUpsPackagesRequest request = new GetTopUpsPackagesRequest();
        request.setServiceName(serviceName);
        response = getTopUpsPackages(request);
        return response;
    }

//    public static net.tedata.webservices.v2.paymentmanagement.PaymentManagementResponseType paymentManagment(net.tedata.webservices.v2.paymentmanagement.PaymentManagementRequestType paymentManagementRequestType) throws PaymentManagementTEDServiceExceptionMessage {
//        net.tedata.webservices.v2.paymentmanagement.PaymentManagementResponseType response;
//        net.tedata.webservices.v2.paymentmanagement.FEPaymentManagement service = new net.tedata.webservices.v2.paymentmanagement.FEPaymentManagement();
//        HeaderHandlerResolver handlerResolver = new HeaderHandlerResolver();
//        service.setHandlerResolver(handlerResolver);
//        net.tedata.webservices.v2.paymentmanagement.PaymentManagementPortType port = service.getFEPaymentManagementHttpPort();
//        // TODO process result here
//        response = port.paymentManagement(paymentManagementRequestType);
//        return response;
//    }
    public static net.tedata.webservices.v2.paymentmanagement.PaymentManagementResponseType paymentManagment(net.tedata.webservices.v2.paymentmanagement.PaymentManagementRequestType paymentManagementRequestType) throws PaymentManagementTEDServiceExceptionMessage {
        net.tedata.webservices.v2.paymentmanagement.FEPaymentManagement service = new net.tedata.webservices.v2.paymentmanagement.FEPaymentManagement();
        HeaderHandlerResolver handlerResolver = new HeaderHandlerResolver();
        service.setHandlerResolver(handlerResolver);
        net.tedata.webservices.v2.paymentmanagement.PaymentManagementPortType port = service.getFEPaymentManagementHttpPort();
        net.tedata.webservices.v2.paymentmanagement.PaymentManagementResponseType result = port.paymentManagement(paymentManagementRequestType);
        return result;
//    System.out.println("Result = "+result);
    }

    public static net.tedata.webservices.GetTopUpsPackagesResponse getTopUpsPackages(net.tedata.webservices.GetTopUpsPackagesRequest getTopUpsPackagesRequest) throws RechargePackagesFaultsMsg {
        net.tedata.webservices.GetTopUpsPackagesResponse response;
        //Call Web Service Operation
        net.tedata.webservices.FEGetTopUpsPackages_Service service = new net.tedata.webservices.FEGetTopUpsPackages_Service();
        HeaderHandlerResolver handlerResolver = new HeaderHandlerResolver();
        service.setHandlerResolver(handlerResolver);
        net.tedata.webservices.FEGetTopUpsPackages port = service.getFEGetTopUpsPackagesSOAP();
        response = port.getTopUpsPackages(getTopUpsPackagesRequest);
        return response;
    }

    public static net.tedata.webservices.TopUpResult topUpForPhoneNumber(net.tedata.webservices.TopUpInput request) throws TopUpForPhoneNumberFault {
        net.tedata.webservices.TopUpResult response;

        net.tedata.webservices.FETopUpWS_Service service = new net.tedata.webservices.FETopUpWS_Service();
        HeaderHandlerResolver handlerResolver = new HeaderHandlerResolver();
        service.setHandlerResolver(handlerResolver);
        net.tedata.webservices.FETopUpWS port = service.getFETopUpWSSOAP();
        response = port.topUpForPhoneNumber(request);

        return response;

    }
}
