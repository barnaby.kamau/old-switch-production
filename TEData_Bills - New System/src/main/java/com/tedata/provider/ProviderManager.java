/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tedata.provider;

import static com.tedata.bills.Manager.logger;
import com.tedatal.entites.GetTopUpPackRequest;
import com.tedatal.entites.GetTopUpPackResponse;
import com.tedatal.entites.RSRequest;
import com.tedatal.entites.RSResponse;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import net.tedata.webservices.RechargePackagesFaultsMsg;
import net.tedata.webservices.TopUpForPhoneNumberFault;
import net.tedata.webservices.v1.tedatacommontypes.ESBGeneralExceptionType;
import net.tedata.webservices.v2.paymentmanagement.PaymentManagementTEDServiceExceptionMessage;

/**
 *
 * @author Antonios_knaguib
 */
public class ProviderManager {

    public RSResponse inquiry(RSRequest request) {
        RSResponse response = new RSResponse();
        try {
            if (request.request_id == null || request.user_name == null || request.password == null || request.telephone_code == null || request.Phone == null) {
                response.Code = -1;
                response.Message = "Invalid Request";
                return response;
            }
        } catch (Exception e) {
//            System.out.println(e);
            response.Code = -1;
            response.Message = "Invalid Request";
            return response;
        }
        response.request_id = request.request_id;
        try {
            TEData teData = new TEData();
            net.tedata.webservices.v2.paymentmanagement.PaymentManagementResponseType inquiryResponse;
//            logger.info("Before Inquiry");
            inquiryResponse = teData.inquiry(request.request_id, request.telephone_code, request.Phone);
//            logger.info("After Inquiry");
            logger.info(inquiryResponse.toString());

            if (inquiryResponse.getMessageBody().getPaymentManagementResponse().getInquiryInfo().isErrorOccured()) {

                int errorCode = inquiryResponse.getMessageBody().getPaymentManagementResponse().getInquiryInfo().getErrorCode();
                String errorMessage = inquiryResponse.getMessageBody().getPaymentManagementResponse().getInquiryInfo().getErrorMessage();
                response.Code = -5;
                response.Message = "Provider Error : " + errorCode + " [" + errorMessage + "].";

            } else {
                response.End_Date = inquiryResponse.getMessageBody().getPaymentManagementResponse().getInquiryInfo().getNewExpiryDateAfterRenewal();
                response.Customer_Name = inquiryResponse.getMessageBody().getPaymentManagementResponse().getInquiryInfo().getCustomerName();
//                int invoiceNumber = inquiryResponse.getMessageBody().getPaymentManagementResponse().getInquiryInfo().getCreatedInvoiceNumber();
                response.Total_Amount = inquiryResponse.getMessageBody().getPaymentManagementResponse().getInquiryInfo().getTotalDueForRenewal();
                response.Code = 200;
                response.Message = "Successful Inquiry.";

            }
            return response;
        } catch (PaymentManagementTEDServiceExceptionMessage errMsg) {
            response.Code = -5;
            response.Message = "Provider Error : " + errMsg.getFaultInfo().getFaultCode() + " [" + errMsg.getFaultInfo().getFaultMessage() + "].";
            return response;
        } catch (Exception e) {
            logger.error(e);
            response.Code = -3;
            response.Message = "General Error : " + e;
            return response;
        }
    }

    public RSResponse payment(RSRequest request) {
//        String Mobile_Number = "";
//        String submitbuttonName = "";
        RSResponse response = new RSResponse();
//        Validations
        try {
            if (request.Amount <= 0) {
                response.Code = -1;
                response.Message = "Invalid Amount must be greater than 0.0";
                return response;
            }
            if (request.request_id == null || request.user_name == null || request.password == null || request.telephone_code == null || request.Phone == null) {
                response.Code = -4;
                response.Message = "Invalid Request";
                return response;
            }
        } catch (Exception e) {
            logger.error(e);
            response.Code = -1;
            response.Message = "Invalid Request.";
            return response;
        }

        response.request_id = request.request_id;
        try {
            TEData teData = new TEData();
            net.tedata.webservices.v2.paymentmanagement.PaymentManagementResponseType paymentResponse;
            paymentResponse = teData.payment(request.request_id, request.telephone_code, request.Phone);
//            logger.info("Status Code = " + Integer.parseInt(paymentResponse.getMessageHeader().getOperationStatus().getStatusCode()));
            if (Integer.parseInt(paymentResponse.getMessageHeader().getOperationStatus().getStatusCode()) == 0) {
//            if (paymentResponse.getMessageBody().getPaymentManagementResponse().getInquiryInfo().isErrorOccured()) {
//                response.End_Date = paymentResponse.getMessageBody().getPaymentManagementResponse().getInquiryInfo().getNewExpiryDateAfterRenewal();
//                response.Customer_Name = paymentResponse.getMessageBody().getPaymentManagementResponse().getInquiryInfo().getCustomerName();
////                int invoiceNumber = inquiryResponse.getMessageBody().getPaymentManagementResponse().getInquiryInfo().getCreatedInvoiceNumber();
                response.Total_Amount = request.Amount;
                response.ReceiptNumber = paymentResponse.getMessageBody().getPaymentManagementResponse().getAddPaymentInfo().getRenewalResponse().getReceiptNumber();
                response.Code = 200;
                response.Message = "Successful Payment.";

            } else {

                int errorCode = paymentResponse.getMessageBody().getPaymentManagementResponse().getInquiryInfo().getErrorCode();
                String errorMessage = paymentResponse.getMessageBody().getPaymentManagementResponse().getInquiryInfo().getErrorMessage();
                response.Code = -5;
                response.Message = "Provider Error : " + errorCode + " [" + errorMessage + "].";
            }
            return response;

        } catch (PaymentManagementTEDServiceExceptionMessage errMsg) {
            if (String.valueOf(errMsg.getFaultInfo().getFaultCode()).equals("5067")) {
                response.Code = 200;
                response.Message = "Successful Payment.";
                return response;
            } else {
                response.Code = -5;
                response.Message = "Provider Error : " + errMsg.getFaultInfo().getFaultCode() + " [" + errMsg.getFaultInfo().getFaultMessage() + "].";
                return response;
            }

        } catch (javax.xml.ws.soap.SOAPFaultException faultException) {
            javax.xml.soap.SOAPFault fault = faultException.getFault();
            javax.xml.soap.Detail detail = fault.getDetail();
            Iterator detailEntries = detail.getDetailEntries();
            while (detailEntries.hasNext()) {
                ESBGeneralExceptionType exception = (ESBGeneralExceptionType) detailEntries.next();
                if (exception.getStatusCode().equals("3150")
                        || exception.getStatusCode().equals("3151")
                        || exception.getStatusCode().equals("3701")
                        || exception.getStatusCode().equals("3161")
                        || exception.getStatusCode().equals("4240")
                        || exception.getStatusCode().equals("5016")
                        || exception.getStatusCode().equals("-1")
                        || exception.getStatusCode().equals("0")
                        || exception.getStatusCode().equals("0002")) {
                    response.Code = -10;
                    response.Message = "Please retry. Provider Error : " + exception.getStatusMessage();
                    return response;
                } else {
                    response.Code = -4;
                    response.Message = "fault exception : " + exception.getStatusMessage();
                    return response;
                }

            }
            response.Code = -5;
            response.Message = "fault exception : " + fault.getFaultString();
            return response;
        } catch (Exception e) {
            if (e.getMessage().contains("Connection timed out")
                    || e.getMessage().contains("ESB:System exception - AddPaymentByPhoneNumber_RsFlow")
                    //                    || e.getMessage().contains("Connection refused")
                    || e.getMessage().contains("recv failed")) {
//                return payment(request);
                response.Code = -10;
                response.Message = "Please retry. Provider Error : " + e;
                return response;
            } else {
                response.Code = -10;
                response.Message = "General Error : " + e;
                return response;
            }
        }
    }

    public RSResponse topUpInquiry(RSRequest request) {
        RSResponse response = new RSResponse();
        try {
            if (request.Amount <= 0) {
                response.Code = -1;
                response.Message = "Invalid Amount must be greater than 0.0";
                return response;
            }
            if (request.request_id == null || request.user_name == null || request.password == null || request.telephone_code == null || request.Phone == null) {
                response.Code = -1;
                response.Message = "Invalid Request";
                return response;
            }
        } catch (Exception e) {
//            System.out.println(e);
            response.Code = -1;
            response.Message = "Invalid Request";
            return response;
        }
        response.request_id = request.request_id;
        try {
            TEData teData = new TEData();
            net.tedata.webservices.TopUpResult inquiryResponse;
            inquiryResponse = teData.TopUpInquiry(request.request_id, request.telephone_code, request.Phone, request.Amount);

            if (inquiryResponse.isErrorOccured()) {

                int errorCode = inquiryResponse.getErrorCode();
                String errorMessage = inquiryResponse.getErrorMessage();
                response.Code = -5;
                response.Message = "Provider Error : " + errorCode + " [" + errorMessage + "].";

            } else {
//                response.End_Date = inquiryResponse.getNewExpiryDateAfterRenewal();
                if (inquiryResponse.getCustomerName() == null) {
                    response.Customer_Name = inquiryResponse.getCustomerName();
                } else {
                    response.Customer_Name = "UNKNOWN";
                }
//                int invoiceNumber = inquiryResponse.getMessageBody().getPaymentManagementResponse().getInquiryInfo().getCreatedInvoiceNumber();
                response.Total_Amount = inquiryResponse.getTopUpAmount();
                response.Code = 200;
                response.Message = "Successful Inquiry.";

            }
            return response;
        } catch (TopUpForPhoneNumberFault errMsg) {
            response.Code = -5;
            response.Message = "Provider Error : " + errMsg.getFaultInfo().getStatusCode() + " [" + errMsg.getFaultInfo().getStatusMessage() + "].";
            return response;
        } catch (Exception e) {
            logger.error(e);
            response.Code = -3;
            response.Message = "General Error : " + e;
            return response;
        }
    }

    public RSResponse topUpPayment(RSRequest request) {
//        String Mobile_Number = "";
//        String submitbuttonName = "";
        RSResponse response = new RSResponse();
//        Validations
        try {
            if (request.Amount <= 0) {
                response.Code = -1;
                response.Message = "Invalid Amount must be greater than 0.0";
                return response;
            }
            if (request.request_id == null || request.user_name == null || request.password == null || request.telephone_code == null || request.Phone == null) {
                response.Code = -4;
                response.Message = "Invalid Request";
                return response;
            }
        } catch (Exception e) {
            logger.error(e);
            response.Code = -1;
            response.Message = "Invalid Request.";
            return response;
        }

        response.request_id = request.request_id;
        try {
            TEData teData = new TEData();
            net.tedata.webservices.TopUpResult paymentResponse;

            paymentResponse = teData.TopUpPayment(request.request_id, request.telephone_code, request.Phone, request.Amount);

            if (paymentResponse.isErrorOccured()) {

                int errorCode = paymentResponse.getErrorCode();
                String errorMessage = paymentResponse.getErrorMessage();
                response.Code = -5;
                response.Message = "Provider Error : " + errorCode + " [" + errorMessage + "].";

            } else {
//                response.End_Date = paymentResponse.getMessageBody().getPaymentManagementResponse().getInquiryInfo().getNewExpiryDateAfterRenewal();
                response.Customer_Name = paymentResponse.getCustomerName();
//                int invoiceNumber = inquiryResponse.getMessageBody().getPaymentManagementResponse().getInquiryInfo().getCreatedInvoiceNumber();
                response.Total_Amount = paymentResponse.getTopUpAmount();
                response.ReceiptNumber = paymentResponse.getCreatedReceiptNumber();
                response.Code = 200;
                response.Message = "Successful Payment.";

            }
            return response;

        } catch (TopUpForPhoneNumberFault errMsg) {
            response.Code = -5;
            response.Message = "Provider Error : " + errMsg.getFaultInfo().getStatusCode() + " [" + errMsg.getFaultInfo().getStatusMessage() + "].";
            return response;
        } catch (Exception e) {
             if (e.getMessage().contains("Connection timed out")
                    || e.getMessage().contains("ESB:System exception - AddPaymentByPhoneNumber_RsFlow")
                    //                    || e.getMessage().contains("Connection refused")
                    || e.getMessage().contains("recv failed")) {
//                return payment(request);
                response.Code = 200;
                response.Message = "Please retry. Provider Error : " + e;
                return response;
            } else {
                response.Code = 200;
                response.Message = "General Error : " + e;
                return response;
            }
           
        }
    }

    public GetTopUpPackResponse getTopUpPackages(GetTopUpPackRequest request) {
        GetTopUpPackResponse response = new GetTopUpPackResponse();
        try {
            if (request.serviceName == null || request.user_name == null || request.password == null) {
                response.Code = -1;
                response.Message = "Invalid Request";
                return response;
            }
        } catch (Exception e) {
//            System.out.println(e);
            response.Code = -1;
            response.Message = "Invalid Request";
            return response;
        }
        try {
            TEData teData = new TEData();
            net.tedata.webservices.GetTopUpsPackagesResponse getTopUpsPackagesResponse;
//            logger.info("Before Inquiry");
            getTopUpsPackagesResponse = teData.getTopUpPackages(request.serviceName);
//            logger.info("After Inquiry");
//            logger.info(getTopUpsPackagesResponse.toString());
            List<com.tedatal.entites.Package> packages = new ArrayList<com.tedatal.entites.Package>();
            for (net.tedata.webservices.Package p : getTopUpsPackagesResponse.getReturn()) {
                com.tedatal.entites.Package tePackage = new com.tedatal.entites.Package();
                tePackage.id = p.getId();
                tePackage.name = p.getName();
                tePackage.qouta = p.getQouta();
                tePackage.amount = p.getAmount();
                packages.add(tePackage);
            }
            response.Code = 200;
            response.Message = "Success";
            response.Packages = packages;
            return response;
        } catch (RechargePackagesFaultsMsg errMsg) {
            response.Code = -5;
            response.Message = "Provider Error : " + errMsg.getFaultInfo().getCode() + " [" + errMsg.getFaultInfo().getMessage() + "].";
            return response;
        } catch (Exception e) {
            response.Code = -3;
            response.Message = "General Error : " + e;
            return response;
        }
    }
}
