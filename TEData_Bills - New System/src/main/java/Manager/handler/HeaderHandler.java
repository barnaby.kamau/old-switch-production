/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Manager.handler;

/**
 *
 * @author masary
 */
import static com.tedata.bills.Manager.logger;
import java.io.ByteArrayOutputStream;
import java.util.Collections;
import java.util.Set;
import javax.xml.namespace.QName;
import javax.xml.soap.SOAPElement;
import javax.xml.soap.SOAPEnvelope;
import javax.xml.soap.SOAPHeader;
import javax.xml.soap.SOAPMessage;
import javax.xml.ws.handler.MessageContext;
import javax.xml.ws.handler.soap.SOAPHandler;
import javax.xml.ws.handler.soap.SOAPMessageContext;

/**
 *
 * @author www.javadb.com
 */
public class HeaderHandler implements SOAPHandler<SOAPMessageContext> {

    @Override
    public Set<QName> getHeaders() {
        return Collections.emptySet();
    }

    @Override
    public boolean handleMessage(SOAPMessageContext smc) {
        logger.info("IN handleMessage");
        Boolean outboundProperty = (Boolean) smc.get(MessageContext.MESSAGE_OUTBOUND_PROPERTY);
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        if (outboundProperty) {

            SOAPMessage message = smc.getMessage();

            try {

                SOAPEnvelope envelope = smc.getMessage().getSOAPPart().getEnvelope();
//                SOAPHeader header = smc.getMessage().getSOAPHeader();
                SOAPHeader header = message.getSOAPHeader();
                if (header == null) {
                    header = envelope.addHeader();
                }
//                envelope.getHeader().detachNode();
//                header = envelope.addHeader();

                SOAPElement security
                        = header.addChildElement("Security", "wsse", "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd");
                security.addAttribute(new QName("xmlns:wsu"), "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd");

                SOAPElement usernameToken
                        = security.addChildElement("UsernameToken", "wsse");
                usernameToken.addAttribute(new QName("xmlns:wsu"), "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd");

                SOAPElement username
                        = usernameToken.addChildElement("Username", "wsse");
                username.addTextNode("Momkn");

                SOAPElement password
                        = usernameToken.addChildElement("Password", "wsse");
                password.setAttribute("Type", "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-username-token-profile-1.0#PasswordText");
                password.addTextNode("M0mkN@ssBrK");

//                SOAPElement nonce
//                        = usernameToken.addChildElement("Nonce", "41zrrWzRxDr53whICMleag==");
//                password.setAttribute("EncodingType", "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-soap-message-security-1.0#Base64Binary");
                message.saveChanges();
                try {
                    message.writeTo(out);
                    String msg = out.toString();
                    logOutboundMsg(msg);

                } catch (Exception e) {

                    logger.error(e);

                }
                //Print out the outbound SOAP message to System.out
//                message.writeTo(System.out);
//                logger.info("Message : " + message);

            } catch (Exception e) {
                logger.error(e);
//                TEData_TCPIP.logger.error("Error in setting Username and password "+e);
            }

        } else {
            try {

                //This handler does nothing with the response from the Web Service so
                //we just print out the SOAP message.
                SOAPMessage message = smc.getMessage();
                message.writeTo(out);
                String msg = out.toString();
                logInboundMsg(msg);
//                System.out.println("");
//                logger.info("Message : " + message.toString());

            } catch (Exception ex) {
                logger.error(ex);
//                TEData_TCPIP.logger.error("Error in write message "+ex);
            }
        }

        return outboundProperty;

    }

    @Override
    public boolean handleFault(SOAPMessageContext context) {
        logger.info("IN Fault");
        SOAPMessage message = context.getMessage();
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        Boolean outboundProperty = (Boolean) context
                .get(MessageContext.MESSAGE_OUTBOUND_PROPERTY);
        try {
            message.writeTo(out);
            String msg = out.toString();
            if (outboundProperty) {
                logOutboundMsg(msg);
            } else {
                logInboundMsg(msg);
            }

        } catch (Exception e) {

            logger.error(e);

        }
        return true;
    }

    @Override
    public void close(MessageContext context) {

    }

    private void logOutboundMsg(String msg) {
        logger.info("Outbound message:\n" + msg);
    }

    private void logInboundMsg(String msg) {
        logger.info("Inbound message:\n" + msg);

    }

}
