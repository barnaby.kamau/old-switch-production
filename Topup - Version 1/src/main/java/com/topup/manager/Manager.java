/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.topup.manager;

/**
 *
 * @author Antonios_knaguib
 */
import com.topup.entites.GetBalanceResponse;
import com.topup.entites.RSRequest;
import com.topup.entites.RSResponse;
import com.topup.provider.Bee;
import com.topup.provider.Etisalat;
import com.topup.provider.Khadamaty;
import com.topup.provider.Orange;
import com.topup.provider.Provider;
import com.topup.provider.Tamam;
import com.topup.provider.Vodafone;
import com.topup.provider.Vodafone2;
import com.topup.provider.Vodafone3;
import com.topup.provider.Vodafone4;
import com.topup.provider.Wakty;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import static javax.ws.rs.core.MediaType.APPLICATION_JSON;
import javax.ws.rs.core.Response;
import static javax.ws.rs.core.Response.ok;
import org.apache.log4j.Logger;
import org.json.JSONException;

@Path("/")
@Produces(APPLICATION_JSON)
public class Manager {

    public static Logger logger = Logger.getLogger("R");
    private static volatile int VODAFONE_INSTANCE = 0;

    @POST
    public Response topup(RSRequest request) throws JSONException, UnsupportedEncodingException {
        RSResponse response = new RSResponse();
        logger.info("Topup Request : " + request);
        Provider provider = getProvider(request);
        response = provider.topup(request);
        response.providerID = request.providerID;
        logger.info("Topup Response : " + new String(response.toString().getBytes("UTF-8"), "Windows-1252"));
        return ok(response, APPLICATION_JSON).build();
    }

    @Path("checkTransaction")
    @POST
    public Response checkTransaction(RSRequest request) throws JSONException {
        RSResponse response = new RSResponse();
        logger.info("checkTransaction Request : " + request);
        Provider provider = new Tamam();
        response = provider.checkTransaction(request);
        logger.info("checkTransaction Response : " + response);
        return ok(response, APPLICATION_JSON).build();
    }

    @Path("getBalance")
    @POST
    public Response getBalance(@QueryParam("user_name") String user_name, @QueryParam("password") String password, @QueryParam("providerID") int providerID, @QueryParam("terminalId") String terminalId) {
        RSRequest request = new RSRequest();
        request.userName = user_name;
        request.password = password;
        request.providerID = providerID;
        request.terminalID = terminalId;
        logger.info("Check Transaction Request : " + request);
//        Provider_Manager provider = new Provider_Manager();
        Provider provider = getProvider(request);
        GetBalanceResponse response = provider.GetBalance(request);
        response.providerID = providerID;
        logger.info("Check Transaction Response : " + response);
        return ok(response, APPLICATION_JSON).build();
    }

    private Provider getProvider(RSRequest request) {
        if (request.providerID == 1) {
            return new Bee();
        } else if (request.providerID == 2) {
            int arr[] = {46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 6, 86, 87, 88, 89, 90, 91, 92, 93, 94, 95, 96, 97, 98, 99, 100, 101, 102, 103, 104, 105, 111, 112, 113, 114, 115, 116, 117, 139, 140, 141, 142, 143, 144, 145, 146, 147, 148, 149, 150, 151, 152, 153, 154, 155, 156, 157, 158, 159, 160, 161, 162, 163};
            if (contains(arr, request.serviceID)) {
//                return new Tamam();
                return new Etisalat();
            } else {
                return new Orange();
            }
        } else if (request.providerID == 3) {
            return new Wakty();
        } else if (request.providerID == 4) {
            return getVodafoneInstance();
        } else if (request.providerID == 5) {
            return new Khadamaty();
        } else {
            return new Bee();
        }
    }

    public boolean contains(final int[] arr, final int key) {
        return Arrays.stream(arr).anyMatch(i -> i == key);
    }

    private static volatile int pos = 0;//In order to ensure the correctness of pos, you can also use Integer plus synchronized, or use the atomic class AtomicInteger

    public static Provider getVodafoneInstance() {
        Provider instance = null;
        //The first step we need to create some server ip
        List<Provider> list = new ArrayList<>();
        list.add(new Vodafone());
        list.add(new Vodafone2());
        list.add(new Vodafone3());
        list.add(new Vodafone4());
        //The second step, poll to get the IP we want
        if (VODAFONE_INSTANCE >= list.size()) {
            VODAFONE_INSTANCE = 0;
        }
        instance = list.get(VODAFONE_INSTANCE);
        VODAFONE_INSTANCE++;
        return instance;

    }
//	public static void main(String[] args) {
//			System.out.println(getserverIP());	
//	}
}
