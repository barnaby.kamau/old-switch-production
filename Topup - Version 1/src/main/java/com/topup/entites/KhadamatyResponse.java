/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.topup.entites;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import java.util.List;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;
//import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 *
 * @author Antonios_knaguib
 */
@JsonIgnoreProperties
public class KhadamatyResponse implements Serializable {

    public int ServiceId;
    public String KHDTransId;
    public String SenderTransId;
    public String Consumer;
    public String PaymentStatus;
    public double Amount;
    public double NetAmount;
    public List<InqField> Fields;
    public double Fees;
    public int ServiceCount;
    public double Balance;
    public int StatusCode;
    public String StatusDescription;
    public Reciept Reciept;

        @Override
        public String toString

        
            () {
        return ToStringBuilder.reflectionToString(this,
                    ToStringStyle.MULTI_LINE_STYLE);
        }

    }
