/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.topup.provider;

import com.topup.entites.GetBalanceResponse;
import com.topup.entites.RSRequest;
import com.topup.entites.RSResponse;

/**
 *
 * @author Tony
 */
public interface Provider {

    public RSResponse topup(RSRequest request);
    public RSResponse checkTransaction(RSRequest request);
    public GetBalanceResponse GetBalance(RSRequest request);
}
