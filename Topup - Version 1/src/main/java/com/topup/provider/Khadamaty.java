/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.topup.provider;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.config.ClientConfig;
import com.sun.jersey.api.client.config.DefaultClientConfig;
import com.topup.entites.Auth;
import com.topup.entites.GetBalanceResponse;
import com.topup.entites.InqField;
import com.topup.entites.KhadamatyResponse;
import com.topup.entites.KhadamtyRequest;
import com.topup.entites.RSRequest;
import com.topup.entites.RSResponse;
import static com.topup.manager.Manager.logger;
import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.charset.Charset;
import java.nio.charset.CharsetEncoder;
import java.util.ArrayList;
import java.util.List;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.UriBuilder;
import org.apache.commons.lang.StringUtils;

/**
 *
 * @author Tony
 */
public class Khadamaty implements Provider {

    int SERVICEID = 225;
    String AGENTCODE = "8364";
    int APPID = 27;
    String URL = "https://gateway.khadamaty.com.eg/V3/api/Payment";

    @Override
    public RSResponse topup(RSRequest request) {
        RSResponse response = new RSResponse();
        response.requestID = request.requestID;

        try {
            logger.info("[Khadamaty] RequestID : " + request.requestID + " Provider Request Creation started");
            KhadamtyRequest providerRequest = getProviderRequest(request);
            logger.info("[Khadamaty] RequestID : " + request.requestID + " Provider Request : " + providerRequest.toString().replace(request.password, StringUtils.repeat("*", request.password.length())));
            logger.info("[Khadamaty] RequestID : " + request.requestID + " Provider Request Creation ended");
            logger.info("[Khadamaty] RequestID : " + request.requestID + " Provider Request Sending started");
            ClientConfig config = new DefaultClientConfig();
            Client client = Client.create(config);
            WebResource webResource = client.resource(UriBuilder.fromUri(URL).build());
            KhadamatyResponse providerResponse = webResource.type(MediaType.APPLICATION_JSON).post(KhadamatyResponse.class, providerRequest);
            logger.info("[Khadamaty] RequestID : " + request.requestID + " Provider Response : " + new String(providerResponse.toString().getBytes("UTF-8"), "Windows-1252"));
            logger.info("[Khadamaty] RequestID : " + request.requestID + " Provider Request Sending ended");
            logger.info("[Khadamaty] RequestID : " + request.requestID + " Provider Response Unmarshal ended");
            if (providerResponse.StatusCode == 1 || providerResponse.StatusCode == 3 || providerResponse.StatusCode == 4) {
                response.code = 200;
                response.message = "Successful Payment.";
                response.providerTransactionID = providerResponse.KHDTransId;
            } else if (providerResponse.StatusCode == 0) {
                response.code = -300;
                response.message = "Pending Transaction";
            } else {
                response.code = -2;
//                response.message = "Provider Error : " + new String(providerResponse.StatusDescription.getBytes(), "UTF-8");
                response.message = "Provider Error : " + providerResponse.StatusDescription;
            }

        } catch (Exception e) {
            logger.error(e);
            response.code = -300;
            response.message = "General Error : " + e;
        }

        return response;
    }

    @Override
    public RSResponse checkTransaction(RSRequest request) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public GetBalanceResponse GetBalance(RSRequest request) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    private KhadamtyRequest getProviderRequest(RSRequest request) {
        KhadamtyRequest providerRequest = new KhadamtyRequest();
        providerRequest.SenderTrans = request.requestID;
        providerRequest.ServiceId = SERVICEID;
        providerRequest.amount = request.amount;
        Auth auth = new Auth();
        auth.AgentCode = AGENTCODE;
        auth.AppId = APPID;
        auth.UserId = request.userName;
        auth.Password = request.password;
        providerRequest.Auth = auth;
        List<InqField> fields = new ArrayList<>();
        InqField field = new InqField();
        field.SFId = 971;
        field.Label = "رقم تليفون العميل";
        field.Value = request.phone;
        fields.add(field);
        providerRequest.Fields = fields;
        return providerRequest;
    }
}
