/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.topup.provider;

import com.topup.entites.GetBalanceResponse;
import com.topup.entites.RSRequest;
import com.topup.entites.RSResponse;
import static com.topup.manager.Manager.logger;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.math.BigDecimal;
import java.net.URL;
import java.net.URLConnection;
import java.security.cert.X509Certificate;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import org.apache.commons.lang.StringUtils;
import tamam.dtos.CheckTransactionResponse;
import tamam.dtos.TopupResponse;

/**
 *
 * @author Tony
 */
public class Tamam implements Provider {

    private static final String TERMINALID = "UA007819";
//    private static final String EAN = "4260469316524";
    private static final String CURRENCY = "818";
    private static final String LANGUAGE = "ENG";
    private static final String CHARSPERLINE = "40";
    private static final String HOST = "https://precision.epayworldwide.com/up-interface";
    private static HashMap<Integer, String> TRANSACTION_STATUS_CODES = new HashMap<>();
    private static HashMap<Integer, String> PROVIDER_STATUS_CODES = new HashMap<>();


    public RSResponse topup(RSRequest request) {
        RSResponse response = new RSResponse();
        response.requestID = request.requestID;
        try {
            logger.info("[Tamam] Provider Request Creation started");
            StringBuffer provider_Request = getTopupRequest(request);
            logger.info("[Tamam] Provider Request : " + provider_Request.toString().replace(request.password, StringUtils.repeat("*", request.password.length())));
            logger.info("[Tamam] Provider Request Creation ended");
            logger.info("[Tamam] Provider Request Sending started");
            String provider_Response = send(provider_Request.toString());
            logger.info("[Tamam] Provider Response : " + provider_Response);
            logger.info("[Tamam] Provider Request Sending ended");
            logger.info("[Tamam] Provider Response Unmarshal ended");
            GenericContentHandler handler = new GenericContentHandler();
            TopupResponse ProviderResponse = handler.unMarshal(provider_Response.toString(), TopupResponse.class);
            logger.info("[Tamam] Provider Response Unmarshal ended");
            if (ProviderResponse.getStatusCode() == 0) {
                response.code = 200;
                response.message = ProviderResponse.getStatusMessage();
                response.providerTransactionID = ProviderResponse.getProvidertrxId();
            } else {
                logger.error("[Tamam] Provider Error : " + ProviderResponse.getStatusMessage());
                response.code = -2;
                response.message = "Provider Error : " + ProviderResponse.getStatusMessage();
            }
        } catch (java.net.SocketTimeoutException e) {
            logger.error(this.getClass().getSimpleName() + " - " + e);
            response.code = -300;
            response.message = "Timeout in Connection to provider : " + e;
            response.providerTransactionID = "";
        } catch (Exception e) {
            logger.error(e);
            response.code = -3;
            response.message = "General Error : " + e;
        }

        return response;
    }

    private String send(String xml) throws Exception {
        URL url = new URL(HOST);
        URLConnection conn;
        if (HOST.contains("https")) {
            // Create a trust manager that does not validate certificate chains
            TrustManager[] trustAllCerts = new TrustManager[]{new X509TrustManager() {
                public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                    return null;
                }

                public void checkClientTrusted(X509Certificate[] certs, String authType) {
                }

                public void checkServerTrusted(X509Certificate[] certs, String authType) {
                }
            }};

            // Install the all-trusting trust manager
            final SSLContext sc = SSLContext.getInstance("SSL");
            sc.init(null, trustAllCerts, new java.security.SecureRandom());
            HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
            // Create all-trusting host name verifier
            HostnameVerifier allHostsValid = new HostnameVerifier() {
                public boolean verify(String hostname, SSLSession session) {
                    return true;
                }
            };
            // Install the all-trusting host verifier
            HttpsURLConnection.setDefaultHostnameVerifier(allHostsValid);

            conn = (HttpsURLConnection) url.openConnection();
            conn.setConnectTimeout(20000); //set timeout to 5 seconds
        } else {
            conn = url.openConnection();
        }
        conn.setRequestProperty("Content-Type", "text/xml");
        conn.setDoOutput(true);

        BufferedOutputStream bos = new BufferedOutputStream(conn.getOutputStream());
        bos.write(xml.getBytes("UTF-8"));
        bos.close();

        BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream()));
        String inputLine;
        StringBuffer response = new StringBuffer();

        while ((inputLine = in.readLine()) != null) {
            response.append(inputLine);
        }
        in.close();

        return response.toString();
    }

    private StringBuffer getTopupRequest(RSRequest request) {
        StringBuffer provider_Request = new StringBuffer("<?xml version=\"1.0\" encoding=\"UTF - 8\" ?>");
        provider_Request.append("<REQUEST TYPE=\"TOPUP\">");
        provider_Request.append("<USERNAME>").append(request.userName).append("</USERNAME>");
        provider_Request.append("<PASSWORD>").append(request.password).append("</PASSWORD>");
        provider_Request.append("<TERMINALID>" + TERMINALID + "</TERMINALID>");
        provider_Request.append("<LOCALDATETIME>" + getCurrentTimeStamp() + "</LOCALDATETIME>");
        provider_Request.append("<TXID>").append(request.requestID).append("</TXID>");
        provider_Request.append("<CARD>");
        provider_Request.append("<EAN>").append(getEAN(request)).append("</EAN>");
        provider_Request.append("</CARD>");
        provider_Request.append("<PHONE>").append(request.phone).append("</PHONE>");
        provider_Request.append("<CURRENCY>").append(CURRENCY).append("</CURRENCY>");
        provider_Request.append("<AMOUNT>").append((int) Math.floor(new BigDecimal(String.valueOf(request.amount)).multiply(new BigDecimal("100")).doubleValue())).append("</AMOUNT>");
//        provider_Request.append("<AMOUNT>").append((int) Math.floor(request.amount * 100)).append("</AMOUNT>");
        provider_Request.append("<RECEIPT>");
        provider_Request.append("<LANGUAGE>").append(LANGUAGE).append("</LANGUAGE>");
        provider_Request.append("<CHARSPERLINE>").append(CHARSPERLINE).append("</CHARSPERLINE>");
        provider_Request.append("<TYPE>").append("FULLTEXT").append("</TYPE>");
        provider_Request.append("</RECEIPT>");
        provider_Request.append("</REQUEST>");
        return provider_Request;

    }

    public static String getCurrentTimeStamp() {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date now = new Date();
        String strDate = sdf.format(now);
        return strDate;
    }

    private static String getEAN(RSRequest request) {
        String EAN = "";
        if (request.serviceID == 6) { //Etisalat
            if (request.amount >= 3 && request.amount <= 200) {
                EAN = "8886666666065";
            }

        } else if (request.serviceID == 8) {//Orange
            if (request.amount >= 5 && request.amount <= 620) {
                EAN = "4260484192905";
            }
        } else if (request.serviceID == 542) { //Vodafone
            EAN = "4251404540524";
        } else if (request.serviceID == 86) { //MegaX  3.5
            EAN = "4251755622153";
        } else if (request.serviceID == 87) { //MegaX  5
            EAN = "4251755622153";
        } else if (request.serviceID == 88) { //MegaX  7
            EAN = "4251755622153";
        } else if (request.serviceID == 89) { //MegaX 10
            EAN = "4251755622153";
        } else if (request.serviceID == 90) { //MegaX  15
            EAN = "4251755622153";
        } else if (request.serviceID == 91) { //Social Media 3.5
            EAN = "4251755622160";
        } else if (request.serviceID == 92) { //Social Media 5
            EAN = "4251755622160";
        } else if (request.serviceID == 93) { //Social Media 7
            EAN = "4251755622160";
        } else if (request.serviceID == 94) { //Social Media 10
            EAN = "4251755622160";
        } else if (request.serviceID == 95) { //Social Media 15
            EAN = "4251755622160";
        } else if (request.serviceID == 96) { //Video 3.5
            EAN = "4251755622177";
        } else if (request.serviceID == 97) { //Video 5
            EAN = "4251755622177";
        } else if (request.serviceID == 98) { //Video 7
            EAN = "4251755622177";
        } else if (request.serviceID == 99) { //Video 10
            EAN = "4251755622177";
        } else if (request.serviceID == 100) { //Video 15
            EAN = "4251755622177";
        } else if (request.serviceID == 46) { //Tesla1 Topup 5
            EAN = "8886666660165";
        } else if (request.serviceID == 47) { //Tesla1 Topup 7
            EAN = "8886666660265";
        } else if (request.serviceID == 48) { //Tesla1 Topup 10
            EAN = "8886666660365";
        } else if (request.serviceID == 49) { //Tesla1 Topup 15
            EAN = "8886666660465";
        } else if (request.serviceID == 50) { //Tesla1 Topup 25
            EAN = "8886666660565";
        } else if (request.serviceID == 51) { //Tesla2 Topup 5
            EAN = "8886666660665";
        } else if (request.serviceID == 52) { //Tesla2 Topup 7
            EAN = "8886666660765";
        } else if (request.serviceID == 53) { //Tesla2 Topup 10
            EAN = "8886666660865";
        } else if (request.serviceID == 54) { //Tesla2 Topup 15
            EAN = "8886666660965";
        } else if (request.serviceID == 55) { //Tesla2 Topup 25
            EAN = "8886666661065";
        } else if (request.serviceID == 101) { //اقوى كارت دقايق
            EAN = "4251755629022";
        } else if (request.serviceID == 102) { //اقوى كارت مكسات
            EAN = "4251755629039";
        } else if (request.serviceID == 103) { //اقوى كارت ميجا
            EAN = "4251755622153";
        } else if (request.serviceID == 104) { //اقوى كارت سوشيال ميديا
            EAN = "4251755622160";
        } else if (request.serviceID == 105) { //اقوى كارت فيديو
            EAN = "4251755622177";
        } else if (request.serviceID == 118
                || request.serviceID == 119
                || request.serviceID == 120
                || request.serviceID == 121
                || request.serviceID == 122
                || request.serviceID == 132
                || request.serviceID == 133
                || request.serviceID == 134) { //احسن ناس ميجا
            EAN = "4251755652280";
        } else if (request.serviceID == 139) { //Hekaya 25
            EAN = "4251755670482";
        } else if (request.serviceID == 140
                || request.serviceID == 141
                || request.serviceID == 142
                || request.serviceID == 143) { //Hekaya 35
            EAN = "4251755670499";
        } else if (request.serviceID == 144
                || request.serviceID == 145
                || request.serviceID == 146
                || request.serviceID == 147) { //Hekaya 50
            EAN = "4251755670505";
        } else if (request.serviceID == 148
                || request.serviceID == 149
                || request.serviceID == 150
                || request.serviceID == 151) { //Hekaya 65
            EAN = "4251755670512";
        } else if (request.serviceID == 152
                || request.serviceID == 153
                || request.serviceID == 154
                || request.serviceID == 155) { //Hekaya 100
            EAN = "4251755670529";
        } else if (request.serviceID == 156
                || request.serviceID == 157
                || request.serviceID == 158
                || request.serviceID == 159) { //Hekaya 150
            EAN = "4251755670536";
        } else if (request.serviceID == 160
                || request.serviceID == 161
                || request.serviceID == 162
                || request.serviceID == 163) { //Hekaya 200
            EAN = "4251755670543";
        } else {
        }
        return EAN;
    }

  @Override
    public RSResponse checkTransaction(RSRequest request) {
         RSResponse response = new RSResponse();
        response.requestID = request.requestID;
        try {
            logger.info("[Tamam] Provider Request Creation started");
            StringBuffer provider_Request = getCheckTransactionRequest(request);
            logger.info("[Tamam] Provider Request : " + provider_Request.toString().replace(request.password, StringUtils.repeat("*", request.password.length())));
            logger.info("[Tamam] Provider Request Creation ended");
            logger.info("[Tamam] Provider Request Sending started");
            String provider_Response = send(provider_Request.toString());
            logger.info("[Tamam] Provider Response : " + provider_Response);
            logger.info("[Tamam] Provider Request Sending ended");
            logger.info("[Tamam] Provider Response Unmarshal ended");
            GenericContentHandler handler = new GenericContentHandler();
            CheckTransactionResponse ProviderResponse = handler.unMarshal(provider_Response.toString(), CheckTransactionResponse.class);
            logger.info("[Tamam] Provider Response Unmarshal ended");
            if (ProviderResponse.getStatusCode() == 0) {
                if (ProviderResponse.getTransactions().get(0).getStatusCode() == 0) {
                
                response.code = 200;
                response.message = ProviderResponse.getStatusMessage();
                response.providerTransactionID = ProviderResponse.getTransactions().get(0).getAid();
                }else{
                response.code = -1;
                response.message = "Provider Error : " + ProviderResponse.getTransactions().get(0).getStatusMessage();
                }
            } else {
                logger.error("[Tamam] Provider Error : " + ProviderResponse.getStatusMessage());
                response.code = -1;
                response.message = "Provider Error : " + ProviderResponse.getStatusMessage();
            }
        } catch (java.net.SocketTimeoutException e) {
            logger.error(this.getClass().getSimpleName() + " - " + e);
            response.code = -300;
            response.message = "Timeout in Connection to provider : " + e;
            response.providerTransactionID = "";
        } catch (Exception e) {
            logger.error(e);
            response.code = -3;
            response.message = "General Error : " + e;
        }

        return response;
    }

    @Override
    public GetBalanceResponse GetBalance(RSRequest request) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    private boolean isValidAmount(RSRequest request) {
        boolean isValid = false;
        if (request.serviceID == 6) { //Etisalat
            double clearNumber = (request.amount * 10);
            double decimal = Math.floor(clearNumber);
            double fractional = clearNumber - decimal;
            if (fractional == 0 && request.amount >= 3 && request.amount <= 200) {
                isValid = true;
            }
        } else if (request.serviceID == 8) {
//         	 if (request.amount == 5 || request.amount == 6 || request.amount == 7 || request.amount == 8
//    					|| request.amount == 9 || request.amount == 10 || request.amount == 11 || request.amount == 12
//                        || request.amount == 13 || request.amount == 14 || request.amount == 15 || request.amount == 16
//                        || request.amount == 17 || request.amount == 18 || request.amount == 19 || request.amount == 20
//                        || request.amount == 22 || request.amount == 25 || request.amount == 26 || request.amount == 29
//                        || request.amount == 30 || request.amount == 31 || request.amount == 40 || request.amount == 41
//                		|| request.amount == 45 || request.amount == 46 || request.amount == 50 || request.amount == 51
//        				|| request.amount == 52 || request.amount == 55 || request.amount == 56 || request.amount == 58
//        				|| request.amount == 59 || request.amount == 60 || request.amount == 61 || request.amount == 62
//    					|| request.amount == 64 || request.amount == 65 || request.amount == 66 || request.amount == 70
//    					|| request.amount == 73 || request.amount == 74 || request.amount == 75 || request.amount == 76
//    					|| request.amount == 80 || request.amount == 90 || request.amount == 91 || request.amount == 100
//                        || request.amount == 101 || request.amount == 110 || request.amount == 128 || request.amount == 131
//                        || request.amount == 140 || request.amount == 145 || request.amount == 146 || request.amount == 147
//                        || request.amount == 148 || request.amount == 149 || request.amount == 150 || request.amount == 151
//                        || request.amount == 152 || request.amount == 155 || request.amount == 183 || request.amount == 200
//                        || request.amount == 201 || request.amount == 220 || request.amount == 250 || request.amount == 255
//                        || request.amount == 256 || request.amount == 262 || request.amount == 279 || request.amount == 291
//                        || request.amount == 292 || request.amount == 293 || request.amount == 296 || request.amount == 297
//                        || request.amount == 300 || request.amount == 301 || request.amount == 302 || request.amount == 303
//                        || request.amount == 304 || request.amount == 310 || request.amount == 400 || request.amount == 500
//                        || request.amount == 524 || request.amount == 558 || request.amount == 582 || request.amount == 593
//                        || request.amount == 601 || request.amount == 603 || request.amount == 606 || request.amount == 608
//                        || request.amount == 620) {
            double clearNumber = request.amount;
            double decimal = Math.floor(clearNumber);
            double fractional = clearNumber - decimal;
            if (fractional == 0 && request.amount >= 5 && request.amount <= 620) {
                isValid = true;
            }

        } else if (request.serviceID == 542) {
            if ((request.amount % 1) == 0 && request.amount >= 5 && request.amount <= 500) {
                isValid = true;
            }
        }

        return isValid;
    }
private StringBuffer getCheckTransactionRequest(RSRequest request) throws ParseException {
        StringBuffer provider_Request = new StringBuffer("<?xml version=\"1.0\" encoding=\"ISO-8859-1\" ?>");
        provider_Request.append("<REQUEST TYPE=\"TXLIST\">");
        provider_Request.append("<USERNAME>").append(request.userName).append("</USERNAME>");
        provider_Request.append("<PASSWORD>").append(request.password).append("</PASSWORD>");
        provider_Request.append("<TERMINALID>" + TERMINALID + "</TERMINALID>");
        provider_Request.append("<LOCALDATETIME>" + getCurrentTimeStamp() + "</LOCALDATETIME>");
        provider_Request.append("<LISTOPTIONS>");
        provider_Request.append("<FROM>").append(getOneHourBack(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(request.requestDate))).append("</FROM>");
        provider_Request.append("<UNTIL>").append(getOneHourFront(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(request.requestDate))).append("</UNTIL>");
        provider_Request.append("<TXID>").append(request.requestID).append("</TXID>");
        provider_Request.append("<TRANSACTIONFILTER>SALE</TRANSACTIONFILTER>");
        provider_Request.append("</LISTOPTIONS>");
        provider_Request.append("</REQUEST>");
        return provider_Request;

    }
 private String getOneHourBack(Date now) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Calendar cal1 = Calendar.getInstance();
        cal1.setTime(now);
        cal1.add(Calendar.HOUR, -1);
        Date oneHourBack = cal1.getTime();
        return sdf.format(oneHourBack);

    }
    private String getOneHourFront(Date now) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Calendar cal1 = Calendar.getInstance();
        cal1.setTime(now);
        cal1.add(Calendar.HOUR, 1);
        Date oneHourFront = cal1.getTime();
        return sdf.format(oneHourFront);

    }

}
