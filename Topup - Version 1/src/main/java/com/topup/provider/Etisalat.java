/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.topup.provider;

import vodafone.dtos.TopupResponse;
import com.topup.entites.GetBalanceResponse;
import com.topup.entites.RSRequest;
import com.topup.entites.RSResponse;
import static com.topup.manager.Manager.logger;
import com.topup.utils.Util;
import etisalat.dtos.EtisalatTopupResponse;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.security.cert.X509Certificate;
import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import org.apache.commons.lang.StringUtils;

/**
 *
 * @author Tony
 */
public class Etisalat implements Provider {

    private static final String HOST = "http://10.78.10.49:6060/telepin";

    public RSResponse topup(RSRequest request) {
        RSResponse response = new RSResponse();
        response.requestID = request.requestID;

        try {
            logger.info("[Etisalat] Provider Request Creation started");

            StringBuffer provider_Request = getProviderRequest(request);
            logger.info("[Etisalat] Provider Request : " + provider_Request.toString().replace(request.password, StringUtils.repeat("*", request.password.length())));
            logger.info("[Etisalat] Provider Request Creation ended");
            logger.info("[Etisalat] Provider Request Sending started");
            logger.info("[Etisalat] Host creation started");
            String fullURL = getFullURL(request).toString();
            logger.info("[Etisalat] Host : " + fullURL.replace(request.password, StringUtils.repeat("*", request.password.length())));
            logger.info("[Etisalat] Host Creation ended");
            String provider_Response = send(fullURL, provider_Request.toString());
//            String provider_Response = sendTest(fullURL, provider_Request.toString());
            logger.info("[Etisalat] Provider Response : " + provider_Response);
            logger.info("[Etisalat] Provider Request Sending ended");
            logger.info("[Etisalat] Provider Response Unmarshal ended");
            GenericContentHandler handler = new GenericContentHandler();
            EtisalatTopupResponse ProviderResponse = handler.unMarshal(provider_Response, EtisalatTopupResponse.class);
            logger.info("[Etisalat] Provider Response Unmarshal ended");
            if (ProviderResponse.getResult() == 0) {

                response.code = 200;
                response.message = ProviderResponse.getMessage();
                response.providerTransactionID = ProviderResponse.getProviderTransactionID();
            } else {
                logger.error("[Etisalat] Provider Error : " + ProviderResponse.getMessage());
                response.code = ProviderResponse.getResult();
                response.message = "[Etisalat] Provider Error : " + ProviderResponse.getMessage();

            }

        } catch (Exception e) {
            logger.error(e);
            response.code = -3;
            response.message = "General Error : " + e;
        }

        return response;
    }

    private String send(String fullURL, String xml) throws Exception {
        URL url = new URL(fullURL);
        URLConnection conn;
        if (HOST.contains("https")) {
            // Create a trust manager that does not validate certificate chains
            TrustManager[] trustAllCerts = new TrustManager[]{new X509TrustManager() {
                public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                    return null;
                }

                public void checkClientTrusted(X509Certificate[] certs, String authType) {
                }

                public void checkServerTrusted(X509Certificate[] certs, String authType) {
                }
            }};

            // Install the all-trusting trust manager
            final SSLContext sc = SSLContext.getInstance("SSL");
            sc.init(null, trustAllCerts, new java.security.SecureRandom());
            HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
            // Create all-trusting host name verifier
            HostnameVerifier allHostsValid = new HostnameVerifier() {
                public boolean verify(String hostname, SSLSession session) {
                    return true;
                }
            };
            // Install the all-trusting host verifier
            HttpsURLConnection.setDefaultHostnameVerifier(allHostsValid);

            conn = (HttpsURLConnection) url.openConnection();
        } else {
            conn = url.openConnection();
        }
        conn.setRequestProperty("Content-Type", "text/xml");
        conn.setRequestProperty("Accept-Language", "en-us");
        conn.setDoOutput(true);

        BufferedOutputStream bos = new BufferedOutputStream(conn.getOutputStream());
        bos.write(xml.getBytes("UTF-8"));
        bos.close();

        BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream()));
        String inputLine;
        StringBuffer response = new StringBuffer();

        while ((inputLine = in.readLine()) != null) {
            response.append(inputLine);
        }
        in.close();

        return response.toString();
    }

    private String sendTest(String fullURL, String xml) throws Exception {

        StringBuffer response = new StringBuffer();

        response.append("<?xml version=\"1.0\" ?>\n"
                + "<TCSReply>\n"
                + "    <Result>0</Result>\n"
                + "    <Message>??? ??????? ????? ??? ??????? 3302580198 </Message>\n"
                + "    <param1>3302580198</param1>\n"
                + "    <param2>10.0</param2>\n"
                + "    <param3>10.0</param3>\n"
                + "    <param8>Fri May 17 14:00:00 EET 2024</param8>\n"
                + "    <param9> </param9>\n"
                + "    <param11>3303235062</param11>\n"
                + "    <param14>464674.69</param14>\n"
                + "    <param18>14.28571</param18>\n"
                + "    <param19>4.28571</param19>\n"
                + "    <param20>0.00000</param20>\n"
                + "    <param21>0.00000</param21>\n"
                + "    <param22>0.00000</param22>\n"
                + "    <param23>4.28571</param23>\n"
                + "    <param24>0.00000</param24>\n"
                + "    <param25>14.28571</param25>\n"
                + "    <param26>0.00000</param26>\n"
                + "</TCSReply>");

        return response.toString();
    }

    private StringBuffer getProviderRequest(RSRequest request) {
        Util util = new Util();
        StringBuffer provider_Request = new StringBuffer("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
        provider_Request.append("<TCSRequest>");
        provider_Request.append("<TCSRequest />");
        provider_Request.append("<UserName>").append(request.userName).append("</UserName>");
        provider_Request.append("<Password>").append(request.password).append("</Password>");
        provider_Request.append("<Function name=\"TOPUP\">");
        provider_Request.append("<param1>").append(request.phone).append("</param1>");
        provider_Request.append("<param2>").append(request.amount).append("</param2>");
        provider_Request.append("<param3>").append(request.phone).append("</param3>");
        provider_Request.append("<param4>").append(util.getParam(request.serviceID)).append("</param4>");
        provider_Request.append("</Function>");
        provider_Request.append("<CheckOnly>false</CheckOnly>");
        provider_Request.append("</TCSRequest>");
        return provider_Request;

    }

    private StringBuffer getFullURL(RSRequest request) {
        StringBuffer provider_Request = new StringBuffer(HOST);
//        provider_Request.append("?");
//        provider_Request.append("LOGIN=").append(request.userName).append("&");
//        provider_Request.append("PASSWORD=").append(request.password).append("&");
//        provider_Request.append("REQUEST_GATEWAY_CODE=").append(REQUEST_GATEWAY_CODE).append("&");
//        provider_Request.append("REQUEST_GATEWAY_TYPE=").append(REQUEST_GATEWAY_TYPE).append("&");
//        provider_Request.append("SERVICE_PORT=").append(SERVICE_PORT).append("&");
//        provider_Request.append("SOURCE_TYPE=").append(SOURCE_TYPE);
        return provider_Request;

    }

    @Override
    public RSResponse checkTransaction(RSRequest request) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public GetBalanceResponse GetBalance(RSRequest request) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
