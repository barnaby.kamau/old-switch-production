/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.topup.provider;

import Bee.dtos.Response;
import Bee.dtos.Service;
import com.topup.entites.GetBalanceResponse;
import com.topup.entites.RSRequest;
import com.topup.entites.RSResponse;
import static com.topup.manager.Manager.logger;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.math.RoundingMode;
import java.net.URL;
import java.net.URLConnection;
import java.security.cert.X509Certificate;
import java.text.DecimalFormat;
import java.util.Arrays;
import java.util.HashMap;
import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.StringUtils;

/**
 *
 * @author Tony
 */
public class Bee implements Provider {

    //    private static final String HOST = "https://staging.bee.com.eg:7777/xmlgw/action";
    private static final String HOST = "https://merchant.bee.com.eg:7443/xmlgw/action";
    private static final String LOCALE_ID = "en";
    private static String SERVICE_VERSION = "1";
    private static Response serviceListResponse = new Response();
    private static HashMap<Integer, String> TRANSACTION_STATUS_CODES = new HashMap<>();
    private static HashMap<Integer, String> PROVIDER_STATUS_CODES = new HashMap<>();

    static {
        TRANSACTION_STATUS_CODES.put(0, "Initialized");
        TRANSACTION_STATUS_CODES.put(1, "Under process");
        TRANSACTION_STATUS_CODES.put(2, "Success");
        TRANSACTION_STATUS_CODES.put(3, "Error");
        TRANSACTION_STATUS_CODES.put(4, "Canceled");
        TRANSACTION_STATUS_CODES.put(8, "Deposit error");
        TRANSACTION_STATUS_CODES.put(20007, "Data not found");
        PROVIDER_STATUS_CODES.put(0, "Successful");
        PROVIDER_STATUS_CODES.put(11000, "Authentication failed. Incorrect login or password");
        PROVIDER_STATUS_CODES.put(11001, "Service list cannot be updated");
        PROVIDER_STATUS_CODES.put(11002, "Transaction not accepted");
        PROVIDER_STATUS_CODES.put(11003, "Check transaction status is failed");
        PROVIDER_STATUS_CODES.put(20000, "General error");
        PROVIDER_STATUS_CODES.put(20001, "Request action version error");
        PROVIDER_STATUS_CODES.put(20002, "Service not allowed");
        PROVIDER_STATUS_CODES.put(20003, "Incomplete request");
        PROVIDER_STATUS_CODES.put(20004, "Wrong action");
        PROVIDER_STATUS_CODES.put(20005, "Incorrect XML request");
        PROVIDER_STATUS_CODES.put(20006, "No permission");
        PROVIDER_STATUS_CODES.put(20007, "Data not found");
        PROVIDER_STATUS_CODES.put(20008, "Duplicate transaction");
        PROVIDER_STATUS_CODES.put(20009, "External system must update service list");
        PROVIDER_STATUS_CODES.put(20010, "Deposit error, insufficient deposit to perform operation");
    }

    public RSResponse topup(RSRequest request) {
        RSResponse response = new RSResponse();
        response.requestID = request.requestID;

        try {
            logger.info("[Bee] Provider Request Creation started");
            double totalAmount = request.amount;
            try {
                totalAmount = getTotalAmount(request.serviceID, request.amount);
                request.amount = getAmount(request.serviceID, request.amount);
            } catch (Exception e) {
                updateServiceList(request);
                totalAmount = getTotalAmount(request.serviceID, request.amount);
                request.amount = getAmount(request.serviceID, request.amount);
            }
            if (!isValidAmount(request.serviceID, request.amount)) {
                response.code = -5;
                response.message = "Invalid Amount!";
            }
            StringBuffer provider_Request = getProviderRequest(request.requestID, request.serviceID, request.amount, totalAmount, request.phone, request.userName, request.password, request.terminalID);
            logger.info("[Bee] Provider Request : " + provider_Request.toString().replace(request.password, StringUtils.repeat("*", request.password.length())));
            logger.info("[Bee] Provider Request Creation ended");
            logger.info("[Bee] Provider Request Sending started");
            String provider_Response = send(provider_Request.toString());
            logger.info("[Bee] Provider Response : " + provider_Response);
            logger.info("[Bee] Provider Request Sending ended");
            logger.info("[Bee] Provider Response Unmarshal ended");
            GenericContentHandler handler = new GenericContentHandler();
            Response ProviderResponse = handler.unMarshal(provider_Response.toString(), Response.class);
            logger.info("[Bee] Provider Response Unmarshal ended");
            if (ProviderResponse.getStatus() == 0) {

                if (ProviderResponse.getResponseData().getTransactionStatus() == 0 || ProviderResponse.getResponseData().getTransactionStatus() == 1 || ProviderResponse.getResponseData().getTransactionStatus() == 2) {
                    response.code = 200;
                    response.message = TRANSACTION_STATUS_CODES.get(ProviderResponse.getResponseData().getTransactionStatus());
                    response.providerTransactionID = ProviderResponse.getResponseData().getCcTransactionId();

                } else if (ProviderResponse.getResponseData().getTransactionStatus() == 3) { //Business error
                    logger.error("[Bee] Provider Error : " + ProviderResponse.getResponseData().getInfo());
                    response.code = -4;
                    response.message = "[Bee] Provider Error : " + ProviderResponse.getResponseData().getInfo();

                } else {
                    logger.error("[Bee] Provider Error : " + TRANSACTION_STATUS_CODES.get(ProviderResponse.getResponseData().getTransactionStatus()));
                    response.code = -2;
                    response.message = "[Bee] Provider Error : " + TRANSACTION_STATUS_CODES.get(ProviderResponse.getResponseData().getTransactionStatus());
                }

            } else {
                //Update Service List
                if (ProviderResponse.getStatus() == 20009) {
                    updateServiceList(request);
                    response = topup(request);
                } else {
                    logger.error("[Bee] Provider Error : " + PROVIDER_STATUS_CODES.get(ProviderResponse.getStatus()));
                    response.code = -1;
                    response.message = "[Bee] Provider Error : " + PROVIDER_STATUS_CODES.get(ProviderResponse.getStatus());
                }
            }

        } catch (Exception e) {
            logger.error(e);
            response.code = -3;
            response.message = "General Error : " + e;
        }

        return response;
    }

    public RSResponse checkTransaction(RSRequest request) {
        RSResponse response = new RSResponse();
        response.requestID = request.requestID;

        try {
            logger.info("[Bee] Provider Request Creation started");
            StringBuffer provider_Request = getCTProviderRequest(request.requestID, request.userName, request.password, request.terminalID);
            logger.info("[Bee] Provider Request : " + provider_Request.toString().replace(request.password, StringUtils.repeat("*", request.password.length())));
            logger.info("[Bee] Provider Request Creation ended");
            logger.info("[Bee] Provider Request Sending started");
            String provider_Response = send(provider_Request.toString());
            logger.info("[Bee] Provider Response : " + provider_Response);
            logger.info("[Bee] Provider Request Sending ended");
            logger.info("[Bee] Provider Response Unmarshal ended");
            GenericContentHandler handler = new GenericContentHandler();
            Response ProviderResponse = handler.unMarshal(provider_Response.toString(), Response.class);
            logger.info("[Bee] Provider Response Unmarshal ended");
            if (ProviderResponse.getStatus() == 0) {
                if (ProviderResponse.getResponseData().getTransactionStatus() == 0 || ProviderResponse.getResponseData().getTransactionStatus() == 1 || ProviderResponse.getResponseData().getTransactionStatus() == 2) {
                    response.code = 200;
                    response.message = TRANSACTION_STATUS_CODES.get(ProviderResponse.getResponseData().getTransactionStatus());
                    response.providerTransactionID = ProviderResponse.getResponseData().getCcTransactionId();
                } else {
                    logger.error("[Bee] Provider Error : " + TRANSACTION_STATUS_CODES.get(ProviderResponse.getResponseData().getTransactionStatus()));
                    response.code = -2;
                    response.message = "[Bee] Provider Error : " + TRANSACTION_STATUS_CODES.get(ProviderResponse.getResponseData().getTransactionStatus());
                }
            } else {
                logger.error("[Bee] Provider Error : " + PROVIDER_STATUS_CODES.get(ProviderResponse.getStatus()));
                response.code = -1;
                response.message = "[Bee] Provider Error : " + PROVIDER_STATUS_CODES.get(ProviderResponse.getStatus());
            }

        } catch (Exception e) {
            logger.error(e);
            response.code = -3;
            response.message = "General Error : " + e;
        }

        return response;
    }

    public GetBalanceResponse GetBalance(RSRequest request) {
        GetBalanceResponse response = new GetBalanceResponse();
        try {
            logger.info("[Bee] Provider Request Creation started");
            StringBuffer provider_Request = getBProviderRequest(request.userName, request.password, request.terminalID);
            logger.info("[Bee] Provider Request : " + provider_Request.toString().replace(request.password, StringUtils.repeat("*", request.password.length())));
            logger.info("[Bee] Provider Request Creation ended");
            logger.info("[Bee] Provider Request Sending started");
            String provider_Response = send(provider_Request.toString());
            logger.info("[Bee] Provider Response : " + provider_Response);
            logger.info("[Bee] Provider Request Sending ended");
            logger.info("[Bee] Provider Response Unmarshal ended");
            GenericContentHandler handler = new GenericContentHandler();
            Response ProviderResponse = handler.unMarshal(provider_Response.toString(), Response.class);
            logger.info("[Bee] Provider Response Unmarshal ended");
            if (ProviderResponse.getStatus() == 0) {
                response.Code = 200;
                response.Message = "Success";
                response.balance = ProviderResponse.getResponseData().getBalance();
            } else {
                logger.error("Provider Error : " + PROVIDER_STATUS_CODES.get(ProviderResponse.getStatus()));
                response.Code = -1;
                response.Message = "[Bee] Provider Error : " + PROVIDER_STATUS_CODES.get(ProviderResponse.getStatus());
            }

        } catch (Exception e) {
            logger.error(e);
            response.Code = -3;
            response.Message = "General Error : " + e;
        }
        return response;
    }

    public void updateServiceList(RSRequest request) {

        try {

            logger.info("[Bee] Provider Request Creation started");
            StringBuffer provider_Request = getSVProviderRequest(SERVICE_VERSION, request.userName, request.password, request.terminalID);
            logger.info("[Bee] Provider Request : " + provider_Request.toString().replace(request.password, StringUtils.repeat("*", request.password.length())));
            logger.info("[Bee] Provider Request Creation ended");
            logger.info("[Bee] Provider Request Sending started");
            String provider_Response = send(provider_Request.toString());
            logger.info("[Bee] Provider Response : " + provider_Response);
            logger.info("[Bee] Provider Request Sending ended");
            logger.info("[Bee] Provider Response Unmarshal ended");
            GenericContentHandler handler = new GenericContentHandler();
            Response ProviderResponse = handler.unMarshal(provider_Response.toString(), Response.class);
            logger.info("[Bee] Provider Response Unmarshal ended");
            if (!(SERVICE_VERSION.equals(ProviderResponse.getResponseData().getServiceVersion()))) {
                serviceListResponse = ProviderResponse;
                SERVICE_VERSION = serviceListResponse.getResponseData().getServiceVersion();
            }
            logger.info("Service list updated.");

        } catch (Exception e) {
            logger.error(e);
            logger.error("Couldn't update service list.");
        }

    }

    private String send(String xml) throws Exception {
        URL url = new URL(HOST);
        URLConnection conn;
        if (HOST.contains("https")) {
            // Create a trust manager that does not validate certificate chains
            TrustManager[] trustAllCerts = new TrustManager[]{new X509TrustManager() {
                public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                    return null;
                }

                public void checkClientTrusted(X509Certificate[] certs, String authType) {
                }

                public void checkServerTrusted(X509Certificate[] certs, String authType) {
                }
            }};

            // Install the all-trusting trust manager
            final SSLContext sc = SSLContext.getInstance("SSL");
            sc.init(null, trustAllCerts, new java.security.SecureRandom());
            HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
            // Create all-trusting host name verifier
            HostnameVerifier allHostsValid = new HostnameVerifier() {
                public boolean verify(String hostname, SSLSession session) {
                    return true;
                }
            };
            // Install the all-trusting host verifier
            HttpsURLConnection.setDefaultHostnameVerifier(allHostsValid);

            conn = (HttpsURLConnection) url.openConnection();
        } else {
            conn = url.openConnection();
        }
        conn.setDoOutput(true);

        BufferedOutputStream bos = new BufferedOutputStream(conn.getOutputStream());
        bos.write(xml.getBytes("UTF-8"));
        bos.close();

        BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream()));
        String inputLine;
        StringBuffer response = new StringBuffer();

        while ((inputLine = in.readLine()) != null) {
            response.append(inputLine);
        }
        in.close();

        return response.toString();
    }

    private StringBuffer getProviderRequest(String transactionID, int serviceId, double amount, double totalAmount, String phone, String user_Name, String password, String terminalId) {
        int parameter_ID = 3;
        if (serviceId == 542) {
            parameter_ID = 364;
        } else if (serviceId == 6) {
            parameter_ID = 2;
        } else if (serviceId == 104483) {
            parameter_ID = 1944;
        } else if (serviceId == 92510) {
            parameter_ID = 1385;
        } else if (serviceId == 92511) {
            parameter_ID = 1387;
        } else if (serviceId == 60141) {
            parameter_ID = 864;
        } else if (serviceId == 60142) {
            parameter_ID = 865;
        }
        String REQUEST_MAP = "<item>\n"
                + "<key>" + parameter_ID + "</key>"
                + "<value>" + phone + "</value>"
                + "</item>";

        if (serviceId == 92510) {
            REQUEST_MAP = REQUEST_MAP + "\n"
                    + "<item>\n"
                    + "<key>1384</key>"
                    + "<value>.01</value>"
                    + "</item>";
        } else if (serviceId == 92511) {
            REQUEST_MAP = REQUEST_MAP + "\n"
                    + "<item>\n"
                    + "<key>1386</key>"
                    + "<value>.02</value>"
                    + "</item>";
        } else if (serviceId == 60141) {
            REQUEST_MAP = REQUEST_MAP + "\n"
                    + "<item>\n"
                    + "<key>867</key>"
                    + "<value>.01</value>"
                    + "</item>";
        } else if (serviceId == 60142) {
            REQUEST_MAP = REQUEST_MAP + "\n"
                    + "<item>\n"
                    + "<key>868</key>"
                    + "<value>.02</value>"
                    + "</item>";
        }

//        amount = Math.floor(amount * 100.00) /100.00;
//        totalAmount = Math.floor(totalAmount*100.00)/100.00;
        StringBuffer provider_Request = new StringBuffer("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
        provider_Request.append("<Request action=\"Transaction\" version=\"2\">");
        provider_Request.append("<login>").append(user_Name).append("</login>");
        provider_Request.append("<password>").append(password).append("</password>");
        provider_Request.append("<terminal_id>").append(terminalId).append("</terminal_id>");
        provider_Request.append("<locale>" + LOCALE_ID + "</locale>");
        provider_Request.append("<data>");
        provider_Request.append("<serviceVersion>" + SERVICE_VERSION + "</serviceVersion>");
        provider_Request.append("<transactionId>").append(transactionID).append("</transactionId>");
        provider_Request.append("<serviceAccountId>").append(serviceId).append("</serviceAccountId>");
        provider_Request.append("<amount>").append(amount).append("</amount>");
        provider_Request.append("<totalAmount>").append(totalAmount).append("</totalAmount>");
        provider_Request.append("<requestMap>").append(REQUEST_MAP).append("</requestMap>");
        provider_Request.append("</data>");
        provider_Request.append("</Request>");
        return provider_Request;

    }

    private StringBuffer getCTProviderRequest(String transactionID, String user_Name, String password, String terminalId) {
        StringBuffer provider_Request = new StringBuffer("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
        provider_Request.append("<Request action=\"TransactionStatus\" version=\"2\">");
        provider_Request.append("<login>").append(user_Name).append("</login>");
        provider_Request.append("<password>").append(password).append("</password>");
        provider_Request.append("<terminal_id>").append(terminalId).append("</terminal_id>");
        provider_Request.append("<locale>" + LOCALE_ID + "</locale>");
        provider_Request.append("<data>");
        provider_Request.append("<serviceVersion>" + SERVICE_VERSION + "</serviceVersion>");
        provider_Request.append("<transactionId>").append(transactionID).append("</transactionId>");
        provider_Request.append("</data>");
        provider_Request.append("</Request>");
        return provider_Request;

    }

    private StringBuffer getSVProviderRequest(String service_version, String user_Name, String password, String terminalId) {
        StringBuffer provider_Request = new StringBuffer("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
        provider_Request.append("<Request action=\"ServiceList\" version=\"2\">");
        provider_Request.append("<login>").append(user_Name).append("</login>");
        provider_Request.append("<password>").append(password).append("</password>");
        provider_Request.append("<terminal_id>").append(terminalId).append("</terminal_id>");
        provider_Request.append("<locale>" + LOCALE_ID + "</locale>");
        provider_Request.append("<data>");
        provider_Request.append("<serviceVersion>" + service_version + "</serviceVersion>");
        provider_Request.append("</data>");
        provider_Request.append("</Request>");
        return provider_Request;

    }

    private StringBuffer getBProviderRequest(String user_Name, String password, String terminalId) {
        StringBuffer provider_Request = new StringBuffer("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
        provider_Request.append("<Request action=\"GetBalance\" version=\"2\">");
        provider_Request.append("<login>").append(user_Name).append("</login>");
        provider_Request.append("<password>").append(password).append("</password>");
        provider_Request.append("<terminal_id>").append(terminalId).append("</terminal_id>");
        provider_Request.append("<locale>" + LOCALE_ID + "</locale>");
        provider_Request.append("<data>");
        provider_Request.append("</data>");
        provider_Request.append("</Request>");
        return provider_Request;

    }

     private double getTotalAmount(int serviceId, double amount) {
        double totalAmount = 0;
////TO_DO
        /*
         *  Calculate service charge
         */
        int COMMISSION_TYPE_ZERO = 0;//0% service charge
        int COMMISSION_TYPE_FIXED = 1;//Fixed service charge
        int COMMISSION_TYPE_RANGE = 2;//Range service charge [default, from, to]
        int COMMISSION_VALUE_TYPE_PERCENT = 0;//in percent
        int COMMISSION_VALUE_TYPE_AMOUNT = 1;//fixed amount

        double serviceCharge = 0;
        for (Service service : serviceListResponse.getResponseData().getServiceList().getServices()) {

            if (service.getAccountId() == serviceId) {
                if (service.getCommissionType() == COMMISSION_TYPE_FIXED) {
                    if (service.getCommissionValueType() == COMMISSION_VALUE_TYPE_AMOUNT) {
                        serviceCharge = service.getFixedCommission();
                    } else if (service.getCommissionValueType() == COMMISSION_VALUE_TYPE_PERCENT) {
                        serviceCharge = amount * service.getFixedCommission() / 100;
                    }
                } else if (service.getCommissionType() == COMMISSION_TYPE_RANGE) {
                    // Use DefaultCommission value, or any value between FromCommission and ToCommission values
                    serviceCharge = amount * service.getDefaultCommission() / 100;
                } else if (service.getCommissionType() == COMMISSION_TYPE_ZERO) {
                    serviceCharge = 0;
                }
                totalAmount = amount + serviceCharge;

            }
        }

        return roundDouble(totalAmount);
    }


    private double getAmount(int serviceId, double amount) {
        double finalAmount = 0;
        /*
         *  Calculate service amount
         */
        for (Service service : serviceListResponse.getResponseData().getServiceList().getServices()) {

            if (service.getAccountId() == serviceId) {
                if (service.getPriceType() == 1) {
                    finalAmount = Double.parseDouble(service.getServiceValue());
                } else {
                    finalAmount = amount;
                }
            }
        }

        return finalAmount;
    }

    private boolean isValidAmount(int serviceId, double amount) {
        boolean isValidAmount = true;
        /*
         *  Calculate service amount
         */
        for (Service service : serviceListResponse.getResponseData().getServiceList().getServices()) {

            if (service.getAccountId() == serviceId) {
                if (service.getPriceType() == 0 && (amount < service.getMinValue() || amount > service.getMaxValue())) {
                    isValidAmount = false;
                } else if (service.getPriceType() == 3) {
                    String[] ary = service.getServiceValueList().split(";");

                    double[] parsed = new double[ary.length];
                    for (int i = 0; i < ary.length; i++) {
                        parsed[i] = Double.valueOf(ary[i]);
                    }
                    if (!ArrayUtils.contains(parsed, amount)) {
                        isValidAmount = false;
                    }

                }
            }
        }

        return isValidAmount;
    }

    private double roundDouble(double no) {
        DecimalFormat df = new DecimalFormat("#.##");
        df.setRoundingMode(RoundingMode.FLOOR);
        return Double.parseDouble(df.format(no));
    }

}
