/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.topup.provider;

import com.topup.entites.GetBalanceResponse;
import com.topup.entites.RSRequest;
import com.topup.entites.RSResponse;
import static com.topup.manager.Manager.logger;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import org.apache.commons.lang.StringUtils;
import org.json.JSONObject;
import wakty.dtos.WaktyResponse;

/**
 *
 * @author Tony
 */
public class Wakty implements Provider {

    private static final String HOST = "http://wakty.com/webservice/momken_topup/transferCredit/";
    private static final String WEBSERVICE_PAR1 = "562318";
    private static final String WEBSERVICE_PAR2 = "23465";
    private static final String PROVIDER_ID = "4";
    private static final String PIN_CODE = "5494";
    private static final String ACC_ID = "38251196";

    @Override
    public RSResponse topup(RSRequest request) {
        RSResponse response = new RSResponse();
        response.requestID = request.requestID;

        try {
            logger.info("[Wakty] Provider Request Creation started");
            if (!isValidAmount(request.serviceID, request.amount)) {
                response.code = -5;
                response.message = "Invalid Amount!";
            }
            StringBuffer provider_Request = getProviderRequest(request.requestID, request.serviceID, request.amount, request.phone, request.userName, request.password, request.terminalID);
            logger.info("[Wakty] Provider Request : " + provider_Request.toString().replace(request.password, StringUtils.repeat("*", request.password.length())));
            logger.info("[Wakty] Provider Request Creation ended");
            logger.info("[Wakty] Provider Request Sending started");
            String provider_Response = send(provider_Request.toString());
            logger.info("[Wakty] Provider Response : " + provider_Response);
            logger.info("[Wakty] Provider Request Sending ended");
            logger.info("[Wakty] Provider Response Unmarshal ended");
            //Read JSON response and print
            JSONObject json = new JSONObject(provider_Response);
            WaktyResponse waktyResponse = getWaktyResponse(json);
            logger.info("[Wakty] Provider Response Unmarshal ended");
            if (waktyResponse.getStatus() == 1) {
                response.code = 200;
                response.message = "Successful Transaction";
                response.providerTransactionID = waktyResponse.getRequest_id();

            } else {

                logger.error("[Wakty] Provider Error : " + waktyResponse.getData());
                response.code = -1;
                response.message = "[Wakty] Provider Error : " + org.apache.commons.lang3.StringEscapeUtils.unescapeJava(waktyResponse.getData());

            }

        } catch (Exception e) {
            logger.error(e);
            response.code = -3;
            response.message = "General Error : " + e;
        }

        return response;
    }

    @Override
    public RSResponse checkTransaction(RSRequest request) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public GetBalanceResponse GetBalance(RSRequest request) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    private boolean isValidAmount(int serviceId, double amount) {
        boolean isValidAmount = true;
        return isValidAmount;
    }

    private StringBuffer getProviderRequest(String transactionID, int serviceId, double amount, String phone, String user_Name, String password, String terminalId) {

        StringBuffer provider_Request = new StringBuffer(HOST);
        provider_Request.append(WEBSERVICE_PAR1);
        provider_Request.append("/").append(WEBSERVICE_PAR2).append("/");
        provider_Request.append(PROVIDER_ID).append("/");
        provider_Request.append(amount).append("/");
        provider_Request.append(phone).append("/");
        provider_Request.append(PIN_CODE).append("/");
        provider_Request.append(ACC_ID).append("/");
        provider_Request.append(phone).append("/");
        provider_Request.append(password);
        return provider_Request;

    }

    public String send(String destinationURL) throws Exception {
        URL obj = new URL(destinationURL);
        HttpURLConnection con = (HttpURLConnection) obj.openConnection();
        // optional default is GET
        con.setRequestMethod("GET");
        //add request header
        con.setRequestProperty("User-Agent", "Mozilla/5.0");
        int responseCode = con.getResponseCode();
        logger.info("[Wakty] \nSending 'GET' request to URL : " + destinationURL);
        BufferedReader in = new BufferedReader(
                new InputStreamReader(con.getInputStream()));
        String inputLine;
        StringBuffer response = new StringBuffer();
        while ((inputLine = in.readLine()) != null) {
            response.append(inputLine);
        }
        in.close();

        //print in String
        return response.toString();

    }

    private WaktyResponse getWaktyResponse(JSONObject json) {
        WaktyResponse WaktyResponse = new WaktyResponse();
        WaktyResponse.setStatus(json.getInt("status"));
//        WaktyResponse.setData(org.apache.commons.lang3.StringEscapeUtils.unescapeJava(json.getString("data")));
        WaktyResponse.setData(json.getString("data"));
        if (json.has("requestID")) {
            WaktyResponse.setRequest_id(json.getString("requestID"));
        }
        if (json.has("phone")) {
            WaktyResponse.setPhone(json.getString("phone"));
        }
        if (json.has("custmer_service")) {
            WaktyResponse.setCustmer_service(json.getString("custmer_service"));
        }
        if (json.has("user_balance")) {
            WaktyResponse.setUser_balance(json.getDouble("user_balance"));
        }
        return WaktyResponse;
    }
}
