/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package etisalat.dtos;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 *
 * @author Antonios_knaguib
 */
@XmlAccessorType(XmlAccessType.FIELD)
//@XmlType(name = "COMMAND")
@XmlRootElement(name = "TCSReply")
public class EtisalatTopupResponse {

    @XmlElement(name = "Result", required = true)
    protected int result;
    @XmlElement(name = "Message", required = true)
    protected String message;
    @XmlElement(name = "param1", required = false)
    protected String providerTransactionID;

    public int getResult() {
        return result;
    }

    public void setResult(int result) {
        this.result = result;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getProviderTransactionID() {
        return providerTransactionID;
    }

    public void setProviderTransactionID(String providerTransactionID) {
        this.providerTransactionID = providerTransactionID;
    }

}
