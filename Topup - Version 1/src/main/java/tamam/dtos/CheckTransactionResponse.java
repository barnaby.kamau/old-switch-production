/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tamam.dtos;

import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlElements;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

/**
 *
 * @author Antonios_knaguib
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TXLIST")
@XmlRootElement(name = "RESPONSE")
public class CheckTransactionResponse {

    @XmlElement(name = "TERMINALID", required = true)
    protected String terminalId;
    @XmlElement(name = "LOCALDATETIME", required = true)
    protected String localDateTime;
    @XmlElement(name = "SERVERDATETIME", required = true)
    protected String serverDateTime;
    @XmlElement(name = "TXID", required = true)
    protected String trxId;
    @XmlElement(name = "LIMIT", required = true)
    protected double limit;
    @XmlElement(name = "RESULT", required = true)
    protected int statusCode;
    @XmlElement(name = "RESULTTEXT", required = true)
    protected String statusMessage;
    @XmlElementWrapper(name = "TXLIST")
    @XmlElements({
        @XmlElement(name = "TX", type = Transaction.class)
    })
    protected List<Transaction> transactions = null;

    public String getTerminalId() {
        return terminalId;
    }

    public void setTerminalId(String terminalId) {
        this.terminalId = terminalId;
    }

    public String getLocalDateTime() {
        return localDateTime;
    }

    public void setLocalDateTime(String localDateTime) {
        this.localDateTime = localDateTime;
    }

    public String getServerDateTime() {
        return serverDateTime;
    }

    public void setServerDateTime(String serverDateTime) {
        this.serverDateTime = serverDateTime;
    }

    public String getTrxId() {
        return trxId;
    }

    public void setTrxId(String trxId) {
        this.trxId = trxId;
    }

    public double getLimit() {
        return limit;
    }

    public void setLimit(double limit) {
        this.limit = limit;
    }

    public int getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(int statusCode) {
        this.statusCode = statusCode;
    }

    public String getStatusMessage() {
        return statusMessage;
    }

    public void setStatusMessage(String statusMessage) {
        this.statusMessage = statusMessage;
    }

//    public Transactions getTransactions() {
//        return transactions;
//    }
//
//    public void setTransactions(Transactions transactions) {
//        this.transactions = transactions;
//    }
    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this,
                ToStringStyle.MULTI_LINE_STYLE);
    }

    public List<Transaction> getTransactions() {
        return transactions;
    }

    public void setTransactions(List<Transaction> transactions) {
        this.transactions = transactions;
    }

}
