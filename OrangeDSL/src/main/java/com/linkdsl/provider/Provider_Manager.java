/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.linkdsl.provider;

import static com.linkdsl.bills.Manager.logger;
import com.linkdsl.entites.RSRequest;
import com.linkdsl.entites.RSResponse;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 *
 * @author Antonios_knaguib
 */
public class Provider_Manager {

    public RSResponse inquiry(RSRequest request) {
        RSResponse response = new RSResponse();
        response.requestID = request.requestID;

        System.setProperty("webdriver.chrome.driver", "D:\\Driver\\chromedriver.exe");
        ChromeOptions options = new ChromeOptions();
//            options.addArguments("headless");
        options.addArguments("disable-extensions");
        options.addArguments("disable-infobars");
//
//            //Create driver object for Chrome and assign chromeoptions to it 
        WebDriver driver = new ChromeDriver(options);
        driver.manage().window().fullscreen();
//        WebDriver driver = new ChromeDriver();
        try {
//            Google browse startup configuration
//            ChromeOptions option = driver.;
//            # Configuration parameters prohibit Chrome from being controlled by automated software
//            option.add_argument('disable-infobars');
// # The configuration parameter prohibits the appearance of data;
//            option.add_argument('user-data-dir=C:\python27\profile');

// # Open chrome browser driver = webdriver.Chrome(chrome_options = option)
            driver.get("https://scp.orange.eg/web/guest/login");
            driver.findElement(By.id("_58_login")).sendKeys(request.userName);
            driver.findElement(By.id("_58_password")).sendKeys(request.password);
            driver.findElement(By.id("_58_password")).submit();

            driver.findElement(By.xpath("//*[@id=\"portlet_MainNavigationController_WAR_LayoutManagmentportlet\"]/div/div/div/ul/li/a")).click();

            Thread.sleep(1000);

            driver.switchTo().frame("_48_INSTANCE_tZe05HWje2Mf_iframe");

            driver.findElement(By.linkText("ADSL Bill Payment")).click();

            WebDriverWait wait = new WebDriverWait(driver, 30);

            wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='form:customer_sln']"))).sendKeys(request.telephoneCode + request.phone);

            driver.findElement(By.xpath("//*[@id=\"form:j_idt123\"]")).click();
            wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"form:j_idt183\"]")));

            double totalAmount = Double.parseDouble(driver.findElement(By.xpath("//*[@id=\"form:j_idt175\"]")).getText());
            wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"form:j_idt183\"]"))).click();

            if (Math.round(totalAmount * 100) % 10 > 0) {
                totalAmount = Math.round(totalAmount * 100) / 100;
            }
//            System.out.println("Result : "+result);
            if (totalAmount == 0.0) {
                response.code = -2;
                response.message = "No bills Founded";
            } else {
                response.code = 200;
                response.message = "Successful Payment.";
                response.phoneNumber = request.telephoneCode + request.phone;
                response.totalAmount = totalAmount;
            }
            driver.quit();

        } catch (Exception e) {
            logger.error(e);
            response.code = -3;
            response.message = "General Error : " + e;
            return response;
        }
        driver.quit();

        return response;
    }

    public RSResponse payment(RSRequest request) {
//        String Mobile_Number = "";
        boolean ispaymentDone = false;
        RSResponse response = new RSResponse();
        response.requestID = request.requestID;
        response.totalAmount = request.amount;
//        Validations
        try {
            if (request.amount <= 0) {
                response.code = -1;
                response.message = "Invalid Amount must be greater than 0.0";
                return response;
            }
        } catch (Exception e) {
            logger.error(e);
            response.code = -1;
            response.message = "Invalid Request.";
            return response;
        }

        System.setProperty("webdriver.chrome.driver", "D:\\Driver\\chromedriver.exe");
//            ChromeOptions options = new ChromeOptions();
//            options.addArguments("headless");
//
//            //Create driver object for Chrome and assign chromeoptions to it 
//            WebDriver driver = new ChromeDriver(options);

//         ChromeOptions options = new ChromeOptions();
////            options.addArguments("headless");
//            options.addArguments("disable-extensions");
//            options.addArguments("disable-infobars");
////
//            //Create driver object for Chrome and assign chromeoptions to it 
//            WebDriver driver = new ChromeDriver(options);
        WebDriver driver = new ChromeDriver();

        try {
            driver.get("https://scp.orange.eg/web/guest/login");
            String originalHandle = driver.getWindowHandle();

    //Do something to open new tabs
            for (String handle : driver.getWindowHandles()) {
                if (!handle.equals(originalHandle)) {
                    driver.switchTo().window(handle);
                    driver.close();
                }
            }

            driver.switchTo().window(originalHandle);

            driver.findElement(By.id("_58_login")).sendKeys(request.userName);
            driver.findElement(By.id("_58_password")).sendKeys(request.password);
            driver.findElement(By.id("_58_password")).submit();

            driver.findElement(By.xpath("//*[@id=\"portlet_MainNavigationController_WAR_LayoutManagmentportlet\"]/div/div/div/ul/li/a")).click();

            Thread.sleep(1000);

            driver.switchTo().frame("_48_INSTANCE_tZe05HWje2Mf_iframe");

            driver.findElement(By.linkText("ADSL Bill Payment")).click();

            WebDriverWait wait = new WebDriverWait(driver, 30);

            wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='form:customer_sln']"))).sendKeys(request.telephoneCode + request.phone);

            driver.findElement(By.xpath("//*[@id=\"form:j_idt123\"]")).click();
            wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"form:j_idt176\"]")));
//            Thread.sleep(5000);

//            double totalAmount = Double.parseDouble(driver.findElement(By.xpath("//*[@id=\"form:j_idt175\"]")).getText());
            double totalAmount = Double.parseDouble(driver.findElement(By.xpath("//*[@id=\"form:j_idt169\"]")).getText().replace(" pt", ""));
//            wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"form:j_idt183\"]"))).click();
//            System.out.println("hereeeeeeeeeeeee");
            wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"form:j_idt176\"]"))).click();
//            System.out.println("After hereeeeeeeeeeeee");

            if (Math.round(totalAmount * 100) % 10 > 0) {
                totalAmount = Math.round(totalAmount * 100) / 100;
            }

//            System.out.print(">>>>>>>>>>> " + totalAmount);
            if (totalAmount > 0) {
                wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"form:payment_type\"]/tbody/tr/td[3]/div/div[2]/span"))).click();
                wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"form:total_amount\"]"))).clear();
                driver.findElement(By.xpath("//*[@id=\"form:total_amount\"]")).sendKeys(String.format("%.2f", request.amount));
            } else {
                wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"form:inadvance_payment\"]"))).clear();
                driver.findElement(By.xpath("//*[@id=\"form:inadvance_payment\"]")).sendKeys(String.format("%.2f", request.amount));
            }
            driver.manage().window().maximize();
            wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"form:pay_button\"]"))).click();
//            driver.findElement(By.xpath("//*[@id=\"form:pay_button\"]")).click();
            ispaymentDone = true;
            wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"invoiceFormId:printReceiptInvoiceId\"]/span")));
//            wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"invoiceFormId:j_idt48\"]")));
            String providerTransactionID = "";
            try {
                providerTransactionID = driver.findElement(By.xpath("//*[@id=\"invoiceFormId:j_idt48\"]")).getAttribute("value");
                providerTransactionID = driver.findElement(By.xpath("//*[@id=\"invoiceFormId:j_idt48\"]")).getAttribute("value");
            } catch (Exception ex) {
            }
            try {
                providerTransactionID = driver.findElement(By.xpath("//*[@id=\"invoiceFormId:j_idt46\"]")).getAttribute("value");
            } catch (Exception ex) {

            }

//            System.out.print(">>>>>>>>>>> " + providerTransactionID);
            response.code = 200;
            response.message = "Successful Payment.";
            response.providerTransactionID = providerTransactionID;
            driver.quit();

        } catch (Exception e) {
            e.printStackTrace();
            logger.error(e);
            if (ispaymentDone) {
                response.code = -200;
                response.message = "Pending transaction ";
            } else {
                response.code = -3;
                response.message = "General Error.";
            }
            driver.quit();
            return response;
        }

        return response;
    }

}
