/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.linkdsl.provider;

import static com.linkdsl.bills.Manager.logger;
import com.linkdsl.entites.Auth;
import com.linkdsl.entites.InqField;
import com.linkdsl.entites.KhadamatyResponse;
import com.linkdsl.entites.KhadamtyRequest;
import com.linkdsl.entites.RSRequest;
import com.linkdsl.entites.RSResponse;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.config.ClientConfig;
import com.sun.jersey.api.client.config.DefaultClientConfig;
import java.util.ArrayList;
import java.util.List;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.UriBuilder;
import org.apache.commons.lang.StringUtils;

/**
 *
 * @author Antonios_knaguib
 */
public class Khadamaty {

//    int SERVICEID = 22;
    int SERVICEID = 368;
    String AGENTCODE = "8364";
    int APPID = 27;
    String USERNAME = "momkn";
    String PASSWORD = "hZ3BGBayXUCXsTnr";
    String URL = "https://gateway.khadamaty.com.eg/V3/api/Inquiry";

    public RSResponse inquiry(RSRequest request) {
        RSResponse response = new RSResponse();
        response.requestID = request.requestID;
        try {
            logger.info("[Khadamaty] RequestID : " + request.requestID + " Provider Request Creation started");
            KhadamtyRequest providerRequest = getProviderRequest(request);
            logger.info("[Khadamaty] RequestID : " + request.requestID + " Provider Request : " + providerRequest.toString().replace(PASSWORD, StringUtils.repeat("*", PASSWORD.length())));
            logger.info("[Khadamaty] RequestID : " + request.requestID + " Provider Request Creation ended");

            logger.info("[Khadamaty] RequestID : " + request.requestID + " Provider Request Sending started");
            ClientConfig config = new DefaultClientConfig();
            Client client = Client.create(config);
            WebResource webResource = client.resource(UriBuilder.fromUri(URL).build());
            KhadamatyResponse providerResponse = webResource.type(MediaType.APPLICATION_JSON).post(KhadamatyResponse.class, providerRequest);
            logger.info("[Khadamaty] RequestID : " + request.requestID + " Provider Response : " + providerResponse.toString());
            logger.info("[Khadamaty] RequestID : " + request.requestID + " Provider Request Sending ended");
            logger.info("[Khadamaty] RequestID : " + request.requestID + " Provider Response Unmarshal ended");
            if (providerResponse.StatusCode == 1) {
                response.code = 200;
                response.message = "Successful Inquiry.";
                response.phoneNumber = request.telephoneCode + request.phone;
                response.totalAmount = providerResponse.Amount;
                if (providerResponse.ExtraBillInfo!= null && providerResponse.ExtraBillInfo.length() > 0) {
                    response.customerName = providerResponse.ExtraBillInfo.substring(providerResponse.ExtraBillInfo.indexOf("Name:") + "Name:".length(), providerResponse.ExtraBillInfo.length());
                }else{
                    response.customerName = "";
                }
            } else {
                response.code = -2;
                response.message = "Provider Error : " + providerResponse.StatusDescription;
            }

        } catch (Exception e) {
            logger.error(e);
            response.code = -3;
            response.message = "General Error : " + e;
            return response;
        }

        return response;
    }

    private KhadamtyRequest getProviderRequest(RSRequest request) {
        KhadamtyRequest providerRequest = new KhadamtyRequest();
        providerRequest.SenderTrans = request.requestID;
        providerRequest.ServiceId = 22;
        Auth auth = new Auth();
        auth.AgentCode = AGENTCODE;
        auth.AppId = APPID;
        auth.UserId = USERNAME;
        auth.Password = PASSWORD;
        providerRequest.Auth = auth;
        List<InqField> InqFields = new ArrayList<>();
        InqField field = new InqField();
        field.SFId = 141;
        field.Label = "كود المحافظة";
        field.Value = request.telephoneCode;
        InqFields.add(field);
        field = new InqField();
        field.SFId = 92;
        field.Label = "رقم تليفون الخدمة";
        field.Value = request.phone;
        InqFields.add(field);
        providerRequest.InqFields = InqFields;
        return providerRequest;
    }
}
