/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.linkdsl.provider;

import com.linkdsl.entites.Auth;
import com.linkdsl.entites.InqField;
import com.linkdsl.entites.KhadamatyResponse;
import com.linkdsl.entites.KhadamtyRequest;
import java.io.IOException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.UriBuilder;
import org.apache.http.client.ClientProtocolException;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.config.ClientConfig;
import com.sun.jersey.api.client.config.DefaultClientConfig;
import com.sun.jersey.core.util.MultivaluedMapImpl;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author nehal
 */
public class test {

    public static void main(String[] args) throws IOException, InterruptedException {

        ClientConfig config = new DefaultClientConfig();
        Client client = Client.create(config);
        WebResource webResource = client.resource(UriBuilder.fromUri("https://gateway.khadamaty.com.eg/V3/api/Inquiry").build());
        KhadamtyRequest providerRequest =  new KhadamtyRequest();
        providerRequest.SenderTrans = "123456464";
        providerRequest.ServiceId = 22;
        Auth auth = new Auth();
        auth.AgentCode="8364";
        auth.AppId = 27;
        auth.UserId = "momkn";
        auth.Password = "hZ3BGBayXUCXsTnr";
        providerRequest.Auth = auth;
        List<InqField> InqFields = new ArrayList<>();
        InqField field =  new InqField();
        field.SFId=141;
        field.Label = "كود المحافظة";
        field.Value = "02";
        InqFields.add(field);
        field =  new InqField();
        field.SFId=92;
        field.Label = "رقم تليفون الخدمة";
        field.Value = "22055074";
        InqFields.add(field);
        providerRequest.InqFields = InqFields;
//        MultivaluedMap formData = new MultivaluedMapImpl();
//        formData.add("name1", "val1");
//        formData.add("name2", "val2");
        KhadamatyResponse providerResponse = webResource.type(MediaType.APPLICATION_JSON).post(KhadamatyResponse.class, providerRequest);
//        KhadamatyResponse providerResponse = response.getEntity(KhadamatyResponse.class);
        System.out.println("Name "+ providerResponse.ExtraBillInfo);
        System.out.println("Amount "+ providerResponse.Amount);
    }
}
