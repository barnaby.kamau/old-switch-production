/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.linkdsl.bills;

/**
 *
 * @author Antonios_knaguib
 */
import com.linkdsl.entites.RSRequest;
import com.linkdsl.entites.RSResponse;
import com.linkdsl.provider.Khadamaty;
import com.linkdsl.provider.Provider_Manager;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import static javax.ws.rs.core.MediaType.APPLICATION_JSON;
import javax.ws.rs.core.Response;
import static javax.ws.rs.core.Response.ok;
import org.apache.log4j.Logger;
import org.json.JSONException;

@Path("/")
@Produces(APPLICATION_JSON)
public class Manager {

    public static Logger logger = Logger.getLogger("R");

    @GET
    public Response Inquiry(@QueryParam("requestID") String requestID, @QueryParam("phone") String phone, @QueryParam("telephoneCode") String telephoneCode, @QueryParam("userName") String userName, @QueryParam("password") String password) throws JSONException {
        RSResponse response = new RSResponse();
        RSRequest request = new RSRequest();
        request.requestID = requestID;
        request.userName = userName;
        request.password = password;
        request.phone = phone;
        request.telephoneCode = telephoneCode;
        logger.info("Inquiry Request : " + request);
        Khadamaty provider = new Khadamaty();
//        Provider_Manager provider = new Provider_Manager();
        response = provider.inquiry(request);
        logger.info("Inquiry Response : " + response);
        return ok(response, APPLICATION_JSON).build();
    }

    @POST
    public Response payment(RSRequest request) throws JSONException {
        RSResponse response = new RSResponse();
        response.requestID = request.requestID;
        response.totalAmount = request.amount;
        logger.info("Payment Request : " + request);
        Provider_Manager provider = new Provider_Manager();
        response = provider.payment(request);
        logger.info("Payment Response : " + response);
        return ok(response, APPLICATION_JSON).build();
    }

}
