/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.linkdsl.entites;

import java.io.Serializable;
import java.util.List;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

/**
 *
 * @author Antonios_knaguib
 */
public class KhadamatyResponse implements Serializable{

    public int StatusCode;
    public String StatusDescription;
    public int ServiceId;
    public double Amount;
    public String BillerStatus;
    public String BillRefNumber;
    public String AsyncRqUID;
    public String ExtraBillInfo;
    public double Fees;
    public List<BillInfo> BillInfo;

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this,
                ToStringStyle.MULTI_LINE_STYLE);
    }

}
