/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.etisalat.provider;

import static com.etisalat.bills.Manager.logger;
import com.etisalat.entites.RSRequest;
import com.etisalat.entites.RSResponse;
import java.awt.Image;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.math.BigDecimal;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.security.cert.X509Certificate;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import javax.imageio.ImageIO;
import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import net.sourceforge.javaocr.ocrPlugins.mseOCR.CharacterRange;
import net.sourceforge.javaocr.ocrPlugins.mseOCR.OCRScanner;
import net.sourceforge.javaocr.ocrPlugins.mseOCR.TrainingImage;
import net.sourceforge.javaocr.ocrPlugins.mseOCR.TrainingImageLoader;
import net.sourceforge.javaocr.scanner.PixelImage;
import org.apache.commons.lang.StringUtils;
import tamam.dtos.InquiryResponse;
import tamam.dtos.PaymentResponse;

/**
 *
 * @author Antonios_knaguib
 */
public class Provider_Manager {

    private static HashMap<Integer, String> TRANSACTION_STATUS_CODES = new HashMap<>();
    private static HashMap<Integer, String> PROVIDER_STATUS_CODES = new HashMap<>();
//    private static final String TERMINALID = "UA007819";
    private static final String TERMINALID = "UA007819";
    private static final String EAN = "4260469316531";
    private static final String CURRENCY = "818";
    private static final String LANGUAGE = "ENG";
    private static final String CHARSPERLINE = "40";
    private static final String HOST = "https://precision.epayworldwide.com/up-interface";

    static {
        TRANSACTION_STATUS_CODES.put(0, "Initialized");
        TRANSACTION_STATUS_CODES.put(1, "Under process");
        TRANSACTION_STATUS_CODES.put(2, "Success");
        TRANSACTION_STATUS_CODES.put(3, "Error");
        TRANSACTION_STATUS_CODES.put(4, "Canceled");
        TRANSACTION_STATUS_CODES.put(8, "Deposit error");
        TRANSACTION_STATUS_CODES.put(20007, "Data not found");
        PROVIDER_STATUS_CODES.put(0, "Successful");
        PROVIDER_STATUS_CODES.put(11000, "Authentication failed. Incorrect login or password");
        PROVIDER_STATUS_CODES.put(11001, "Service list cannot be updated");
        PROVIDER_STATUS_CODES.put(11002, "Transaction not accepted");
        PROVIDER_STATUS_CODES.put(11003, "Check transaction status is failed");
        PROVIDER_STATUS_CODES.put(20000, "General error");
        PROVIDER_STATUS_CODES.put(20001, "Request action version error");
        PROVIDER_STATUS_CODES.put(20002, "Service not allowed");
        PROVIDER_STATUS_CODES.put(20003, "Incomplete request");
        PROVIDER_STATUS_CODES.put(20004, "Wrong action");
        PROVIDER_STATUS_CODES.put(20005, "Incorrect XML request");
        PROVIDER_STATUS_CODES.put(20006, "No permission");
        PROVIDER_STATUS_CODES.put(20007, "Data not found");
        PROVIDER_STATUS_CODES.put(20008, "Duplicate transaction");
        PROVIDER_STATUS_CODES.put(20009, "External system must update service list");
        PROVIDER_STATUS_CODES.put(20010, "Deposit error, insufficient deposit to perform operation");
    }

    public RSResponse inquiry(RSRequest request) {
        RSResponse response = new RSResponse();
        response.request_id = request.requestId;
        try {

            StringBuffer provider_Request = getInquiryRequest(request);
            logger.info("Provider Inquiry Request : " + provider_Request.toString().replace(request.password, StringUtils.repeat("*", request.password.length())));
            logger.info("Provider Inquiry Request Creation ended");
            logger.info("Provider Inquiry Request Sending started");
            String provider_Response = send(provider_Request.toString());
//            String provider_Response = "<?xml version=\"1.0\" encoding=\"UTF-8\" ?>\n"
//                    + "<RESPONSE TYPE=\"SMARTCARDBALANCE\">\n"
//                    + "   <TERMINALID>UAE12854</TERMINALID>\n"
//                    + "   <LOCALDATETIME>2016-05-05 02:01:20</LOCALDATETIME>\n"
//                    + "   <SERVERDATETIME>2016-05-05 12:01:41</SERVERDATETIME>\n"
//                    + "   <TXID>1</TXID>\n"
//                    + "   <HOSTTXID>UAE12854000000</HOSTTXID>\n"
//                    + "   <CURRENTBALANCE>1</CURRENTBALANCE>\n"
//                    + "   <LIMIT>0</LIMIT>\n"
//                    + "   <RESULT>0</RESULT>\n"
//                    + "   <RESULTTEXT>transaction successful</RESULTTEXT>\n"
//                    + "</RESPONSE>";
            logger.info("Provider Inquiry Response : " + provider_Response);
            logger.info("Provider Inquiry Request Sending ended");
            logger.info("Provider Inquiry Response Unmarshal ended");
            GenericContentHandler handler = new GenericContentHandler();
            InquiryResponse ProviderResponse = handler.unMarshal(provider_Response.toString(), InquiryResponse.class);
            logger.info("Provider Inquiry Response Unmarshal ended");
            if (ProviderResponse.getStatusCode() == 0) {
                response.Total_Amount = ProviderResponse.getCurrentBalance() / 100;
                response.Code = 200;
                response.Message = "Successful Inquiry.";
                return response;
            }else if (ProviderResponse.getStatusCode() == 9998){
                response.Code = -5;
                response.Message = "Provider Error : Invalid billing account";
                return response;
            } else {
                response.Code = -2;
                response.Message = "Provider Error : " + ProviderResponse.getStatusMessage();
                return response;
            }
        } catch (Exception e) {
            response.Code = -3;
            response.Message = "General Error : " + e;
            return response;
        }
    }

    public RSResponse payment(RSRequest request) {
//        String Mobile_Number = "";
        RSResponse response = new RSResponse();
        response.request_id = request.requestId;
//        Validations
        try {
            if (request.amount <= 0) {
                response.Code = -1;
                response.Message = "Invalid Amount must be greater than 0.0";
                return response;
            }
        } catch (Exception e) {
            logger.error(e);
            response.Code = -4;
            response.Message = "Invalid Request.";
            return response;
        }

        try {

            StringBuffer provider_Request = getPaymentRequest(request);
            logger.info("Provider Payment Request : " + provider_Request.toString().replace(request.password, StringUtils.repeat("*", request.password.length())));
            logger.info("Provider Payment Request Creation ended");
            logger.info("Provider Payment Request Sending started");
            String provider_Response = send(provider_Request.toString());
//            String provider_Response = "<?xml version=\"1.0\" encoding=\"UTF-8\" ?>\n"
//                    + "<RESPONSE TYPE=\"SALE\">\n"
//                    + "   <TERMINALID>UAE12854</TERMINALID>\n"
//                    + "   <LOCALDATETIME>2016-05-05 02:01:20</LOCALDATETIME>\n"
//                    + "   <SERVERDATETIME>2016-05-05 12:01:41</SERVERDATETIME>\n"
//                    + "   <TXID>1</TXID>\n"
//                    + "   <HOSTTXID>UAE12854000000</HOSTTXID>\n"
//                    + "   <AMOUNT>1</AMOUNT>\n"
//                    + "   <CURRENCY>818</CURRENCY>\n"
//                    + "   <LIMIT>0</LIMIT>\n"
//                    + "   <RESULT>0</RESULT>\n"
//                    + "   <RESULTTEXT>transaction successful</RESULTTEXT>\n"
//                    + "</RESPONSE>";
            logger.info("Provider Payment Response : " + provider_Response);
            logger.info("Provider Payment Request Sending ended");
            logger.info("Provider Payment Response Unmarshal ended");
            GenericContentHandler handler = new GenericContentHandler();
            PaymentResponse ProviderResponse = handler.unMarshal(provider_Response.toString(), PaymentResponse.class);
            logger.info("Provider Payment Response Unmarshal ended");
            if (ProviderResponse.getStatusCode() == 0) {
                response.Code = 200;
                response.Message = "Successful Payment.";
                response.Total_Amount = request.amount;
                response.request_id = request.requestId;
                response.ProviderTransactionId = ProviderResponse.getProvidertrxId();
                return response;
            } else {
                response.Code = -2;
                response.Message = "Provider Error : " + ProviderResponse.getStatusMessage();
                return response;
            }
        } catch (Exception e) {
            logger.error(e);
            response.Code = -3;
            response.Message = "General Error : " + e;
            return response;
        }
    }

    private Map<String, List<String>> send(String targetURL, String urlParameters, String method, HashMap<String, String> request_Headers, Map<String, Object> params) throws IOException {
        StringBuilder postData = new StringBuilder();
        for (Map.Entry<String, Object> param : params.entrySet()) {
            if (postData.length() != 0) {
                postData.append('&');
            }
            postData.append(URLEncoder.encode(param.getKey(), "UTF-8"));
            postData.append('=');
            postData.append(URLEncoder.encode(String.valueOf(param.getValue()), "UTF-8"));
        }
        byte[] postDataBytes = postData.toString().getBytes("UTF-8");

        HashMap<String, List<String>> response_properties = new HashMap<String, List<String>>();
        URL obj = new URL(targetURL);
        HttpURLConnection connection = (HttpURLConnection) obj.openConnection();
        connection.setRequestMethod(method);
        Set set = request_Headers.entrySet();
        Iterator iterator = set.iterator();
        while (iterator.hasNext()) {
            Map.Entry mentry = (Map.Entry) iterator.next();
            connection.setRequestProperty(mentry.getKey().toString(), mentry.getValue().toString());
        }

        connection.setRequestProperty("Content-Length", String.valueOf(postDataBytes.length));
        connection.setDoOutput(true);
        connection.getOutputStream().write(postDataBytes);

        int responseCode = connection.getResponseCode();
        List<String> cookies = connection.getHeaderFields().get("Set-Cookie");

        if (responseCode == HttpURLConnection.HTTP_OK || responseCode == HttpURLConnection.HTTP_MOVED_TEMP) { // success
            BufferedReader in = new BufferedReader(new InputStreamReader(
                    connection.getInputStream(), "UTF-8"));
            String inputLine;
            StringBuffer response = new StringBuffer();

            while ((inputLine = in.readLine()) != null) {
                response.append(new String(inputLine.getBytes("ISO-8859-1"), "UTF-8"));
            }
            in.close();
            response_properties = new HashMap<String, List<String>>();
            List<String> res = new ArrayList<String>();
            res.add(response.toString());
            response_properties.put("Set-Cookie", cookies);
            response_properties.put("Form", res);
//            logger.info(res);
            //print result
//            System.out.println(response.toString());
        } else {
            logger.error("Error In sending request");
            logger.error(connection.getResponseMessage());
        }
        return response_properties;
    }

    public String getCapacha(String cookies_out, String URL, String name) throws Exception {
//        String Path = "D:\\Momken Setup\\Etisalat\\";
        String Path = "C:\\Momken Setup\\Etisalat\\";
        OCRScanner scanner = new OCRScanner();
        TrainingImageLoader loader = new TrainingImageLoader();
        HashMap<Character, ArrayList<TrainingImage>> trainingImageMap = new HashMap<Character, ArrayList<TrainingImage>>();
        loader.load(Path + "training\\0.png", new CharacterRange('0', '0'), trainingImageMap);
        loader.load(Path + "training\\1.png", new CharacterRange('1', '1'), trainingImageMap);
        loader.load(Path + "training\\2.png", new CharacterRange('2', '2'), trainingImageMap);
        loader.load(Path + "training\\3.png", new CharacterRange('3', '3'), trainingImageMap);
        loader.load(Path + "training\\4.png", new CharacterRange('4', '4'), trainingImageMap);
        loader.load(Path + "training\\5.png", new CharacterRange('5', '5'), trainingImageMap);
        loader.load(Path + "training\\6.png", new CharacterRange('6', '6'), trainingImageMap);
        loader.load(Path + "training\\7.png", new CharacterRange('7', '7'), trainingImageMap);
        loader.load(Path + "training\\8.png", new CharacterRange('8', '8'), trainingImageMap);
        loader.load(Path + "training\\9.png", new CharacterRange('9', '9'), trainingImageMap);
        scanner.addTrainingImages(trainingImageMap);
        StringBuffer cookies = new StringBuffer();
        cookies.append(cookies_out);
        HashMap<String, String> request_Headers = new HashMap<String, String>();
        request_Headers = new HashMap<String, String>();
        request_Headers.put("User-Agent", "Mozilla/5.0 (compatible; MSIE 9.0; Windows NT 6.1; WOW64; Trident/5.0)");
        request_Headers.put("Cookie", cookies.toString());
        String fileURL = URL;
        String saveDir = Path + "images";

        HttpDownloadUtility.downloadFile(fileURL, saveDir, request_Headers);
        String imagePath = Path + "images\\" + name;
        String destinationFile = imagePath;
        Image image = ImageIO.read(new File(imagePath));
        PixelImage pixelImage = new PixelImage(image);
        pixelImage.toGrayScale(true);
        pixelImage.filter();

        String text = scanner.scan(image, 0, 0, 0, 0, null);
        logger.info("Capacha text :" + text);
        return text.replace(" ", "");
    }

    public void updateCookies(Properties oldCookies, String newCookies) {
        Properties tempProp = parsePropertiesString(newCookies);
        for (Map.Entry<?, ?> entry : tempProp.entrySet()) {
            String key = (String) entry.getKey();
            String value = (String) entry.getValue();
            oldCookies.setProperty(key, value);
        }
    }

    public Properties parsePropertiesString(String s) {
        Properties p = new Properties();
        try {

            p.load(new StringReader(s));
        } catch (Exception e) {
        }
        return p;
    }

    private Map<String, List<String>> sendInquiryRequest(String targetURL, HashMap<String, String> request_Headers) throws Exception {
        HashMap<String, List<String>> response_properties = new HashMap<String, List<String>>();
        String url = targetURL;

        URL obj = new URL(url);
        HttpsURLConnection con = (HttpsURLConnection) obj.openConnection();

        // optional default is GET
        con.setRequestMethod("GET");

        //add request header
        Set set = request_Headers.entrySet();
        Iterator iterator = set.iterator();
        while (iterator.hasNext()) {
            Map.Entry mentry = (Map.Entry) iterator.next();
            con.setRequestProperty(mentry.getKey().toString(), mentry.getValue().toString());
        }

        int responseCode = con.getResponseCode();
        List<String> cookies = con.getHeaderFields().get("Set-Cookie");
        if (responseCode == HttpURLConnection.HTTP_OK) { // success
            BufferedReader in = new BufferedReader(
                    new InputStreamReader(con.getInputStream()));
            String inputLine;
            StringBuffer response = new StringBuffer();

            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }
            in.close();
            response_properties = new HashMap<String, List<String>>();
            List<String> res = new ArrayList<String>();
            res.add(response.toString());
            response_properties.put("Set-Cookie", cookies);
            response_properties.put("Form", res);
            //print result
            System.out.println("Tony :" + response.toString());
        } else {
            logger.error("Error In sending request");
            logger.error(con.getResponseMessage());
        }
        return response_properties;
    }

    private Map<String, List<String>> sendPostnew(String targetURL, HashMap<String, String> request_Headers, Map<String, Object> params) throws Exception {
        StringBuilder postData = new StringBuilder();
        for (Map.Entry<String, Object> param : params.entrySet()) {
            if (postData.length() != 0) {
                postData.append('&');
            }
            postData.append(URLEncoder.encode(param.getKey(), "UTF-8"));
            postData.append('=');
            postData.append(URLEncoder.encode(String.valueOf(param.getValue()), "UTF-8"));
        }
        byte[] postDataBytes = postData.toString().getBytes("UTF-8");
        HashMap<String, List<String>> response_properties = new HashMap<String, List<String>>();
        String url = targetURL;
        URL obj = new URL(url);
        HttpsURLConnection con = (HttpsURLConnection) obj.openConnection();

        //add request header
        Set set = request_Headers.entrySet();
        Iterator iterator = set.iterator();
        while (iterator.hasNext()) {
            Map.Entry mentry = (Map.Entry) iterator.next();
            con.setRequestProperty(mentry.getKey().toString(), mentry.getValue().toString());
        }

        String urlParameters = postData.toString();
        // Send post request
        con.setDoOutput(true);
        DataOutputStream wr = new DataOutputStream(con.getOutputStream());
        wr.writeBytes(urlParameters);
        wr.flush();
        wr.close();

        int responseCode = con.getResponseCode();
        List<String> cookies = con.getHeaderFields().get("Set-Cookie");

        if (responseCode == HttpURLConnection.HTTP_OK) { // success
            BufferedReader in = new BufferedReader(new InputStreamReader(
                    con.getInputStream()));
            String inputLine;
            StringBuffer response = new StringBuffer();

            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }
            in.close();
            response_properties = new HashMap<String, List<String>>();
            List<String> res = new ArrayList<String>();
            res.add(response.toString());
            response_properties.put("Set-Cookie", cookies);
            response_properties.put("Form", res);
            //print result
            System.out.println("Tony2 :" + response.toString());
        } else {
            logger.error("Error In sending request");
            logger.error(con.getResponseMessage());
        }
        return response_properties;
    }

    public String getCookies(List<String> cookies) {
        StringBuilder builder = new StringBuilder();
        for (String s : cookies) {
            builder.append(s.substring(0, s.indexOf(";") + 1) + " ");
        }
        return builder.toString().trim();
    }

//    ------------------------------------------
    public Properties getAppCookies(String Cookies) {
        List<String> cookieList = Arrays.asList(Cookies.split(";"));
        Properties TempCookies = new Properties();
        for (String cookie : cookieList) {
            TempCookies.put(getKey(cookie.trim()), getValue(cookie.trim()));
        }
        return TempCookies;
    }

    public String getKey(String prop) {
        return prop.substring(0, prop.indexOf("="));
    }

    public String getValue(String prop) {
        return prop.substring(prop.indexOf("="), prop.length());
    }

    public String parsePropertiesToString(Properties cookiesProp) {
        StringBuilder cookies = new StringBuilder();
        for (Map.Entry<?, ?> entry : cookiesProp.entrySet()) {
            String key = (String) entry.getKey();
            String value = (String) entry.getValue();
            cookies.append(key + value + "; ");
        }
        return cookies.toString();
    }

    private StringBuffer getInquiryRequest(RSRequest request) {
        StringBuffer provider_Request = new StringBuffer("<?xml version=\"1.0\" encoding=\"UTF - 8\" ?>");
        provider_Request.append("<REQUEST TYPE=\"SMARTCARDBALANCE\">");
        provider_Request.append("<USERNAME>").append(request.userName).append("</USERNAME>");
        provider_Request.append("<PASSWORD>").append(request.password).append("</PASSWORD>");
        provider_Request.append("<TERMINALID>" + TERMINALID + "</TERMINALID>");
        provider_Request.append("<LOCALDATETIME>" + getCurrentTimeStamp() + "</LOCALDATETIME>");
        provider_Request.append("<TXID>").append(request.requestId).append("</TXID>");
        provider_Request.append("<CARD>");
        provider_Request.append("<EAN>").append(EAN).append("</EAN>");
        provider_Request.append("<PAN>").append(request.mobileNumber).append("</PAN>");
        provider_Request.append("</CARD>");
        provider_Request.append("<CURRENCY>").append(CURRENCY).append("</CURRENCY>");
        provider_Request.append("<AMOUNT>").append(request.amount).append("</AMOUNT>");
        provider_Request.append("<RECEIPT>");
        provider_Request.append("<LANGUAGE>").append(LANGUAGE).append("</LANGUAGE>");
        provider_Request.append("<CHARSPERLINE>").append(CHARSPERLINE).append("</CHARSPERLINE>");
        provider_Request.append("</RECEIPT>");
        provider_Request.append("</REQUEST>");
        return provider_Request;

    }

    private StringBuffer getValidationRequest(RSRequest request) {
        StringBuffer provider_Request = new StringBuffer("<?xml version=\"1.0\" encoding=\"UTF - 8\" ?>");
        provider_Request.append("<REQUEST Type=\"SALE\" mode=\"CAPTURE\">");
        provider_Request.append("<USERNAME>").append(request.userName).append("</USERNAME>");
        provider_Request.append("<PASSWORD>").append(request.password).append("</PASSWORD>");
        provider_Request.append("<TERMINALID>" + TERMINALID + "</TERMINALID>");
        provider_Request.append("<LOCALDATETIME>" + getCurrentTimeStamp() + "</LOCALDATETIME>");
        provider_Request.append("<TXID>").append(request.requestId).append("</TXID>");
        provider_Request.append("<CARD>");
        provider_Request.append("<EAN>").append(EAN).append("</EAN>");
        provider_Request.append("<PAN>").append(request.mobileNumber).append("</PAN>");
        provider_Request.append("</CARD>");
        provider_Request.append("<CURRENCY>").append(CURRENCY).append("</CURRENCY>");
        provider_Request.append("<AMOUNT>").append((int) Math.floor(request.amount * 100)).append("</AMOUNT>");
        provider_Request.append("<RECEIPT>");
        provider_Request.append("<LANGUAGE>").append(LANGUAGE).append("</LANGUAGE>");
        provider_Request.append("<CHARSPERLINE>").append(CHARSPERLINE).append("</CHARSPERLINE>");
        provider_Request.append("</RECEIPT>");
        provider_Request.append("</REQUEST>");
        return provider_Request;

    }

    private StringBuffer getPaymentRequest(RSRequest request) {
        StringBuffer provider_Request = new StringBuffer("<?xml version=\"1.0\" encoding=\"UTF - 8\" ?>");
        provider_Request.append("<REQUEST Type=\"SALE\" mode=\"RESERVE\">");
        provider_Request.append("<USERNAME>").append(request.userName).append("</USERNAME>");
        provider_Request.append("<PASSWORD>").append(request.password).append("</PASSWORD>");
        provider_Request.append("<TERMINALID>" + TERMINALID + "</TERMINALID>");
        provider_Request.append("<LOCALDATETIME>" + getCurrentTimeStamp() + "</LOCALDATETIME>");
        provider_Request.append("<TXID>").append(request.requestId).append("</TXID>");
        provider_Request.append("<CARD>");
        provider_Request.append("<EAN>").append(EAN).append("</EAN>");
        provider_Request.append("<PAN>").append(request.mobileNumber).append("</PAN>");
        provider_Request.append("</CARD>");
        provider_Request.append("<CURRENCY>").append(CURRENCY).append("</CURRENCY>");
        
        BigDecimal amount = new BigDecimal(String.valueOf(request.amount));
        BigDecimal BDa = new BigDecimal("100");
        BigDecimal finalAmount = amount.multiply(BDa);
        provider_Request.append("<AMOUNT>").append(finalAmount.intValue()).append("</AMOUNT>");
        provider_Request.append("<RECEIPT>");
        provider_Request.append("<LANGUAGE>").append(LANGUAGE).append("</LANGUAGE>");
        provider_Request.append("<CHARSPERLINE>").append(CHARSPERLINE).append("</CHARSPERLINE>");
        provider_Request.append("</RECEIPT>");
        provider_Request.append("</REQUEST>");
        return provider_Request;

    }

    private String send(String xml) throws Exception {
        URL url = new URL(HOST);
        URLConnection conn;
        if (HOST.contains("https")) {
            // Create a trust manager that does not validate certificate chains
            TrustManager[] trustAllCerts = new TrustManager[]{new X509TrustManager() {
                public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                    return null;
                }

                public void checkClientTrusted(X509Certificate[] certs, String authType) {
                }

                public void checkServerTrusted(X509Certificate[] certs, String authType) {
                }
            }};

            // Install the all-trusting trust manager
            final SSLContext sc = SSLContext.getInstance("SSL");
            sc.init(null, trustAllCerts, new java.security.SecureRandom());
            HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
            // Create all-trusting host name verifier
            HostnameVerifier allHostsValid = new HostnameVerifier() {
                public boolean verify(String hostname, SSLSession session) {
                    return true;
                }
            };
            // Install the all-trusting host verifier
            HttpsURLConnection.setDefaultHostnameVerifier(allHostsValid);

            conn = (HttpsURLConnection) url.openConnection();
        } else {
            conn = url.openConnection();
        }
        conn.setRequestProperty("Content-Type", "text/xml");
        conn.setDoOutput(true);

        BufferedOutputStream bos = new BufferedOutputStream(conn.getOutputStream());
        bos.write(xml.getBytes("UTF-8"));
        bos.close();

        BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream()));
        String inputLine;
        StringBuffer response = new StringBuffer();

        while ((inputLine = in.readLine()) != null) {
            response.append(inputLine);
        }
        in.close();

        return response.toString();
    }

    public static String getCurrentTimeStamp() {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date now = new Date();
        String strDate = sdf.format(now);
        return strDate;
    }

}
