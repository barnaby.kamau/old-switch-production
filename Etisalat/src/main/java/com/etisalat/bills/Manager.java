/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.etisalat.bills;

/**
 *
 * @author Antonios_knaguib
 */
import com.etisalat.entites.RSRequest;
import com.etisalat.entites.RSResponse;
import com.etisalat.provider.Provider_Manager;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import static javax.ws.rs.core.MediaType.APPLICATION_JSON;
import javax.ws.rs.core.Response;
import static javax.ws.rs.core.Response.ok;
import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.json.JSONException;
import org.json.JSONObject;

@Path("/")
@Produces(APPLICATION_JSON)
public class Manager {

    public static Logger logger = Logger.getLogger("R");

    @GET
    public Response inquiry(@QueryParam("request_id") String request_id, @QueryParam("phone") String phone, @QueryParam("userName") String userName, @QueryParam("password") String password) throws JSONException {
        RSResponse response = new RSResponse();
        RSRequest request = new RSRequest();
        request.requestId = request_id;
        request.requestType = 1;
        request.mobileNumber = phone;
        request.userName = userName;
        request.password = password;
        logger.info("Inquiry Request : " + request);
        Provider_Manager provider = new Provider_Manager();
        response = provider.inquiry(request);
        logger.info("Inquiry Response : " + response);
        return ok(response, APPLICATION_JSON).build();
    }

    @POST
    public Response payment(@QueryParam("request_id") String request_id, @QueryParam("phone") String phone, @QueryParam("amount") double amount, @QueryParam("userName") String userName, @QueryParam("password") String password) throws JSONException {
        RSResponse response = new RSResponse();
        RSRequest request = new RSRequest();
        request.requestId = request_id;
        request.requestType = 2;
        request.mobileNumber = phone;
        request.amount = amount;
        request.userName = userName;
        request.password = password;
        logger.info("Payment Request : " + request);
        Provider_Manager provider = new Provider_Manager();
        response = provider.payment(request);
        logger.info("Payment Response : " + response);
        return ok(response, APPLICATION_JSON).build();
    }

}
