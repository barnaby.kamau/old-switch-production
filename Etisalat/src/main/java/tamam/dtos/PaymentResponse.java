/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tamam.dtos;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 *
 * @author Antonios_knaguib
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SALE")
@XmlRootElement(name = "RESPONSE")
public class PaymentResponse {

    @XmlAttribute(name = "TERMINALID", required = true)
    protected String terminalId;
    @XmlElement(name = "LOCALDATETIME", required = true)
    protected String localDateTime;
    @XmlElement(name = "SERVERDATETIME", required = true)
    protected String serverDateTime;
    @XmlElement(name = "TXID", required = true)
    protected String trxId;
    @XmlElement(name = "HOSTTXID", required = true)
    protected String providertrxId;
    @XmlElement(name = "AMOUNT", required = true)
    protected double amount;
    @XmlElement(name = "CURRENCY", required = true)
    protected int currency;
    @XmlElement(name = "LIMIT", required = true)
    protected double limit;
    @XmlElement(name = "RESULT", required = true)
    protected int statusCode;
    @XmlElement(name = "RESULTTEXT", required = true)
    protected String statusMessage;

    public String getTerminalId() {
        return terminalId;
    }

    public void setTerminalId(String terminalId) {
        this.terminalId = terminalId;
    }

    public String getLocalDateTime() {
        return localDateTime;
    }

    public void setLocalDateTime(String localDateTime) {
        this.localDateTime = localDateTime;
    }

    public String getServerDateTime() {
        return serverDateTime;
    }

    public void setServerDateTime(String serverDateTime) {
        this.serverDateTime = serverDateTime;
    }

    public String getTrxId() {
        return trxId;
    }

    public void setTrxId(String trxId) {
        this.trxId = trxId;
    }

    public String getProvidertrxId() {
        return providertrxId;
    }

    public void setProvidertrxId(String providertrxId) {
        this.providertrxId = providertrxId;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public int getCurrency() {
        return currency;
    }

    public void setCurrency(int currency) {
        this.currency = currency;
    }

    public double getLimit() {
        return limit;
    }

    public void setLimit(double limit) {
        this.limit = limit;
    }

    public int getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(int statusCode) {
        this.statusCode = statusCode;
    }

    public String getStatusMessage() {
        return statusMessage;
    }

    public void setStatusMessage(String statusMessage) {
        this.statusMessage = statusMessage;
    }

}
