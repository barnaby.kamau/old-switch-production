/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mobinil.provider;

import static com.mobinil.bills.Manager.logger;
import com.mobinil.entites.RSRequest;
import com.mobinil.entites.RSResponse;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.math.BigDecimal;
import java.net.URL;
import java.net.URLConnection;
import java.security.cert.X509Certificate;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import org.apache.commons.lang.StringUtils;
import tamam.dtos.CaptureResponse;
import tamam.dtos.InquiryResponse;
import tamam.dtos.PaymentResponse;

/**
 *
 * @author Antonios_knaguib
 */
public class Tamam implements Provider {

    private static HashMap<Integer, String> TRANSACTION_STATUS_CODES = new HashMap<>();
    private static HashMap<Integer, String> PROVIDER_STATUS_CODES = new HashMap<>();
//    private static final String TERMINALID = "UA007871";
    private static final String TERMINALID = "UA007819";
    private static final String EAN = "4251604168771";
    private static final String CURRENCY = "818";
    private static final String LANGUAGE = "ENG";
    private static final String CHARSPERLINE = "40";
    private static final String HOST = "https://precision.epayworldwide.com/up-interface";
//    private static final String HOST = "https://precision.epayworldwide.com/up-interface";

    static {
        TRANSACTION_STATUS_CODES.put(0, "Initialized");
        TRANSACTION_STATUS_CODES.put(1, "Under process");
        TRANSACTION_STATUS_CODES.put(2, "Success");
        TRANSACTION_STATUS_CODES.put(3, "Error");
        TRANSACTION_STATUS_CODES.put(4, "Canceled");
        TRANSACTION_STATUS_CODES.put(8, "Deposit error");
        TRANSACTION_STATUS_CODES.put(20007, "Data not found");
        PROVIDER_STATUS_CODES.put(0, "Successful");
        PROVIDER_STATUS_CODES.put(11000, "Authentication failed. Incorrect login or password");
        PROVIDER_STATUS_CODES.put(11001, "Service list cannot be updated");
        PROVIDER_STATUS_CODES.put(11002, "Transaction not accepted");
        PROVIDER_STATUS_CODES.put(11003, "Check transaction status is failed");
        PROVIDER_STATUS_CODES.put(20000, "General error");
        PROVIDER_STATUS_CODES.put(20001, "Request action version error");
        PROVIDER_STATUS_CODES.put(20002, "Service not allowed");
        PROVIDER_STATUS_CODES.put(20003, "Incomplete request");
        PROVIDER_STATUS_CODES.put(20004, "Wrong action");
        PROVIDER_STATUS_CODES.put(20005, "Incorrect XML request");
        PROVIDER_STATUS_CODES.put(20006, "No permission");
        PROVIDER_STATUS_CODES.put(20007, "Data not found");
        PROVIDER_STATUS_CODES.put(20008, "Duplicate transaction");
        PROVIDER_STATUS_CODES.put(20009, "External system must update service list");
        PROVIDER_STATUS_CODES.put(20010, "Deposit error, insufficient deposit to perform operation");
    }

    @Override
    public RSResponse inquiry(RSRequest request) {
        RSResponse response = new RSResponse();
        response.request_id = request.requestId;

        try {

            StringBuffer provider_Request = getInquiryRequest(request);
            logger.info("Provider Inquiry Request : " + provider_Request.toString().replace(request.password, StringUtils.repeat("*", request.password.length())));
            logger.info("Provider Inquiry Request Creation ended");
            logger.info("Provider Inquiry Request Sending started");
            String provider_Response = send(provider_Request.toString());
            logger.info("Provider Inquiry Response : " + provider_Response);
            logger.info("Provider Inquiry Request Sending ended");
            logger.info("Provider Inquiry Response Unmarshal ended");
            GenericContentHandler handler = new GenericContentHandler();
            InquiryResponse ProviderResponse = handler.unMarshal(provider_Response.toString(), InquiryResponse.class);
            logger.info("Provider Inquiry Response Unmarshal ended");
            if (ProviderResponse.getStatusCode() == 0) {
                response.Total_Amount = ProviderResponse.getCurrentBalance();
                response.Code = 200;
                response.Message = "Successful Inquiry.";
                return response;
            } else {
                response.Code = -2;
                response.Message = "Provider Error : " + ProviderResponse.getStatusMessage();
                return response;
            }
        } catch (Exception e) {
            response.Code = -3;
            response.Message = "General Error : " + e;
            return response;
        }
    }

    @Override
    public RSResponse payment(RSRequest request) {
        RSResponse response = new RSResponse();
        response.request_id = request.requestId;
//      Validations
        try {
            int code = isValid(request);
//          if (code == 9998) {
//              response.Code = -100;
//              response.Message = "Can't pay for this number.";
//              return response;
//          } else 
            if (code != 0) {
                if (code == 16 || code == 20) {
                    response.Code = -100;
                    response.Message = "Can't pay for this number now. Please try again later";
                    return response;
                } else {
                    response.Code = -101;
                    response.Message = "Can't pay for this number now. Please try again later";
                    return response;
                }
            }
            if (request.amount <= 0 || request.amount > 5000) {
                response.Code = -1;
                response.Message = "Invalid Amount must be greater than 0.0 and less than 5000";
                return response;
            }
        } catch (Exception e) {
            response.Code = -1;
            response.Message = "Invalid Request.";
            return response;
        }

        try {

            StringBuffer provider_Request = getPaymentRequest(request);
            logger.info("Provider Payment Request : " + provider_Request.toString().replace(request.password, StringUtils.repeat("*", request.password.length())));
            logger.info("Provider Payment Request Creation ended");
            logger.info("Provider Payment Request Sending started");
            String provider_Response = send(provider_Request.toString());
            logger.info("Provider Payment Response : " + provider_Response);
            logger.info("Provider Payment Request Sending ended");
            logger.info("Provider Payment Response Unmarshal ended");
            GenericContentHandler handler = new GenericContentHandler();
            PaymentResponse ProviderResponse = handler.unMarshal(provider_Response.toString(), PaymentResponse.class);
            logger.info("Provider Payment Response Unmarshal ended");
            logger.info("ProviderResponse.getStatusCode() >>>>>> " + ProviderResponse.getStatusCode());
            if (ProviderResponse.getStatusCode() == 0) {
                response.Code = 200;
                response.Message = "Successful Payment.";
                response.Total_Amount = request.amount;
                response.request_id = request.requestId;
                response.ProviderTransactionId = ProviderResponse.getProvidertrxId();
                return response;
            } else if (ProviderResponse.getStatusCode() == 9998) {
                response.Code = 201;
                response.Message = "Pending transaction check with provider, Provider Error : " + ProviderResponse.getStatusMessage();
                return response;
            } else if (ProviderResponse.getStatusCode() == 16 || ProviderResponse.getStatusCode() == 20) {
                response.Code = -100;
//                response.Message = "Can't pay for this number , Provider Error : " + ProviderResponse.getStatusMessage();
                response.Message = ProviderResponse.getStatusMessage();
                return response;
            } else {
                response.Code = -101;
                response.Message = "Can't pay for this number now. Please try again later , Provider Error : " + ProviderResponse.getStatusMessage();
                return response;
            }
        } catch (Exception e) {
            logger.error(e);
            response.Code = -3;
            response.Message = "General Error : " + e;
            return response;
        }
    }

    private int isValid(RSRequest request) {
        int code = -1;
        try {
            StringBuffer providerRequest = getValidationRequest(request);
            logger.info("Provider Validation Request : " + providerRequest.toString().replace(request.password, StringUtils.repeat("*", request.password.length())));
            logger.info("Provider Validation Request Creation ended");
            logger.info("Provider Validation Request Sending started");
            String provider_Response = send(providerRequest.toString());
//            String provider_Response = "<?xml version=\"1.0\" encoding=\"UTF-8\" ?>\n"
//                    + "<RESPONSE TYPE=\"SALE\">\n"
//                    + "   <TERMINALID>UAE12854</TERMINALID>\n"
//                    + "   <LOCALDATETIME>2016-05-05 02:01:20</LOCALDATETIME>\n"
//                    + "   <SERVERDATETIME>2016-05-05 12:01:41</SERVERDATETIME>\n"
//                    + "   <TXID>1</TXID>\n"
//                    + "   <HOSTTXID>UAE12854000000</HOSTTXID>\n"
//                    + "   <AMOUNT>1</AMOUNT>\n"
//                    + "   <CURRENCY>818</CURRENCY>\n"
//                    + "   <LIMIT>0</LIMIT>\n"
//                    + "   <RESULT>0</RESULT>\n"
//                    + "   <RESULTTEXT>transaction successful</RESULTTEXT>\n"
//                    + "</RESPONSE>";
            logger.info("Provider Validation Response : " + provider_Response);
            logger.info("Provider Validation Request Sending ended");
            logger.info("Provider Validation Response Unmarshal ended");
            GenericContentHandler handler = new GenericContentHandler();
            CaptureResponse ProviderResponse = handler.unMarshal(provider_Response.toString(), CaptureResponse.class);
            logger.info("Provider Validation Response Unmarshal ended");
            code = ProviderResponse.getStatusCode();
        } catch (Exception e) {

        }
        return code;
    }

    private StringBuffer getInquiryRequest(RSRequest request) {
        StringBuffer provider_Request = new StringBuffer("<?xml version=\"1.0\" encoding=\"UTF - 8\" ?>");
        provider_Request.append("<REQUEST TYPE=\"SMARTCARDBALANCE\">");
        provider_Request.append("<USERNAME>").append(request.userName).append("</USERNAME>");
        provider_Request.append("<PASSWORD>").append(request.password).append("</PASSWORD>");
        provider_Request.append("<TERMINALID>" + TERMINALID + "</TERMINALID>");
        provider_Request.append("<LOCALDATETIME>" + getCurrentTimeStamp() + "</LOCALDATETIME>");
        provider_Request.append("<TXID>").append(request.requestId).append("</TXID>");
        provider_Request.append("<CARD>");
        provider_Request.append("<EAN>").append(EAN).append("</EAN>");
        provider_Request.append("<PAN>").append(request.mobileNumber).append("</PAN>");
        provider_Request.append("</CARD>");
        provider_Request.append("<CURRENCY>").append(CURRENCY).append("</CURRENCY>");
        provider_Request.append("<AMOUNT>").append(request.amount).append("</AMOUNT>");
        provider_Request.append("<RECEIPT>");
        provider_Request.append("<LANGUAGE>").append(LANGUAGE).append("</LANGUAGE>");
        provider_Request.append("<CHARSPERLINE>").append(CHARSPERLINE).append("</CHARSPERLINE>");
        provider_Request.append("</RECEIPT>");
        provider_Request.append("</REQUEST>");
        return provider_Request;

    }

    private StringBuffer getPaymentRequest(RSRequest request) {
        StringBuffer provider_Request = new StringBuffer("<?xml version=\"1.0\" encoding=\"UTF - 8\" ?>");
        provider_Request.append("<REQUEST Type=\"SALE\" mode=\"CAPTURE\">");
        provider_Request.append("<USERNAME>").append(request.userName).append("</USERNAME>");
        provider_Request.append("<PASSWORD>").append(request.password).append("</PASSWORD>");
        provider_Request.append("<TERMINALID>" + TERMINALID + "</TERMINALID>");
        provider_Request.append("<LOCALDATETIME>" + getCurrentTimeStamp() + "</LOCALDATETIME>");
        provider_Request.append("<TXID>").append(request.requestId).append("</TXID>");
        provider_Request.append("<CARD>");
        provider_Request.append("<EAN>").append(EAN).append("</EAN>");
        provider_Request.append("<PAN>").append(request.mobileNumber).append("</PAN>");
        provider_Request.append("</CARD>");
        provider_Request.append("<CURRENCY>").append(CURRENCY).append("</CURRENCY>");
        provider_Request.append("<AMOUNT>").append((int) Math.floor(new BigDecimal(String.valueOf(request.amount)).multiply(new BigDecimal("100")).doubleValue())).append("</AMOUNT>");
        provider_Request.append("<RECEIPT>");
        provider_Request.append("<LANGUAGE>").append(LANGUAGE).append("</LANGUAGE>");
        provider_Request.append("<CHARSPERLINE>").append(CHARSPERLINE).append("</CHARSPERLINE>");
        provider_Request.append("</RECEIPT>");
        provider_Request.append("</REQUEST>");
        return provider_Request;

    }

    private StringBuffer getValidationRequest(RSRequest request) {
        StringBuffer provider_Request = new StringBuffer("<?xml version=\"1.0\" encoding=\"UTF - 8\" ?>");
        provider_Request.append("<REQUEST Type=\"SALE\" mode=\"RESERVE\">");
        provider_Request.append("<USERNAME>").append(request.userName).append("</USERNAME>");
        provider_Request.append("<PASSWORD>").append(request.password).append("</PASSWORD>");
        provider_Request.append("<TERMINALID>" + TERMINALID + "</TERMINALID>");
        provider_Request.append("<LOCALDATETIME>" + getCurrentTimeStamp() + "</LOCALDATETIME>");
        provider_Request.append("<TXID>").append(request.requestId).append("</TXID>");
        provider_Request.append("<CARD>");
        provider_Request.append("<EAN>").append(EAN).append("</EAN>");
        provider_Request.append("<PAN>").append(request.mobileNumber).append("</PAN>");
        provider_Request.append("</CARD>");
        provider_Request.append("<CURRENCY>").append(CURRENCY).append("</CURRENCY>");
        provider_Request.append("<AMOUNT>").append((int) Math.floor(request.amount * 100)).append("</AMOUNT>");
        provider_Request.append("<RECEIPT>");
        provider_Request.append("<LANGUAGE>").append(LANGUAGE).append("</LANGUAGE>");
        provider_Request.append("<CHARSPERLINE>").append(CHARSPERLINE).append("</CHARSPERLINE>");
        provider_Request.append("</RECEIPT>");
        provider_Request.append("</REQUEST>");
        return provider_Request;

    }

    private String send(String xml) throws Exception {
        URL url = new URL(HOST);
        URLConnection conn;
        if (HOST.contains("https")) {
            // Create a trust manager that does not validate certificate chains
            TrustManager[] trustAllCerts = new TrustManager[]{new X509TrustManager() {
                public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                    return null;
                }

                public void checkClientTrusted(X509Certificate[] certs, String authType) {
                }

                public void checkServerTrusted(X509Certificate[] certs, String authType) {
                }
            }};

            // Install the all-trusting trust manager
            final SSLContext sc = SSLContext.getInstance("SSL");
            sc.init(null, trustAllCerts, new java.security.SecureRandom());
            HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
            // Create all-trusting host name verifier
            HostnameVerifier allHostsValid = new HostnameVerifier() {
                public boolean verify(String hostname, SSLSession session) {
                    return true;
                }
            };
            // Install the all-trusting host verifier
            HttpsURLConnection.setDefaultHostnameVerifier(allHostsValid);

            conn = (HttpsURLConnection) url.openConnection();
        } else {
            conn = url.openConnection();
        }
        conn.setRequestProperty("Content-Type", "text/xml");
        conn.setDoOutput(true);

        BufferedOutputStream bos = new BufferedOutputStream(conn.getOutputStream());
        bos.write(xml.getBytes("UTF-8"));
        bos.close();

        BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream()));
        String inputLine;
        StringBuffer response = new StringBuffer();

        while ((inputLine = in.readLine()) != null) {
            response.append(inputLine);
        }
        in.close();

        return response.toString();
    }

    public static String getCurrentTimeStamp() {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date now = new Date();
        String strDate = sdf.format(now);
        return strDate;
    }

}
