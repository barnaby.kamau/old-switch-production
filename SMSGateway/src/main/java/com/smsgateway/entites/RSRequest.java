/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smsgateway.entites;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

/**
 *
 * @author Antonios_knaguib
 */
public class RSRequest {

    public String accountId;
    public String password;
    public String senderName;
    public String SMSText;
    public String receiverMSISDN;
    public String secretkey;
    public int providerID;
    

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this,
                ToStringStyle.MULTI_LINE_STYLE);
    }

}
