/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smsgateway.manager;

/**
 *
 * @author Antonios_knaguib
 */
import com.smsgateway.entites.RSRequest;
import com.smsgateway.entites.RSResponse;
import com.smsgateway.provider.Provider;
import com.smsgateway.provider.Vodafone;
import java.util.Arrays;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import static javax.ws.rs.core.MediaType.APPLICATION_JSON;
import javax.ws.rs.core.Response;
import static javax.ws.rs.core.Response.ok;
import org.apache.log4j.Logger;
import org.json.JSONException;

@Path("/")
@Produces(APPLICATION_JSON)
public class Manager {

    public static Logger logger = Logger.getLogger("R");
    public static int VODAFONE_PROVIDER = 1;

    @POST
    public Response sendSMS(RSRequest request) throws JSONException {
        RSResponse response = new RSResponse();
        logger.info("sendSMS Request : " + request);
        Provider provider = getProvider(request);
        response = provider.sendSMS(request);
        response.providerID = request.providerID;
        logger.info("sendSMS Response : " + response);
        return ok(response, APPLICATION_JSON).build();
    }

    private Provider getProvider(RSRequest request) {
        if (request.providerID == 1) {
            return new Vodafone();
        } else if (request.providerID == 2) {
            return new Vodafone();
        } else {
            return new Vodafone();
        }
    }

    public boolean contains(final int[] arr, final int key) {
        return Arrays.stream(arr).anyMatch(i -> i == key);
    }
}
