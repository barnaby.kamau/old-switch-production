/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smsgateway.provider;

import vodafone.dtos.VodafoneResponse;
import com.smsgateway.entites.RSRequest;
import com.smsgateway.entites.RSResponse;
import static com.smsgateway.manager.Manager.logger;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.net.URLConnection;
import java.nio.charset.Charset;
import java.security.InvalidKeyException;
import java.security.Key;
import java.security.NoSuchAlgorithmException;
import java.security.SignatureException;
import java.security.cert.X509Certificate;
import java.util.Formatter;
import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.lang.StringUtils;

/**
 *
 * @author Tony
 */
public class Vodafone implements Provider {

    private static final String HOST = "https://e3len.vodafone.com.eg/web2sms/sms/submit/";
    private static final String HASH_ALGORITHM = "HmacSHA256";

    public RSResponse sendSMS(RSRequest request) {
        RSResponse response = new RSResponse();

        try {
            logger.info("[Vodafone] Provider Request Creation started");

            StringBuffer provider_Request = getProviderRequest(request);
            logger.info("[Vodafone] Provider Request : " + provider_Request.toString().replace(request.password, StringUtils.repeat("*", request.password.length())));
            logger.info("[Vodafone] Provider Request Creation ended");
            logger.info("[Vodafone] Provider Request Sending started");
            logger.info("[Vodafone] Host creation started");
            logger.info("[Vodafone] Host : " + HOST.replace(request.password, StringUtils.repeat("*", request.password.length())));
            logger.info("[Vodafone] Host Creation ended");
            String provider_Response = send(HOST, provider_Request.toString());
            logger.info("[Vodafone] Provider Response : " + provider_Response);
            logger.info("[Vodafone] Provider Request Sending ended");
            logger.info("[Vodafone] Provider Response Unmarshal ended");
            GenericContentHandler handler = new GenericContentHandler();
//            logger.info(provider_Response.replace(" xmlns=\"http://www.edafa.com/web2sms/sms/model/\"", ""));
            VodafoneResponse ProviderResponse = handler.unMarshal(provider_Response.replace(" xmlns=\"http://www.edafa.com/web2sms/sms/model/\"", ""), VodafoneResponse.class);
//            VodafoneResponse ProviderResponse = handler.unMarshal("<?xml version=\"1.0\" encoding=\"UTF-8\"?><SubmitSMSResponse><SMSStatus>SUBMITTED</SMSStatus><ResultStatus>SUCCESS</ResultStatus></SubmitSMSResponse>", VodafoneResponse.class);
            logger.info("[Vodafone] Provider Response Unmarshal ended");
            if (ProviderResponse.getResultStatus().equals("SUCCESS")) {
                response.code = 200;
                response.message = ProviderResponse.getResultStatus();
            } else {
                logger.error("[Vodafone] Provider Error : " + ProviderResponse.getDescription());
                response.code = -2;
                response.message = "[Vodafone] Provider Error : " + ProviderResponse.getDescription();
            }

        } catch (Exception e) {
            logger.error(e);
            response.code = -3;
            response.message = "General Error : " + e;
        }

        return response;
    }

    private String send(String fullURL, String xml) throws Exception {
        URL url = new URL(fullURL);
        URLConnection conn;
        if (HOST.contains("https")) {
            // Create a trust manager that does not validate certificate chains
            TrustManager[] trustAllCerts = new TrustManager[]{new X509TrustManager() {
                public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                    return null;
                }

                public void checkClientTrusted(X509Certificate[] certs, String authType) {
                }

                public void checkServerTrusted(X509Certificate[] certs, String authType) {
                }
            }};

            // Install the all-trusting trust manager
            final SSLContext sc = SSLContext.getInstance("SSL");
            sc.init(null, trustAllCerts, new java.security.SecureRandom());
            HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
            // Create all-trusting host name verifier
            HostnameVerifier allHostsValid = new HostnameVerifier() {
                public boolean verify(String hostname, SSLSession session) {
                    return true;
                }
            };
            // Install the all-trusting host verifier
            HttpsURLConnection.setDefaultHostnameVerifier(allHostsValid);

            conn = (HttpsURLConnection) url.openConnection();
        } else {
            conn = url.openConnection();
        }
        conn.setRequestProperty("Content-Type", "application/xml");
        conn.setRequestProperty("Accept-Language", "en-us");
        conn.setDoOutput(true);

        BufferedOutputStream bos = new BufferedOutputStream(conn.getOutputStream());
        bos.write(xml.getBytes("UTF-8"));
        bos.close();

        BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream()));
        String inputLine;
        StringBuffer response = new StringBuffer();

        while ((inputLine = in.readLine()) != null) {
            response.append(inputLine);
        }
        in.close();

        return response.toString();
    }

    private StringBuffer getProviderRequest(RSRequest request) throws UnsupportedEncodingException {
        StringBuffer provider_Request = new StringBuffer("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
        provider_Request.append("<SubmitSMSRequest xmlns=\"http://www.edafa.com/web2sms/sms/model/\">");
        provider_Request.append("<AccountId>").append(request.accountId).append("</AccountId>");
        provider_Request.append("<Password>").append(request.password).append("</Password>");
        provider_Request.append("<SecureHash>").append(getSecureHashe(request).toUpperCase()).append("</SecureHash>");
        provider_Request.append("<SMSList>");
        provider_Request.append("<SenderName>").append(request.senderName).append("</SenderName>");
        provider_Request.append("<ReceiverMSISDN>").append(request.receiverMSISDN).append("</ReceiverMSISDN>");
//        provider_Request.append("<SMSText>").append(request.SMSText).append("</SMSText>");
        provider_Request.append("<SMSText>").append(new String(request.SMSText.getBytes("Windows-1252"), "UTF-8")).append("</SMSText>");
        provider_Request.append("</SMSList>");
        provider_Request.append("</SubmitSMSRequest>");
        return provider_Request;

    }

    private String getSecureHashe(RSRequest request) {
        try {
//            logger.info("Charset : " + Charset.defaultCharset().name());
//            logger.info(hashMac("AccountId=" + request.accountId + "&Password=" + request.password + "&SenderName=" + request.senderName + "&ReceiverMSISDN=" + request.receiverMSISDN + "&SMSText=" + new String(request.SMSText.getBytes("Windows-1252"), "UTF-8"), request.secretkey));
//            byte[] sourceBytes = getRawBytes();
//            String data = new String(sourceBytes, "Windows-1252");
//            byte[] destinationBytes = data.getBytes("UTF-8");
//            HMACGenerator generator = new HMACGenerator();
//            return generator.createHMAC("HMac-SHA256", "5D0655B50EBB4E6696C2356C64C2942F", "AccountId=" + request.accountId + "&Password=" + request.password + "&SenderName=" + request.senderName + "&ReceiverMSISDN=" + request.receiverMSISDN + "&SMSText=" + request.SMSText).toUpperCase();
            return hashMac("AccountId=" + request.accountId + "&Password=" + request.password + "&SenderName=" + request.senderName + "&ReceiverMSISDN=" + request.receiverMSISDN + "&SMSText=" + new String(request.SMSText.getBytes("Windows-1252"), "UTF-8"), request.secretkey);
//            return hashMac("AccountId=" + request.accountId + "&Password=" + request.password + "&SenderName=" + request.senderName + "&ReceiverMSISDN=" + request.receiverMSISDN + "&SMSText=" + "تيست", request.secretkey);
        } catch (Exception ex) {

            return "Error in hashing";
        }
    }

    public String hashMac(String text, String secretKey) throws SignatureException, InvalidKeyException, NoSuchAlgorithmException, UnsupportedEncodingException {

        try {
            Key sk = new SecretKeySpec(secretKey.getBytes(), HASH_ALGORITHM);
            Mac mac = Mac.getInstance(sk.getAlgorithm());
            mac.init(sk);
            final byte[] hmac = mac.doFinal(text.getBytes("UTF-8"));
            return toHexString(hmac);
        } catch (NoSuchAlgorithmException e1) {
            // throw an exception or pick a different encryption method
            throw new SignatureException("error building signature, no such algorithm in device " + HASH_ALGORITHM);
        } catch (InvalidKeyException e) {
            throw new SignatureException("error building signature, invalid key " + HASH_ALGORITHM);
        }
    }

    public String toHexString(byte[] bytes) {
        StringBuilder sb = new StringBuilder(bytes.length * 2);

        Formatter formatter = new Formatter(sb);
        for (byte b : bytes) {
            formatter.format("%02x", b);
        }

        return sb.toString();
    }

    private static String escapeNonAscii(String str) {

        StringBuilder retStr = new StringBuilder();
        for (int i = 0; i < str.length(); i++) {
            int cp = Character.codePointAt(str, i);
            int charCount = Character.charCount(cp);
            if (charCount > 1) {
                i += charCount - 1; // 2.
                if (i >= str.length()) {
                    throw new IllegalArgumentException("truncated unexpectedly");
                }
            }

            if (cp < 128) {
                retStr.appendCodePoint(cp);
            } else {
                retStr.append(String.format("\\u%x", cp));
            }
        }
        return retStr.toString();
    }
}
