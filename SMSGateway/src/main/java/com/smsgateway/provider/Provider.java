/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smsgateway.provider;

import com.smsgateway.entites.RSRequest;
import com.smsgateway.entites.RSResponse;

/**
 *
 * @author Tony
 */
public interface Provider {

    public RSResponse sendSMS(RSRequest request);
}
