/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vodafone.dtos;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 *
 * @author Antonios_knaguib
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SubmitSMSResponse")
@XmlRootElement(name = "SubmitSMSResponse")
public class VodafoneResponse {

    @XmlElement(name = "SMSStatus", required = true)
    protected String SMSStatus;
    @XmlElement(name = "ResultStatus", required = true)
    protected String ResultStatus;
    @XmlElement(name = "Description", required = false)
    protected String Description;

    public String getSMSStatus() {
        return SMSStatus;
    }

    public void setSMSStatus(String SMSStatus) {
        this.SMSStatus = SMSStatus;
    }

    public String getResultStatus() {
        return ResultStatus;
    }

    public void setResultStatus(String ResultStatus) {
        this.ResultStatus = ResultStatus;
    }

    public String getDescription() {
        return Description;
    }

    public void setDescription(String Description) {
        this.Description = Description;
    }
    

}
