Drop TABLE REQUEST;
Drop TABLE PROCESSING_STATUS;
Drop TABLE CUSTOMER_STATUS;
Drop TABLE REQUEST_STATUS;
Drop TABLE PROVIDER_STATUS;
Drop TABLE PROVIDER_SERVICE;
Drop TABLE REQUEST_TYPE;
Drop TABLE LANG;


DROP FUNCTION getCustomerStatus;
DROP PROCEDURE insert_Request;
DROP FUNCTION isActive;
DROP FUNCTION getServicePrice;
DROP FUNCTION isValid;
DROP FUNCTION authenticate;
DROP FUNCTION isNotRepeated;
DROP FUNCTION isValidateRequest;
DROP PROCEDURE insert_Topup_Request;
DROP PROCEDURE PROCESS_TOPUP_REQUEST;
DROP SEQUENCE REQUEST_SEQ;

CREATE TABLE [dbo].[ERROR_LOG](
ERROR_DATE DATETIME,
ERROR_NUMBER NVARCHAR(500),
ERROR_SEVERITY NVARCHAR(500),
ERROR_STATE NVARCHAR(500),
ERROR_PROCEDURE NVARCHAR(500),
ERROR_LINE NVARCHAR(500),
ERROR_MESSAGE NVARCHAR(500)
);

CREATE SEQUENCE REQUEST_SEQ
  AS BIGINT
  START WITH 1
  INCREMENT BY 1
  MINVALUE 1
  MAXVALUE 999999999999
  NO CYCLE
  CACHE 10;
  
CREATE TABLE [dbo].[LANG](
ID INT PRIMARY KEY,
NAME NVARCHAR(50)
);

CREATE TABLE [dbo].[PROVIDER](
ID INT PRIMARY KEY,
NAME NVARCHAR(50)
);

CREATE TABLE [dbo].[PROVIDER_SERVICE](
ID INT PRIMARY KEY,
SERVICE_ID INT,
PROVIDER_ID INT,
FOREIGN KEY (PROVIDER_ID) REFERENCES PROVIDER(ID)
);

CREATE TABLE [dbo].[REQUEST_TYPE](
ID INT PRIMARY KEY,
NAME NVARCHAR(50)
);

CREATE TABLE [dbo].[PROCESSING_STATUS](
ID INT PRIMARY KEY,
NAME NVARCHAR(50)
);

CREATE TABLE [dbo].[REQUEST_STATUS](
ID INT PRIMARY KEY,
NAME NVARCHAR(50)
);

CREATE TABLE [dbo].[CUSTOMER_STATUS](
ID INT PRIMARY KEY,
CODE NVARCHAR(50),
MESSAGE NVARCHAR(900),
LANG_ID INT,
REQUEST_STATUS_ID INT,
FOREIGN KEY (REQUEST_STATUS_ID) REFERENCES REQUEST_STATUS(ID),
FOREIGN KEY (LANG_ID) REFERENCES LANG(ID)
);

CREATE TABLE [dbo].[PROVIDER_STATUS](
ID INT PRIMARY KEY,
PROVIDER_SERVICE_ID INT,
CODE NVARCHAR(50),
--FOREIGN KEY (CODE) REFERENCES CUSTOMER_STATUS(CODE),
FOREIGN KEY (PROVIDER_SERVICE_ID) REFERENCES PROVIDER_SERVICE(ID)
);


CREATE TABLE REQUEST(
ID INT PRIMARY KEY,
USER_ID INT,
PHONE NVARCHAR(50),
SERVICE_ID INT,
ORIGINAL_AMOUNT float,
TRANSACTION_AMOUNT float,
TRANSACTION_ID INT,
REQUEST_TYPE INT,
REQUEST nvarchar(900),
RESPONSE nvarchar(900),
REQUEST_DATE DATETIME,
RESPONSE_DATE DATETIME,
PROVIDER_REQUEST nvarchar(900),
PROVIDER_RESPONSE nvarchar(900),
PROVIDER_REQUEST_DATE DATETIME,
PROVIDER_RESPONSE_DATE DATETIME,
STATUS_ID INT,
PROCESSING_STATUS_ID INT,
BALANCE_BEFORE float,
BALANCE_AFTER float,
FOREIGN KEY (REQUEST_TYPE) REFERENCES REQUEST_TYPE(ID),
FOREIGN KEY (PROCESSING_STATUS_ID) REFERENCES PROCESSING_STATUS(ID),
FOREIGN KEY (STATUS_ID) REFERENCES REQUEST_STATUS(ID)
);


INSERT INTO [dbo].[LANG](ID,NAME) VALUES(1,'English');
INSERT INTO [dbo].[LANG](ID,NAME) VALUES(2,'Arabic');

INSERT INTO [dbo].[PROVIDER](ID,NAME) VALUES(0,'Momkn');
INSERT INTO [dbo].[PROVIDER](ID,NAME) VALUES(1,'BEE');
INSERT INTO [dbo].[PROVIDER](ID,NAME) VALUES(2,'Masary');

//TODO:------------
INSERT INTO [dbo].[PROVIDER](ID,NAME) VALUES(26,'Tamam');
//------------------

INSERT INTO [dbo].[PROVIDER_SERVICE](ID, SERVICE_ID, PROVIDER_ID) VALUES(1,1,0);

INSERT INTO [dbo].[PROVIDER_SERVICE](ID, SERVICE_ID, PROVIDER_ID) VALUES(135, 1, 1);
INSERT INTO [dbo].[PROVIDER_SERVICE](ID, SERVICE_ID, PROVIDER_ID) VALUES(110, 1, 2);

INSERT INTO [dbo].[PROVIDER_SERVICE](ID, SERVICE_ID, PROVIDER_ID) VALUES(133, 2, 1);
INSERT INTO [dbo].[PROVIDER_SERVICE](ID, SERVICE_ID, PROVIDER_ID) VALUES(109, 2, 2);

INSERT INTO [dbo].[PROVIDER_SERVICE](ID, SERVICE_ID, PROVIDER_ID) VALUES(136, 3, 1);
INSERT INTO [dbo].[PROVIDER_SERVICE](ID, SERVICE_ID, PROVIDER_ID) VALUES(111, 3, 2);

//TODO:------------
INSERT INTO [dbo].[PROVIDER_SERVICE](ID, SERVICE_ID, PROVIDER_ID) VALUES(152,1,26);
INSERT INTO [dbo].[PROVIDER_SERVICE](ID, SERVICE_ID, PROVIDER_ID) VALUES(153,2,26);
INSERT INTO [dbo].[PROVIDER_SERVICE](ID, SERVICE_ID, PROVIDER_ID) VALUES(154,3,26);
INSERT INTO [dbo].[PROVIDER_SERVICE](ID, SERVICE_ID, PROVIDER_ID) VALUES(155,4,26);
//------------------


INSERT INTO [dbo].[REQUEST_TYPE] (ID, NAME) VALUES(1, 'TOPUP');

INSERT INTO [dbo].[REQUEST_STATUS] (ID,NAME) VALUES(1,'Successful');
INSERT INTO [dbo].[REQUEST_STATUS] (ID,NAME) VALUES(2,'Failed');
INSERT INTO [dbo].[REQUEST_STATUS] (ID,NAME) VALUES(3,'Pending');

INSERT INTO [dbo].[PROCESSING_STATUS] (ID,NAME) VALUES(1,'Successful');
INSERT INTO [dbo].[PROCESSING_STATUS] (ID,NAME) VALUES(2,'Failed');
INSERT INTO [dbo].[PROCESSING_STATUS] (ID,NAME) VALUES(3,'Pending');
INSERT INTO [dbo].[PROCESSING_STATUS] (ID,NAME) VALUES(4,'Validation is processing'); 
INSERT INTO [dbo].[PROCESSING_STATUS] (ID,NAME) VALUES(5,'Valid');
INSERT INTO [dbo].[PROCESSING_STATUS] (ID,NAME) VALUES(6,'Not valid');
INSERT INTO [dbo].[PROCESSING_STATUS] (ID,NAME) VALUES(7,'Under processing');


INSERT INTO [dbo].[CUSTOMER_STATUS](ID,CODE,MESSAGE,LANG_ID,REQUEST_STATUS_ID) VALUES(1,'200','Successful Transaction',1,1);
INSERT INTO [dbo].[CUSTOMER_STATUS](ID,CODE,MESSAGE,LANG_ID,REQUEST_STATUS_ID) VALUES(2,'200','عمليه ناجحه',2,1);
INSERT INTO [dbo].[CUSTOMER_STATUS](ID,CODE,MESSAGE,LANG_ID,REQUEST_STATUS_ID) VALUES(3,'-200','Transaction under processing',1,3);
INSERT INTO [dbo].[CUSTOMER_STATUS](ID,CODE,MESSAGE,LANG_ID,REQUEST_STATUS_ID) VALUES(4,'-200','العمليه قيد الانتظار',2,3);
INSERT INTO [dbo].[CUSTOMER_STATUS](ID,CODE,MESSAGE,LANG_ID,REQUEST_STATUS_ID) VALUES(5,'-1','Failed Transaction',1,2);
INSERT INTO [dbo].[CUSTOMER_STATUS](ID,CODE,MESSAGE,LANG_ID,REQUEST_STATUS_ID) VALUES(6,'-1','عمليه فاشله',2,2);
INSERT INTO [dbo].[CUSTOMER_STATUS](ID,CODE,MESSAGE,LANG_ID,REQUEST_STATUS_ID) VALUES(7,'-100','You don''t have enough balance',1,2);
INSERT INTO [dbo].[CUSTOMER_STATUS](ID,CODE,MESSAGE,LANG_ID,REQUEST_STATUS_ID) VALUES(8,'-100','ليس لديك رصيد كاف لاتمام العمليه',2,2);
INSERT INTO [dbo].[CUSTOMER_STATUS](ID,CODE,MESSAGE,LANG_ID,REQUEST_STATUS_ID) VALUES(9,'-101','Service Not Active. Please try again later.',1,2);
INSERT INTO [dbo].[CUSTOMER_STATUS](ID,CODE,MESSAGE,LANG_ID,REQUEST_STATUS_ID) VALUES(10,'-101','الخدمه متوقفه الان برجاء المحاوله لاحقا.',2,2);
INSERT INTO [dbo].[CUSTOMER_STATUS](ID,CODE,MESSAGE,LANG_ID,REQUEST_STATUS_ID) VALUES(11,'-102','Repeated Request!',1,2);
INSERT INTO [dbo].[CUSTOMER_STATUS](ID,CODE,MESSAGE,LANG_ID,REQUEST_STATUS_ID) VALUES(12,'-102','طلب مكرر.',2,2);
INSERT INTO [dbo].[CUSTOMER_STATUS](ID,CODE,MESSAGE,LANG_ID,REQUEST_STATUS_ID) VALUES(13,'-103','The mobile number is not valid!',1,2);
INSERT INTO [dbo].[CUSTOMER_STATUS](ID,CODE,MESSAGE,LANG_ID,REQUEST_STATUS_ID) VALUES(14,'-103','رقم التليفون غير صحيح',2,2);
INSERT INTO [dbo].[CUSTOMER_STATUS](ID,CODE,MESSAGE,LANG_ID,REQUEST_STATUS_ID) VALUES(15,'-104','Invalid user name or password!',1,2);
INSERT INTO [dbo].[CUSTOMER_STATUS](ID,CODE,MESSAGE,LANG_ID,REQUEST_STATUS_ID) VALUES(16,'-104','الاسم او الرقم التعريفى غير صحيح',2,2);
INSERT INTO [dbo].[CUSTOMER_STATUS](ID,CODE,MESSAGE,LANG_ID,REQUEST_STATUS_ID) VALUES(17,'-3','Internal system error. Try again later !',1,2);
INSERT INTO [dbo].[CUSTOMER_STATUS](ID,CODE,MESSAGE,LANG_ID,REQUEST_STATUS_ID) VALUES(18,'-3','خطأ فى النظام الداخلى. حاول مره اخرى لاحقا!',2,2);
//TODO:---------------
INSERT INTO [dbo].[CUSTOMER_STATUS](ID,CODE,MESSAGE,LANG_ID,REQUEST_STATUS_ID) VALUES(19,'-111','Invalid Topup Amount',1,2);
INSERT INTO [dbo].[CUSTOMER_STATUS](ID,CODE,MESSAGE,LANG_ID,REQUEST_STATUS_ID) VALUES(20,'-111','قيمة الشحن غير صحيحة',2,2);
//--------------------
INSERT INTO [dbo].[PROVIDER_STATUS](ID,PROVIDER_SERVICE_ID,CODE) VALUES(1,1,'200');
INSERT INTO [dbo].[PROVIDER_STATUS](ID,PROVIDER_SERVICE_ID,CODE) VALUES(2,1,'-100');
INSERT INTO [dbo].[PROVIDER_STATUS](ID,PROVIDER_SERVICE_ID,CODE) VALUES(3,1,'-101');
INSERT INTO [dbo].[PROVIDER_STATUS](ID,PROVIDER_SERVICE_ID,CODE) VALUES(4,1,'-102');
INSERT INTO [dbo].[PROVIDER_STATUS](ID,PROVIDER_SERVICE_ID,CODE) VALUES(5,1,'-103');
INSERT INTO [dbo].[PROVIDER_STATUS](ID,PROVIDER_SERVICE_ID,CODE) VALUES(6,1,'-104');
INSERT INTO [dbo].[PROVIDER_STATUS](ID,PROVIDER_SERVICE_ID,CODE) VALUES(7,135,'200');
INSERT INTO [dbo].[PROVIDER_STATUS](ID,PROVIDER_SERVICE_ID,CODE) VALUES(8,1,'-3');
INSERT INTO [dbo].[PROVIDER_STATUS](ID,PROVIDER_SERVICE_ID,CODE) VALUES(9,1,'-1');
INSERT INTO [dbo].[PROVIDER_STATUS](ID,PROVIDER_SERVICE_ID,CODE) VALUES(10,135,'-111');

INSERT INTO [dbo].[PROVIDER_STATUS](ID,PROVIDER_SERVICE_ID,CODE)VALUES(11,133,'200');
INSERT INTO [dbo].[PROVIDER_STATUS](ID,PROVIDER_SERVICE_ID,CODE)VALUES(12,110,'200');


INSERT INTO [dbo].[PROVIDER_STATUS](ID,PROVIDER_SERVICE_ID,CODE)VALUES(13,136,'200');
INSERT INTO [dbo].[PROVIDER_STATUS](ID,PROVIDER_SERVICE_ID,CODE)VALUES(14,109,'200');
INSERT INTO [dbo].[PROVIDER_STATUS](ID,PROVIDER_SERVICE_ID,CODE)VALUES(15,111,'200');

INSERT INTO [dbo].[PROVIDER_STATUS](ID,PROVIDER_SERVICE_ID,CODE)VALUES(16,135,'-100');
INSERT INTO [dbo].[PROVIDER_STATUS](ID,PROVIDER_SERVICE_ID,CODE)VALUES(17,133,'-100');
INSERT INTO [dbo].[PROVIDER_STATUS](ID,PROVIDER_SERVICE_ID,CODE)VALUES(18,110,'-100');
INSERT INTO [dbo].[PROVIDER_STATUS](ID,PROVIDER_SERVICE_ID,CODE)VALUES(19,136,'-100');
INSERT INTO [dbo].[PROVIDER_STATUS](ID,PROVIDER_SERVICE_ID,CODE)VALUES(20,109,'-100');
INSERT INTO [dbo].[PROVIDER_STATUS](ID,PROVIDER_SERVICE_ID,CODE)VALUES(21,111,'-100');

INSERT INTO [dbo].[PROVIDER_STATUS](ID,PROVIDER_SERVICE_ID,CODE)VALUES(22,135,'-101');
INSERT INTO [dbo].[PROVIDER_STATUS](ID,PROVIDER_SERVICE_ID,CODE)VALUES(23,133,'-101');
INSERT INTO [dbo].[PROVIDER_STATUS](ID,PROVIDER_SERVICE_ID,CODE)VALUES(24,110,'-101');
INSERT INTO [dbo].[PROVIDER_STATUS](ID,PROVIDER_SERVICE_ID,CODE)VALUES(25,136,'-101');
INSERT INTO [dbo].[PROVIDER_STATUS](ID,PROVIDER_SERVICE_ID,CODE)VALUES(26,109,'-101');
INSERT INTO [dbo].[PROVIDER_STATUS](ID,PROVIDER_SERVICE_ID,CODE)VALUES(27,111,'-101');

INSERT INTO [dbo].[PROVIDER_STATUS](ID,PROVIDER_SERVICE_ID,CODE)VALUES(28,135,'-102');
INSERT INTO [dbo].[PROVIDER_STATUS](ID,PROVIDER_SERVICE_ID,CODE)VALUES(29,133,'-102');
INSERT INTO [dbo].[PROVIDER_STATUS](ID,PROVIDER_SERVICE_ID,CODE)VALUES(30,110,'-102');
INSERT INTO [dbo].[PROVIDER_STATUS](ID,PROVIDER_SERVICE_ID,CODE)VALUES(31,136,'-102');
INSERT INTO [dbo].[PROVIDER_STATUS](ID,PROVIDER_SERVICE_ID,CODE)VALUES(32,109,'-102');
INSERT INTO [dbo].[PROVIDER_STATUS](ID,PROVIDER_SERVICE_ID,CODE)VALUES(33,111,'-102');

INSERT INTO [dbo].[PROVIDER_STATUS](ID,PROVIDER_SERVICE_ID,CODE)VALUES(34,135,'-103');
INSERT INTO [dbo].[PROVIDER_STATUS](ID,PROVIDER_SERVICE_ID,CODE)VALUES(35,133,'-103');
INSERT INTO [dbo].[PROVIDER_STATUS](ID,PROVIDER_SERVICE_ID,CODE)VALUES(36,110,'-103');
INSERT INTO [dbo].[PROVIDER_STATUS](ID,PROVIDER_SERVICE_ID,CODE)VALUES(37,136,'-103');
INSERT INTO [dbo].[PROVIDER_STATUS](ID,PROVIDER_SERVICE_ID,CODE)VALUES(38,109,'-103');
INSERT INTO [dbo].[PROVIDER_STATUS](ID,PROVIDER_SERVICE_ID,CODE)VALUES(39,111,'-103');

INSERT INTO [dbo].[PROVIDER_STATUS](ID,PROVIDER_SERVICE_ID,CODE)VALUES(40,135,'-104');
INSERT INTO [dbo].[PROVIDER_STATUS](ID,PROVIDER_SERVICE_ID,CODE)VALUES(41,133,'-104');
INSERT INTO [dbo].[PROVIDER_STATUS](ID,PROVIDER_SERVICE_ID,CODE)VALUES(42,110,'-104');
INSERT INTO [dbo].[PROVIDER_STATUS](ID,PROVIDER_SERVICE_ID,CODE)VALUES(43,136,'-104');
INSERT INTO [dbo].[PROVIDER_STATUS](ID,PROVIDER_SERVICE_ID,CODE)VALUES(44,109,'-104');
INSERT INTO [dbo].[PROVIDER_STATUS](ID,PROVIDER_SERVICE_ID,CODE)VALUES(45,111,'-104');

INSERT INTO [dbo].[PROVIDER_STATUS](ID,PROVIDER_SERVICE_ID,CODE)VALUES(46,135,'-1');
INSERT INTO [dbo].[PROVIDER_STATUS](ID,PROVIDER_SERVICE_ID,CODE)VALUES(47,133,'-1');
INSERT INTO [dbo].[PROVIDER_STATUS](ID,PROVIDER_SERVICE_ID,CODE)VALUES(48,110,'-1');
INSERT INTO [dbo].[PROVIDER_STATUS](ID,PROVIDER_SERVICE_ID,CODE)VALUES(49,136,'-1');
INSERT INTO [dbo].[PROVIDER_STATUS](ID,PROVIDER_SERVICE_ID,CODE)VALUES(50,109,'-1');
INSERT INTO [dbo].[PROVIDER_STATUS](ID,PROVIDER_SERVICE_ID,CODE)VALUES(51,111,'-1');

INSERT INTO [dbo].[PROVIDER_STATUS](ID,PROVIDER_SERVICE_ID,CODE)VALUES(52,1,'-111');
INSERT INTO [dbo].[PROVIDER_STATUS](ID,PROVIDER_SERVICE_ID,CODE)VALUES(53,133,'-111');
INSERT INTO [dbo].[PROVIDER_STATUS](ID,PROVIDER_SERVICE_ID,CODE)VALUES(54,110,'-111');
INSERT INTO [dbo].[PROVIDER_STATUS](ID,PROVIDER_SERVICE_ID,CODE)VALUES(55,136,'-111');
INSERT INTO [dbo].[PROVIDER_STATUS](ID,PROVIDER_SERVICE_ID,CODE)VALUES(56,109,'-111');
INSERT INTO [dbo].[PROVIDER_STATUS](ID,PROVIDER_SERVICE_ID,CODE)VALUES(57,111,'-111');

//TODO:-------------------
INSERT INTO [dbo].[PROVIDER_STATUS](ID,PROVIDER_SERVICE_ID,CODE)VALUES(58,1,'-200');
INSERT INTO [dbo].[PROVIDER_STATUS](ID,PROVIDER_SERVICE_ID,CODE)VALUES(59,133,'-200');
INSERT INTO [dbo].[PROVIDER_STATUS](ID,PROVIDER_SERVICE_ID,CODE)VALUES(60,110,'-200');
INSERT INTO [dbo].[PROVIDER_STATUS](ID,PROVIDER_SERVICE_ID,CODE)VALUES(61,136,'-200');
INSERT INTO [dbo].[PROVIDER_STATUS](ID,PROVIDER_SERVICE_ID,CODE)VALUES(62,109,'-200');
INSERT INTO [dbo].[PROVIDER_STATUS](ID,PROVIDER_SERVICE_ID,CODE)VALUES(63,111,'-200');

INSERT INTO [dbo].[PROVIDER_STATUS](ID,PROVIDER_SERVICE_ID,CODE)VALUES(64,152,'200');
INSERT INTO [dbo].[PROVIDER_STATUS](ID,PROVIDER_SERVICE_ID,CODE)VALUES(65,152,'-200');
INSERT INTO [dbo].[PROVIDER_STATUS](ID,PROVIDER_SERVICE_ID,CODE)VALUES(66,152,'-1');
INSERT INTO [dbo].[PROVIDER_STATUS](ID,PROVIDER_SERVICE_ID,CODE)VALUES(67,152,'-100');
INSERT INTO [dbo].[PROVIDER_STATUS](ID,PROVIDER_SERVICE_ID,CODE)VALUES(68,152,'-102');
INSERT INTO [dbo].[PROVIDER_STATUS](ID,PROVIDER_SERVICE_ID,CODE)VALUES(69,152,'-103');
INSERT INTO [dbo].[PROVIDER_STATUS](ID,PROVIDER_SERVICE_ID,CODE)VALUES(70,152,'-104');
INSERT INTO [dbo].[PROVIDER_STATUS](ID,PROVIDER_SERVICE_ID,CODE)VALUES(71,152,'-3');
INSERT INTO [dbo].[PROVIDER_STATUS](ID,PROVIDER_SERVICE_ID,CODE)VALUES(72,152,'-111');

INSERT INTO [dbo].[PROVIDER_STATUS](ID,PROVIDER_SERVICE_ID,CODE)VALUES(73,153,'200');
INSERT INTO [dbo].[PROVIDER_STATUS](ID,PROVIDER_SERVICE_ID,CODE)VALUES(74,153,'-200');
INSERT INTO [dbo].[PROVIDER_STATUS](ID,PROVIDER_SERVICE_ID,CODE)VALUES(75,153,'-1');
INSERT INTO [dbo].[PROVIDER_STATUS](ID,PROVIDER_SERVICE_ID,CODE)VALUES(76,153,'-100');
INSERT INTO [dbo].[PROVIDER_STATUS](ID,PROVIDER_SERVICE_ID,CODE)VALUES(77,153,'-102');
INSERT INTO [dbo].[PROVIDER_STATUS](ID,PROVIDER_SERVICE_ID,CODE)VALUES(78,153,'-103');
INSERT INTO [dbo].[PROVIDER_STATUS](ID,PROVIDER_SERVICE_ID,CODE)VALUES(79,153,'-104');
INSERT INTO [dbo].[PROVIDER_STATUS](ID,PROVIDER_SERVICE_ID,CODE)VALUES(80,153,'-3');
INSERT INTO [dbo].[PROVIDER_STATUS](ID,PROVIDER_SERVICE_ID,CODE)VALUES(81,153,'-111');

INSERT INTO [dbo].[PROVIDER_STATUS](ID,PROVIDER_SERVICE_ID,CODE)VALUES(82,154,'200');
INSERT INTO [dbo].[PROVIDER_STATUS](ID,PROVIDER_SERVICE_ID,CODE)VALUES(83,154,'-200');
INSERT INTO [dbo].[PROVIDER_STATUS](ID,PROVIDER_SERVICE_ID,CODE)VALUES(84,154,'-1');
INSERT INTO [dbo].[PROVIDER_STATUS](ID,PROVIDER_SERVICE_ID,CODE)VALUES(85,154,'-100');
INSERT INTO [dbo].[PROVIDER_STATUS](ID,PROVIDER_SERVICE_ID,CODE)VALUES(86,154,'-102');
INSERT INTO [dbo].[PROVIDER_STATUS](ID,PROVIDER_SERVICE_ID,CODE)VALUES(87,154,'-103');
INSERT INTO [dbo].[PROVIDER_STATUS](ID,PROVIDER_SERVICE_ID,CODE)VALUES(88,154,'-104');
INSERT INTO [dbo].[PROVIDER_STATUS](ID,PROVIDER_SERVICE_ID,CODE)VALUES(89,154,'-3');
INSERT INTO [dbo].[PROVIDER_STATUS](ID,PROVIDER_SERVICE_ID,CODE)VALUES(90,154,'-111');

INSERT INTO [dbo].[PROVIDER_STATUS](ID,PROVIDER_SERVICE_ID,CODE)VALUES(91,155,'200');
INSERT INTO [dbo].[PROVIDER_STATUS](ID,PROVIDER_SERVICE_ID,CODE)VALUES(92,155,'-200');
INSERT INTO [dbo].[PROVIDER_STATUS](ID,PROVIDER_SERVICE_ID,CODE)VALUES(93,155,'-1');
INSERT INTO [dbo].[PROVIDER_STATUS](ID,PROVIDER_SERVICE_ID,CODE)VALUES(94,155,'-100');
INSERT INTO [dbo].[PROVIDER_STATUS](ID,PROVIDER_SERVICE_ID,CODE)VALUES(95,155,'-102');
INSERT INTO [dbo].[PROVIDER_STATUS](ID,PROVIDER_SERVICE_ID,CODE)VALUES(96,155,'-103');
INSERT INTO [dbo].[PROVIDER_STATUS](ID,PROVIDER_SERVICE_ID,CODE)VALUES(97,155,'-104');
INSERT INTO [dbo].[PROVIDER_STATUS](ID,PROVIDER_SERVICE_ID,CODE)VALUES(98,155,'-3');
INSERT INTO [dbo].[PROVIDER_STATUS](ID,PROVIDER_SERVICE_ID,CODE)VALUES(99,155,'-111');

//------------------------

/****************************************************************************************************/

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Tony Kaood>
-- Create date: <13-05-2017>
-- Description:	<Get customer status>
-- =============================================

CREATE FUNCTION [dbo].[getCustomerStatus](@lang int,@providerID int, @providerStatusCode nvarchar(50),@serviceID int)
RETURNS @retStatus TABLE 
(
    -- Columns returned by the function
    StatusCode nvarchar(500) PRIMARY KEY NOT NULL, 
    StatusMessage nvarchar(900), 
    RequestStatus int
)
AS 
-- Returns the StatusCode, StatusMessage, RequestStatus.
BEGIN
    DECLARE 
        @StatusCode nvarchar(500) ,
		@StatusMessage nvarchar(900),
		@RequestStatus int;
		
    -- Get common USER information
	SELECT     
	 @StatusCode = cs.CODE,
	 @StatusMessage = cs.MESSAGE,
	 @RequestStatus = cs.REQUEST_STATUS_ID
		FROM PROVIDER_SERVICE ps
		INNER JOIN PROVIDER_STATUS pStat ON ps.ID = pStat.PROVIDER_SERVICE_ID
		INNER JOIN CUSTOMER_STATUS cs ON pStat.CODE = cs.CODE 
		INNER JOIN LANG l ON cs.LANG_ID = l.ID
		WHERE	ps.PROVIDER_ID = @providerID
		AND		ps.ID = @serviceID
		AND 	pStat.CODE = @providerStatusCode
		AND 	l.ID = @lang;
	
    -- Return the information to the caller
    IF @StatusCode IS NOT NULL 
    BEGIN
        INSERT @retStatus
        SELECT @StatusCode, @StatusMessage, @RequestStatus;
    END;
    RETURN;
END;
GO

/****************************************************************************************************/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Tony Kaood>
-- Create date: <13-05-2017>
-- Description:	<Validate the mobile number>
-- =============================================
CREATE PROCEDURE [dbo].[insert_Request] 
(
@USER_ID INT,
@PHONE nvarchar(50),
@SERVICE_ID INT,
@ORIGINAL_AMOUNT float,
@TRANSACTION_AMOUNT float,
@REQUEST_TYPE nvarchar(50),
@REQUEST nvarchar(900),
@REQUEST_DATE DATETIME,
@PROCESSING_STATUS_ID INT,
@ID INT OUTPUT
)
AS
BEGIN
	
	SET @ID = SCOPE_IDENTITY();
	
	INSERT INTO [dbo].[REQUEST]
	(
	ID,
	USER_ID,
	PHONE,
	SERVICE_ID,
	ORIGINAL_AMOUNT,
	TRANSACTION_AMOUNT,
	REQUEST_TYPE,
	REQUEST,
	REQUEST_DATE,
	PROCESSING_STATUS_ID
	)
	VALUES
	(
	@ID,
	@USER_ID,
	@PHONE,
	@SERVICE_ID,
	@ORIGINAL_AMOUNT,
	@TRANSACTION_AMOUNT,
	@REQUEST_TYPE,
	@REQUEST,
	@REQUEST_DATE,
	@PROCESSING_STATUS_ID
	);

END
GO

/****************************************************************************************************/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Tony Kaood>
-- Create date: <13-05-2017>
-- Description:	<Validate the mobile number>
-- =============================================
CREATE FUNCTION [dbo].[isActive] 
(
@serviceID int
) RETURNS int
AS
BEGIN
	-- Declare the return variable here
	DECLARE @isActive int
	SELECT @isActive = Active FROM Services WHERE Id = @serviceID

	-- Return the result of the function
	RETURN @isActive

END
GO


/****************************************************************************************************/

USE [momken]
GO
/****** Object:  UserDefinedFunction [dbo].[getServicePrice]    Script Date: 7/8/2017 7:39:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Tony Kaood>
-- Create date: <13-05-2017>
-- Description:	<Return service price>
-- =============================================
CREATE FUNCTION [dbo].[getServicePrice] 
(
@amount float,
@serviceID int
) RETURNS float
AS
BEGIN
DECLARE @servicePrice float
SET @servicePrice = (SELECT 
CASE 'B' 
WHEN 'A' THEN ISNULL(Services.Price_A,0)
WHEN 'B' THEN ISNULL(Services.Price_B,0)
WHEN 'C' THEN ISNULL(Services.Price_C,0)
ELSE 0 
END  * @amount
FROM Services 
WHERE Services.Id = @serviceID);

RETURN @servicePrice
END


/****************************************************************************************************/


SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Tony Kaood>
-- Create date: <13-05-2017>
-- Description:	<Validate the mobile number>
-- =============================================
CREATE FUNCTION [dbo].[isValid] 
(
@phoneNumber nvarchar(50)
) RETURNS int
AS
BEGIN
	-- Declare the return variable here
	DECLARE @isValid int
	DECLARE @phoneLenth int

	SET @phoneLenth = LEN(@phoneNumber)

	IF( @phoneLenth = 11 )
	BEGIN
		SET @isValid= 1
	END
	ELSE
	BEGIN
		SET @isValid = 0
	END 

	-- Return the result of the function
	RETURN @isValid

END
GO

/****************************************************************************************************/

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Tony Kaood>
-- Create date: <13-05-2017>
-- Description:	<Validate Topup request>
-- =============================================
CREATE FUNCTION [dbo].[authenticate](@userName nvarchar(50),@password nvarchar(50),@centerID int, @ip nvarchar(50))
RETURNS @retUserInformation TABLE 
(
    -- Columns returned by the function
    UserId int PRIMARY KEY NOT NULL, 
    RoleId int NULL, 
    HomePage nvarchar(500) NULL, 
    Balance int NULL, 
    Category nvarchar(1) NULL
)
AS 
-- Returns the first name, last name, job title, and contact type for the specified contact.
BEGIN
    DECLARE 
        @UserId int ,
		@RoleId int,
		@HomePage nvarchar(500),
		@Balance int,
		@Category nvarchar(1);
    -- Get common USER information
	SELECT     
	 @UserId = Users.Id,
	 @RoleId = Users.RoleId,
	 @HomePage = Features.HomePage,
	 @Balance = ISNULL(cast(Centers.Balance AS NUMERIC(10, 2)),0),
	 @Category = Cat
		FROM       Users 
		INNER JOIN Roles ON Users.RoleId = Roles.Id 
		LEFT OUTER JOIN Features ON Roles.DefFeatureId = Features.Id
		INNER JOIN Centers ON Users.CenterId = Centers.Id
		WHERE	(Users.Login = @userName)
		 AND	(Users.Password = @password) 
		 AND	(Users.CenterId = @centerID) 
		 AND	(Users.Active = 1)
		 AND	(Centers.Active = 1)
		 AND	ISNULL(Users.IP,0) = CASE Required_ip WHEN 1 THEN @ip ELSE  ISNULL(Users.IP,0) END;
	
    -- Return the information to the caller
    IF @UserId IS NOT NULL 
    BEGIN
        INSERT @retUserInformation
        SELECT @UserId, @RoleId, @HomePage, @Balance, @Category;
    END;
    RETURN;
END;
GO

/****************************************************************************************************/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Tony Kaood>
-- Create date: <13-05-2017>
-- Description:	<Validate the mobile number>
-- =============================================
CREATE FUNCTION [dbo].[isNotRepeated] 
(
@phoneNumber nvarchar(50)
) RETURNS int
AS
BEGIN
	-- Declare the return variable here
	DECLARE @isNotRepeated int

	IF  EXISTS(SELECT * FROM electronic_charge_log  WHERE  mobile_number = @phoneNumber   AND DATEDIFF(SECOND, AddedTime,GETDATE())<320 and charge_status not like '%-4 Provider Error%') 
	BEGIN
		SET @isNotRepeated= 0
	END
	ELSE
	BEGIN
		SET @isNotRepeated = 1
	END 

	-- Return the result of the function
	RETURN @isNotRepeated

END
GO

/****************************************************************************************************/

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Tony Kaood>
-- Create date: <13-05-2017>
-- Description:	<Validate Topup request>
-- =============================================
CREATE FUNCTION [dbo].[isValidRequest] 
(
	@userName nvarchar(50),
	@password nvarchar(50),
	@centerID int,
	@ip nvarchar(50),
	@mobileNumber nvarchar(50),
	@serviceID int,
	@amount int,
	@ProviderID int,
	@lang int
) RETURNS @retUserInformation TABLE 
(
    -- Columns returned by the function
    UserId int PRIMARY KEY NOT NULL, 
    RoleId int NULL, 
	TransactionAmount float,
    HomePage nvarchar(500) NULL, 
	ProviderServiceId int,
    BalanceBefore float NULL,
    Category nvarchar(1) NULL,
	IsValid int NULL,
	StatusCode nvarchar(20) NULL, 
	StatusMessage nvarchar(500) NULL,
	RequestStatus int NULL
) 
AS
BEGIN
DECLARE 

	@UserId int ,
	@RoleId int,
	@TransactionAmount float,
	@HomePage nvarchar(500),
	@BalanceBefore float,
	@Category nvarchar(1),
	@ProviderServiceId int,
	@IsValid int,
	@ServicePrice int,
	@StatusCode nvarchar(20), 
	@StatusMessage nvarchar(500),
	@RequestStatus int,
	@EqServiceID int;
	
	SET @IsValid = 0;
	SET @EqServiceID = @serviceID;	
	
	SELECT 
		@UserId = UserId,
		@RoleId = RoleId, 
		@HomePage = HomePage, 
		@BalanceBefore = Balance, 
		@Category = Category
	FROM [dbo].[authenticate](@userName, @password, @centerID, @ip);

	IF @UserId IS NOT NULL 
	BEGIN
	
		SELECT @IsValid = (SELECT [dbo].[isValid](@mobileNumber));
			IF @IsValid = 1  
			BEGIN
				
				SELECT @IsValid = (SELECT [dbo].[isNotRepeated](@mobileNumber));
				IF @IsValid = 1  
				BEGIN
						
					SELECT @EqServiceID = ID FROM [dbo].[PROVIDER_SERVICE] WHERE SERVICE_ID = @serviceID AND  PROVIDER_ID = @ProviderID;
					SET @ProviderServiceId = @EqServiceID;
						SELECT @IsValid = (SELECT [dbo].[isActive](@EqServiceID));
						IF @IsValid = 1  
						BEGIN
							SELECT @ServicePrice = (SELECT [dbo].[getServicePrice] (@amount,@EqServiceID));
							SET @TransactionAmount = @ServicePrice
							
							IF @ServicePrice < @BalanceBefore
							BEGIN
								SET @IsValid = 1
								
								SELECT 
									@StatusCode = StatusCode, 
									@StatusMessage = StatusMessage, 
									@RequestStatus = RequestStatus
								FROM [dbo].[getCustomerStatus](@lang,0,'200',1);
								
								--SET @Message = 'Valid'
								INSERT @retUserInformation
								SELECT @UserId, @RoleId, @TransactionAmount, @HomePage, @ProviderServiceId, @BalanceBefore, @Category, @IsValid, @StatusCode, @StatusMessage, @RequestStatus;
							-- CONTINUE
							END
							ELSE
							BEGIN
								-- You don't have enough balance
								SET @IsValid = 0
								SELECT 
									@StatusCode = StatusCode, 
									@StatusMessage = StatusMessage, 
									@RequestStatus = RequestStatus
								FROM [dbo].[getCustomerStatus](@lang,0,'-100',1);
								-- @Message = 'You don''t have enough balance'
								INSERT @retUserInformation
								SELECT @UserId, @RoleId,@TransactionAmount, @HomePage, @ProviderServiceId, @BalanceBefore, @Category, @IsValid, @StatusCode, @StatusMessage, @RequestStatus;
							END;
						END
					ELSE
					BEGIN
				
						-- Service Not Active
						SET @IsValid = 0
						SELECT 
							@StatusCode = StatusCode, 
							@StatusMessage = StatusMessage, 
							@RequestStatus = RequestStatus
						FROM [dbo].[getCustomerStatus](@lang,0,'-101',1);
						--SET @Message = 'Service Not Active!'
						SET @UserId = 0
						INSERT @retUserInformation
						SELECT @UserId, @RoleId,@TransactionAmount, @HomePage, @ProviderServiceId, @BalanceBefore, @Category, @IsValid, @StatusCode, @StatusMessage, @RequestStatus;
					END;
						
				END
				ELSE
				BEGIN
					-- Repeated Request
						SET @IsValid = 0
						--SET @Message = 'Repeated Request!'
						SELECT 
							@StatusCode = StatusCode, 
							@StatusMessage = StatusMessage, 
							@RequestStatus = RequestStatus
						FROM [dbo].[getCustomerStatus](@lang,0,'-102',1);
						SET @UserId = 0
						INSERT @retUserInformation
						SELECT @UserId, @RoleId,@TransactionAmount, @HomePage, @ProviderServiceId, @BalanceBefore, @Category, @IsValid, @StatusCode, @StatusMessage, @RequestStatus;
				END;
			END
			ELSE
			BEGIN
				-- The mobile number is not valid
					SET @IsValid = 0
					--SET @Message = 'The mobile number is not valid!'
					SELECT 
						@StatusCode = StatusCode, 
						@StatusMessage = StatusMessage, 
						@RequestStatus = RequestStatus
					FROM [dbo].[getCustomerStatus](@lang,0,'-103',1);
					SET @UserId = 0
					INSERT @retUserInformation
					SELECT @UserId, @RoleId,@TransactionAmount, @HomePage, @ProviderServiceId, @BalanceBefore, @Category, @IsValid, @StatusCode, @StatusMessage, @RequestStatus;
			END;
	END
	ELSE
	BEGIN

		-- Invalid user name or password
		SET @IsValid = 0
		--SET @Message = 'Invalid user name or password!'
		SELECT 
			@StatusCode = StatusCode, 
			@StatusMessage = StatusMessage, 
			@RequestStatus = RequestStatus
		FROM [dbo].[getCustomerStatus](@lang,0,'-104',1);
		
		SET @UserId = 0
		INSERT @retUserInformation
		SELECT @UserId, @RoleId,@TransactionAmount, @HomePage, @ProviderServiceId, @BalanceBefore, @Category, @IsValid, @StatusCode, @StatusMessage, @RequestStatus;
	END;
	RETURN;
END




go
USE [momken]
GO
/****** Object:  StoredProcedure [dbo].[insert_Topup_Request]    Script Date: 8/10/2017 7:01:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[insert_Topup_Request] 
(
@userName nvarchar(50),
@password nvarchar(50),
@centerID int,
@ip nvarchar(50),
@mobileNumber nvarchar(50),
@serviceID int,
@amount float,
@ProviderID int,
@REQUEST nvarchar(900),
@lang int,
@IsValidRequest int output,
@RequestId int output,
@RequestStatusCode nvarchar(20) output,
@RequestStatusMessage nvarchar(500) output
)
AS
BEGIN TRY
DECLARE 
	@ID INT,
	@UserId INT, 
	@RoleId INT, 
	@TransactionAmount float,
	@HomePage nvarchar(50), 
	@BalanceBefore MONEY,
	@Category nvarchar(50),
	@IsValid INT,
	@StatusCode nvarchar(20), 
	@StatusMessage nvarchar(500),
	@RequestStatus int ,
	@REQUEST_TYPE INT,
	@PROCESSING_STATUS_ID INT,
	@Response nvarchar(500),
	@ProviderServiceId int;

	SET @REQUEST_TYPE = 1; --Topup
	SET @PROCESSING_STATUS_ID = 1;
	
	SELECT 
		@UserId = UserId, 
		@RoleId = RoleId,
		@TransactionAmount = TransactionAmount,	
		@HomePage = HomePage, 
		@BalanceBefore = BalanceBefore, 
		@Category = Category,
		@IsValid = IsValid,
		@StatusCode = StatusCode, 
		@StatusMessage = StatusMessage,
		@RequestStatus = RequestStatus,
		@ProviderServiceId = ProviderServiceId
		FROM [dbo].[isValidRequest]
		(@userName,@password,@centerID,@ip,@mobileNumber,@serviceID,@amount,@ProviderID,@lang)

	SET @ID = NEXT VALUE FOR REQUEST_SEQ
	SET @RequestId = @ID;
	SET @IsValidRequest = @IsValid;
	SEt @RequestStatusCode = @StatusCode;
	SET @RequestStatusMessage = @StatusMessage;
	
	IF @IsValid = 1 
	BEGIN
		SET @Response = NULL;
		SET @PROCESSING_STATUS_ID = 5;
	END
	ELSE
	BEGIN
		SET @Response = CONCAT(@StatusCode,' : ',@StatusMessage);
		SET @PROCESSING_STATUS_ID = 6;
	END;

	
	INSERT INTO [dbo].[REQUEST]
	(
	ID,
	USER_ID,
	PHONE,
	SERVICE_ID,
	ORIGINAL_AMOUNT,
	TRANSACTION_AMOUNT,
	REQUEST_TYPE,
	REQUEST,
	REQUEST_DATE,
	PROCESSING_STATUS_ID,
	STATUS_ID,
	RESPONSE,
	BALANCE_BEFORE
	)
	VALUES
	(
	@ID,
	@UserId,
	@mobileNumber,
	@ProviderServiceId,
	@Amount,
	@TransactionAmount,
	@REQUEST_TYPE,
	@REQUEST,
	GETDATE() , --@REQUEST_DATE,
	@PROCESSING_STATUS_ID,
	@RequestStatus,
	@StatusCode+' : '+@StatusMessage,
	@BalanceBefore
	
	);
	END TRY
	BEGIN CATCH
	
	INSERT INTO ERROR_LOG(ERROR_DATE,ERROR_NUMBER,ERROR_SEVERITY,ERROR_STATE,ERROR_PROCEDURE,ERROR_LINE,ERROR_MESSAGE)
	SELECT GETDATE(), ERROR_NUMBER(),ERROR_SEVERITY(),ERROR_STATE(),ERROR_PROCEDURE(),ERROR_LINE(),ERROR_MESSAGE();
	SELECT 
			@StatusCode = StatusCode, 
			@StatusMessage = StatusMessage, 
			@RequestStatus = RequestStatus
		FROM [dbo].[getCustomerStatus](@lang,0,'-3',1);
		
	SET @Response = CONCAT(@StatusCode,' : ',@StatusMessage);
	SET @PROCESSING_STATUS_ID = 6;
	
	INSERT INTO [dbo].[REQUEST]
	(
	ID,
	USER_ID,
	PHONE,
	SERVICE_ID,
	ORIGINAL_AMOUNT,
	TRANSACTION_AMOUNT,
	REQUEST_TYPE,
	REQUEST,
	REQUEST_DATE,
	PROCESSING_STATUS_ID,
	STATUS_ID,
	RESPONSE,
	BALANCE_BEFORE
	)
	VALUES
	(
	@ID,
	@UserId,
	@mobileNumber,
	@ProviderServiceId,
	@Amount,
	@TransactionAmount,
	@REQUEST_TYPE,
	@REQUEST,
	GETDATE() , --@REQUEST_DATE,
	@PROCESSING_STATUS_ID,
	@RequestStatus,
	@StatusCode+' : '+@StatusMessage,
	@BalanceBefore
	);
	
	END CATCH
GO

USE [momken]
GO
/****** Object:  StoredProcedure [dbo].[PROCESS_TOPUP_REQUEST]    Script Date: 8/10/2017 7:00:50 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Tony Kaood>
-- Create date: <21-06-2017>
-- Description:	<insert topup request>
-- =============================================
CREATE PROCEDURE [dbo].[PROCESS_TOPUP_REQUEST] 
(
	@RequestId int,
	@ProviderStatusCode nvarchar(900),
	@ProviderRequest nvarchar(900),
	@ProviderResponse nvarchar(900),
	@ProviderRequestDate DATETIME,
	@ProviderResponseDate DATETIME,
	@lang int,
	@IsValidRequest int output,
	@RequestStatusCode nvarchar(20) output,
	@RequestStatusMessage nvarchar(500) output
)
AS
BEGIN TRY
DECLARE 

	@UserId int,
	@CenterId int,
	@CenterName nvarchar(50),
	@PhoneNumber nvarchar(50),
	@ServiceId int,
	@ProviderID int,
	@ProviderName NVARCHAR(50),
	@OriginalAmount MONEY,
	@TransactionAmount MONEY,
	@BalanceBefore MONEY,
	@PROCESSING_STATUS_ID INT,
	@TransactionId int,
	@desc nvarchar(500),
	@total_invoices decimal(18,3),
	@DesBalance nvarchar(200),
	@StatusCode nvarchar(20), 
	@StatusMessage nvarchar(500),
	@RequestStatus int,
	@Response nvarchar(900),
	@IsSuccess int,
	@log_id  int
	
	/* Request must be valid */
	SET @PROCESSING_STATUS_ID = 5;
	SET	@DesBalance='شحن ممكن3 المسدد' 
	
	SELECT 
		@UserId = [dbo].[REQUEST].USER_ID,
		@CenterId = [dbo].[Users].CenterId,
		@CenterName = [dbo].[Centers].CenterName,
		@PhoneNumber = [dbo].[REQUEST].PHONE,
		@ServiceId = [dbo].[REQUEST].SERVICE_ID,
		@ProviderID = [dbo].[PROVIDER_SERVICE].PROVIDER_ID,
		@ProviderName = [dbo].[PROVIDER].NAME,
		@OriginalAmount = [dbo].[REQUEST].ORIGINAL_AMOUNT,
		@TransactionAmount = [dbo].[REQUEST].TRANSACTION_AMOUNT ,
		@BalanceBefore = [dbo].[REQUEST].BALANCE_BEFORE
	FROM [dbo].[REQUEST]
	INNER JOIN [dbo].[Users] 			ON [dbo].[REQUEST].USER_ID 				= [dbo].[Users].Id
	INNER JOIN [dbo].[Centers] 			ON [dbo].[Users].CenterId 				= [dbo].[Centers].Id
	INNER JOIN [dbo].[PROVIDER_SERVICE] ON [dbo].[REQUEST].SERVICE_ID 			= [dbo].[PROVIDER_SERVICE].ID
	INNER JOIN [dbo].[PROVIDER] 		ON [dbo].[PROVIDER_SERVICE].PROVIDER_ID = [dbo].[PROVIDER].ID
	WHERE [dbo].[REQUEST].ID = @RequestId
	AND [dbo].[REQUEST].PROCESSING_STATUS_ID = @PROCESSING_STATUS_ID;
	
	INSERT INTO electronic_charge_log
	(
	provider_company,
	mobile_number,
	value,
	invoice_price,
	charge_status,
	status_transfer,
	UserId,
	CenterId,
	[AddedTime],
	Center_Name
	) 
	values
	(
	@ProviderID,
	@PhoneNumber,
	@OriginalAmount,
	@TransactionAmount,
	@ProviderResponse,
	1,
	@UserId,
	@CenterId,
	getdate(),
	@CenterName
	);

		SET  @log_id = SCOPE_IDENTITY()

	SELECT 
		@StatusCode = StatusCode, 
		@StatusMessage = StatusMessage, 
		@RequestStatus = RequestStatus
	FROM [dbo].[getCustomerStatus](@lang,@ProviderID,@ProviderStatusCode,@ServiceId);

	SET @RequestStatusCode 		=	@StatusCode;
	SET @RequestStatusMessage  	= @StatusMessage;
	
	IF @StatusCode = '200' 
	BEGIN
		
		--  Create transaction ID 
		SET  @TransactionId = SCOPE_IDENTITY()

		UPDATE Centers SET Balance=isnull(Balance,0)-isnull(@TransactionAmount,0) WHERE Id=@CenterId
		SET @total_invoices = (SELECT isnull(cast(sum(isnull(Balance,0)) as decimal(18, 3)),0) AS Balance FROM Centers);
		SET @desc='شحن ممكن3 بقيمة '+'( '+Cast(@TransactionAmount as varchar(20))+' )'+' لرقم '+'( '+Cast(@PhoneNumber as varchar(20))+' )'

		 INSERT INTO electronic_charge
	(provider_company,mobile_number,value,invoice_price,charge_status,status_transfer,UserId,CenterId,[AddedTime],Center_Name,provider,ServId,charge,Log_Id)
	VALUES(
	@ProviderID,
	@PhoneNumber,
	@OriginalAmount,
	@TransactionAmount,
	@ProviderResponse, --TODO
	1,
	@UserId,
	@CenterId,
	getdate(),
	@CenterName,
	@ProviderName,
	@ServiceId,
	0,
	@log_id
	); 

		INSERT INTO [dbo].[Balance]
			   ([CenterId]
			   ,[Amount]
			   ,[Type]
			   ,[Descrp]
			   ,UserId,Before,InvId,ServId,trans_id,total_invoices,total_invoices_date,tel)
		VALUES
			   (@centerId
			   ,isnull(@TransactionAmount,0)
			   ,1
			   ,@desc
			   ,@UserId,isnull(@BalanceBefore,0),@TransactionId,@ServiceId,1,isnull(@total_invoices,0),getdate(),@PhoneNumber);

		--UPDATE electronic_charge_log SET is_charged=1 WHERE Id = @RequestId

		EXECUTE dbo.[UpdateChargeInCome] @TransactionId  
		
		SET @PROCESSING_STATUS_ID = 1;
		SET @IsValidRequest = 1;
		
		--EXECUTE dbo.Add_service_Balance @ServiceId,null,@TransactionAmount,1,@DesBalance,@UserId,@TransactionId
			
	END
	ELSE
	BEGIN
		SET @IsValidRequest = 0;
		SET @PROCESSING_STATUS_ID = 2;
		
	END;
	
	SET @Response = CONCAT(@StatusCode,' : ',@StatusMessage);
	
	UPDATE [dbo].[REQUEST] SET 
								RESPONSE_DATE = GETDATE(),
								PROVIDER_REQUEST = @ProviderRequest,
								PROVIDER_RESPONSE = @ProviderResponse,
								PROVIDER_REQUEST_DATE = @ProviderRequestDate,
								PROVIDER_RESPONSE_DATE = @ProviderResponseDate,
								TRANSACTION_ID = @TransactionId,
								RESPONSE = @Response,
								STATUS_ID = @RequestStatus,
								PROCESSING_STATUS_ID = @PROCESSING_STATUS_ID,
								BALANCE_AFTER = @BalanceBefore-@TransactionAmount
	WHERE ID = @RequestId;

END TRY
BEGIN CATCH

	SET @IsValidRequest = 0;
	SET @PROCESSING_STATUS_ID = 2;
	
	INSERT INTO ERROR_LOG(ERROR_DATE,ERROR_NUMBER,ERROR_SEVERITY,ERROR_STATE,ERROR_PROCEDURE,ERROR_LINE,ERROR_MESSAGE)
	SELECT GETDATE(), ERROR_NUMBER(),ERROR_SEVERITY(),ERROR_STATE(),ERROR_PROCEDURE(),ERROR_LINE(),ERROR_MESSAGE();
	
	SELECT 
			@StatusCode = StatusCode, 
			@StatusMessage = StatusMessage, 
			@RequestStatus = RequestStatus
	FROM [dbo].[getCustomerStatus](@lang,0,'-3',1);
	
	SET @Response = CONCAT(@StatusCode,' : ',@StatusMessage);
	
	UPDATE [dbo].[REQUEST] SET 
								RESPONSE_DATE = GETDATE(),
								PROVIDER_REQUEST = @ProviderRequest,
								PROVIDER_RESPONSE = @ProviderResponse,
								PROVIDER_REQUEST_DATE = @ProviderRequestDate,
								PROVIDER_RESPONSE_DATE = @ProviderResponseDate,
								TRANSACTION_ID = @TransactionId,
								RESPONSE = @Response,
								STATUS_ID = @RequestStatus,
								PROCESSING_STATUS_ID = @PROCESSING_STATUS_ID,
								BALANCE_AFTER = @BalanceBefore-@TransactionAmount
	WHERE ID = @RequestId;
	
END CATCH


GO


declare @IsValidRequest int ;
declare @RequestId int ;
declare @RequestStatusCode nvarchar(20) ;
declare @RequestStatusMessage nvarchar(500) ;

EXEC momken.dbo.insert_Topup_Request '0987'
  ,'123456'
  ,100
  ,''
  ,'01200341116'
  ,1
  ,30
  ,1
  ,'Request',
  1,
  @IsValidRequest output,
  @RequestId output,
  @RequestStatusCode output,
  @RequestStatusMessage output
  ;  
  
  SELECT @IsValidRequest,
  @RequestId,
  @RequestStatusCode,
  @RequestStatusMessage;
GO 


USE [momken]
GO

DECLARE	@return_value int,
		@IsValidRequest int,
		@RequestStatusCode nvarchar(20),
		@RequestStatusMessage nvarchar(500)

EXEC	@return_value = [dbo].[PROCESS_TOPUP_REQUEST]
		@RequestId = 36,
		@ProviderStatusCode = '-111',
		@ProviderRequest = 'Provider Request',
		@ProviderResponse = 'Provider Response',
		@ProviderRequestDate = '12-01-2017',
		@ProviderResponseDate = '12-01-2017',
		@lang = 2,
		@IsValidRequest = @IsValidRequest OUTPUT,
		@RequestStatusCode = @RequestStatusCode OUTPUT,
		@RequestStatusMessage = @RequestStatusMessage OUTPUT

SELECT	@IsValidRequest as N'@IsValidRequest',
		@RequestStatusCode as N'@RequestStatusCode',
		@RequestStatusMessage as N'@RequestStatusMessage'

SELECT	'Return Value' = @return_value

GO

