/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Momken.dtos;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import org.codehaus.jackson.annotate.JsonIgnore;

/**
 *
 * @author Tony
 */
@Entity
@Table(name = "SERVICE_DENOMINATION")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ServiceDenomination.findAll", query = "SELECT s FROM ServiceDenomination s"),
    @NamedQuery(name = "ServiceDenomination.findByServiceDenominationId", query = "SELECT s FROM ServiceDenomination s WHERE s.serviceDenominationId = :serviceDenominationId"),
    @NamedQuery(name = "ServiceDenomination.findByServiceDenominationName", query = "SELECT s FROM ServiceDenomination s WHERE s.serviceDenominationName = :serviceDenominationName"),
    @NamedQuery(name = "ServiceDenomination.findByProviderServiceId", query = "SELECT s FROM ServiceDenomination s WHERE s.providerServiceId = :providerServiceId"),
    @NamedQuery(name = "ServiceDenomination.findByExcelSheetNumber", query = "SELECT s FROM ServiceDenomination s WHERE s.excelSheetNumber = :excelSheetNumber"),
    @NamedQuery(name = "ServiceDenomination.findByFrontEndServiceIdwzDenomination", query = "SELECT s FROM ServiceDenomination s WHERE s.frontEndServiceId = :frontEndServiceId AND s.serviceDenominationValue =:serviceDenominationValue"),
    @NamedQuery(name = "ServiceDenomination.findByServiceDenominationValue", query = "SELECT s FROM ServiceDenomination s WHERE s.serviceDenominationValue = :serviceDenominationValue")})
public class ServiceDenomination implements Serializable {
    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "SERVICE_DENOMINATION_ID", nullable = false, precision = 0, scale = -127)
    private BigDecimal serviceDenominationId;
    @Size(max = 50)
    @Column(name = "SERVICE_DENOMINATION_NAME", length = 50)
    private String serviceDenominationName;
    @Column(name = "PROVIDER_SERVICE_ID")
    private BigInteger providerServiceId;
    @Column(name = "EXCEL_SHEET_NUMBER")
    private BigInteger excelSheetNumber;
    @Column(name = "FRONT_END_SERVICE_ID")
    private BigInteger frontEndServiceId;
    @Column(name = "SERVICE_DENOMINATION_VALUE")
    private double serviceDenominationValue;
    @OneToMany(mappedBy = "serviceDenominationId", fetch = FetchType.LAZY)
    private List<LocalVoucher> localVoucherList;

    public ServiceDenomination() {
    }

    public ServiceDenomination(BigDecimal serviceDenominationId) {
        this.serviceDenominationId = serviceDenominationId;
    }

    public BigDecimal getServiceDenominationId() {
        return serviceDenominationId;
    }

    public void setServiceDenominationId(BigDecimal serviceDenominationId) {
        this.serviceDenominationId = serviceDenominationId;
    }

    public String getServiceDenominationName() {
        return serviceDenominationName;
    }

    public void setServiceDenominationName(String serviceDenominationName) {
        this.serviceDenominationName = serviceDenominationName;
    }

    public BigInteger getProviderServiceId() {
        return providerServiceId;
    }

    public void setProviderServiceId(BigInteger providerServiceId) {
        this.providerServiceId = providerServiceId;
    }

    public BigInteger getExcelSheetNumber() {
        return excelSheetNumber;
    }

    public void setExcelSheetNumber(BigInteger excelSheetNumber) {
        this.excelSheetNumber = excelSheetNumber;
    }

    public BigInteger getFrontEndServiceId() {
        return frontEndServiceId;
    }

    public void setFrontEndServiceId(BigInteger frontEndServiceId) {
        this.frontEndServiceId = frontEndServiceId;
    }

    public double getServiceDenominationValue() {
        return serviceDenominationValue;
    }

    public void setServiceDenominationValue(double serviceDenominationValue) {
        this.serviceDenominationValue = serviceDenominationValue;
    }

    @XmlTransient
    @JsonIgnore
    public List<LocalVoucher> getLocalVoucherList() {
        return localVoucherList;
    }

    public void setLocalVoucherList(List<LocalVoucher> localVoucherList) {
        this.localVoucherList = localVoucherList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (serviceDenominationId != null ? serviceDenominationId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ServiceDenomination)) {
            return false;
        }
        ServiceDenomination other = (ServiceDenomination) object;
        if ((this.serviceDenominationId == null && other.serviceDenominationId != null) || (this.serviceDenominationId != null && !this.serviceDenominationId.equals(other.serviceDenominationId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Momken.dtos.ServiceDenomination[ serviceDenominationId=" + serviceDenominationId + " ]";
    }
    
}
