/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Momken.dtos;

public class Voucher {

    private String requestId;
    private String serviceId;
    private double amount;
    private String pin;
    private String serial;
    private int Code;
    private String Message;
    private String ProviderTransactionId;
    private int providerId;
    private int voucherUploaded;

    public String getRequestId() {
        return requestId;
    }

    public void setRequestId(String requestId) {
        this.requestId = requestId;
    }

    public String getServiceId() {
        return serviceId;
    }

    public void setServiceId(String serviceId) {
        this.serviceId = serviceId;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public String getPin() {
        return pin;
    }

    public void setPin(String pin) {
        this.pin = pin;
    }

    public String getSerial() {
        return serial;
    }

    public void setSerial(String serial) {
        this.serial = serial;
    }

    public int getCode() {
        return Code;
    }

    public void setCode(int Code) {
        this.Code = Code;
    }

    public String getMessage() {
        return Message;
    }

    public void setMessage(String Message) {
        this.Message = Message;
    }

    public String getProviderTransactionId() {
        return ProviderTransactionId;
    }

    public void setProviderTransactionId(String ProviderTransactionId) {
        this.ProviderTransactionId = ProviderTransactionId;
    }

    public int getProviderId() {
        return providerId;
    }

    public void setProviderId(int providerId) {
        this.providerId = providerId;
    }

    public int getVoucherUploaded() {
        return voucherUploaded;
    }

    public void setVoucherUploaded(int voucherUploaded) {
        this.voucherUploaded = voucherUploaded;
    }

}
