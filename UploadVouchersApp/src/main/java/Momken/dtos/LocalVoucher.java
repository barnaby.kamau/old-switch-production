/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Momken.dtos;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Tony
 */
@Entity
@Table(name = "LOCAL_VOUCHER")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "LocalVoucher.findAll", query = "SELECT l FROM LocalVoucher l"),
    @NamedQuery(name = "LocalVoucher.findByVoucherPin", query = "SELECT l FROM LocalVoucher l WHERE l.voucherPin = :voucherPin"),
    @NamedQuery(name = "LocalVoucher.findByVoucherSerial", query = "SELECT l FROM LocalVoucher l WHERE l.voucherSerial = :voucherSerial"),
    @NamedQuery(name = "LocalVoucher.findByAvailable", query = "SELECT l FROM LocalVoucher l WHERE l.available = :available"),
    @NamedQuery(name = "LocalVoucher.findByInsertedDate", query = "SELECT l FROM LocalVoucher l WHERE l.insertedDate = :insertedDate"),
    @NamedQuery(name = "LocalVoucher.findByUpdatedDate", query = "SELECT l FROM LocalVoucher l WHERE l.updatedDate = :updatedDate")})
public class LocalVoucher implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "VOUCHER_PIN", nullable = false, length = 50)
    private String voucherPin;
    @Size(max = 50)
    @Column(name = "VOUCHER_SERIAL", length = 50)
    private String voucherSerial;
    private BigInteger available;
    @Column(name = "INSERTED_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date insertedDate;
    @Column(name = "UPDATED_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedDate;
    @Size(max = 50)
    @Column(name = "TRANSACTION_ID", length = 50)
    private String transactionId;
    @JoinColumn(name = "SERVICE_DENOMINATION_ID", referencedColumnName = "SERVICE_DENOMINATION_ID")
    @ManyToOne(fetch = FetchType.LAZY)
    private ServiceDenomination serviceDenominationId;

    public LocalVoucher() {
    }

    public LocalVoucher(String voucherPin) {
        this.voucherPin = voucherPin;
    }

    public String getVoucherPin() {
        return voucherPin;
    }

    public void setVoucherPin(String voucherPin) {
        this.voucherPin = voucherPin;
    }

    public String getVoucherSerial() {
        return voucherSerial;
    }

    public void setVoucherSerial(String voucherSerial) {
        this.voucherSerial = voucherSerial;
    }

    public BigInteger getAvailable() {
        return available;
    }

    public void setAvailable(BigInteger available) {
        this.available = available;
    }

    public Date getInsertedDate() {
        return insertedDate;
    }

    public void setInsertedDate(Date insertedDate) {
        this.insertedDate = insertedDate;
    }

    public Date getUpdatedDate() {
        return updatedDate;
    }

    public void setUpdatedDate(Date updatedDate) {
        this.updatedDate = updatedDate;
    }

    public ServiceDenomination getServiceDenominationId() {
        return serviceDenominationId;
    }

    public void setServiceDenominationId(ServiceDenomination serviceDenominationId) {
        this.serviceDenominationId = serviceDenominationId;
    }

    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (voucherPin != null ? voucherPin.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof LocalVoucher)) {
            return false;
        }
        LocalVoucher other = (LocalVoucher) object;
        if ((this.voucherPin == null && other.voucherPin != null) || (this.voucherPin != null && !this.voucherPin.equals(other.voucherPin))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Momken.dtos.LocalVoucher[ voucherPin=" + voucherPin + " ]";
    }

}
