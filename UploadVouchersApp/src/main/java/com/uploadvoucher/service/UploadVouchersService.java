/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uploadvoucher.service;

/**
 *
 * @author Antonios_knaguib
 */

import com.uploadvoucher.MomkenManager;
import com.uploadvoucher.entites.RSRequest;
import com.uploadvoucher.entites.RSResponse;
import java.util.UUID;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import static javax.ws.rs.core.MediaType.APPLICATION_JSON;
import javax.ws.rs.core.Response;
import static javax.ws.rs.core.Response.ok;
import org.apache.log4j.Logger;
import org.json.JSONException;

@Path("/")
@Produces(APPLICATION_JSON)
@RequestScoped
public class UploadVouchersService {

     static Logger logger = Logger.getLogger("R");
    @Inject
    MomkenManager manager;

    @Path("upload")
    @POST
    public Response uploadVoucher(@QueryParam("fileName") String FileName,@QueryParam("providerId") int providerId) throws JSONException {

        RSRequest request = new RSRequest();
        request.trxNumber = UUID.randomUUID().toString();
        request.fileName = FileName;
        request.providerId = providerId;
//        MomkenManager manager = new MomkenManager();
        RSResponse response = manager.upload(request);
        logger.info("Upload Vouchers Response : " + response);
        return ok(response, APPLICATION_JSON).build();
    }

}
