/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uploadvoucher;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 *
 * @author Tony
 */
public class Util {

    public String getProperty(String name) {
        String value = "";
        Properties prop = new Properties();
        InputStream input = null;

        try {

            String fileName = "config.properties";
            input = this.getClass().getClassLoader().getResourceAsStream(fileName);
//
//            // load a properties file
            prop.load(input);

            //get the property value and print it out
            value = prop.getProperty(name);
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            if (input != null) {
                try {
                    input.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return value;
    }
}
