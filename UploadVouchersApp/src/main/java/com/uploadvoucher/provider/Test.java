/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uploadvoucher.provider;

import Momken.dtos.Voucher;
import com.uploadvoucher.ApplicationConfig;
import com.uploadvoucher.Util;
import com.uploadvoucher.entites.RSRequest;
import com.uploadvoucher.entites.RSResponse;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

/**
 *
 * @author Antonios_knaguib
 */
public class Test {

//    private static final String FILE_NAME = "C:\\Offline Vouchers/Vouchers.xlsx";
//    private static final int BEE = 1;
//
//    public static void main(String[] args) {
//        RSRequest request = new RSRequest();
//        request.terminalId = "1";
//        request.user_Name = "User";
//        request.password = "pass";
//        List<Request> requests = new ArrayList<Request>();
//        RSResponse rSResponse = new RSResponse();
//        try {
//
//            FileInputStream excelFile = new FileInputStream(new File(FILE_NAME));
//            XSSFWorkbook workbook = new XSSFWorkbook(excelFile);
//            XSSFSheet datatypeSheet = workbook.getSheetAt(0);
//            Iterator<Row> iterator = datatypeSheet.iterator();
//            Voucher newRequest = null;
//            while (iterator.hasNext()) {
//                Row currentRow = iterator.next();
//                if (currentRow.getRowNum() == 0) {
//                    continue;
//                } else {
//                    newRequest = getNewRequest(request, currentRow);
//                    XSSFRow row = datatypeSheet.getRow(currentRow.getRowNum());
//                    getVoucher(newRequest);
//                    upadetRequest(newRequest, row);
//                }
//            }
//            excelFile.close();
//            FileOutputStream outputStream = new FileOutputStream(new File(FILE_NAME));
//            workbook.write(outputStream);
//            workbook.close();
//        } catch (FileNotFoundException e) {
//            e.printStackTrace();
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//
////        try {
////            FileInputStream fis = new FileInputStream(new File(FILE_NAME));
////            XSSFWorkbook workbook = new XSSFWorkbook(fis);
////            XSSFSheet sheet = workbook.getSheetAt(0);
////            //Create First Row
////            XSSFRow row1 = sheet.createRow(0);
////            XSSFCell r1c1 = row1.createCell(0);
////            r1c1.setCellValue("Emd Id");
////            XSSFCell r1c2 = row1.createCell(1);
////            r1c2.setCellValue("NAME");
////            XSSFCell r1c3 = row1.createCell(2);
////            r1c3.setCellValue("AGE");
////            //Create Second Row
////            XSSFRow row2 = sheet.createRow(1);
////            XSSFCell r2c1 = row2.createCell(0);
////            r2c1.setCellValue("1");
////            XSSFCell r2c2 = row2.createCell(1);
////            r2c2.setCellValue("Ram");
////            XSSFCell r2c3 = row2.createCell(2);
////            r2c3.setCellValue("20");
////            //Create Third Row
////            XSSFRow row3 = sheet.createRow(2);
////            XSSFCell r3c1 = row3.createCell(0);
////            r3c1.setCellValue("2");
////            XSSFCell r3c2 = row3.createCell(1);
////            r3c2.setCellValue("Shyam");
////            XSSFCell r3c3 = row3.createCell(2);
////            r3c3.setCellValue("25");
////            fis.close();
////            FileOutputStream fos = new FileOutputStream(new File(FILE_NAME));
////            workbook.write(fos);
////            fos.close();
////            System.out.println("Done");
////        } catch (FileNotFoundException e) {
////            e.printStackTrace();
////        } catch (IOException e) {
////            e.printStackTrace();
////        }
//    }
//
//    private static Voucher getNewRequest(RSRequest request, Row row) {
//        Voucher newRequest = getNewRequestFromExcel(row);
//        newRequest.setUserName(request.user_Name);
//        newRequest.setPassword(request.password);
//        newRequest.setTerminalId(request.terminalId);
//        return newRequest;
//    }
//
//    private static void getVoucher(Voucher request) {
//        Response response = new Response();
//        response.setCode(200);
//        response.setMessage("Success");
//        response.setPin("123456789");
//        response.setSerial_number("987654321");
//        response.setProviderTransactionId("123");
//        request.setCode(response.getCode());
//        request.setMessage(response.getMessage());
//        request.setProviderTransactionId(response.getProviderTransactionId());
//        request.setPin(response.getPin());
//        request.setSerial(response.getSerial_number());
//    }
//
//    private static void upadetRequest(Voucher request, XSSFRow row) {
//        row.createCell(3).setCellValue(request.getPin());
//        row.createCell(4).setCellValue(request.getSerial());
//        row.createCell(5).setCellValue(request.getCode());
//        row.createCell(6).setCellValue(request.getMessage());
//    }
//
//    private static Voucher getNewRequestFromExcel(Row row) {
//        Voucher request = new Voucher();
//        request.setProviderId(BEE);
//        request.setRequestId(row.getCell(0).getStringCellValue());
//        request.setServiceId(row.getCell(1).getStringCellValue());
//        request.setAmount(row.getCell(2).getNumericCellValue());
//        System.out.println(row.getCell(0).getStringCellValue());
//        System.out.println(row.getCell(1).getStringCellValue());
//        System.out.println(row.getCell(2).getNumericCellValue());
//        return request;
//    }
//}
    public static void main(String[] args) {
        Util util = new Util();
        System.out.println(util.getProperty("url"));
        System.out.println(util.getProperty("filePath"));
//        System.out.println(Util.class.getResource("config.properties").getFile());
    }
}
