/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.uploadvoucher.model;

import javax.interceptor.AroundInvoke;
import javax.interceptor.InvocationContext;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceUnit;

/**
 *
 * @author mohamed_aboulzahab
 */
public class ClearCashInterceptor {
       @PersistenceUnit(unitName = "VoucherPU")
       protected EntityManagerFactory emf;
       
        @AroundInvoke
       public Object clearJPACash(InvocationContext invocationContext) throws Exception{
           emf.getCache().evictAll();
           return invocationContext.proceed();
           
       } 
}
