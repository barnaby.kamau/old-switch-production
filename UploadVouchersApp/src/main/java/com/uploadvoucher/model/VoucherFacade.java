/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uploadvoucher.model;

import com.opencsv.CSVReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.util.Date;
import javax.annotation.Resource;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.interceptor.Interceptors;
import javax.sql.DataSource;
import org.apache.commons.lang.StringUtils;
//import au.com.bytecode.opencsv.CSVReader;

/**
 *
 * @author mohamed_aboulzahab
 */
@Stateless
@Interceptors(ClearCashInterceptor.class)
public class VoucherFacade extends VoucherAbstractFacade {

    //A.mohsen MOSS-463
    @Resource(name = "voucher/jdbc", lookup = "voucher/jdbc")
    private DataSource dataSource;
    private static final String SQL_INSERT = "INSERT INTO ${table}(${keys}) VALUES(${values})";
    private static final String TABLE_REGEX = "\\$\\{table\\}";
    private static final String KEYS_REGEX = "\\$\\{keys\\}";
    private static final String VALUES_REGEX = "\\$\\{values\\}";

//    private Connection connection;
    private static final char seprator = ',';

    public VoucherFacade() {
    }

    @TransactionAttribute(TransactionAttributeType.NEVER)
    public void loadCSV(String csvFile, String tableName,
            boolean truncateBeforeLoad,int Provider) throws Exception {

        CSVReader csvReader = null;
        if (null == dataSource.getConnection()) {
            throw new Exception("Not a valid connection.");
        }
        try {

            csvReader = new CSVReader(new FileReader(csvFile), this.seprator);

        } catch (Exception e) {
            e.printStackTrace();
            throw new Exception("Error occured while executing file. "
                    + e.getMessage());
        }

        String[] headerRow = csvReader.readNext();

        if (null == headerRow) {
            throw new FileNotFoundException(
                    "No columns defined in given CSV file."
                    + "Please check the CSV file format.");
        }

//		String questionmarks = StringUtils.repeat("?,", headerRow.length);
        String questionmarks = StringUtils.repeat("?,", 4);
        questionmarks = (String) questionmarks.subSequence(0, questionmarks
                .length() - 1);

        String query = SQL_INSERT.replaceFirst(TABLE_REGEX, tableName);
//		query = query
//				.replaceFirst(KEYS_REGEX, StringUtils.join(headerRow, ","));
        query = query
                .replaceFirst(KEYS_REGEX, "PIN,SERIAL,AMOUNT,VALID");
        query = query.replaceFirst(VALUES_REGEX, questionmarks);
//
//        System.out.println("Query: " + query);

        String[] nextLine;
        Connection con = null;
        PreparedStatement ps = null;
        try {
            con = dataSource.getConnection();
            con.setAutoCommit(false);
            ps = con.prepareStatement(query);

            if (truncateBeforeLoad) {
                //delete data from table before loading csv
                con.createStatement().execute("TRUNCATE TABLE " + tableName);
            }

            int batchSize = Integer.parseInt(headerRow[0]);
            int count = 0;
            Date date = null;
            while ((nextLine = csvReader.readNext()) != null || batchSize == count) {
//                System.out.println("batchSize >> "+batchSize);
//                System.out.println("count >> "+count);
                if (null != nextLine && count < batchSize) {
                    int index = 1;
                    for (String string : nextLine) {
                        if (index <= 3) {
                            date = DateUtil.convertToDate(string);
                            if (null != date) {
                                ps.setDate(index++, new java.sql.Date(date
                                        .getTime()));
                            } else {
                                ps.setString(index++, string);
                            }
                        } else {
                            break;
                        }
                    }
                    ps.setInt(4, 1);
                    ps.addBatch();
                    ++count;
                } else {
//                    System.out.println("End");
                    break;
                }
            }
//            System.out.println(ps.toString());
            ps.executeBatch(); // insert remaining records
            con.createStatement().execute("UPDATE " + tableName + " TV SET TV.VALID = 0\n"
                    + "WHERE EXISTS (SELECT 1 FROM LOCAL_VOUCHER LV WHERE LV.VOUCHER_PIN = TV.PIN AND LV.VOUCHER_SERIAL = TV.SERIAL ) ");
            con.createStatement().execute("MERGE INTO " + tableName + " t\n"
                    + "    USING (\n"
                    + "    SELECT SD.SERVICE_DENOMINATION_ID ,SDM.PROVIDER_DENOMINATION_VALUE\n"
                    + "        FROM SERVICE_DENOMINATION SD\n"
                    + "        INNER JOIN SERVICE_DENOMINATION_MAPPING SDM ON SDM.PROVIDER_SERVICE_ID = SD.PROVIDER_SERVICE_ID AND SDM.SERVICE_DENOMINATION_VALUE = SD.SERVICE_DENOMINATION_VALUE\n"
                    + "        WHERE SDM.provider_service_id = "+Provider+"\n"
                    + "    ) S\n"
                    + "    ON (S.PROVIDER_DENOMINATION_VALUE = T.AMOUNT)\n"
                    + "  WHEN MATCHED THEN\n"
                    + "    UPDATE SET T.SERVICE_DENOMINATION_ID = S.SERVICE_DENOMINATION_ID WHERE T.SERVICE_DENOMINATION_ID IS NULL ");
            con.createStatement().execute("INSERT INTO LOCAL_VOUCHER(\n"
                    + "VOUCHER_PIN,\n"
                    + "VOUCHER_SERIAL,\n"
                    + "INSERTED_DATE,\n"
                    + "AVAILABLE,\n"
                    + "SERVICE_DENOMINATION_ID\n"
                    + ")\n"
                    + "(\n"
                    + "SELECT temp.PIN,temp.SERIAL,SYSDATE,1,SERVICE_DENOMINATION_ID \n"
                    + "FROM \n"
                    + tableName + " temp\n"
                    + "WHERE temp.valid=1\n"
                    + ") ");
            con.commit();
        } catch (Exception e) {
            con.rollback();
            e.printStackTrace();
            throw new Exception(
                    "Error occured while loading data from file to database."
                    + e.getMessage());
        } finally {
            if (null != ps) {
                ps.close();
            }
            if (null != con) {
                con.close();
            }

            csvReader.close();
        }
    }

}
