/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.uploadvoucher.model;

import java.sql.Timestamp;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import static javax.persistence.TemporalType.DATE;
import static javax.persistence.TemporalType.TIMESTAMP;
import javax.persistence.TypedQuery;

/**
 *
 * @author Gohar
 */
public class VoucherAbstractFacade {

    //Gohar, MOSS-16
    @PersistenceContext(unitName = "VoucherPU")
    EntityManager em;

    public EntityManager getEntityManager() {
        return em;
    }

    public void setEntityManager(EntityManager em) {
        this.em = em;
    }

    public <T extends Object> T edit(T entity) {
        return getEntityManager().merge(entity);

    }

    public <T> void create(T entity) {
        getEntityManager().persist(entity);
    }

    public <T> void remove(T entity) {
        getEntityManager().remove(getEntityManager().merge(entity));
        //MOSS-1219
        getEntityManager().flush();
    }

    public <T> List<T> find(String namedQuery, Class<T> type, int maximumRecord) {

        if (namedQuery == null && namedQuery.isEmpty()) {
            throw new IllegalArgumentException("Named query can't be null or empty.");
        }

        return findByParameters(namedQuery, type, null, maximumRecord);
    }

    public <T> List<T> findAll(Class<T> entity) {
        javax.persistence.criteria.CriteriaQuery cq = getEntityManager().getCriteriaBuilder().createQuery();
        cq.select(cq.from(entity));
        return getEntityManager().createQuery(cq).getResultList();
    }

    public <T extends Object> T find(Class<T> entityClass, Object primaryKey) {
        return getEntityManager().find(entityClass, primaryKey);
    }

    public <T> List<T> find(String namedQuery, Class<T> type) {

        if (namedQuery == null && namedQuery.isEmpty()) {
            throw new IllegalArgumentException("Named query can't be null or empty.");
        }

        return findByParameters(namedQuery, type, null);
    }

    public <T> List<T> find(String namedQuery, Class<T> type, Map<String, Object> parameters) {

        if (namedQuery == null && namedQuery.isEmpty()) {
            throw new IllegalArgumentException("Named query can't be null or empty.");
        }

        return findByParameters(namedQuery, type, parameters);
    }

    public <T> List<T> find(String namedQuery, Class<T> type, Map<String, Object> parameters, int maximumRecord) {

        if (namedQuery == null && namedQuery.isEmpty()) {
            throw new IllegalArgumentException("Named query can't be null or empty.");
        }

        return findByParameters(namedQuery, type, parameters, maximumRecord);
    }

    private <T> List<T> findByParameters(String namedQuery, Class<T> type, Map<String, Object> parameters, int maximumRecord) {

        if (namedQuery == null && namedQuery.isEmpty()) {
            throw new IllegalArgumentException("Named query can't be null or empty.");
        }

        TypedQuery<T> query = getEntityManager().createNamedQuery(namedQuery, type);
        query.setMaxResults(maximumRecord);
        prepareParameterizeQuery(query, parameters);

        return query.getResultList();

    }

    private <T> List<T> findByParameters(String namedQuery, Class<T> type, Map<String, Object> parameters) {

        if (namedQuery == null && namedQuery.isEmpty()) {
            throw new IllegalArgumentException("Named query can't be null or empty.");
        }

        TypedQuery<T> query = getEntityManager().createNamedQuery(namedQuery, type);

        prepareParameterizeQuery(query, parameters);

        return query.getResultList();
    }

    private void prepareParameterizeQuery(Query query, Map<String, Object> parameters) {

        if (parameters == null || parameters.isEmpty()) {
            return;
        }

        Iterator<String> iterator = parameters.keySet().iterator();
        while (iterator.hasNext()) {
            String key = iterator.next();
            Object value = parameters.get(key);
            if (value instanceof Timestamp) {
                query.setParameter(key, (Timestamp) value, TIMESTAMP);
            } else if (value instanceof Date) {
                query.setParameter(key, (Date) value, DATE);
            } else {
                query.setParameter(key, value);
            }
        }

    }

    public List findRange(int[] range, Class entityClass) {
        javax.persistence.criteria.CriteriaQuery cq = getEntityManager().getCriteriaBuilder().createQuery();
        cq.select(cq.from(entityClass));
        javax.persistence.Query q = getEntityManager().createQuery(cq);
        q.setMaxResults(range[1] - range[0] + 1);
        q.setFirstResult(range[0]);
        return q.getResultList();
    }

    public int count(Class entityClass) {
        javax.persistence.criteria.CriteriaQuery cq = getEntityManager().getCriteriaBuilder().createQuery();
        javax.persistence.criteria.Root<Class> rt = cq.from(entityClass);
        cq.select(getEntityManager().getCriteriaBuilder().count(rt));
        javax.persistence.Query q = getEntityManager().createQuery(cq);
        return ((Long) q.getSingleResult()).intValue();
    }

}
