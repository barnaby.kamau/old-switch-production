/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uploadvoucher;

import Momken.dtos.LocalVoucher;
import Momken.dtos.ServiceDenomination;
import Momken.dtos.Voucher;
import com.uploadvoucher.entites.RSRequest;
import com.uploadvoucher.entites.RSResponse;
import com.uploadvoucher.model.VoucherFacade;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.math.BigInteger;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import org.apache.log4j.Logger;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

/**
 *
 * @author Tony
 */
@RequestScoped
public class MomkenManager {

    static Logger logger = Logger.getLogger("R");
    @EJB
    private VoucherFacade facade;
    private static final int BEE = 1;

    public RSResponse upload(RSRequest request) {
        RSResponse rSResponse = new RSResponse();
        Util util = new Util();
        String fileName = util.getProperty("filePath") + request.fileName + ".xlsx";
        try {
            logger.info("FileName = " + fileName);
            FileInputStream excelFile = new FileInputStream(new File(fileName));
            XSSFWorkbook workbook = new XSSFWorkbook(excelFile);
            XSSFSheet datatypeSheet = workbook.getSheetAt(0);
            Iterator<Row> iterator = datatypeSheet.iterator();
            Voucher newVoucher = null;
            while (iterator.hasNext()) {
                Row currentRow = iterator.next();
                double alreadyUploaded = 0;
                try {
                    alreadyUploaded = currentRow.getCell(8).getNumericCellValue();
                } catch (Exception e) {
                    alreadyUploaded = 0;
                }
                if (currentRow.getRowNum() == 0) {
                    continue;
                } else if (currentRow.getRowNum() > 0 && currentRow.getCell(5).getNumericCellValue() == 200 && alreadyUploaded != 1) {
                    newVoucher = getNewVoucher(currentRow);
                    if (saveVoucher(getVoucherEntity(newVoucher))) {
                        newVoucher.setVoucherUploaded(1);
                    } else {
                        newVoucher.setVoucherUploaded(0);
                    }
                    XSSFRow row = datatypeSheet.getRow(currentRow.getRowNum());
                    updateVoucher(newVoucher, row);
                }
            }
            excelFile.close();
            FileOutputStream outputStream = new FileOutputStream(new File(fileName));
            workbook.write(outputStream);
            workbook.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        rSResponse.code = 200;
        rSResponse.message = "Success";
        rSResponse.file_path = fileName;
        return rSResponse;
    }

    private boolean saveVoucher(LocalVoucher voucher) {
        if (voucher != null) {
            facade.edit(voucher);
            return true;
        } else {
            return false;
        }
    }

    private Voucher getNewVoucher(Row row) {
        Voucher voucher = new Voucher();
        voucher.setProviderId(BEE);
        voucher.setRequestId(row.getCell(0).getStringCellValue());
        voucher.setServiceId(row.getCell(1).getStringCellValue());
        voucher.setAmount(row.getCell(2).getNumericCellValue());
        voucher.setSerial(row.getCell(3).getStringCellValue());
        voucher.setPin(row.getCell(4).getStringCellValue());
        voucher.setProviderTransactionId(row.getCell(7).getStringCellValue());
        return voucher;
    }

    private void updateVoucher(Voucher voucher, XSSFRow row) {
        row.createCell(8).setCellValue(voucher.getVoucherUploaded());
    }

    private LocalVoucher getVoucherEntity(Voucher voucher) {
        LocalVoucher localVoucher = new LocalVoucher();
        localVoucher.setVoucherPin(voucher.getPin());
        localVoucher.setVoucherSerial(voucher.getSerial());
        localVoucher.setInsertedDate(new Date());
        localVoucher.setTransactionId(voucher.getProviderTransactionId());
        localVoucher.setAvailable(new BigInteger("1"));
        HashMap<String, Object> params = new HashMap<String, Object>();
        params.put("frontEndServiceId", new BigInteger(voucher.getServiceId()));
        params.put("serviceDenominationValue", voucher.getAmount());
        List<ServiceDenomination> serviceDenomination = facade.find("ServiceDenomination.findByFrontEndServiceIdwzDenomination", ServiceDenomination.class, params);
        if (serviceDenomination.isEmpty()) {
            return null;
        } else {
            localVoucher.setServiceDenominationId(serviceDenomination.get(0));
        }
        return localVoucher;
    }

}
