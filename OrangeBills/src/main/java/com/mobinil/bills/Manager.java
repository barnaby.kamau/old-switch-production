/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mobinil.bills;

import static javax.ws.rs.core.MediaType.APPLICATION_JSON;
import static javax.ws.rs.core.Response.ok;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;

import org.apache.log4j.Logger;
import org.json.JSONException;

/**
 *
 * @author Antonios_knaguib
 */
import com.mobinil.entites.RSRequest;
import com.mobinil.entites.RSResponse;
import com.mobinil.provider.Provider;
import com.mobinil.provider.Tamam;

@Path("/")
@Produces(APPLICATION_JSON)
public class Manager {

    public static Logger logger = Logger.getLogger("R");

    @GET
    public Response Inquiry(@QueryParam("request_id") String request_id, @QueryParam("mobile") String mobile, @QueryParam("userName") String userName, @QueryParam("password") String password) throws JSONException {
        RSResponse response = new RSResponse();
        RSRequest request = new RSRequest();
        request.requestId = request_id;
        request.requestType = 1;
        request.mobileNumber = mobile;
        request.userName = userName;
        request.password = password;
        logger.info("Inquiry Request : " + request);
    	Provider tamam = new Tamam();
        response = tamam.inquiry(request);
        logger.info("Inquiry Response : " + response);
        return ok(response, APPLICATION_JSON).build();
    }

    @POST
    public Response Payment(@QueryParam("request_id") String request_id, @QueryParam("mobile") String mobile, @QueryParam("amount") double amount, @QueryParam("userName") String userName, @QueryParam("password") String password ) throws JSONException {
        RSResponse response = new RSResponse();
        RSRequest request = new RSRequest();
        request.requestId = request_id;
        request.requestType = 2;
        request.mobileNumber = mobile;
        request.amount = amount;
        request.userName = userName;
        request.password = password;
        logger.info("Payment Request : " + request);
    	Provider tamam = new Tamam();
        response = tamam.payment(request);
        logger.info("Payment Response : " + response);
        return ok(response, APPLICATION_JSON).build();
    }
    
//    @GET
//    public Response Inquiry(@QueryParam("request_id") String request_id, @QueryParam("mobile") String mobile, @QueryParam("userName") String userName, @QueryParam("password") String password , @QueryParam("isCompany") boolean isCompany) throws JSONException {
//        RSResponse response = new RSResponse();
//        RSRequest request = new RSRequest();
//        request.requestId = request_id;
//        request.requestType = 1;
//        request.mobileNumber = mobile;
//        request.userName = userName;
//        request.password = password;
//        request.isCompany = isCompany;
//        logger.info("Inquiry Request : " + request);
//        try{
//        	Provider momkn = new Momkn();
//        	response = momkn.inquiry(request);
//        }catch(Exception ex){
//        	Provider tamam = new Tamam();
//            response = tamam.inquiry(request);
//        }
//        logger.info("Inquiry Response : " + response);
//        return ok(response, APPLICATION_JSON).build();
//    }
//
//    @POST
//    public Response Payment(@QueryParam("request_id") String request_id, @QueryParam("mobile") String mobile, @QueryParam("amount") double amount, @QueryParam("userName") String userName, @QueryParam("password") String password , @QueryParam("isCompany") boolean isCompany) throws JSONException {
//        RSResponse response = new RSResponse();
//        RSRequest request = new RSRequest();
//        request.requestId = request_id;
//        request.requestType = 2;
//        request.mobileNumber = mobile;
//        request.amount = amount;
//        request.userName = userName;
//        request.password = password;
//        request.isCompany = isCompany;
//        logger.info("Payment Request : " + request);
//        try{
//        	Provider momkn = new Momkn();
//        	response = momkn.payment(request);
//        }catch(Exception ex){
//        	Provider tamam = new Tamam();
//            response = tamam.payment(request);
//        }
//        logger.info("Payment Response : " + response);
//        return ok(response, APPLICATION_JSON).build();
//    }

}
