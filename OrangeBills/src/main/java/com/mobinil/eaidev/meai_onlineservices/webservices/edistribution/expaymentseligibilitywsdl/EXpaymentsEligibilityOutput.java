
package com.mobinil.eaidev.meai_onlineservices.webservices.edistribution.expaymentseligibilitywsdl;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for EXpaymentsEligibility_Output complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="EXpaymentsEligibility_Output">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="isEligible" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="lineStatus" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="reason" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ErrorDoc" type="{http://eaidev.mobinil.com/MEAI_OnlineServices/webServices/EDistribution/EXpaymentsEligibilityWSDL}ErrorDoc"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "EXpaymentsEligibility_Output", propOrder = {
    "isEligible",
    "lineStatus",
    "reason",
    "errorDoc"
})
public class EXpaymentsEligibilityOutput {

    @XmlElement(required = true, nillable = true)
    protected String isEligible;
    @XmlElementRef(name = "lineStatus", type = JAXBElement.class, required = false)
    protected JAXBElement<String> lineStatus;
    @XmlElementRef(name = "reason", type = JAXBElement.class, required = false)
    protected JAXBElement<String> reason;
    @XmlElement(name = "ErrorDoc", required = true, nillable = true)
    protected ErrorDoc errorDoc;

    /**
     * Gets the value of the isEligible property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsEligible() {
        return isEligible;
    }

    /**
     * Sets the value of the isEligible property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsEligible(String value) {
        this.isEligible = value;
    }

    /**
     * Gets the value of the lineStatus property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getLineStatus() {
        return lineStatus;
    }

    /**
     * Sets the value of the lineStatus property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setLineStatus(JAXBElement<String> value) {
        this.lineStatus = value;
    }

    /**
     * Gets the value of the reason property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getReason() {
        return reason;
    }

    /**
     * Sets the value of the reason property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setReason(JAXBElement<String> value) {
        this.reason = value;
    }

    /**
     * Gets the value of the errorDoc property.
     * 
     * @return
     *     possible object is
     *     {@link ErrorDoc }
     *     
     */
    public ErrorDoc getErrorDoc() {
        return errorDoc;
    }

    /**
     * Sets the value of the errorDoc property.
     * 
     * @param value
     *     allowed object is
     *     {@link ErrorDoc }
     *     
     */
    public void setErrorDoc(ErrorDoc value) {
        this.errorDoc = value;
    }

}
