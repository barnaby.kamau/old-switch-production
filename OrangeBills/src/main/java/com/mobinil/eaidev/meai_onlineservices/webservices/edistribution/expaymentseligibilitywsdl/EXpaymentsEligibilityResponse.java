
package com.mobinil.eaidev.meai_onlineservices.webservices.edistribution.expaymentseligibilitywsdl;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for EXpaymentsEligibilityResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="EXpaymentsEligibilityResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="EXpaymentsEligibility_Output" type="{http://eaidev.mobinil.com/MEAI_OnlineServices/webServices/EDistribution/EXpaymentsEligibilityWSDL}EXpaymentsEligibility_Output"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "EXpaymentsEligibilityResponse", propOrder = {
    "eXpaymentsEligibilityOutput"
})
public class EXpaymentsEligibilityResponse {

    @XmlElement(name = "EXpaymentsEligibility_Output", required = true, nillable = true)
    protected EXpaymentsEligibilityOutput eXpaymentsEligibilityOutput;

    /**
     * Gets the value of the eXpaymentsEligibilityOutput property.
     * 
     * @return
     *     possible object is
     *     {@link EXpaymentsEligibilityOutput }
     *     
     */
    public EXpaymentsEligibilityOutput getEXpaymentsEligibilityOutput() {
        return eXpaymentsEligibilityOutput;
    }

    /**
     * Sets the value of the eXpaymentsEligibilityOutput property.
     * 
     * @param value
     *     allowed object is
     *     {@link EXpaymentsEligibilityOutput }
     *     
     */
    public void setEXpaymentsEligibilityOutput(EXpaymentsEligibilityOutput value) {
        this.eXpaymentsEligibilityOutput = value;
    }

}
