
package com.mobinil.eaidev.meai_onlineservices.webservices.edistribution.expaymentseligibilitywsdl;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.mobinil.eaidev.meai_onlineservices.webservices.edistribution.expaymentseligibilitywsdl package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _EXpaymentsEligibilityResponse_QNAME = new QName("http://eaidev.mobinil.com/MEAI_OnlineServices/webServices/EDistribution/EXpaymentsEligibilityWSDL", "EXpaymentsEligibilityResponse");
    private final static QName _EXpaymentsEligibility_QNAME = new QName("http://eaidev.mobinil.com/MEAI_OnlineServices/webServices/EDistribution/EXpaymentsEligibilityWSDL", "EXpaymentsEligibility");
    private final static QName _EXpaymentsEligibilityOutputLineStatus_QNAME = new QName("", "lineStatus");
    private final static QName _EXpaymentsEligibilityOutputReason_QNAME = new QName("", "reason");
    private final static QName _EXpaymentsEligibilityInputTransactionID_QNAME = new QName("", "transactionID");
    private final static QName _ErrorDocErrorMessage_QNAME = new QName("", "errorMessage");
    private final static QName _ErrorDocErrorCode_QNAME = new QName("", "errorCode");
    private final static QName _ErrorDocStatus_QNAME = new QName("", "status");
    private final static QName _EXpaymentsEligibilityLogId_QNAME = new QName("", "logId");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.mobinil.eaidev.meai_onlineservices.webservices.edistribution.expaymentseligibilitywsdl
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link EXpaymentsEligibility }
     * 
     */
    public EXpaymentsEligibility createEXpaymentsEligibility() {
        return new EXpaymentsEligibility();
    }

    /**
     * Create an instance of {@link EXpaymentsEligibilityResponse }
     * 
     */
    public EXpaymentsEligibilityResponse createEXpaymentsEligibilityResponse() {
        return new EXpaymentsEligibilityResponse();
    }

    /**
     * Create an instance of {@link ErrorDoc }
     * 
     */
    public ErrorDoc createErrorDoc() {
        return new ErrorDoc();
    }

    /**
     * Create an instance of {@link EXpaymentsEligibilityOutput }
     * 
     */
    public EXpaymentsEligibilityOutput createEXpaymentsEligibilityOutput() {
        return new EXpaymentsEligibilityOutput();
    }

    /**
     * Create an instance of {@link EXpaymentsEligibilityInput }
     * 
     */
    public EXpaymentsEligibilityInput createEXpaymentsEligibilityInput() {
        return new EXpaymentsEligibilityInput();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link EXpaymentsEligibilityResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://eaidev.mobinil.com/MEAI_OnlineServices/webServices/EDistribution/EXpaymentsEligibilityWSDL", name = "EXpaymentsEligibilityResponse")
    public JAXBElement<EXpaymentsEligibilityResponse> createEXpaymentsEligibilityResponse(EXpaymentsEligibilityResponse value) {
        return new JAXBElement<EXpaymentsEligibilityResponse>(_EXpaymentsEligibilityResponse_QNAME, EXpaymentsEligibilityResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link EXpaymentsEligibility }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://eaidev.mobinil.com/MEAI_OnlineServices/webServices/EDistribution/EXpaymentsEligibilityWSDL", name = "EXpaymentsEligibility")
    public JAXBElement<EXpaymentsEligibility> createEXpaymentsEligibility(EXpaymentsEligibility value) {
        return new JAXBElement<EXpaymentsEligibility>(_EXpaymentsEligibility_QNAME, EXpaymentsEligibility.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "lineStatus", scope = EXpaymentsEligibilityOutput.class)
    public JAXBElement<String> createEXpaymentsEligibilityOutputLineStatus(String value) {
        return new JAXBElement<String>(_EXpaymentsEligibilityOutputLineStatus_QNAME, String.class, EXpaymentsEligibilityOutput.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "reason", scope = EXpaymentsEligibilityOutput.class)
    public JAXBElement<String> createEXpaymentsEligibilityOutputReason(String value) {
        return new JAXBElement<String>(_EXpaymentsEligibilityOutputReason_QNAME, String.class, EXpaymentsEligibilityOutput.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "transactionID", scope = EXpaymentsEligibilityInput.class)
    public JAXBElement<String> createEXpaymentsEligibilityInputTransactionID(String value) {
        return new JAXBElement<String>(_EXpaymentsEligibilityInputTransactionID_QNAME, String.class, EXpaymentsEligibilityInput.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "errorMessage", scope = ErrorDoc.class)
    public JAXBElement<String> createErrorDocErrorMessage(String value) {
        return new JAXBElement<String>(_ErrorDocErrorMessage_QNAME, String.class, ErrorDoc.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "errorCode", scope = ErrorDoc.class)
    public JAXBElement<String> createErrorDocErrorCode(String value) {
        return new JAXBElement<String>(_ErrorDocErrorCode_QNAME, String.class, ErrorDoc.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "status", scope = ErrorDoc.class)
    public JAXBElement<String> createErrorDocStatus(String value) {
        return new JAXBElement<String>(_ErrorDocStatus_QNAME, String.class, ErrorDoc.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "logId", scope = EXpaymentsEligibility.class)
    public JAXBElement<String> createEXpaymentsEligibilityLogId(String value) {
        return new JAXBElement<String>(_EXpaymentsEligibilityLogId_QNAME, String.class, EXpaymentsEligibility.class, value);
    }

}
