
package com.mobinil.eaidev.meai_onlineservices.webservices.edistribution.expaymentseligibilitywsdl;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for EXpaymentsEligibility complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="EXpaymentsEligibility">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="EXpaymentsEligibility_Input" type="{http://eaidev.mobinil.com/MEAI_OnlineServices/webServices/EDistribution/EXpaymentsEligibilityWSDL}EXpaymentsEligibility_Input"/>
 *         &lt;element name="logId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "EXpaymentsEligibility", propOrder = {
    "eXpaymentsEligibilityInput",
    "logId"
})
public class EXpaymentsEligibility {

    @XmlElement(name = "EXpaymentsEligibility_Input", required = true, nillable = true)
    protected EXpaymentsEligibilityInput eXpaymentsEligibilityInput;
    @XmlElementRef(name = "logId", type = JAXBElement.class, required = false)
    protected JAXBElement<String> logId;

    /**
     * Gets the value of the eXpaymentsEligibilityInput property.
     * 
     * @return
     *     possible object is
     *     {@link EXpaymentsEligibilityInput }
     *     
     */
    public EXpaymentsEligibilityInput getEXpaymentsEligibilityInput() {
        return eXpaymentsEligibilityInput;
    }

    /**
     * Sets the value of the eXpaymentsEligibilityInput property.
     * 
     * @param value
     *     allowed object is
     *     {@link EXpaymentsEligibilityInput }
     *     
     */
    public void setEXpaymentsEligibilityInput(EXpaymentsEligibilityInput value) {
        this.eXpaymentsEligibilityInput = value;
    }

    /**
     * Gets the value of the logId property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getLogId() {
        return logId;
    }

    /**
     * Sets the value of the logId property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setLogId(JAXBElement<String> value) {
        this.logId = value;
    }

}
