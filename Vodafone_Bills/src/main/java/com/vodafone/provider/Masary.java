/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vodafone.provider;

import static com.vodafone.bills.Manager.logger;
import com.vodafone.entites.RSRequest;
import com.vodafone.entites.RSResponse;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import org.apache.commons.lang.StringUtils;

/**
 *
 * @author Antonios_knaguib
 */
public class Masary implements Provider {

    private final String USER_AGENT = "Mozilla/5.0";
    private final String IP = "e-masary.net";

    @Override
    public RSResponse inquiry(RSRequest request) {
        RSResponse response = new RSResponse();
        response.request_id = request.requestId;
        try {
            try {
                if (request.requestId == null || request.userName == null || request.password == null || request.mobileNumber == null) {
                    response.Code = -1;
                    response.Message = "Invalid Request";
                    return response;
                }
            } catch (Exception e) {
//            System.out.println(e);
                response.Code = -1;
                response.Message = "Invalid Request";
                return response;
            }
            Map<String, String> map = new HashMap<>();
            map.put("id", request.userName);
            map.put("password", request.password);
            String getDateResponse = "";
            String getBillInquiryResponse = "";

            try {
                map.put("btc", "111");
                map.put("msisdn", request.mobileNumber);
                String provider_Request = buildInquiryURL(map);
                logger.info(this.getClass().getSimpleName() + " - " + "Provider Request : " + provider_Request.replace(request.password, StringUtils.repeat("*", request.password.length())));
                logger.info(this.getClass().getSimpleName() + " - " + "Provider Request Creation ended");
                logger.info(this.getClass().getSimpleName() + " - " + "Provider Request Sending started");
                getBillInquiryResponse = sendGet(provider_Request);
//                getBillInquiryResponse = "statusCode : 200 statusDesc : Success. Bill Type : 111 Billing Acount : 01003857275 Bill Amount : 0.0 Fees : 1.5 Total : 1.5 Bill Reference Number : 37059567 Customer Name : Bill Date : null";
//                logger.info(this.getClass().getSimpleName() + " - " + "Provider GetVoucher Request Creation ended , GetVoucher Response : " + getBillInquiryResponse);
                logger.info(this.getClass().getSimpleName() + " - " + "Provider Request Sending ended");

                if (getBillInquiryResponse != null) {
                    int toIndex = getBillInquiryResponse.indexOf("statusDesc");
                    String statusCode = getKeyValue(getBillInquiryResponse, "statusCode : ", toIndex).trim();
                    if (getBillInquiryResponse.indexOf("Bill Type") > 0) {
                        toIndex = getBillInquiryResponse.indexOf("Bill Type");
                    } else {
                        toIndex = getBillInquiryResponse.length();

                    }
//                    toIndex = getBillInquiryResponse.indexOf("Bill Type");
                    String statusDesc = getKeyValue(getBillInquiryResponse, "statusDesc : ", toIndex).trim();
                    if (statusCode.equals("200")) {
                        toIndex = getBillInquiryResponse.indexOf("Billing Acount");
                        String billType = getKeyValue(getBillInquiryResponse, "Bill Type : ", toIndex).trim();
                        toIndex = getBillInquiryResponse.indexOf("Bill Amount");
                        String billingAccount = getKeyValue(getBillInquiryResponse, "Billing Account : ", toIndex).trim();
                        toIndex = getBillInquiryResponse.indexOf("Fees");
                        String billAmount = getKeyValue(getBillInquiryResponse, "Bill Amount : ", toIndex).trim();
                        toIndex = getBillInquiryResponse.indexOf("Total");
                        String fees = getKeyValue(getBillInquiryResponse, "Fees : ", toIndex).trim();
                        toIndex = getBillInquiryResponse.indexOf("Bill Reference Number");
                        String total = getKeyValue(getBillInquiryResponse, "Total : ", toIndex).trim();
                        toIndex = getBillInquiryResponse.indexOf("Customer Name");
                        String billReferenceNumber = getKeyValue(getBillInquiryResponse, "Bill Reference Number : ", toIndex).trim();
                        toIndex = getBillInquiryResponse.length();
//                        String customerName = getKeyValue(getBillInquiryResponse, "Customer Name : ", toIndex);
                        if (Double.parseDouble(billAmount) > 0) {
                            response.Code = 200;
                            response.Message = "Successful Inquiry.";
                            response.billReferenceNumber = billReferenceNumber;
                            response.Total_Amount = Double.parseDouble(total);
                            response.amount = Double.parseDouble(billAmount);
                        } else {
                            response.Code = -2;
                            response.Message = "Provider Error : لا يوجد فواتير مستحقه";
                            response.ProviderTransactionId = "";
                        }
                    } else {
                        logger.error(this.getClass().getSimpleName() + " - " + "Provider Error : " + statusDesc);
                        response.Code = -2;
                        response.Message = "Provider Error : " + statusDesc;
                        response.ProviderTransactionId = "";
                    }

                } else {
                    logger.error(this.getClass().getSimpleName() + " - " + "Provider Error : null");
                    response.Code = -2;
                    response.Message = "Provider Error : null";
                    response.ProviderTransactionId = "";
                }
            } catch (Exception e) {
                logger.error(this.getClass().getSimpleName() + " - " + "Provider Error : Can't get Inquiry Response with error: " + e);
                response.Code = -1;
                response.Message = "Provider Error : Can't get Inquiry Response with error: " + e;
                response.ProviderTransactionId = "";

            }
        } catch (Exception e) {
            logger.error(e);
            response.Code = -3;
            response.Message = "General Error : " + e;
        }
        return response;
    }

    @Override
    public RSResponse payment(RSRequest request) {
        RSResponse response = new RSResponse();
        response.request_id = request.requestId;
        response.Total_Amount = request.amount;
//      Validations
        try {
            if (request.amount <= 0) {
                response.Code = -1;
                response.Message = "Invalid Amount must be greater than 0.0";
                return response;
            }
        } catch (Exception e) {
            response.Code = -1;
            response.Message = "Invalid Request.";
            return response;
        }

        try {
            logger.info("Provider Request Creation started");
            Map<String, String> map = new HashMap<>();
            map.put("id", request.userName);
            map.put("password", request.password);
            map.put("timestamp", sendGet(buildGetDate(map)));
            map.put("Sendertrx", request.requestId);
            map.put("btc", "124");
            map.put("amount", String.valueOf(request.amount));
            map.put("brn", request.billReferenceNumber);

            String provider_Request = buildPaymentURL(map);
            logger.info("Provider Request : " + provider_Request.toString().replace(request.password, StringUtils.repeat("*", request.password.length())));
            logger.info("Provider Request Creation ended");
            logger.info("Provider Request Sending started");
            String provider_Response = sendGet(provider_Request);
//            String provider_Response = "statusCode : 200 statusDesc : Success. TransactionID : 124215 TransactionDate : (02)25199413 Bill Amount : 64.1 Fees : 3.0 Total : 67.1 Bill Reference Number : 3fea754c-3fef-3fc3-3fee-3fddb82d81b5 Customer Name : انطونيوس قاعوذ نجيب عياد";
            logger.info("Provider Payment Response : " + provider_Response);
            if (provider_Response != null) {
                int toIndex = provider_Response.indexOf("statusDesc");
                String statusCode = getKeyValue(provider_Response, "statusCode : ", toIndex).trim();
                if (provider_Response.indexOf("TransactionID") > 0) {
                    toIndex = provider_Response.indexOf("TransactionID");
                } else {
                    toIndex = provider_Response.length();

                }
                String statusDesc = getKeyValue(provider_Response, "statusDesc : ", toIndex).trim();
                if (statusCode.equals("200")) {
                    toIndex = provider_Response.indexOf("TransactionDate");
                    String transactionID = getKeyValue(provider_Response, "TransactionID : ", toIndex).trim();
                    response.Code = 200;
                    response.Message = "Success";
                    response.ProviderTransactionId = transactionID;

                } else {
                    logger.error(this.getClass().getSimpleName() + " - " + "Provider Error : " + statusDesc);
                    response.Code = -2;
                    response.Message = "Provider Error : " + statusDesc;
                    response.ProviderTransactionId = "";
                }

            } else {
                logger.error(this.getClass().getSimpleName() + " - " + "Provider Error : null");
                response.Code = -2;
                response.Message = "Provider Error : null";
                response.ProviderTransactionId = "";
            }

            logger.info("Provider Response Unmarshal ended");

        } catch (Exception e) {
            logger.error(e);
            response.Code = -3;
            response.Message = "General Error : " + e;
        }

        return response;
    }

    private String buildInquiryURL(Map<String, String> map) {
        StringBuilder url = new StringBuilder();
        url.append("https://" + IP + "/topup/service?action=billinquiry");
        for (Map.Entry<String, String> entry : map.entrySet()) {
            url.append("&").append(entry.getKey()).append("=").append(entry.getValue());
        }
        return url.toString();
    }

    private String buildPaymentURL(Map<String, String> map) {
        StringBuilder url = new StringBuilder();
        url.append("https://" + IP + "/topup/service?action=billpayment");
        for (Map.Entry<String, String> entry : map.entrySet()) {
            url.append("&").append(entry.getKey()).append("=").append(entry.getValue());
        }
        return url.toString();
    }

    private String buildGetDate(Map<String, String> map) {
        StringBuilder url = new StringBuilder();
        url.append("https://" + IP + "/topup/service?action=gettime");
        for (Map.Entry<String, String> entry : map.entrySet()) {
            url.append("&").append(entry.getKey()).append("=").append(entry.getValue());
        }
        return url.toString();
    }

    // HTTP GET request
    private String sendGet(String url) throws Exception {
//        System.out.println(url);
        URL obj = new URL(url);
        HttpURLConnection con = (HttpURLConnection) obj.openConnection();

        // optional default is GET
        con.setRequestMethod("GET");

        //add request header
        con.setRequestProperty("User-Agent", USER_AGENT);

        int responseCode = con.getResponseCode();

        BufferedReader in = new BufferedReader(
                new InputStreamReader(con.getInputStream()));
        String inputLine;
        StringBuffer response = new StringBuffer();

        while ((inputLine = in.readLine()) != null) {
            response.append(inputLine);
        }
        in.close();

        //print result
//        System.out.println(response.toString());
        return response.toString();

    }

    private String getKeyValue(String fullMsg, String key, int toIndex) {
        return fullMsg.substring(fullMsg.indexOf(key) + key.length(), toIndex);
    }

}
