package com.vodafone.provider;

import com.vodafone.entites.RSRequest;
import com.vodafone.entites.RSResponse;

public interface Provider {
	
	public RSResponse inquiry(RSRequest request);
	public RSResponse payment(RSRequest request);

}
