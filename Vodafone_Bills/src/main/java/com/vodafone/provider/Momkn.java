package com.vodafone.provider;

import java.util.List;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.vodafone.entites.RSRequest;
import com.vodafone.entites.RSResponse;

public class Momkn implements Provider{
    static WebDriver driver;
    static  WebDriverWait wait;
//    static StringdriverPath = "C:/Users/antonios_knaguib/Documents/NetBeansProjects/TestApp/lib/";
    static String driverPath = "/lib/chromedriver.exe";
	public static Logger logger = Logger.getLogger("R");
	
	@Override
	public RSResponse inquiry(RSRequest rsRequest){
		
	      String mobileNumber = "";
	      RSResponse rsResponse = new RSResponse();
	      rsResponse.request_id = rsRequest.requestId;
	      double totalAmount;
//	      Validations
	      try {
	          mobileNumber = rsRequest.mobileNumber;
	          if (mobileNumber.startsWith("0")) {
	              mobileNumber = mobileNumber.substring(1, mobileNumber.length());
	              logger.info("Mobile Number Altered  "+mobileNumber);
	          }
	          if (mobileNumber.length() != 10) {
	              rsResponse.Code = -1;
	              rsResponse.Message = "Invalid Mobile Number.";
	              logger.info("Mobile Number is invalid");
	              return rsResponse;
	          }
	          rsRequest.mobileNumber = mobileNumber;
	          logger.info("Mobile Number validated successfully : " + mobileNumber);
	      } catch (Exception e) {
	          rsResponse.Code = -1;
	          rsResponse.Message = "Invalid Request.";
	          logger.error("An error occured E : " + e.toString());
	          return rsResponse;
	      }
	      logger.info("Will Start the Requests");
	      try{
	    	  try{
	    		  totalAmount = inquiry(rsRequest.mobileNumber, rsRequest.amount, rsRequest.isCompany);
	    	  }catch(Exception ex){
	    		  totalAmount = inquiry(rsRequest.mobileNumber, rsRequest.amount, rsRequest.isCompany); 
	    	  }
	    	 rsResponse.Total_Amount = totalAmount;
	      }catch (Exception e) {
	    	  rsResponse.Code = -3;
	        e.printStackTrace();
	        rsResponse.Message = "General Error : " + e;
	        return rsResponse;
	    }
			
			return rsResponse;
	}
	
	@Override
    public RSResponse payment(RSRequest request) {
        String Mobile_Number = "";
        RSResponse response = new RSResponse();
        response.request_id = request.requestId;
//        Validations
        try {
            Mobile_Number = request.mobileNumber;
            if (Mobile_Number.startsWith("0")) {
                Mobile_Number = Mobile_Number.substring(1, Mobile_Number.length());
            }
            if (Mobile_Number.length() != 10) {
                response.Code = -1;
                response.Message = "Invalid Mobile Number.";
                return response;
            }
            if (request.amount <= 0) {
                response.Code = -1;
                response.Message = "Invalid Amount must be greater than 0.0";
                return response;
            }
        } catch (Exception e) {
            response.Code = -1;
            response.Message = "Invalid Request.";
            return response;
        }
        request.mobileNumber = Mobile_Number;
	      logger.info("Will Start the Requests");
	      try{
	    	  try{
	    		  payment(request.mobileNumber, request.amount, request.isCompany);
	    	  }catch(Exception ex){
	    		  payment(request.mobileNumber, request.amount, request.isCompany); 
	    	  }
	      }catch (Exception e) {
	    	  response.Code = -3;
	        e.printStackTrace();
	        response.Message = "General Error : " + e;
	        return response;
	    }
			
			return response;
    }
    
	private static double inquiry(String mobileNumber, double amount , boolean isCompany) {
    	
        try {

            System.setProperty("webdriver.chrome.driver", driverPath);

            String loginPageUrl = "https://scp.orange.eg/web/guest/login";
            String username = "3600002.033";
            String password = "Mobinil@2010";
            driver = new ChromeDriver();
//            HtmlUnitDriver driver = new HtmlUnitDriver();
            logger.info("*******************");
            logger.info("launching Chrome browser");
            logger.info("opening orange login page");
            driver.get(loginPageUrl);
            logger.info("writing username in the username text box");
            driver.findElement(By.id("_58_login")).sendKeys(username);
            logger.info("writing password in the password textBox");
            driver.findElement(By.id("_58_password")).sendKeys(password);
            logger.info("Submitting the userName and password");
            driver.findElement(By.id("_58_password")).submit();
            logger.info("getting the inquiry page after logging in");
//			new Select(driver.findElement(By.className("dropdown"))).deselectByVisibleText("Orange PGW");
            driver.findElement(By.xpath("//*[@id='portlet_MainNavigationController_WAR_LayoutManagmentportlet']/div/div/div/ul"));
            List<WebElement> liElements = driver.findElements(By.tagName("li"));
            for (WebElement li : liElements) {
                if (li.getText().equals("Orange PGW")) {
                    li.click();
                    break;
                }
            }
            Thread.sleep(1000);
            driver.switchTo().frame(driver.findElement(By.id("_48_INSTANCE_hUHl2UkgDJIR_iframe")));
            Thread.sleep(500);
            logger.info("Opening the required page");
            driver.findElement(By.xpath("//*[@id='menuform:j_idt15']/ul/li[1]/a")).click();
            wait = new WebDriverWait(driver, 30);
            if(isCompany){
            	wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='form:j_idt49']/div[3]"))).click();
            	wait.until(ExpectedConditions.elementToBeClickable(By.id("form:j_idt49_1"))).click();
            }
            
            logger.info("Writing the mobile number ");
            wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='form:mobile_no']"))).sendKeys(mobileNumber);
            logger.info("Inquiry about the number");
            wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='form:j_idt149']"))).click();
            wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='form:invoicestblid:j_idt208']/div/div[2]/span"))).click();

            Thread.sleep(1000);
            wait.until(ExpectedConditions.elementToBeClickable(By.id("form:j_idt224"))).click();
            Thread.sleep(500);
            
//            double totalAmount = Double.parseDouble(driver.findElement(By.xpath("//*[@id='form:billAmount']")).getAttribute("value"));
            double totalAmount = Double.parseDouble(driver.findElement(By.xpath("//*[@id='form:billAmount']")).getAttribute("value"));
            logger.info("This is the totalamount required : " + totalAmount + "&& inserted Amount : " + amount);
            
            if(Math.round(totalAmount*100) %10 > 0){
            	totalAmount = Math.round(totalAmount*100)/100;
            	 logger.info("after rounding This is the totalamount required : " + totalAmount + "&& inserted Amount : " + amount);
            }
            driver.close();
            return totalAmount;
            
           

        } catch (Exception e) {
            logger.error("Exception happen in the inquiry Method " + e);
            //throw new RuntimeException("an Exception Happen in the inquiry method ");
            driver.close();
        }
        
        return -1;
    }

    
     private void payment(String mobileNumber , double amount , boolean isCompany) {
        try {
        	boolean partOfTheBill = true;
            System.setProperty("webdriver.chrome.driver", driverPath);

            String loginPageUrl = "https://scp.orange.eg/web/guest/login";
            String username = "3600002.033";
            String password = "Mobinil@2010";
            driver = new ChromeDriver();
//            HtmlUnitDriver driver = new HtmlUnitDriver();
            logger.info("*******************");
            logger.info("launching Chrome browser");
            logger.info("opening orange login page");
            driver.get(loginPageUrl);
            logger.info("writing username in the username text box");
            driver.findElement(By.id("_58_login")).sendKeys(username);
            logger.info("writing password in the password textBox");
            driver.findElement(By.id("_58_password")).sendKeys(password);
            logger.info("Submitting the userName and password");
            driver.findElement(By.id("_58_password")).submit();
            logger.info("getting the inquiry page after logging in");
//			new Select(driver.findElement(By.className("dropdown"))).deselectByVisibleText("Orange PGW");
            driver.findElement(By.xpath("//*[@id='portlet_MainNavigationController_WAR_LayoutManagmentportlet']/div/div/div/ul"));
            List<WebElement> liElements = driver.findElements(By.tagName("li"));
            for (WebElement li : liElements) {
                if (li.getText().equals("Orange PGW")) {
                    li.click();
                    break;
                }
            }
            Thread.sleep(1000);
            driver.switchTo().frame(driver.findElement(By.id("_48_INSTANCE_hUHl2UkgDJIR_iframe")));
            Thread.sleep(500);
            logger.info("Opening the required page");
            driver.findElement(By.xpath("//*[@id='menuform:j_idt15']/ul/li[1]/a")).click();
            wait = new WebDriverWait(driver, 30);
            if(isCompany){
            	wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='form:j_idt49']/div[3]"))).click();
            	wait.until(ExpectedConditions.elementToBeClickable(By.id("form:j_idt49_1"))).click();
            }

            logger.info("Writing the mobile number ");
            wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='form:mobile_no']"))).sendKeys(mobileNumber);
            logger.info("Inquiry about the number");
            wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='form:j_idt149']"))).click();
            wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='form:invoicestblid:j_idt208']/div/div[2]/span"))).click();

            Thread.sleep(1000);
            wait.until(ExpectedConditions.elementToBeClickable(By.id("form:j_idt224"))).click();
            Thread.sleep(500);
            
//            double totalAmount = Double.parseDouble(driver.findElement(By.xpath("//*[@id='form:billAmount']")).getAttribute("value"));
            double totalAmount = Double.parseDouble(driver.findElement(By.xpath("//*[@id='form:billAmount']")).getAttribute("value"));
            logger.info("This is the totalamount required : " + totalAmount + "&& inserted Amount : " + amount);
            
            if(Math.round(totalAmount*100) %10 > 0){
            	totalAmount = Math.round(totalAmount*100)/100;
            	 logger.info("after rounding This is the totalamount required : " + totalAmount + "&& inserted Amount : " + amount);
            }
            
         if(amount == totalAmount){
         	partOfTheBill=true;
         	logger.info("partOfTheBill = false");
         }
             driver.findElement(By.id("form:j_idt290")).clear();
             if(partOfTheBill == true){
             	wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='form:paymentType']/tbody/tr/td[3]/div/div[2]/span"))).click();
                 driver.findElement(By.id("form:billAmount")).clear();
                 Thread.sleep(500);
                 driver.findElement(By.id("form:billAmount")).sendKeys(Double.toString(amount));
             }
             
             wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='form:j_idt290']"))).sendKeys(String.valueOf(amount));

             wait.until(ExpectedConditions.elementToBeClickable(By.id("form:j_idt420"))).click();

             Thread.sleep(3000);
             
             driver.close();

        } catch (Exception e) {
            logger.error("Exception happen in the inquiry Method " + e);
            //throw new RuntimeException("an Exception Happen in the inquiry method ");
            driver.close();
        }
    }
    
}
