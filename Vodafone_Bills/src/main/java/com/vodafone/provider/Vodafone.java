/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vodafone.provider;

import com.manager.handler.HeaderHandlerResolver;
import com.vodafone.bills.Manager;
import com.vodafone.entites.RSRequest;
import com.vodafone.entites.RSResponse;
import genericinterface.epg.edafa.com.BillPaymentArg;
import genericinterface.epg.edafa.com.BillPaymentReturn;
import genericinterface.epg.edafa.com.EPGGenericInterface;
import genericinterface.epg.edafa.com.EPGGenericInterfaceService;
import genericinterface.epg.edafa.com.GetPostPaidOpenAmountArg;
import genericinterface.epg.edafa.com.GetPostPaidOpenAmountReturn;
import javax.xml.ws.handler.HandlerResolver;

public class Vodafone
        implements Provider {

    private static final String ChannelName = "Tamam";
    private static final String ChannelPassword = "V0daTamam18";
    private static final String ChannelCode = "5678";
    private static final String Langauge1 = "EN";
    private static final String Langauge2 = "EN";
//    private static final String PAYMENTTYPE = "ADV";
    private static final String PAYMENTTYPE = "OA";

    public RSResponse inquiry(RSRequest request) {
        RSResponse response = new RSResponse();
        response.request_id = request.requestId;
        response.billReferenceNumber = request.requestId;

        try {
            GetPostPaidOpenAmountArg provider_Request = getInquiryRequest(request);

            Manager.logger.info("Provider Inquiry Request Creation ended");
            Manager.logger.info("Provider Inquiry Request Sending started");
            GetPostPaidOpenAmountReturn ProviderResponse = inquiry(provider_Request);

            Manager.logger.info("Provider Inquiry Request Sending ended");
            if (ProviderResponse.getStatus().equals("1")) {
                response.Total_Amount = ProviderResponse.getOpenAmount() / new Double(100);
                response.Code = 200;
                response.Message = "Successful Inquiry.";
                return response;
            }
            response.Code = -2;
            response.Message = "Provider Error : " + ProviderResponse.getStatusDescription();
            return response;
        } catch (Exception e) {
            response.Code = -3;
            response.Message = "General Error : " + e;
            return response;
        }
    }

    public RSResponse payment(RSRequest request) {
        RSResponse response = new RSResponse();
        response.request_id = request.requestId;
        response.billReferenceNumber = request.requestId;
        try {
            if (request.amount <= 0.0D) {
                response.Code = -1;
                response.Message = "Invalid Amount must be greater than 0.0";
                return response;
            }
        } catch (Exception e) {
            response.Code = -1;
            response.Message = "Invalid Request.";
            return response;
        }

        try {
            BillPaymentArg billPaymentArg = getPaymentRequest(request);
            Manager.logger.info("Provider Payment Request Creation ended");
            Manager.logger.info("Provider Payment Request Sending started");
            BillPaymentReturn ProviderResponse = payment(billPaymentArg);

            Manager.logger.info("Provider Inquiry Request Sending ended");
            if (ProviderResponse.getStatus().equals("1")) {
                response.Total_Amount = ProviderResponse.getPaidAmount() / 100;
                response.Code = 200;
                response.Message = "Successful Payment.";
                response.request_id = request.requestId;
                response.ProviderTransactionId = ProviderResponse.getPgTrxId();
                return response;
            }
            response.Code = -101;
            response.Message = "Can't pay for this number now. Please try again later , Provider Error : " + ProviderResponse.getStatusDescription();
            return response;
        } catch (Exception e) {
            response.Code = -3;
            response.Message = "General Error : " + e;
            return response;
        }

    }

    private GetPostPaidOpenAmountArg getInquiryRequest(RSRequest request) {
        GetPostPaidOpenAmountArg getPostPaidOpenAmountInput = new GetPostPaidOpenAmountArg();
        getPostPaidOpenAmountInput.setChannelName(ChannelName);
        getPostPaidOpenAmountInput.setChannelPassword(ChannelPassword);
        getPostPaidOpenAmountInput.setChannelCode(ChannelCode);
        getPostPaidOpenAmountInput.setExternalTrxId(request.requestId);
        getPostPaidOpenAmountInput.setReceiverMSISDN(request.mobileNumber);
        return getPostPaidOpenAmountInput;
    }

    private BillPaymentArg getPaymentRequest(RSRequest request) {
        BillPaymentArg billPaymentInput = new BillPaymentArg();
        billPaymentInput.setChannelName(ChannelName);
        billPaymentInput.setChannelPassword(ChannelPassword);
        billPaymentInput.setChannelCode(ChannelCode);
        billPaymentInput.setExternalTrxId(request.requestId);
        billPaymentInput.setReceiverMSISDN("2" + request.mobileNumber);
        billPaymentInput.setRequestedAmount((long) (request.amount * 100));
        billPaymentInput.setNotifyFlag(true);
        billPaymentInput.setNotifyMSISDN("2" + request.mobileNumber);
        billPaymentInput.setLangauge1(Langauge1);
        billPaymentInput.setLangauge2(Langauge2);
        billPaymentInput.setPaymentType(PAYMENTTYPE);

        return billPaymentInput;
    }

    public GetPostPaidOpenAmountReturn inquiry(GetPostPaidOpenAmountArg getPostPaidOpenAmountInput) {
        try {
            EPGGenericInterfaceService service = new EPGGenericInterfaceService();
            EPGGenericInterface port = service.getEPGGenericInterfacePort();
            HeaderHandlerResolver handlerResolver = new HeaderHandlerResolver();
            service.setHandlerResolver((HandlerResolver) handlerResolver);
            return port.getPostPaidOpenAmount(getPostPaidOpenAmountInput);
        } catch (Exception ex) {
            return new GetPostPaidOpenAmountReturn();
        }
    }

    public BillPaymentReturn payment(BillPaymentArg billPaymentInput) {
        try {
            EPGGenericInterfaceService service = new EPGGenericInterfaceService();
            EPGGenericInterface port = service.getEPGGenericInterfacePort();
            HeaderHandlerResolver handlerResolver = new HeaderHandlerResolver();
            service.setHandlerResolver((HandlerResolver) handlerResolver);
            return port.billPayment(billPaymentInput);
        } catch (Exception ex) {
            return new BillPaymentReturn();
        }
    }

}
