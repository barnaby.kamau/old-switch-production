/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vodafone.provider;

import static com.vodafone.provider.Tamam.getCurrentTimeStamp;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.security.cert.X509Certificate;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import org.apache.commons.lang.StringUtils;

import com.vodafone.entites.RSRequest;
import java.math.BigDecimal;

import tamam.dtos.CaptureResponse;

/**
 *
 * @author Tony
 */
public class Test {

//    private static final String TERMINALID = "UA007816";
    private static final String TERMINALID = "UA007819";
    private static final String EAN = "4260469316524";
    private static final String CURRENCY = "818";
    private static final String LANGUAGE = "ENG";
    private static final String CHARSPERLINE = "40";
    private static final String HOST = "https://precision.epayworldwide.com/up-interface";

    public static void main(String[] args) {
//        RSRequest request = new RSRequest();
////        request.userName = "UP_Live_Distel_Egypt_(UAE)";
//        request.userName = "UP_Live_Distel_Egypt";
//        request.password = "67c3f094d029222a";
//        request.requestId = "123456568";
////        request.password = "Ay2imXy2dJn3NUL";
//        request.mobileNumber = "01200341116";
//        isValid(request);

//        double amount = 133.98;
        double a = 133.98;
        BigDecimal amount = new BigDecimal(String.valueOf(a));
        BigDecimal BDa = new BigDecimal("100");

        BigDecimal finalAmount = amount.multiply(BDa);

        System.out.println("BDc= " + finalAmount.intValue());
        long OpenAmount = 19830;
        double Total_Amount = OpenAmount / new Double(100);
        System.out.println(">>>>>>>>>>>>>>>>>>> "+Total_Amount);

    }

    private static boolean isValid(RSRequest request) {
        boolean IsValid = false;
        try {
            StringBuffer providerRequest = getValidationRequest(request);
            System.out.println("Provider Validation Request : " + providerRequest.toString().replace(request.password, StringUtils.repeat("*", request.password.length())));
            System.out.println("Provider Validation Request Creation ended");
            System.out.println("Provider Validation Request Sending started");
            String provider_Response = send(providerRequest.toString());
//            String provider_Response = "<?xml version=\"1.0\" encoding=\"UTF-8\" ?>\n"
//                    + "<RESPONSE TYPE=\"SALE\">\n"
//                    + "   <TERMINALID>UAE12854</TERMINALID>\n"
//                    + "   <LOCALDATETIME>2016-05-05 02:01:20</LOCALDATETIME>\n"
//                    + "   <SERVERDATETIME>2016-05-05 12:01:41</SERVERDATETIME>\n"
//                    + "   <TXID>1</TXID>\n"
//                    + "   <HOSTTXID>UAE12854000000</HOSTTXID>\n"
//                    + "   <AMOUNT>1</AMOUNT>\n"
//                    + "   <CURRENCY>818</CURRENCY>\n"
//                    + "   <LIMIT>0</LIMIT>\n"
//                    + "   <RESULT>0</RESULT>\n"
//                    + "   <RESULTTEXT>transaction successful</RESULTTEXT>\n"
//                    + "</RESPONSE>";
            System.out.println("Provider Validation Response : " + provider_Response);
            System.out.println("Provider Validation Request Sending ended");
            System.out.println("Provider Validation Response Unmarshal ended");
            GenericContentHandler handler = new GenericContentHandler();
            CaptureResponse ProviderResponse = handler.unMarshal(provider_Response.toString(), CaptureResponse.class);
            System.out.println("Provider Validation Response Unmarshal ended");
            if (ProviderResponse.getStatusCode() == 0) {
                IsValid = true;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return IsValid;
    }

    private static StringBuffer getValidationRequest(RSRequest request) {
        StringBuffer provider_Request = new StringBuffer("<?xml version=\"1.0\" encoding=\"UTF - 8\" ?>");
        provider_Request.append("<REQUEST Type=\"SALE\" mode=\"CAPTURE\">");
        provider_Request.append("<USERNAME>").append(request.userName).append("</USERNAME>");
        provider_Request.append("<PASSWORD>").append(request.password).append("</PASSWORD>");
        provider_Request.append("<TERMINALID>" + TERMINALID + "</TERMINALID>");
        provider_Request.append("<LOCALDATETIME>" + getCurrentTimeStamp() + "</LOCALDATETIME>");
        provider_Request.append("<TXID>").append(request.requestId).append("</TXID>");
        provider_Request.append("<CARD>");
        provider_Request.append("<EAN>").append(EAN).append("</EAN>");
        provider_Request.append("<PAN>").append(request.mobileNumber).append("</PAN>");
        provider_Request.append("</CARD>");
        provider_Request.append("<CURRENCY>").append(CURRENCY).append("</CURRENCY>");
        provider_Request.append("<AMOUNT>").append(request.amount).append("</AMOUNT>");
        provider_Request.append("<RECEIPT>");
        provider_Request.append("<LANGUAGE>").append(LANGUAGE).append("</LANGUAGE>");
        provider_Request.append("<CHARSPERLINE>").append(CHARSPERLINE).append("</CHARSPERLINE>");
        provider_Request.append("</RECEIPT>");
        provider_Request.append("</REQUEST>");
        return provider_Request;

    }

    private static String send(String xml) throws Exception {
        URL url = new URL(HOST);
        URLConnection conn;
        if (HOST.contains("https")) {
            // Create a trust manager that does not validate certificate chains
            TrustManager[] trustAllCerts = new TrustManager[]{new X509TrustManager() {
                public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                    return null;
                }

                public void checkClientTrusted(X509Certificate[] certs, String authType) {
                }

                public void checkServerTrusted(X509Certificate[] certs, String authType) {
                }
            }};

            // Install the all-trusting trust manager
            final SSLContext sc = SSLContext.getInstance("SSL");
            sc.init(null, trustAllCerts, new java.security.SecureRandom());
            HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
            // Create all-trusting host name verifier
            HostnameVerifier allHostsValid = new HostnameVerifier() {
                public boolean verify(String hostname, SSLSession session) {
                    return true;
                }
            };
            // Install the all-trusting host verifier
            HttpsURLConnection.setDefaultHostnameVerifier(allHostsValid);

            conn = (HttpsURLConnection) url.openConnection();
        } else {
            conn = url.openConnection();
        }
        conn.setRequestProperty("Content-Type", "text/xml");
        conn.setDoOutput(true);

        BufferedOutputStream bos = new BufferedOutputStream(conn.getOutputStream());
        bos.write(xml.getBytes("UTF-8"));
        bos.close();

        BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream()));
        String inputLine;
        StringBuffer response = new StringBuffer();

        while ((inputLine = in.readLine()) != null) {
            response.append(inputLine);
        }
        in.close();

        return response.toString();
    }

}
