package com.vodafone.provider;


public interface ContentHandler {
	
	public <T> T unMarshal(String content, Class<T> clasz);

	public <T> String marshal(T object);
	

}
