/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vodafone.entites;

import javax.xml.bind.annotation.XmlRootElement;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

/**
 *
 * @author Antonios_knaguib
 */
@XmlRootElement
public class RSResponse {

    public int Code;
    public String Message;
    public String Customer_Code;
    public int Open_Bills;
    public double Unbilled_Amount;
    public double Open_Amount;
    public double Total_Amount;
    public String Provider_Status;
    public int Bill_Cycle_Day;
    public String request_id;
    public String ProviderTransactionId;
    public String billReferenceNumber;
    public double amount;

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this,
                ToStringStyle.MULTI_LINE_STYLE);
    }

}
