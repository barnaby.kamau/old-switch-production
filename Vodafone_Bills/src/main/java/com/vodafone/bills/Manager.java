/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vodafone.bills;

import static javax.ws.rs.core.MediaType.APPLICATION_JSON;
import static javax.ws.rs.core.Response.ok;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;

import org.apache.log4j.Logger;
import org.json.JSONException;

/**
 *
 * @author Antonios_knaguib
 */
import com.vodafone.entites.RSRequest;
import com.vodafone.entites.RSResponse;
import com.vodafone.provider.Masary;
import com.vodafone.provider.Provider;
import com.vodafone.provider.Tamam;
import com.vodafone.provider.Vodafone;

@Path("/")
@Produces(APPLICATION_JSON)
public class Manager {

    public static Logger logger = Logger.getLogger("R");

    @GET
    public Response Inquiry(@QueryParam("request_id") String request_id, @QueryParam("mobile") String mobile, @QueryParam("userName") String userName, @QueryParam("password") String password) throws JSONException {
        RSResponse response = new RSResponse();
        RSRequest request = new RSRequest();
        request.requestId = request_id;
        request.requestType = 1;
        request.mobileNumber = mobile;
        request.userName = userName;
        request.password = password;
        logger.info("Inquiry Request : " + request);
//    	Provider tamam = new Tamam();
//        response = tamam.inquiry(request);
        Provider provider = new Vodafone();
        response = provider.inquiry(request);
        logger.info("Inquiry Response : " + response);
        return ok(response, APPLICATION_JSON).build();
    }

    @POST
    public Response Payment(RSRequest request) throws JSONException {
        RSResponse response = new RSResponse();
        logger.info("Payment Request : " + request);
//    	Provider provider = new Tamam();
        Provider provider = new Vodafone();
        response = provider.payment(request);
        logger.info("Payment Response : " + response);
        return ok(response, APPLICATION_JSON).build();
    }
}
