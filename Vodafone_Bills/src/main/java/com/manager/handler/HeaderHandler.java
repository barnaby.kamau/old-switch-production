package com.manager.handler;

import com.vodafone.bills.Manager;
import java.io.ByteArrayOutputStream;
import java.util.Collections;
import java.util.Set;
import javax.xml.namespace.QName;
import javax.xml.soap.SOAPMessage;
import javax.xml.ws.handler.MessageContext;
import javax.xml.ws.handler.soap.SOAPHandler;
import javax.xml.ws.handler.soap.SOAPMessageContext;

public class HeaderHandler
        implements SOAPHandler<SOAPMessageContext> {

    public Set<QName> getHeaders() {
        return Collections.emptySet();
    }

    public boolean handleMessage(SOAPMessageContext smc) {
        System.out.println("IN handleMessage");
        Manager.logger.info("IN handleMessage");
        Boolean outboundProperty = (Boolean) smc.get("javax.xml.ws.handler.message.outbound");
        ByteArrayOutputStream out = new ByteArrayOutputStream();

        try {
            SOAPMessage message = smc.getMessage();
            message.writeTo(out);
            String msg = out.toString();
            logInboundMsg(msg);

        } catch (Exception ex) {
            Manager.logger.error(ex);
        }

        return outboundProperty.booleanValue();
    }

    public boolean handleFault(SOAPMessageContext context) {
        Manager.logger.info("IN Fault");
        SOAPMessage message = context.getMessage();
        ByteArrayOutputStream out = new ByteArrayOutputStream();

        Boolean outboundProperty = (Boolean) context.get("javax.xml.ws.handler.message.outbound");
        try {
            message.writeTo(out);
            String msg = out.toString();
            if (outboundProperty.booleanValue()) {
                logOutboundMsg(msg);
            } else {
                logInboundMsg(msg);
            }

        } catch (Exception e) {

            Manager.logger.error(e);
        }

        return true;
    }

    public void close(MessageContext context) {
    }

    private void logOutboundMsg(String msg) {
        Manager.logger.info("Outbound message:\n" + msg);
    }

    private void logInboundMsg(String msg) {
        Manager.logger.info("Inbound message:\n" + msg);
    }
}
