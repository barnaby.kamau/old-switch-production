//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.8-b130911.1802 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2021.08.04 at 08:18:05 PM EET 
//


package genericinterface.epg.edafa.com;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for reverseBillPaymentArg complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="reverseBillPaymentArg">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="channelName" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="channelPassword" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="channelCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="terminalId" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="externalTrxId" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="extraIn1" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="extraIn2" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="extraIn3" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="pgTrxId" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "reverseBillPaymentArg", propOrder = {
    "channelName",
    "channelPassword",
    "channelCode",
    "terminalId",
    "externalTrxId",
    "extraIn1",
    "extraIn2",
    "extraIn3",
    "pgTrxId"
})
public class ReverseBillPaymentArg {

    @XmlElement(required = true)
    protected String channelName;
    @XmlElement(required = true)
    protected String channelPassword;
    @XmlElement(required = true)
    protected String channelCode;
    @XmlElement(required = true, nillable = true)
    protected String terminalId;
    @XmlElement(required = true)
    protected String externalTrxId;
    @XmlElement(required = true, nillable = true)
    protected String extraIn1;
    @XmlElement(required = true, nillable = true)
    protected String extraIn2;
    @XmlElement(required = true, nillable = true)
    protected String extraIn3;
    @XmlElement(required = true, nillable = true)
    protected String pgTrxId;

    /**
     * Gets the value of the channelName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getChannelName() {
        return channelName;
    }

    /**
     * Sets the value of the channelName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setChannelName(String value) {
        this.channelName = value;
    }

    /**
     * Gets the value of the channelPassword property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getChannelPassword() {
        return channelPassword;
    }

    /**
     * Sets the value of the channelPassword property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setChannelPassword(String value) {
        this.channelPassword = value;
    }

    /**
     * Gets the value of the channelCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getChannelCode() {
        return channelCode;
    }

    /**
     * Sets the value of the channelCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setChannelCode(String value) {
        this.channelCode = value;
    }

    /**
     * Gets the value of the terminalId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTerminalId() {
        return terminalId;
    }

    /**
     * Sets the value of the terminalId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTerminalId(String value) {
        this.terminalId = value;
    }

    /**
     * Gets the value of the externalTrxId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getExternalTrxId() {
        return externalTrxId;
    }

    /**
     * Sets the value of the externalTrxId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setExternalTrxId(String value) {
        this.externalTrxId = value;
    }

    /**
     * Gets the value of the extraIn1 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getExtraIn1() {
        return extraIn1;
    }

    /**
     * Sets the value of the extraIn1 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setExtraIn1(String value) {
        this.extraIn1 = value;
    }

    /**
     * Gets the value of the extraIn2 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getExtraIn2() {
        return extraIn2;
    }

    /**
     * Sets the value of the extraIn2 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setExtraIn2(String value) {
        this.extraIn2 = value;
    }

    /**
     * Gets the value of the extraIn3 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getExtraIn3() {
        return extraIn3;
    }

    /**
     * Sets the value of the extraIn3 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setExtraIn3(String value) {
        this.extraIn3 = value;
    }

    /**
     * Gets the value of the pgTrxId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPgTrxId() {
        return pgTrxId;
    }

    /**
     * Sets the value of the pgTrxId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPgTrxId(String value) {
        this.pgTrxId = value;
    }

}
