//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.8-b130911.1802 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2021.08.04 at 08:18:05 PM EET 
//


package genericinterface.epg.edafa.com;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for TopupUnRegCreditCard complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="TopupUnRegCreditCard">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="TopupUnRegCreditCardArg" type="{http://com.edafa.epg.genericInterface/}topupUnRegCreditCardArg" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TopupUnRegCreditCard", propOrder = {
    "topupUnRegCreditCardArg"
})
public class TopupUnRegCreditCard {

    @XmlElement(name = "TopupUnRegCreditCardArg")
    protected TopupUnRegCreditCardArg topupUnRegCreditCardArg;

    /**
     * Gets the value of the topupUnRegCreditCardArg property.
     * 
     * @return
     *     possible object is
     *     {@link TopupUnRegCreditCardArg }
     *     
     */
    public TopupUnRegCreditCardArg getTopupUnRegCreditCardArg() {
        return topupUnRegCreditCardArg;
    }

    /**
     * Sets the value of the topupUnRegCreditCardArg property.
     * 
     * @param value
     *     allowed object is
     *     {@link TopupUnRegCreditCardArg }
     *     
     */
    public void setTopupUnRegCreditCardArg(TopupUnRegCreditCardArg value) {
        this.topupUnRegCreditCardArg = value;
    }

}
