//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.8-b130911.1802 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2021.08.04 at 08:18:05 PM EET 
//


package genericinterface.epg.edafa.com;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for AdslCustomerInquiry complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="AdslCustomerInquiry">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="AdslCustomerInquiryInput" type="{http://com.edafa.epg.genericInterface/}adslCustomerInquiryArg" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AdslCustomerInquiry", propOrder = {
    "adslCustomerInquiryInput"
})
public class AdslCustomerInquiry {

    @XmlElement(name = "AdslCustomerInquiryInput")
    protected AdslCustomerInquiryArg adslCustomerInquiryInput;

    /**
     * Gets the value of the adslCustomerInquiryInput property.
     * 
     * @return
     *     possible object is
     *     {@link AdslCustomerInquiryArg }
     *     
     */
    public AdslCustomerInquiryArg getAdslCustomerInquiryInput() {
        return adslCustomerInquiryInput;
    }

    /**
     * Sets the value of the adslCustomerInquiryInput property.
     * 
     * @param value
     *     allowed object is
     *     {@link AdslCustomerInquiryArg }
     *     
     */
    public void setAdslCustomerInquiryInput(AdslCustomerInquiryArg value) {
        this.adslCustomerInquiryInput = value;
    }

}
