//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.8-b130911.1802 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2021.08.04 at 08:18:05 PM EET 
//


package genericinterface.epg.edafa.com;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for billPaymentViaCreditCard complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="billPaymentViaCreditCard">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="BillPaymentViaCreditCardInput" type="{http://com.edafa.epg.genericInterface/}billPaymentViaCreditCardArg" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "billPaymentViaCreditCard", propOrder = {
    "billPaymentViaCreditCardInput"
})
public class BillPaymentViaCreditCard {

    @XmlElement(name = "BillPaymentViaCreditCardInput")
    protected BillPaymentViaCreditCardArg billPaymentViaCreditCardInput;

    /**
     * Gets the value of the billPaymentViaCreditCardInput property.
     * 
     * @return
     *     possible object is
     *     {@link BillPaymentViaCreditCardArg }
     *     
     */
    public BillPaymentViaCreditCardArg getBillPaymentViaCreditCardInput() {
        return billPaymentViaCreditCardInput;
    }

    /**
     * Sets the value of the billPaymentViaCreditCardInput property.
     * 
     * @param value
     *     allowed object is
     *     {@link BillPaymentViaCreditCardArg }
     *     
     */
    public void setBillPaymentViaCreditCardInput(BillPaymentViaCreditCardArg value) {
        this.billPaymentViaCreditCardInput = value;
    }

}
