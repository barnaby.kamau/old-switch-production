//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.8-b130911.1802 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2021.08.04 at 08:18:05 PM EET 
//


package genericinterface.epg.edafa.com;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for getStoredTokensReturn complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="getStoredTokensReturn">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="pgTrxId" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="status" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="statusDescription" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="extraOut1" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="extraOut2" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="tokenList" type="{http://com.edafa.epg.genericInterface/}payfortCustomerCreditCardResponse" maxOccurs="unbounded"/>
 *         &lt;element name="profileStatus" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "getStoredTokensReturn", propOrder = {
    "pgTrxId",
    "status",
    "statusDescription",
    "extraOut1",
    "extraOut2",
    "tokenList",
    "profileStatus"
})
public class GetStoredTokensReturn {

    @XmlElement(required = true, nillable = true)
    protected String pgTrxId;
    @XmlElement(required = true)
    protected String status;
    @XmlElement(required = true)
    protected String statusDescription;
    @XmlElement(required = true, nillable = true)
    protected String extraOut1;
    @XmlElement(required = true, nillable = true)
    protected String extraOut2;
    @XmlElement(required = true)
    protected List<PayfortCustomerCreditCardResponse> tokenList;
    @XmlElement(required = true)
    protected String profileStatus;

    /**
     * Gets the value of the pgTrxId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPgTrxId() {
        return pgTrxId;
    }

    /**
     * Sets the value of the pgTrxId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPgTrxId(String value) {
        this.pgTrxId = value;
    }

    /**
     * Gets the value of the status property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStatus() {
        return status;
    }

    /**
     * Sets the value of the status property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStatus(String value) {
        this.status = value;
    }

    /**
     * Gets the value of the statusDescription property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStatusDescription() {
        return statusDescription;
    }

    /**
     * Sets the value of the statusDescription property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStatusDescription(String value) {
        this.statusDescription = value;
    }

    /**
     * Gets the value of the extraOut1 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getExtraOut1() {
        return extraOut1;
    }

    /**
     * Sets the value of the extraOut1 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setExtraOut1(String value) {
        this.extraOut1 = value;
    }

    /**
     * Gets the value of the extraOut2 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getExtraOut2() {
        return extraOut2;
    }

    /**
     * Sets the value of the extraOut2 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setExtraOut2(String value) {
        this.extraOut2 = value;
    }

    /**
     * Gets the value of the tokenList property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the tokenList property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getTokenList().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link PayfortCustomerCreditCardResponse }
     * 
     * 
     */
    public List<PayfortCustomerCreditCardResponse> getTokenList() {
        if (tokenList == null) {
            tokenList = new ArrayList<PayfortCustomerCreditCardResponse>();
        }
        return this.tokenList;
    }

    /**
     * Gets the value of the profileStatus property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProfileStatus() {
        return profileStatus;
    }

    /**
     * Sets the value of the profileStatus property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProfileStatus(String value) {
        this.profileStatus = value;
    }

}
