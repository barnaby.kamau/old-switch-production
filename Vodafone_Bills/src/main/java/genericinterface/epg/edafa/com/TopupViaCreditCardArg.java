//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.8-b130911.1802 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2021.08.04 at 08:18:05 PM EET 
//


package genericinterface.epg.edafa.com;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for topupViaCreditCardArg complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="topupViaCreditCardArg">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="channelName" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="channelPassword" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="channelCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="terminalId" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="externalTrxId" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="extraIn1" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="extraIn2" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="extraIn3" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="location" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="receiverMSISDN" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="requestedAmount" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="notifyFlag" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="langauge1" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="langauge2" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="promoId" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="cardHolderMSISDN" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="cardHolderCustomerCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="creditCardNo" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="ccv" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="pin" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="expiryDate" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="notifyCardHolderFlag" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "topupViaCreditCardArg", propOrder = {
    "channelName",
    "channelPassword",
    "channelCode",
    "terminalId",
    "externalTrxId",
    "extraIn1",
    "extraIn2",
    "extraIn3",
    "location",
    "receiverMSISDN",
    "requestedAmount",
    "notifyFlag",
    "langauge1",
    "langauge2",
    "promoId",
    "cardHolderMSISDN",
    "cardHolderCustomerCode",
    "creditCardNo",
    "ccv",
    "pin",
    "expiryDate",
    "notifyCardHolderFlag"
})
public class TopupViaCreditCardArg {

    @XmlElement(required = true)
    protected String channelName;
    @XmlElement(required = true)
    protected String channelPassword;
    @XmlElement(required = true)
    protected String channelCode;
    @XmlElement(required = true, nillable = true)
    protected String terminalId;
    @XmlElement(required = true)
    protected String externalTrxId;
    @XmlElement(required = true, nillable = true)
    protected String extraIn1;
    @XmlElement(required = true, nillable = true)
    protected String extraIn2;
    @XmlElement(required = true, nillable = true)
    protected String extraIn3;
    @XmlElement(required = true, nillable = true)
    protected String location;
    @XmlElement(required = true, nillable = true)
    protected String receiverMSISDN;
    @XmlElement(required = true, type = Long.class, nillable = true)
    protected Long requestedAmount;
    @XmlElement(required = true, type = Boolean.class, nillable = true)
    protected Boolean notifyFlag;
    @XmlElement(required = true, nillable = true)
    protected String langauge1;
    @XmlElement(required = true, nillable = true)
    protected String langauge2;
    @XmlElement(required = true, nillable = true)
    protected String promoId;
    @XmlElement(required = true)
    protected String cardHolderMSISDN;
    @XmlElement(required = true, nillable = true)
    protected String cardHolderCustomerCode;
    @XmlElement(required = true)
    protected String creditCardNo;
    @XmlElement(required = true, nillable = true)
    protected String ccv;
    @XmlElement(required = true, nillable = true)
    protected String pin;
    @XmlElement(required = true, nillable = true)
    protected String expiryDate;
    @XmlElement(required = true, type = Boolean.class, nillable = true)
    protected Boolean notifyCardHolderFlag;

    /**
     * Gets the value of the channelName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getChannelName() {
        return channelName;
    }

    /**
     * Sets the value of the channelName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setChannelName(String value) {
        this.channelName = value;
    }

    /**
     * Gets the value of the channelPassword property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getChannelPassword() {
        return channelPassword;
    }

    /**
     * Sets the value of the channelPassword property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setChannelPassword(String value) {
        this.channelPassword = value;
    }

    /**
     * Gets the value of the channelCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getChannelCode() {
        return channelCode;
    }

    /**
     * Sets the value of the channelCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setChannelCode(String value) {
        this.channelCode = value;
    }

    /**
     * Gets the value of the terminalId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTerminalId() {
        return terminalId;
    }

    /**
     * Sets the value of the terminalId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTerminalId(String value) {
        this.terminalId = value;
    }

    /**
     * Gets the value of the externalTrxId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getExternalTrxId() {
        return externalTrxId;
    }

    /**
     * Sets the value of the externalTrxId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setExternalTrxId(String value) {
        this.externalTrxId = value;
    }

    /**
     * Gets the value of the extraIn1 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getExtraIn1() {
        return extraIn1;
    }

    /**
     * Sets the value of the extraIn1 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setExtraIn1(String value) {
        this.extraIn1 = value;
    }

    /**
     * Gets the value of the extraIn2 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getExtraIn2() {
        return extraIn2;
    }

    /**
     * Sets the value of the extraIn2 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setExtraIn2(String value) {
        this.extraIn2 = value;
    }

    /**
     * Gets the value of the extraIn3 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getExtraIn3() {
        return extraIn3;
    }

    /**
     * Sets the value of the extraIn3 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setExtraIn3(String value) {
        this.extraIn3 = value;
    }

    /**
     * Gets the value of the location property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLocation() {
        return location;
    }

    /**
     * Sets the value of the location property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLocation(String value) {
        this.location = value;
    }

    /**
     * Gets the value of the receiverMSISDN property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReceiverMSISDN() {
        return receiverMSISDN;
    }

    /**
     * Sets the value of the receiverMSISDN property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReceiverMSISDN(String value) {
        this.receiverMSISDN = value;
    }

    /**
     * Gets the value of the requestedAmount property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getRequestedAmount() {
        return requestedAmount;
    }

    /**
     * Sets the value of the requestedAmount property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setRequestedAmount(Long value) {
        this.requestedAmount = value;
    }

    /**
     * Gets the value of the notifyFlag property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isNotifyFlag() {
        return notifyFlag;
    }

    /**
     * Sets the value of the notifyFlag property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setNotifyFlag(Boolean value) {
        this.notifyFlag = value;
    }

    /**
     * Gets the value of the langauge1 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLangauge1() {
        return langauge1;
    }

    /**
     * Sets the value of the langauge1 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLangauge1(String value) {
        this.langauge1 = value;
    }

    /**
     * Gets the value of the langauge2 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLangauge2() {
        return langauge2;
    }

    /**
     * Sets the value of the langauge2 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLangauge2(String value) {
        this.langauge2 = value;
    }

    /**
     * Gets the value of the promoId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPromoId() {
        return promoId;
    }

    /**
     * Sets the value of the promoId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPromoId(String value) {
        this.promoId = value;
    }

    /**
     * Gets the value of the cardHolderMSISDN property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCardHolderMSISDN() {
        return cardHolderMSISDN;
    }

    /**
     * Sets the value of the cardHolderMSISDN property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCardHolderMSISDN(String value) {
        this.cardHolderMSISDN = value;
    }

    /**
     * Gets the value of the cardHolderCustomerCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCardHolderCustomerCode() {
        return cardHolderCustomerCode;
    }

    /**
     * Sets the value of the cardHolderCustomerCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCardHolderCustomerCode(String value) {
        this.cardHolderCustomerCode = value;
    }

    /**
     * Gets the value of the creditCardNo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCreditCardNo() {
        return creditCardNo;
    }

    /**
     * Sets the value of the creditCardNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCreditCardNo(String value) {
        this.creditCardNo = value;
    }

    /**
     * Gets the value of the ccv property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCcv() {
        return ccv;
    }

    /**
     * Sets the value of the ccv property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCcv(String value) {
        this.ccv = value;
    }

    /**
     * Gets the value of the pin property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPin() {
        return pin;
    }

    /**
     * Sets the value of the pin property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPin(String value) {
        this.pin = value;
    }

    /**
     * Gets the value of the expiryDate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getExpiryDate() {
        return expiryDate;
    }

    /**
     * Sets the value of the expiryDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setExpiryDate(String value) {
        this.expiryDate = value;
    }

    /**
     * Gets the value of the notifyCardHolderFlag property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isNotifyCardHolderFlag() {
        return notifyCardHolderFlag;
    }

    /**
     * Sets the value of the notifyCardHolderFlag property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setNotifyCardHolderFlag(Boolean value) {
        this.notifyCardHolderFlag = value;
    }

}
