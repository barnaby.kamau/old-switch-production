package rest.application.config;

import java.util.Set;
import javax.ws.rs.core.Application;
import javax.ws.rs.ApplicationPath;

@ApplicationPath("rest")
public class ApplicationConfig extends Application {

	public Set<Class<?>> getClasses() {
		return getRestClasses();
	}

	private Set<Class<?>> getRestClasses() {
		Set<Class<?>> resources = new java.util.HashSet<Class<?>>();
		resources.add(com.momkn.rest.BulkVoucherService.class);
		resources.add(rest.application.config.RestAuthenticationFilter.class);
		return resources;
	}
}