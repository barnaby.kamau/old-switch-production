package rest.application.config;

import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.ext.Provider;

import com.momkn.common.Constant;
import com.momkn.common.CustomWebApplicationException;

@Provider
public class RestAuthenticationFilter implements javax.ws.rs.container.ContainerRequestFilter {
	public static final String AUTHENTICATION_HEADER = "Authorization";

	@Override
	public void filter(ContainerRequestContext containerRequest) throws CustomWebApplicationException {
		String authCredentials = containerRequest.getHeaderString(AUTHENTICATION_HEADER);
		// better injected
		AuthenticationService authenticationService = new AuthenticationService();
		boolean authenticationStatus = authenticationService.authenticate(authCredentials);
		if (!authenticationStatus) {
			throw new CustomWebApplicationException(Constant.UNAUTHORIZED_RESPONSE,
					Status.UNAUTHORIZED.getStatusCode());
		}
	}
}