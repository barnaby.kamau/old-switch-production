package rest.application.config;

import java.io.IOException;
import java.io.InputStream;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.Base64;
import java.util.Properties;
import java.util.StringTokenizer;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

import com.momkn.common.CryptoConverter;

public class AuthenticationService {

	private static final String KEY = "699e2633-1721-4f";
	private static String username;
	private static String password;
	
	static {
		loadConfig();
	}

	public static void loadConfig() {
		Properties props = new Properties();
		String resourceName = "/resources/momknConfig.properties";
		try (InputStream resourceStream = AuthenticationService.class.getResourceAsStream(resourceName)) {
			props.load(resourceStream);
			username = props.getProperty("username");
			username = CryptoConverter.decryptWithAESKey(username, KEY);
			password = props.getProperty("password");
			password = CryptoConverter.decryptWithAESKey(password, KEY);
		} catch (IOException | InvalidKeyException | NoSuchAlgorithmException | NoSuchPaddingException | IllegalBlockSizeException | BadPaddingException e) {
			System.out.println(e);
			e.printStackTrace();
		}
	}
	
	public static void main(String[] args) {
		try {
			System.out.println(CryptoConverter.encryptWithAESKey("momknBulkVoucherAdminUser", KEY));
			System.out.println(CryptoConverter.encryptWithAESKey("momknBulkVoucherAdminPass", KEY));
		} catch (InvalidKeyException | NoSuchAlgorithmException | NoSuchPaddingException | IllegalBlockSizeException
				| BadPaddingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public boolean authenticate(String authCredentials) {
		if (null == authCredentials)
			return false;
		// header value format will be "Basic encodedstring" for Basic
		// authentication. Example "Basic YWRtaW46YWRtaW4="
		final String encodedUserPassword = authCredentials.replaceFirst("Basic" + " ", "");
		String usernameAndPassword = null;
		try {
			byte[] decodedBytes = Base64.getDecoder().decode(encodedUserPassword);
			usernameAndPassword = new String(decodedBytes, "UTF-8");
		} catch (IOException e) {
			e.printStackTrace();
		}
		final StringTokenizer tokenizer = new StringTokenizer(usernameAndPassword, ":");
		final String tokenUsername = tokenizer.nextToken();
		final String tokenPassword = tokenizer.nextToken();

		
		boolean authenticationStatus = username.equals(tokenUsername) && password.equals(tokenPassword);
		return authenticationStatus;
	}
}