package com.momkn.common;

public interface Constant {
	
	public static final long SUCCCESS_CODE = 200L;
	public static final long PENDIING_CODE = 720L;
	public static final long GENERAL_ERROR_CODE = 710L;
	public static final long SYSTEM_ERROR_CODE = 700L;
	public static final long NO_VOUCHER_FOUND_ERROR_CODE = 790L;
	
	public static final String SUCCCESS_RESPONSE = "Success";
	public static final String PENDING_RESPONSE = "Pending transaction";
	public static final String GENERAL_ERROR_RESPONSE = "General error";
	public static final String SYSTEM_ERROR_RESPONSE = "System error";
	public static final String MISSING_REQUEST_BODY_RESPONSE = "Request body cannot be blank";
	public static final String MISSING_MANDATORY_PARAMETER_RESPONSE = "Missing mandatory parameter";
	public static final String UNAUTHORIZED_RESPONSE = "You are unauthorized to make this request";
	public static final String NO_VOUCHER_FOUND_RESPONSE = "No vouchers found with this count";
}
