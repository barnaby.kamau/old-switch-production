package com.momkn.common;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import flexjson.JSONSerializer;

public class CustomWebApplicationException extends WebApplicationException {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private static JSONSerializer jsonSerializer = new JSONSerializer().exclude("*.class");

	public class ErrorResonse {
		public String error;
	}

	public CustomWebApplicationException(String message, int staus) {
		super(Response.status(staus).entity(jsonSerializer.serialize(getErrorResonse(message)))
				.type(MediaType.APPLICATION_JSON_TYPE).build());
	}

	public CustomWebApplicationException() {
	}

	private static ErrorResonse getErrorResonse(String error) {
		ErrorResonse ErrorResonse = new CustomWebApplicationException().new ErrorResonse();
		ErrorResonse.error = error;
		return ErrorResonse;
	}

}
