package com.momkn.common;

public class BusinessException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private long errorCode;
	private String errorDescription;
	private Exception exception;
	
	public BusinessException(long errorCode, String errorDescription){
		this.errorCode = errorCode;
		this.errorDescription = errorDescription;
	}
	
	public BusinessException(long errorCode, String errorDescription, Exception exception){
		this.errorCode = errorCode;
		this.errorDescription = errorDescription;
		this.exception = exception;
	}

	public long getErrorCode() {
		return errorCode;
	}

	public String getErrorDescription() {
		return errorDescription;
	}
	
	public Exception getException() {
		return exception;
	}
}
