package com.momkn.common;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.List;

public class Util {

	static DateFormat dateFormat = new SimpleDateFormat("yyyy'-'MM'-'dd'T'HH':'mm':'ss");

	public static boolean isNullOrEmpty(String value) {
		return value == null || value.isEmpty();
	}

	public static boolean isNullOrEmpty(String... args) {
		for (String arg : args) {
			if (arg == null || arg.isEmpty()) {
				return true;
			}
		}
		return false;
	}

	public static boolean isNullOrEmpty(List<?> list) {
		if (list == null || list.isEmpty()) {
			return true;
		}
		return false;
	}

}
