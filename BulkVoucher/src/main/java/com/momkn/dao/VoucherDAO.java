package com.momkn.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.momkn.common.BusinessException;
import com.momkn.common.Constant;
import com.momkn.common.SystemException;
import com.momkn.model.Voucher;

public class VoucherDAO extends DAO {

    private static final Logger logger = LogManager.getLogger(VoucherDAO.class);

    private static final String selectVouchersSQL = "SELECT V.VOUCHER_PIN,V.VOUCHER_SERIAL FROM LOCAL_VOUCHER V  WHERE V.TRANSACTION_ID = ? ";

    private static final String updateVouchersSQL = "MERGE INTO LOCAL_VOUCHER T\n"
            + "   USING (SELECT * from (SELECT VOUCHER_PIN,VOUCHER_SERIAL,\n"
            + "		 AVAILABLE,UPDATED_DATE,TRANSACTION_ID\n"
            + " FROM SERVICE_DENOMINATION SD \n"
            + " INNER JOIN LOCAL_VOUCHER V ON SD.SERVICE_DENOMINATION_ID = V.SERVICE_DENOMINATION_ID \n"
            + " WHERE SD.FRONT_END_SERVICE_ID = ? AND SD.SERVICE_DENOMINATION_VALUE = ? AND V.AVAILABLE = 1"
            + " ORDER BY V.INSERTED_DATE ASC)where ROWNUM <= ?) S\n"
            + "   ON (T.VOUCHER_PIN = S.VOUCHER_PIN AND T.VOUCHER_SERIAL = S.VOUCHER_SERIAL)\n"
            + "   WHEN MATCHED THEN UPDATE SET T.AVAILABLE = 0, T.UPDATED_DATE = sysdate, T.TRANSACTION_ID = ?";

    private static final String rollbackUpdateVouchersSQL = "UPDATE LOCAL_VOUCHER SET AVAILABLE = 1, UPDATED_DATE = NULL, TRANSACTION_ID = NULL WHERE TRANSACTION_ID = ? ";

    public List<Voucher> getVouchers(long serviceId, double denomination, String transactionId, int count)
            throws Exception, BusinessException, SystemException {
        List<Voucher> vouchers = new ArrayList<>();
        try (Connection connection = getConnection()) {
            logger.info(updateVouchersSQL);
            PreparedStatement updateVoucherPS = connection.prepareStatement(updateVouchersSQL);
            updateVoucherPS.setLong(1, serviceId);
            updateVoucherPS.setDouble(2, denomination);
            updateVoucherPS.setInt(3, count);
            updateVoucherPS.setString(4, transactionId);
            int rows = updateVoucherPS.executeUpdate();
//            updateVoucherPS.addBatch();
//            int[] upd = updateVoucherPS.executeBatch();
//            updateVoucherPS.
            logger.info(">>>>>>>>>>>>>>>>>>>>>>>>>>>>" + count + ">>>>>>>>>>>>>" + rows);
            if (rows != count) {
                updateVoucherPS = connection.prepareStatement(rollbackUpdateVouchersSQL);
                updateVoucherPS.setString(1, transactionId);
                rows = updateVoucherPS.executeUpdate();
//                updateVoucherPS.addBatch();
//                upd = updateVoucherPS.executeBatch();
                logger.error("No [" + count + "] vouchers found with service id [" + serviceId + "] and"
                        + " denomination [" + denomination + "].");
                throw new BusinessException(Constant.NO_VOUCHER_FOUND_ERROR_CODE, Constant.NO_VOUCHER_FOUND_RESPONSE);
            } else {
                Voucher voucher;
                PreparedStatement selectVoucherPS = connection.prepareStatement(selectVouchersSQL);
                selectVoucherPS.setString(1, transactionId);
                ResultSet resultSet = selectVoucherPS.executeQuery();
                while (resultSet.next()) {
                    voucher = new Voucher();
                    voucher.setPin(getFormatedPIN(resultSet.getString("VOUCHER_PIN")));
                    voucher.setSerial(resultSet.getString("VOUCHER_SERIAL"));
                    vouchers.add(voucher);
                }
            }
        } catch (SQLException e) {
            logger.error("Error while Getting [ " + count + " ] vouchers service id [" + serviceId + "], "
                    + "denomination [" + denomination + "]." + e);
            throw new SystemException(Constant.SYSTEM_ERROR_CODE, "Error while Getting [ " + count + " ] vouchers service id [" + serviceId + "], "
                    + "denomination [" + denomination + "].");
        }
        return vouchers;
    }

    private String getFormatedPIN(String pin) {

        StringBuilder result = new StringBuilder();
        for (int i = 0; i < pin.length(); i++) {
            if (i % 4 == 0 && i != 0) {
                result.append("-");
            }

            result.append(pin.charAt(i));
        }
        return result.toString();

    }

}
