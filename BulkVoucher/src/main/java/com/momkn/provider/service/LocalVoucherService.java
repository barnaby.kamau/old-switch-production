package com.momkn.provider.service;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.momkn.common.BusinessException;
import com.momkn.common.Constant;
import com.momkn.common.SystemException;
import com.momkn.dao.VoucherDAO;
import com.momkn.model.BulkVoucherRequest;
import com.momkn.model.BulkVoucherResponse;
import com.momkn.model.Voucher;

public class LocalVoucherService {

	private static final Logger logger = LogManager.getLogger(LocalVoucherService.class);
	private VoucherDAO voucherDAO = null;

	public BulkVoucherResponse loadVouchers(BulkVoucherRequest request) throws SystemException, BusinessException  {
		logger.info("Load vouchers for transaction id " + request.getTransactionId());
		BulkVoucherResponse response = null;
		try {
			voucherDAO = new VoucherDAO();
			List<Voucher> vouchers = voucherDAO.getVouchers(request.getServiceId(), request.getAmount(), 
					request.getTransactionId(), request.getCount());
			
			if (vouchers == null || vouchers.isEmpty()){
				logger.info("No vouchers found with service id [" + request.getServiceId() + "], " + 
						"denomination [" + request.getAmount() + "] and count [ " + request.getCount() + "].");
				throw new BusinessException(Constant.NO_VOUCHER_FOUND_ERROR_CODE, Constant.NO_VOUCHER_FOUND_RESPONSE);
			}
			response = new BulkVoucherResponse();
			response.setVouchers(vouchers);
			response.setTransactionId(request.getTransactionId());
			
			logger.info("Load voucher for transaction id " + request.getTransactionId() + " loaded successfully");
			return response;
		} catch (BusinessException e) {
			throw e;
		} catch (SystemException e) {
			throw e;
		} catch (Exception e) {
			logger.error("Error while getting [ "+ request.getCount() + " ] vouchers service id [" + request.getServiceId() + "], "
					+ "denomination [" + request.getAmount() + "]." +  e);
			throw new SystemException(Constant.SYSTEM_ERROR_CODE, Constant.SYSTEM_ERROR_RESPONSE);
		}
	}
}