/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.momkn.rest;

import flexjson.JSONSerializer;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.momkn.common.BusinessException;
import com.momkn.common.Constant;
import com.momkn.common.CustomWebApplicationException;
import com.momkn.common.SystemException;
import com.momkn.common.Util;
import com.momkn.model.BulkVoucherRequest;
import com.momkn.model.BulkVoucherResponse;
import com.momkn.provider.service.LocalVoucherService;

@Path("service")
@Consumes({ "application/json" })
@Produces({ "application/json" })
public class BulkVoucherService {
	private static final Logger logger = LogManager.getLogger(BulkVoucherService.class);
	private JSONSerializer jsonSerializer = (new JSONSerializer()).exclude(new String[] { "*.class" });

	private Response ok(Object obj) {
		return createOkResponseFor(obj);
	}

	private Response createOkResponseFor(Object domainObject) {
		return Response.ok(this.jsonSerializer.serialize(domainObject)).header("Cache-Control", "no-cache")
				.header("Pragma", "no-cache").build();
	}    
	
    @POST
    public Response getVouchers(BulkVoucherRequest request) {
    	BulkVoucherResponse response = null;	
		try {
	    	logger.info("Voucher request : " + request);
	    	validateInquireBillsRequestRequest(request);
	    	LocalVoucherService service = new LocalVoucherService();
	    	response = service.loadVouchers(request);
	    	logger.info("Voucher response : " + response + " loaded successfully from local voucher");
			return ok(response);
		} catch (SystemException e) {
			throw new CustomWebApplicationException(Constant.SYSTEM_ERROR_RESPONSE, (int) e.getErrorCode());
		} catch (BusinessException e) {
			throw new CustomWebApplicationException(e.getErrorDescription(), (int) e.getErrorCode());
		}
    }
    
	private void validateInquireBillsRequestRequest(BulkVoucherRequest request) {
		if (request == null) {
			throw new CustomWebApplicationException(Constant.MISSING_REQUEST_BODY_RESPONSE, Status.BAD_REQUEST.getStatusCode());
		} else if (Util.isNullOrEmpty(request.getTransactionId()) || request.getAmount() == 0 
				|| request.getCount() == 0 || request.getServiceId() == 0) {
			throw new CustomWebApplicationException(Constant.MISSING_MANDATORY_PARAMETER_RESPONSE, Status.BAD_REQUEST.getStatusCode());
		}
	}
}
