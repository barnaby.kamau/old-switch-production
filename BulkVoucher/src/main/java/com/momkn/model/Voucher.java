/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.momkn.model;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

/**
 *
 * * @author Omnya
 */
public class Voucher {

    public String pin;
    public String serial;
    
    public String getPin() {
		return pin;
	}
    
    public void setPin(String pin) {
		this.pin = pin;
	}
    
    public String getSerial() {
		return serial;
	}
    
    public void setSerial(String serial) {
		this.serial = serial;
	}

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this,
                ToStringStyle.MULTI_LINE_STYLE);
    }
}
