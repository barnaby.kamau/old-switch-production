/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Bee.dtos;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 *
 * @author Antonios_knaguib
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "data")
public class RequestData {

    @XmlElement(name = "serviceVersion", required = true)
    protected String serviceVersion;
    @XmlElement(name = "transactionId")
    protected String transactionId;
    @XmlElement(name = "serviceAccountId")
    protected String serviceAccountId;
    @XmlElement(name = "amount")
    protected double amount;
    @XmlElement(name = "totalAmount")
    protected double totalAmount;
    @XmlElement(name = "requestMap")
    protected RequestMap requestMap;

    public String getServiceVersion() {
        return serviceVersion;
    }

    public void setServiceVersion(String serviceVersion) {
        this.serviceVersion = serviceVersion;
    }

    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    public String getServiceAccountId() {
        return serviceAccountId;
    }

    public void setServiceAccountId(String serviceAccountId) {
        this.serviceAccountId = serviceAccountId;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public double getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(double totalAmount) {
        this.totalAmount = totalAmount;
    }

    public RequestMap getRequestMap() {
        return requestMap;
    }

    public void setRequestMap(RequestMap requestMap) {
        this.requestMap = requestMap;
    }

}
