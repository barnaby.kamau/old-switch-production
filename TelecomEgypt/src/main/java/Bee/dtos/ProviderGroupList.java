/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Bee.dtos;

import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 *
 * @author Antonios_knaguib
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "providerGroupList")
public class ProviderGroupList {
     @XmlElement(name = "providerGroup", required = true)
    protected List<ProviderGroup> providerGroups;

    public List<ProviderGroup> getProviderGroups() {
        return providerGroups;
    }

    public void setProviderGroups(List<ProviderGroup> providerGroups) {
        this.providerGroups = providerGroups;
    }
     
}
