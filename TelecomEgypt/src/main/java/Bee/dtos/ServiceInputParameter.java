/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Bee.dtos;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 *
 * @author Antonios_knaguib
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "serviceInputParameter")
public class ServiceInputParameter {

    @XmlElement(name = "serviceAccountId", required = true)
    protected int serviceAccountId;
    @XmlElement(name = "id", required = true)
    protected int id;
    @XmlElement(name = "name", required = true)
    protected String name;
    @XmlElement(name = "position", required = true)
    protected int position;
    @XmlElement(name = "visible", required = true)
    protected int visible;
    @XmlElement(name = "required", required = true)
    protected int required;
    @XmlElement(name = "type", required = true)
    protected int type;
    @XmlElement(name = "isClientId", required = true)
    protected int isClientId;
    @XmlElement(name = "defaultValue", required = true)
    protected double defaultValue;
    @XmlElement(name = "minLength", required = true)
    protected int minLength;
    @XmlElement(name = "maxLength", required = true)
    protected int maxLength;

    public int getServiceAccountId() {
        return serviceAccountId;
    }

    public void setServiceAccountId(int serviceAccountId) {
        this.serviceAccountId = serviceAccountId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    public int getVisible() {
        return visible;
    }

    public void setVisible(int visible) {
        this.visible = visible;
    }

    public int getRequired() {
        return required;
    }

    public void setRequired(int required) {
        this.required = required;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public int getIsClientId() {
        return isClientId;
    }

    public void setIsClientId(int isClientId) {
        this.isClientId = isClientId;
    }

    public double getDefaultValue() {
        return defaultValue;
    }

    public void setDefaultValue(double defaultValue) {
        this.defaultValue = defaultValue;
    }

    public int getMinLength() {
        return minLength;
    }

    public void setMinLength(int minLength) {
        this.minLength = minLength;
    }

    public int getMaxLength() {
        return maxLength;
    }

    public void setMaxLength(int maxLength) {
        this.maxLength = maxLength;
    }

}
