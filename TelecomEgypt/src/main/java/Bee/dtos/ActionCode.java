/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Bee.dtos;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;

/**
 *
 * @author Antonios_knaguib
 */
@XmlType(name = "action")
@XmlEnum
public enum ActionCode {

    ServiceList,
    Transaction,
    TransactionStatus,
    GetBalance;

    public String value() {
        return name();
    }

    public static ActionCode fromValue(String v) {
        return valueOf(v);
    }

}
