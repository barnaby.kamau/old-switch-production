/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Bee.dtos;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 *
 * @author Antonios_knaguib
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "service", propOrder = {
    "providerId",
    "accountId",
    "name",
    "priceType",
    "serviceValue",
    "serviceValueList",
    "minValue",
    "maxValue",
    "commissionType",
    "commissionValueType",
    "fixedCommission",
    "defaultCommission",
    "fromCommission",
    "toCommission"
})
public class Service {

    @XmlElement(name = "providerId", required = true)
    protected int providerId;
    @XmlElement(name = "accountId", required = true)
    protected int accountId;
    @XmlElement(name = "name", required = true)
    protected String name;
    @XmlElement(name = "priceType", required = true)
    protected int priceType;
    @XmlElement(name = "serviceValue", required = true)
    protected String serviceValue;
    @XmlElement(name = "serviceValueList", required = true)
    protected String serviceValueList;
    @XmlElement(name = "minValue", required = true)
    protected double minValue;
    @XmlElement(name = "maxValue", required = true)
    protected double maxValue;
    @XmlElement(name = "commissionType", required = true)
    protected int commissionType;
    @XmlElement(name = "commissionValueType", required = true)
    protected int commissionValueType;
    @XmlElement(name = "fixedCommission", required = true)
    protected double fixedCommission;
    @XmlElement(name = "defaultCommission", required = true)
    protected double defaultCommission;
    @XmlElement(name = "fromCommission", required = true)
    protected double fromCommission;
    @XmlElement(name = "toCommission", required = true)
    protected double toCommission;

    public int getProviderId() {
        return providerId;
    }

    public void setProviderId(int providerId) {
        this.providerId = providerId;
    }

    public int getAccountId() {
        return accountId;
    }

    public void setAccountId(int accountId) {
        this.accountId = accountId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getPriceType() {
        return priceType;
    }

    public void setPriceType(int priceType) {
        this.priceType = priceType;
    }

    public String getServiceValue() {
        return serviceValue;
    }

    public void setServiceValue(String serviceValue) {
        this.serviceValue = serviceValue;
    }

    public String getServiceValueList() {
        return serviceValueList;
    }

    public void setServiceValueList(String serviceValueList) {
        this.serviceValueList = serviceValueList;
    }

    public double getMinValue() {
        return minValue;
    }

    public void setMinValue(double minValue) {
        this.minValue = minValue;
    }

    public double getMaxValue() {
        return maxValue;
    }

    public void setMaxValue(double maxValue) {
        this.maxValue = maxValue;
    }

    public int getCommissionType() {
        return commissionType;
    }

    public void setCommissionType(int commissionType) {
        this.commissionType = commissionType;
    }

    public int getCommissionValueType() {
        return commissionValueType;
    }

    public void setCommissionValueType(int commissionValueType) {
        this.commissionValueType = commissionValueType;
    }

    public double getFixedCommission() {
        return fixedCommission;
    }

    public void setFixedCommission(double fixedCommission) {
        this.fixedCommission = fixedCommission;
    }

    public double getDefaultCommission() {
        return defaultCommission;
    }

    public void setDefaultCommission(double defaultCommission) {
        this.defaultCommission = defaultCommission;
    }

    public double getFromCommission() {
        return fromCommission;
    }

    public void setFromCommission(double fromCommission) {
        this.fromCommission = fromCommission;
    }

    public double getToCommission() {
        return toCommission;
    }

    public void setToCommission(double toCommission) {
        this.toCommission = toCommission;
    }

}
