/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.telecomegypt.provider;

/**
 *
 * @author Antonios_knaguib
 */
public class Test {

    public static void main(String[] args) throws Exception {
        String input = "1234567891245752";

        StringBuilder result = new StringBuilder();
        for (int i = 0; i < input.length(); i++) {
            if (i % 4 == 0 && i != 0) {
                result.append("-");
            }

            result.append(input.charAt(i));
        }

        System.out.println(result.toString());
    }

}
