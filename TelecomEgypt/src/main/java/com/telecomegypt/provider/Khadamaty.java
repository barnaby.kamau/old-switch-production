/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.telecomegypt.provider;

import com.telecomegypt.entites.Auth;
import com.telecomegypt.entites.InqField;
import com.telecomegypt.entites.KhadamatyResponse;
import com.telecomegypt.entites.KhadamtyRequest;
import com.telecomegypt.entites.RSRequest;
import com.telecomegypt.entites.RSResponse;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.config.ClientConfig;
import com.sun.jersey.api.client.config.DefaultClientConfig;
import com.telecomegypt.entites.KhadamatyPaymentResponse;
import com.telecomegypt.entites.KhadamtyPaymentRequest;
import static com.telecomegypt.manager.Manager.logger;
import java.util.ArrayList;
import java.util.List;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.UriBuilder;
import org.apache.commons.lang.StringUtils;

/**
 *
 * @author Antonios_knaguib
 */
public class Khadamaty {

    int INQUIRYSERVICEID = 1;
    int PAYMENTSERVICEID = 1;
    String AGENTCODE = "8364";
    int APPID = 27;
    String INQUIRYURL = "https://gateway.khadamaty.com.eg/V3/api/Inquiry";
    String PAYMENTURL = "https://gateway.khadamaty.com.eg/V3/api/Payment";

    public RSResponse inquiry(RSRequest request) {
        RSResponse response = new RSResponse();
        response.requestID = request.requestID;
        try {
            logger.info("[Khadamaty] RequestID : " + request.requestID + " Provider Request Creation started");
            KhadamtyRequest providerRequest = getProviderRequest(request);
            logger.info("[Khadamaty] RequestID : " + request.requestID + " Provider Request : " + providerRequest.toString().replace(request.password, StringUtils.repeat("*", request.password.length())));
            logger.info("[Khadamaty] RequestID : " + request.requestID + " Provider Request Creation ended");

            logger.info("[Khadamaty] RequestID : " + request.requestID + " Provider Request Sending started");
            ClientConfig config = new DefaultClientConfig();
            Client client = Client.create(config);
            WebResource webResource = client.resource(UriBuilder.fromUri(INQUIRYURL).build());
            KhadamatyResponse providerResponse = webResource.type(MediaType.APPLICATION_JSON).post(KhadamatyResponse.class, providerRequest);
            logger.info("[Khadamaty] RequestID : " + request.requestID + " Provider Response : " + providerResponse.toString());
            logger.info("[Khadamaty] RequestID : " + request.requestID + " Provider Request Sending ended");
            logger.info("[Khadamaty] RequestID : " + request.requestID + " Provider Response Unmarshal ended");
            if (providerResponse.StatusCode == 1) {
                response.code = 200;
                response.message = "Successful Inquiry.";
                response.phoneNumber = request.telephoneCode + request.phone;
                response.amount = providerResponse.Amount;
                response.fees = providerResponse.Fees;
                response.totalAmount = providerResponse.Amount + providerResponse.Fees;
                if (providerResponse.ExtraBillInfo != null && providerResponse.ExtraBillInfo.length() > 0) {
                    response.customerName = providerResponse.ExtraBillInfo.substring(providerResponse.ExtraBillInfo.indexOf("Name:") + "Name:".length(), providerResponse.ExtraBillInfo.length());
                } else {
                    response.customerName = "";
                }
            } else if (providerResponse.BillerStatus.contains("لا توجد فواتير على العميل")) {
                response.code = -5;
                response.message = "Provider Error : " + providerResponse.BillerStatus;
            } else {
                response.code = -2;
                response.message = "Provider Error : " + providerResponse.StatusDescription;
            }

        } catch (Exception e) {
            logger.error(e);
            response.code = -3;
            response.message = "General Error : " + e;
            return response;
        }

        return response;
    }

    public RSResponse payment(RSRequest request) {
        RSResponse response = new RSResponse();
        response.requestID = request.requestID;
        response.totalAmount = request.amount;
//        Validations
        try {
            if (request.amount <= 0) {
                response.code = -1;
                response.message = "Invalid Amount must be greater than 0.0";
                return response;
            }
        } catch (Exception e) {
            logger.error(e);
            response.code = -1;
            response.message = "Invalid Request.";
            return response;
        }

        try {
            logger.info("[Khadamaty] RequestID : " + request.requestID + " Provider Request Creation started");
            KhadamtyPaymentRequest providerRequest = getPaymentProviderRequest(request);
            logger.info("[Khadamaty] RequestID : " + request.requestID + " Provider Request : " + providerRequest.toString().replace(request.password, StringUtils.repeat("*", request.password.length())));
            logger.info("[Khadamaty] RequestID : " + request.requestID + " Provider Request Creation ended");

            logger.info("[Khadamaty] RequestID : " + request.requestID + " Provider Request Sending started");
            ClientConfig config = new DefaultClientConfig();
            Client client = Client.create(config);
            WebResource webResource = client.resource(UriBuilder.fromUri(PAYMENTURL).build());
            KhadamatyPaymentResponse providerResponse = webResource.type(MediaType.APPLICATION_JSON).post(KhadamatyPaymentResponse.class, providerRequest);
            logger.info("[Khadamaty] RequestID : " + request.requestID + " Provider Response : " + providerResponse.toString());
            logger.info("[Khadamaty] RequestID : " + request.requestID + " Provider Request Sending ended");
            logger.info("[Khadamaty] RequestID : " + request.requestID + " Provider Response Unmarshal ended");
            if (providerResponse.StatusCode == 1) {
                response.code = 200;
                response.message = "Successful Payment.";
                response.phoneNumber = request.telephoneCode + request.phone;
                response.totalAmount = providerResponse.Amount;
                response.providerTransactionID = providerResponse.KHDTransId;
                response.fees = providerResponse.Fees;
//                if (!providerResponse.BillInfo.isEmpty()) {
//                    for (BillInfo info : providerResponse.BillInfo) {
//                        if (info.Item.equals("AccountName")) {
//                            response.customerName = info.value;
//                        } else {
//                            response.customerName = "";
//                        }
//                    }
//                } else {
//                    response.customerName = "";
//                }
            } else {
                response.code = -2;
                response.message = "Provider Error : " + providerResponse.StatusDescription;
            }

        } catch (Exception e) {
            logger.error(e);
            response.code = -3;
            response.message = "General Error : " + e;
            return response;
        }

        return response;

    }

    private KhadamtyRequest getProviderRequest(RSRequest request) {
        KhadamtyRequest providerRequest = new KhadamtyRequest();
        providerRequest.SenderTrans = request.requestID;
        providerRequest.ServiceId = INQUIRYSERVICEID;
        Auth auth = new Auth();
        auth.AgentCode = AGENTCODE;
        auth.AppId = APPID;
        auth.UserId = request.userName;
        auth.Password = request.password;
        providerRequest.Auth = auth;
        List<InqField> InqFields = new ArrayList<>();
        InqField field = new InqField();
        field.SFId = 46;
        field.Label = "كود المحافظة";
        field.Value = request.telephoneCode;
        InqFields.add(field);
        field = new InqField();
        field.SFId = 2;
        field.Label = "رقم تليفون الخدمة";
        field.Value = request.phone;
        InqFields.add(field);
        providerRequest.InqFields = InqFields;
        return providerRequest;
    }

    private KhadamtyPaymentRequest getPaymentProviderRequest(RSRequest request) {
        KhadamtyPaymentRequest providerRequest = new KhadamtyPaymentRequest();
        providerRequest.SenderTrans = request.requestID;
        providerRequest.ServiceId = PAYMENTSERVICEID;
        providerRequest.amount = request.amount;
        providerRequest.fee = request.fees;
        Auth auth = new Auth();
        auth.AgentCode = AGENTCODE;
        auth.AppId = APPID;
        auth.UserId = request.userName;
        auth.Password = request.password;
        providerRequest.Auth = auth;
        List<InqField> InqFields = new ArrayList<>();
        InqField field = new InqField();
        field.SFId = 46;
        field.Label = "كود المحافظة";
        field.Value = request.telephoneCode;
        InqFields.add(field);
        field = new InqField();
        field.SFId = 2;
        field.Label = "رقم تليفون الخدمة";
        field.Value = request.phone;
        InqFields.add(field);
        providerRequest.Fields = InqFields;
        return providerRequest;
    }
}
